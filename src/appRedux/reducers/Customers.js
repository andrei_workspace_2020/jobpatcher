import {
  SET_ALL_CUSTOMERS,
  SET_SELECTED_CUSTOMER
} from "constants/ActionTypes";

const INIT_STATE = {
  error: "",
  loading: false,
  message: "",
  customers: [],
  selected_customer: null,
  addCustomerSuccess: false
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_ALL_CUSTOMERS: {
      return {
        ...state,
        customers: action.payload
      };
    }
    case SET_SELECTED_CUSTOMER: {
      return {
        ...state,
        selected_customer: action.payload
      };
    }
    default:
      return state;
  }
};
