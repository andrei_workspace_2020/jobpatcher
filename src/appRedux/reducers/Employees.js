import {
  SET_ALL_EMPLOYEES,
  SET_SELECTED_EMPLOYEE
} from "constants/ActionTypes";

const INIT_STATE = {
  error: "",
  loading: false,
  message: "",
  employees: [],
  selected_employee: null
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_ALL_EMPLOYEES: {
      return {
        ...state,
        employees: action.payload
      };
    }
    case SET_SELECTED_EMPLOYEE: {
      return {
        ...state,
        selected_employee: action.payload
      };
    }
    default:
      return state;
  }
};
