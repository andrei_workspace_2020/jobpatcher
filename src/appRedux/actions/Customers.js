//customers
import {
  GET_ALL_CUSTOMERS,
  SET_ALL_CUSTOMERS,
  SAVE_CUSTOMER,
  UPDATE_CUSTOMER,
  DELETE_CUSTOMER,
  GET_CUSTOMER_BY_ID,
  SET_SELECTED_CUSTOMER
} from "constants/ActionTypes";
export const getAllCustomers = () => {
  return {
    type: GET_ALL_CUSTOMERS
  };
};
export const setAllCustomersByOwner = customers => {
  return {
    type: SET_ALL_CUSTOMERS,
    payload: customers
  };
};

export const addCustomer = customer => {
  return {
    type: SAVE_CUSTOMER,
    payload: customer
  };
};
export const updateCustomer = (customerId, customerData) => {
  return {
    type: UPDATE_CUSTOMER,
    payload: { id: customerId, data: customerData }
  };
};
export const deleteCustomer = customerId => {
  return {
    type: DELETE_CUSTOMER,
    payload: customerId
  };
};
export const getCustomerById = customerId => {
  return {
    type: GET_CUSTOMER_BY_ID,
    payload: customerId
  };
};
export const setSelectedCustomerByOwner = customer => {
  return {
    type: SET_SELECTED_CUSTOMER,
    payload: customer
  };
};
