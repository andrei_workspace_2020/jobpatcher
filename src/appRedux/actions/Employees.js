//employees
import {
  GET_ALL_EMPLOYEES,
  SET_ALL_EMPLOYEES,
  SAVE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  DELETE_EMPLOYEE,
  GET_EMPLOYEE_BY_ID,
  SET_SELECTED_EMPLOYEE
} from "constants/ActionTypes";
export const getAllEmployees = () => {
  return {
    type: GET_ALL_EMPLOYEES
  };
};
export const setAllEmployeesByOwner = employees => {
  return {
    type: SET_ALL_EMPLOYEES,
    payload: employees
  };
};

export const addEmployee = employee => {
  return {
    type: SAVE_EMPLOYEE,
    payload: employee
  };
};
export const updateEmployee = (employeeId, employeeData) => {
  return {
    type: UPDATE_EMPLOYEE,
    payload: { id: employeeId, data: employeeData }
  };
};
export const deleteEmployee = employeeId => {
  return {
    type: DELETE_EMPLOYEE,
    payload: employeeId
  };
};
export const getEmployeeById = employeeId => {
  return {
    type: GET_EMPLOYEE_BY_ID,
    payload: employeeId
  };
};
export const setSelectedEmployeeByOwner = employee => {
  return {
    type: SET_SELECTED_EMPLOYEE,
    payload: employee
  };
};
