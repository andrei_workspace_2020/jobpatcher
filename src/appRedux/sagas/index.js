import { all } from "redux-saga/effects";
import authSagas from "./Auth";
import notesSagas from "./Notes";
import customersSagas from "./Customers";
import employeesSagas from "./Employees";

export default function* rootSaga(getState) {
  yield all([authSagas(), notesSagas(), customersSagas(), employeesSagas()]);
}
