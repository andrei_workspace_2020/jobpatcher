import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { db, auth } from "../../firebase/firebase";
import {
  GET_ALL_EMPLOYEES,
  SAVE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  DELETE_EMPLOYEE,
  GET_EMPLOYEE_BY_ID
} from "../../constants/ActionTypes";
import {
  setAllEmployeesByOwner,
  setSelectedEmployeeByOwner
} from "../../appRedux/actions/Employees";

const getEmployees = async owner => {
  let employees = await db
    .collection("users")
    .doc(owner)
    .collection("employees")
    .orderBy("created_date", "asc")
    .get()
    .then(function(docs) {
      let res = [];
      docs.forEach(function(doc) {
        res.push(doc.data());
      });
      return res;
    });
  return employees;
};

const addNewEmployeeDetails = async (owner, employee) => {
  await db
    .collection("users")
    .doc(owner)
    .collection("employees")
    .doc(employee.id)
    .set(employee)
    .then(function() {
      console.log("This employee is added successfully");
    })
    .catch(function(error) {
      console.log(error);
    });
};

const editEmployeeDetails = async (owner, employeeId, employeeData) => {
  console.log("update data: ");
  console.log(employeeData);
  await db
    .collection("users")
    .doc(owner)
    .collection("employees")
    .doc(employeeId)
    .update(employeeData)
    .then(function() {
      console.log("This employee is updated successfully");
    })
    .catch(function(error) {
      console.log(error);
    });
};
const getEmployeeById = async (owner, employeeId) => {
  const employee = await db
    .collection("users")
    .doc(owner)
    .collection("employees")
    .doc(employeeId)
    .get()
    .then(function(res) {
      console.log(res);
      if (res.exists) return res.data();
      else console.log("This isn't employee with this id(" + employeeId + ")");
    })
    .catch(function(error) {
      console.log(error);
    });
  return employee;
};
const removeEmployeeDetails = async (owner, employeeId) => {
  await db
    .collection("users")
    .doc(owner)
    .collection("employees")
    .doc(employeeId)
    .delete()
    .then(function() {
      console.log("Employee(" + employeeId + ") is successfully deleted");
    })
    .catch(function(error) {
      console.log(error);
    });
};
function* getAllEmployeesByOwner({ payload }) {
  try {
    const owner = localStorage.getItem("user_id");
    console.log(owner);
    const employees = yield call(getEmployees, owner);
    console.log(
      "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    );
    console.log(employees);
    yield put(setAllEmployeesByOwner(employees));
  } catch (error) {
    console.log(error);
  }
}

function* addEmployee({ payload }) {
  console.log(payload);
  try {
    const owner = localStorage.getItem("user_id");
    const employee = yield call(getEmployeeById, owner, payload.id);
    console.log(employee);
    yield call(addNewEmployeeDetails, owner, payload);
    const employees = yield call(getEmployees, owner);
    yield put(setAllEmployeesByOwner(employees));
  } catch (error) {
    console.log(error);
  }
}

function* editEmployee({ payload }) {
  console.log(payload);
  try {
    const owner = localStorage.getItem("user_id");
    const employee = yield call(getEmployeeById, owner, payload.id);
    console.log(employee);
    yield call(editEmployeeDetails, owner, payload.id, payload.data);
    const employees = yield call(getEmployees, owner);
    yield put(setAllEmployeesByOwner(employees));
  } catch (error) {
    console.log(error);
  }
}

function* removeEmployee({ payload }) {
  console.log(payload);
  try {
    const owner = localStorage.getItem("user_id");
    yield call(removeEmployeeDetails, owner, payload);
    const employees = yield call(getEmployees, owner);
    console.log("employee after deleting: ", employees);
    yield put(setAllEmployeesByOwner(employees));
  } catch (error) {
    console.log(error);
  }
}
function* getEmployeeUsingId({ payload }) {
  try {
    const owner = localStorage.getItem("user_id");
    const employee = yield call(getEmployeeById, owner, payload);
    console.log(employee);
    if (employee != null) yield put(setSelectedEmployeeByOwner(employee));
  } catch (error) {
    console.log(error);
  }
}
export function* getAllEmployees() {
  yield takeEvery(GET_ALL_EMPLOYEES, getAllEmployeesByOwner);
}
export function* saveEmployee() {
  yield takeEvery(SAVE_EMPLOYEE, addEmployee);
}
export function* updateEmployee() {
  yield takeEvery(UPDATE_EMPLOYEE, editEmployee);
}
export function* deleteEmployee() {
  yield takeEvery(DELETE_EMPLOYEE, removeEmployee);
}
export function* getEmployeeWithId() {
  yield takeEvery(GET_EMPLOYEE_BY_ID, getEmployeeUsingId);
}
export default function* rootSaga() {
  yield all([
    fork(getAllEmployees),
    fork(saveEmployee),
    fork(updateEmployee),
    fork(deleteEmployee),
    fork(getEmployeeWithId)
  ]);
}
