import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { db, auth } from "../../firebase/firebase";
import {
  GET_ALL_CUSTOMERS,
  SAVE_CUSTOMER,
  UPDATE_CUSTOMER,
  DELETE_CUSTOMER,
  GET_CUSTOMER_BY_ID
} from "../../constants/ActionTypes";
import {
  setAllCustomersByOwner,
  setSelectedCustomerByOwner
} from "../../appRedux/actions/Customers";

const getCustomers = async owner => {
  let customers = await db
    .collection("users")
    .doc(owner)
    .collection("customers")
    .orderBy("customer_info.creation_date", "asc")
    .get()
    .then(function(docs) {
      let res = [];
      docs.forEach(function(doc) {
        res.push(doc.data());
      });
      return res;
    });
  return customers;
};

const addNewCustomerDetails = async (owner, customer) => {
  await db
    .collection("users")
    .doc(owner)
    .collection("customers")
    .doc(customer.id)
    .set(customer)
    .then(function() {
      console.log("This customer is added successfully");
    })
    .catch(function(error) {
      console.log(error);
    });
};

const editCustomerDetails = async (owner, customerId, customerData) => {
  console.log("update data: ");
  console.log(customerData);
  await db
    .collection("users")
    .doc(owner)
    .collection("customers")
    .doc(customerId)
    .update(customerData)
    .then(function() {
      console.log("This customer is updated successfully");
    })
    .catch(function(error) {
      console.log(error);
    });
};
const getCustomerById = async (owner, customerId) => {
  const customer = await db
    .collection("users")
    .doc(owner)
    .collection("customers")
    .doc(customerId)
    .get()
    .then(function(res) {
      console.log(res);
      if (res.exists) return res.data();
      else console.log("There isn't customer with this id(" + customerId + ")");
    })
    .catch(function(error) {
      console.log(error);
    });
  return customer;
};
const getCustomerByEmail = async (owner, customerEmail) => {
  const customer = await db
    .collection("users")
    .doc(owner)
    .collection("customers")
    .where("email", "==", customerEmail)
    .get()
    .then(function(querySnapshot) {
      console.log(querySnapshot);
      let res = null;
      querySnapshot.forEach(function(doc) {
        // doc.data() is never undefined for query doc snapshots
        res = doc.data();
      });
      return res;
    })
    .catch(function(error) {
      console.log(error);
    });
  return customer;
};
const removeCustomerDetails = async (owner, customerId) => {
  await db
    .collection("users")
    .doc(owner)
    .collection("customers")
    .doc(customerId)
    .delete()
    .then(function() {
      console.log("Customer(" + customerId + ") is successfully deleted");
    })
    .catch(function(error) {
      console.log(error);
    });
};
function* getAllCustomersByOwner({ payload }) {
  try {
    const owner = localStorage.getItem("user_id");
    const customers = yield call(getCustomers, owner);
    yield put(setAllCustomersByOwner(customers));
  } catch (error) {
    console.log(error);
  }
}

function* addCustomer({ payload }) {
  console.log(payload);
  try {
    const owner = localStorage.getItem("user_id");
    yield call(addNewCustomerDetails, owner, payload);
    const customers = yield call(getCustomers, owner);
    yield put(setAllCustomersByOwner(customers));
  } catch (error) {
    console.log(error);
  }
}

function* editCustomer({ payload }) {
  console.log(payload);
  try {
    const owner = localStorage.getItem("user_id");
    yield call(editCustomerDetails, owner, payload.id, payload.data);
    const customers = yield call(getCustomers, owner);
    const customer = yield call(getCustomerById, owner, payload.id);
    yield put(setAllCustomersByOwner(customers));
    yield put(setSelectedCustomerByOwner(customer));
  } catch (error) {
    console.log(error);
  }
}

function* removeCustomer({ payload }) {
  console.log(payload);
  try {
    const owner = localStorage.getItem("user_id");
    yield call(removeCustomerDetails, owner, payload);
    if (localStorage.getItem("customer_ID"))
      localStorage.removeItem("customer_ID");
    const customers = yield call(getCustomers, owner);
    console.log("customer after deleting: ", customers);
    yield put(setAllCustomersByOwner(customers));
    yield put(setSelectedCustomerByOwner(null));
  } catch (error) {
    console.log(error);
  }
}
function* getCustomerUsingId({ payload }) {
  try {
    const owner = localStorage.getItem("user_id");
    const customer = yield call(getCustomerById, owner, payload);
    console.log(customer);
    if (customer != null) yield put(setSelectedCustomerByOwner(customer));
  } catch (error) {
    console.log(error);
  }
}
export function* getAllCustomers() {
  yield takeEvery(GET_ALL_CUSTOMERS, getAllCustomersByOwner);
}
export function* saveCustomer() {
  yield takeEvery(SAVE_CUSTOMER, addCustomer);
}
export function* updateCustomer() {
  yield takeEvery(UPDATE_CUSTOMER, editCustomer);
}
export function* deleteCustomer() {
  yield takeEvery(DELETE_CUSTOMER, removeCustomer);
}
export function* getCustomerWithId() {
  yield takeEvery(GET_CUSTOMER_BY_ID, getCustomerUsingId);
}
export default function* rootSaga() {
  yield all([
    fork(getAllCustomers),
    fork(saveCustomer),
    fork(updateCustomer),
    fork(deleteCustomer),
    fork(getCustomerWithId)
  ]);
}
