import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import {
  auth,
  firebase,
  db,
  facebookAuthProvider,
  githubAuthProvider,
  googleAuthProvider,
  twitterAuthProvider
} from "../../firebase/firebase";
import {
  SIGNIN_FACEBOOK_USER,
  SIGNIN_GITHUB_USER,
  SIGNIN_GOOGLE_USER,
  SIGNIN_TWITTER_USER,
  SIGNIN_USER,
  SIGNOUT_USER,
  SIGNUP_USER,
  FORGOTPASSWORD_USER,
  UPDATEPASSWORD_USER,
  USER_CHECK_WITH_EMAIL,
  SETUP_USER_DETAIL
} from "constants/ActionTypes";
import {
  showAuthMessage,
  hideMessage,
  emailExistSuccess,
  userSignInSuccess,
  userSignOutSuccess,
  userSignUpSuccess
} from "../../appRedux/actions/Auth";
import {
  userFacebookSignInSuccess,
  userGithubSignInSuccess,
  userGoogleSignInSuccess,
  userTwitterSignInSuccess
} from "../actions/Auth";

const createUserWithEmailPasswordRequest = async (email, password) =>
  await auth
    .createUserWithEmailAndPassword(email, password)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserWithEmailPasswordRequest = async (email, password) =>
  await auth
    .signInWithEmailAndPassword(email, password)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserCheckWithEmailRequest = async email =>
  await auth
    .fetchSignInMethodsForEmail(email)
    .then(authUser => authUser)
    .catch(error => error);

const signOutRequest = async () =>
  await auth
    .signOut()
    .then(authUser => authUser)
    .catch(error => error);

const forgotUserPasswordRequest = async email =>
  await auth
    .sendPasswordResetEmail(email)
    .then(authUser => authUser)
    .catch(error => error);

const updateUserPasswordRequest = async (email, password) =>
  await auth.currentUser
    .updatePassword(password)
    .then(() => {
      console.log(auth.currentUser);
    })
    .catch(error => error);

const signInUserWithGoogleRequest = async () =>
  await auth
    .signInWithPopup(googleAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserWithFacebookRequest = async () =>
  await auth
    .signInWithPopup(facebookAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserWithGithubRequest = async () =>
  await auth
    .signInWithPopup(githubAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);

const signInUserWithTwitterRequest = async () =>
  await auth
    .signInWithPopup(twitterAuthProvider)
    .then(authUser => authUser)
    .catch(error => error);

const getUserByUID = async uid => {
  let user = await db
    .collection("users")
    .doc(uid)
    .get()
    .then(function(res) {
      return res.data();
    })
    .catch(function(error) {
      console.log(error);
    });
  return user;
};

const getUserByEmail = async email => {
  let user = await db
    .collection("users")
    .get()
    .then(snapshot => {
      let res = null;
      snapshot.forEach(doc => {
        const data = doc.data();
        if (data.email === email) res = data;
      });
      return res;
    });
  return user;
};

const insertUser = async userInfo => {
  await db
    .collection("users")
    .doc(userInfo.uid)
    .set({
      first_name: userInfo.first_name,
      last_name: userInfo.last_name,
      email: userInfo.email,
      password: btoa(userInfo.password)
    })
    .then(function() {
      console.log("Document successfully written!");
    })
    .catch(function(error) {
      console.error("Error writing document: ", error);
    });
};

const updateUserDetails = async (uid, details) => {
  await db
    .collection("users")
    .doc(uid)
    .update({
      company_name: details.company_name,
      phone_number: details.phone_number,
      service_industry: details.service_industry,
      role: details.role,
      company_size: details.company_size,
      exp_years: details.exp_years
    })
    .catch(error => console.log(error));
};

function* createUserWithEmailPassword({ payload }) {
  const { first_name, last_name, email, password } = payload;
  try {
    const signUpUser = yield call(
      createUserWithEmailPasswordRequest,
      email,
      password
    );
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem("user_id", signUpUser.user.uid);
      yield call(insertUser, {
        uid: signUpUser.user.uid,
        first_name: first_name,
        last_name: last_name,
        email: email,
        password: password
      });

      const user = yield call(getUserByEmail, email);
      localStorage.setItem("user_info", JSON.stringify(user));

      yield put(hideMessage());
      yield put(userSignUpSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* setupUserDetails({ payload }) {
  const {
    company_name,
    phone_number,
    service_industry,
    role,
    company_size,
    exp_years
  } = payload;

  try {
    const uid = localStorage.getItem("user_id");
    const signInUser = yield call(getUserByUID, uid);
    yield call(updateUserDetails, uid, {
      email: signInUser.email,
      first_name: signInUser.first_name,
      last_name: signInUser.last_name,
      password: signInUser.password,
      company_name: company_name,
      phone_number: phone_number,
      service_industry: service_industry,
      role: role,
      company_size: company_size,
      exp_years: exp_years
    });
    localStorage.removeItem("user_info");
    const user = yield call(getUserByEmail, signInUser.email);
    localStorage.setItem("user_info", JSON.stringify(user));
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* signInUserWithGoogle() {
  try {
    const signUpUser = yield call(signInUserWithGoogleRequest);
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem("user_id", signUpUser.user.uid);
      yield put(userGoogleSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* signInUserWithFacebook() {
  try {
    const signUpUser = yield call(signInUserWithFacebookRequest);
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem("user_id", signUpUser.user.uid);
      yield put(userFacebookSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* signInUserWithGithub() {
  try {
    const signUpUser = yield call(signInUserWithGithubRequest);
    if (signUpUser.message) {
      yield put(showAuthMessage(signUpUser.message));
    } else {
      localStorage.setItem("user_id", signUpUser.user.uid);
      yield put(userGithubSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* signInUserWithTwitter() {
  try {
    const signUpUser = yield call(signInUserWithTwitterRequest);
    if (signUpUser.message) {
      if (signUpUser.message.length > 100) {
        yield put(showAuthMessage("Your request has been canceled."));
      } else {
        yield put(showAuthMessage(signUpUser.message));
      }
    } else {
      localStorage.setItem("user_id", signUpUser.user.uid);
      yield put(userTwitterSignInSuccess(signUpUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* signInUserWithEmailPassword({ payload }) {
  const { email, password } = payload;
  try {
    const signInUser = yield call(
      signInUserWithEmailPasswordRequest,
      email,
      password
    );
    if (signInUser.message) {
      yield put(showAuthMessage(signInUser.message));
    } else {
      localStorage.setItem("user_id", signInUser.user.uid);
      const user = yield call(getUserByEmail, email);
      localStorage.setItem("user_info", JSON.stringify(user));
      yield put(userSignInSuccess(signInUser.user.uid));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* signInUserCheckWithEmail({ payload }) {
  const { email } = payload;

  try {
    const signInType = yield call(signInUserCheckWithEmailRequest, email);
    //const user = yield call(getUserByEmail, email);
    if (signInType.message) {
      yield put(showAuthMessage(signInType.message));
      yield put(emailExistSuccess({ exist: false, user: null }));
    } else if (signInType.length == 0) {
      yield put(showAuthMessage("This user doesn't exist!"));
      yield put(emailExistSuccess({ exist: false, user: null }));
    } else {
      const user = yield call(getUserByEmail, email);
      if (user != null) {
        yield put(hideMessage());
        yield put(emailExistSuccess({ exist: true, user: user }));
      } else {
        yield put(showAuthMessage("This user doesn't exist!"));
        yield put(emailExistSuccess({ exist: false, user: null }));
      }
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* signOut() {
  try {
    const signOutUser = yield call(signOutRequest);
    if (signOutUser === undefined) {
      localStorage.removeItem("user_id");
      localStorage.removeItem("user_info");
      yield put(userSignOutSuccess(signOutUser));
    } else {
      yield put(showAuthMessage(signOutUser.message));
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* forgotUserPassword({ payload }) {
  const { email } = payload;
  try {
    const signInUser = yield call(forgotUserPasswordRequest, email);
    if (signInUser) {
      yield put(showAuthMessage(signInUser.message));
    } else {
      yield put(hideMessage());
    }
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

function* updateUserPassword({ payload }) {
  const { email, password } = payload;
  try {
    yield call(updateUserPasswordRequest, email, password);
    const uid = localStorage.getItem("user_id");
    const signInUser = yield call(getUserByUID, uid);
    yield call(updateUserDetails, uid, {
      email: email,
      first_name: signInUser.first_name,
      last_name: signInUser.last_name,
      password: password,
      company_name: signInUser.company_name,
      phone_number: signInUser.phone_number,
      service_industry: signInUser.service_industry,
      role: signInUser.role,
      company_size: signInUser.company_size,
      exp_years: signInUser.exp_years
    });
    localStorage.removeItem("user_info");
    const user = yield call(getUserByEmail, email);
    localStorage.setItem("user_info", JSON.stringify(user));
  } catch (error) {
    yield put(showAuthMessage(error));
  }
}

export function* createUserAccount() {
  yield takeEvery(SIGNUP_USER, createUserWithEmailPassword);
}

export function* signInWithGoogle() {
  yield takeEvery(SIGNIN_GOOGLE_USER, signInUserWithGoogle);
}

export function* signInWithFacebook() {
  yield takeEvery(SIGNIN_FACEBOOK_USER, signInUserWithFacebook);
}

export function* signInWithTwitter() {
  yield takeEvery(SIGNIN_TWITTER_USER, signInUserWithTwitter);
}

export function* signInWithGithub() {
  yield takeEvery(SIGNIN_GITHUB_USER, signInUserWithGithub);
}

export function* signInUser() {
  yield takeEvery(SIGNIN_USER, signInUserWithEmailPassword);
}

export function* userCheckWithEmail() {
  yield takeEvery(USER_CHECK_WITH_EMAIL, signInUserCheckWithEmail);
}

export function* setupUserAccount() {
  yield takeEvery(SETUP_USER_DETAIL, setupUserDetails);
}

export function* signOutUser() {
  yield takeEvery(SIGNOUT_USER, signOut);
}

export function* forgotPasswordUser() {
  yield takeEvery(FORGOTPASSWORD_USER, forgotUserPassword);
}

export function* updatePasswordUser() {
  yield takeEvery(UPDATEPASSWORD_USER, updateUserPassword);
}
export default function* rootSaga() {
  yield all([
    fork(signInUser),
    fork(userCheckWithEmail),
    fork(createUserAccount),
    fork(setupUserAccount),
    fork(forgotPasswordUser),
    fork(updatePasswordUser),
    fork(signInWithGoogle),
    fork(signInWithFacebook),
    fork(signInWithTwitter),
    fork(signInWithGithub),
    fork(signOutUser)
  ]);
}
