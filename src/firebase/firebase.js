const firebase = require("firebase");

// Initialize Firebase
const config = {
  apiKey: "AIzaSyAFJ8TNSgiFNaUvxpjGopGdGPGGIbRyDFA",
  authDomain: "jobpatcher-1570077458938.firebaseapp.com",
  databaseURL: "https://jobpatcher-1570077458938.firebaseio.com/",
  projectId: "jobpatcher-1570077458938",
  storageBucket: "jobpatcher-1570077458938.appspot.com",
  messagingSenderId: "309515438989"
};

firebase.initializeApp(config);
const auth = firebase.auth();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const githubAuthProvider = new firebase.auth.GithubAuthProvider();
const twitterAuthProvider = new firebase.auth.TwitterAuthProvider();

const db = firebase.firestore();

export {
  db,
  auth,
  firebase,
  googleAuthProvider,
  githubAuthProvider,
  facebookAuthProvider,
  twitterAuthProvider
};
