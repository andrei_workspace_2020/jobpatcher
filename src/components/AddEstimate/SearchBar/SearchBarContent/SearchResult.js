import React, { Component } from "react";

const Avatar = ({ icon }) => {
  return (
    <div className="gx-main-avatar gx-size-32 gx-mr-12">
      <i
        className="material-icons gx-w-100 gx-text-center"
        style={{ color: "#fbfbfd", fontSize: "24px" }}
      >
        {icon}
      </i>
    </div>
  );
};

class SearchResult extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};

    this.selectCustomer = this.selectCustomer.bind(this);
  }

  selectCustomer(customer) {
    this.props.onSetSearchable(false);
    this.props.onSelectCustomer(customer);
  }

  render() {
    const { data } = this.props;
    return (
      <div className="gx-addjob-search-container">
        {!data.length && (
          <div className="gx-menuitem gx-addjob-search-result-item">
            {" "}
            No Result{" "}
          </div>
        )}
        {data.map((customer, index) => (
          <div key={index} className="gx-menuitem gx-addjob-search-result-item">
            <div
              className="gx-div-align-center"
              style={{ width: "100%" }}
              onClick={() => this.selectCustomer(customer)}
            >
              <Avatar icon="person" />
              <div
                className="gx-addjob-search-result-item-name gx-ss-fw-500"
                style={{ fontSize: "14px" }}
              >
                {customer.first_name + " " + customer.last_name}
              </div>
              <div
                className="gx-addjob-search-result-item-address"
                style={{ color: "#9399a2", fontSize: "12px" }}
              >
                {customer.address[0].street +
                  " " +
                  customer.address[0].city +
                  ", " +
                  customer.address[0].state +
                  " " +
                  customer.address[0].zipcode}
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default SearchResult;
