import React, { Component } from "react";
import {
  Col,
  Row,
  InputNumber,
  Input,
  Tag,
  Button,
  Modal,
  Menu,
  Tabs
} from "antd";
import { injectIntl } from "react-intl";
import Widget from "components/Widget";
import Auxiliary from "util/Auxiliary";
import { Link } from "react-router-dom";
import IntlMessages from "util/IntlMessages";
import ButtonGroup from "antd/lib/button/button-group";

import {
  switchLanguage,
  updateWindowWidth,
  setCurrentPage
} from "../../appRedux/actions/Setting";
import { connect } from "react-redux";
import SearchBox from "components/SearchBox";

const TabPane = Tabs.TabPane;
const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};
class JobSubPane extends Component {
  constructor(props, context) {
    super(props, context);
  }
  state = {
    searchText: "",
    visible: false,
    selected_category: "",
    items: [],
    new_items: [
      {
        name: "",
        qty: 1,
        rate: 0,
        taxable: true,
        amount: "",
        description: ""
      }
    ],
    isAddItemModalVisible: false
  };
  componentDidMount() {
    const items = this.props.items.map(item => {
      let temp = { ...item };

      return temp;
    });

    this.setState({ items });

    this.props.updateWindowWidth(window.innerWidth);
    window.addEventListener("resize", () => {
      this.props.updateWindowWidth(window.innerWidth);
    });
  }
  componentDidUpdate() {}
  updateSearchText = e => {
    this.setState({ searchText: e.target.value });
  };
  handleOk = () => {
    this.setState({ visible: false });
  };
  handleCancel = () => {
    this.setState({ visible: false });
  };
  addItems = () => {
    this.setState({ isAddItemModalVisible: false });
    this.props.addItems(this.props.kind, this.state.items);
  };
  onAddItemModalVisibleChange = visible => {
    this.setState({ isAddItemModalVisible: visible });
  };
  removeItem = index => {
    this.setState({
      new_items: this.state.new_items.filter((_, i) => i !== index)
    });
  };
  onChangeItemName = (e, index) => {
    var itemsTemp = this.state.new_items;
    itemsTemp[index].name = e.target.value;
    this.setState({ new_items: itemsTemp });
  };
  onChangeItemQty = (value, index) => {
    var itemsTemp = this.state.new_items;
    itemsTemp[index].qty = value;
    this.setState({ new_items: itemsTemp });
  };
  onChangeItemRate = (value, index) => {
    var itemsTemp = this.state.new_items;
    itemsTemp[index].rate = value;
    this.setState({ new_items: itemsTemp });
  };
  onChangeItemTaxable = (taxable, index) => {
    var itemsTemp = this.state.new_items;
    itemsTemp[index].taxable = taxable;
    this.setState({ new_items: itemsTemp });
  };
  onChangeItemDescription = (e, index) => {
    var itemsTemp = this.state.new_items;
    itemsTemp[index].description = e.target.value;
    this.setState({ new_items: itemsTemp });
  };
  getMaterialCountByCategory = category => {
    let array = [];
    if (category == "All " + this.props.kind.toLowerCase() + "s")
      array = this.state.items;
    else array = this.state.items.filter(item => item.category == category);
    return array.length;
  };
  setSelectedCategory = cat => {
    if (cat != "All " + this.props.kind.toLowerCase() + "s")
      this.setState({ selected_category: cat });
    else this.setState({ selected_category: "" });
  };

  plusQTY = id => {
    let temp = [...this.state.items];
    temp.map(item => {
      if (item.id == id) item.quantity = item.quantity + 1;
    });
    this.setState({ items: temp });
  };
  minusQTY = id => {
    let temp = [...this.state.items];
    temp.map(item => {
      if (item.id == id)
        if (item.quantity != 0) item.quantity = item.quantity - 1;
      return item;
    });
    this.setState({ services: temp });
  };
  onAddItemModalClose = () => {
    // console.log("close");
    // console.log(this.props.items);
    const items = this.props.items.map(item => {
      let temp = { ...item };

      return temp;
    });

    this.setState({ items });
    // this.setState({ items: [...this.props.items] });
    this.setState({ isAddItemModalVisible: false });
  };
  render() {
    const {
      width,
      intl: { formatMessage }
    } = this.props;
    return (
      <Auxiliary>
        {width >= 768 ? (
          <Widget
            styleName={`gx-card-full gx-dispatcher-job-panel gx-addjob-step2-subpane  gx-addjob-step2-subpane-${this.props.kind}`}
          >
            <div className="gx-panel-title-bar">
              <div
                className="gx-w-100"
                style={{ display: "flex", justifyContent: "space-between" }}
              >
                <Row className="gx-p-0">
                  <Col
                    span={16}
                    className="gx-flex-row gx-justify-content-center"
                  >
                    <Row className="gx-p-0" gutter={20}>
                      <Col span={14} className="gutter-row gx-pl-0">
                        <div className="gx-addjob-step2-subpane-header-item">
                          <h5>{this.props.kind}</h5>
                        </div>
                      </Col>
                      <Col span={5} className="gutter-row">
                        <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                          <h5>QTY</h5>
                        </div>
                      </Col>
                      <Col span={5} className="gutter-row">
                        <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                          <h5>Unit price</h5>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                  <Col
                    span={8}
                    className="gx-flex-row gx-justify-content-center"
                  >
                    <Row className="gx-p-0" gutter={20}>
                      <Col span={8} className="gutter-row">
                        <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                          <h5>Tax</h5>
                        </div>
                      </Col>
                      <Col span={8} className="gutter-row">
                        <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                          <h5>Amount</h5>
                        </div>
                      </Col>
                      <Col span={8} className="gutter-row gx-pr-0">
                        <div className="gx-addjob-step2-subpane-header-item gx-addjob-step2-subpane-header-title-add gx-justify-content-end">
                          <i
                            className="material-icons"
                            onClick={() =>
                              this.onAddItemModalVisibleChange(true)
                            }
                          >
                            add_box
                          </i>
                        </div>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </div>
            </div>
            <div className="gx-panel-content">
              {this.state.new_items.map((item, index) => (
                <div key={index} className="gx-panel-item gx-flex-row">
                  {/* <div className="gx-addjob-step2-subpane-body-app">
                      <span className="item-index">{index + 1}</span>
                    </div> */}
                  <div className="gx-flex-column gx-w-100">
                    <div className="gx-flex-row">
                      <Row className="gx-p-0">
                        <Col
                          span={16}
                          className="gx-flex-row gx-justify-content-center"
                        >
                          <Row gutter={20} className="gx-p-0">
                            <Col span="14" className="gutter-row gx-pl-0">
                              <div className="gx-addjob-step2-subpane-body-detail-item">
                                <Input
                                  value={item.name}
                                  placeholder="Item name"
                                  onChange={e =>
                                    this.onChangeItemName(e, index)
                                  }
                                />
                              </div>
                            </Col>
                            <Col span="5" className="gutter-row">
                              <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                <InputNumber
                                  style={{ direction: "rtl" }}
                                  min={1}
                                  precision={1}
                                  defaultValue={1}
                                  value={item.qty}
                                  onChange={value =>
                                    this.onChangeItemQty(value, index)
                                  }
                                />
                              </div>
                            </Col>
                            <Col span="5" className="gutter-row">
                              <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                <InputNumber
                                  min={0}
                                  defaultValue={1.0}
                                  value={item.rate}
                                  precision={2}
                                  formatter={value =>
                                    (value = `$ ${value}`.replace(
                                      /\B(?=(\d{3})+(?!\d))/g,
                                      ","
                                    ))
                                  }
                                  parser={value =>
                                    value.replace(/\$\s?|(,*)[A-Za-z]/g, "")
                                  }
                                  onChange={value =>
                                    this.onChangeItemRate(value, index)
                                  }
                                />
                              </div>
                            </Col>
                          </Row>
                        </Col>
                        <Col
                          span={8}
                          className="gx-flex-row gx-justify-content-center"
                        >
                          <Row className="gx-p-0" gutter={20}>
                            <Col span={8} className="gutter-row">
                              <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                <ButtonGroup className="gx-custom-toggle-buttons">
                                  <Button
                                    className={`gx-btn-toggle ${
                                      item.taxable ? "gx-btn-toggle-yes" : ""
                                    }`}
                                    size="small"
                                    onClick={() =>
                                      this.onChangeItemTaxable(true, index)
                                    }
                                  >
                                    {item.taxable ? (
                                      <span>Yes</span>
                                    ) : (
                                      <ToggleButton></ToggleButton>
                                    )}
                                  </Button>
                                  <Button
                                    className={`gx-btn-toggle ${
                                      !item.taxable ? "gx-btn-toggle-no" : ""
                                    }`}
                                    size="small"
                                    onClick={() =>
                                      this.onChangeItemTaxable(false, index)
                                    }
                                  >
                                    {!item.taxable ? (
                                      <span>No</span>
                                    ) : (
                                      <ToggleButton></ToggleButton>
                                    )}
                                  </Button>
                                </ButtonGroup>
                              </div>
                            </Col>
                            <Col span={8} className="gutter-row">
                              <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                <h5>$ {(item.qty * item.rate).toFixed(2)}</h5>
                              </div>
                            </Col>
                            <Col span={8} className="gutter-row gx-pr-0">
                              <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-end">
                                <i
                                  className="material-icons gx-icon-action"
                                  onClick={() => this.removeItem(index)}
                                >
                                  cancel
                                </i>
                              </div>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </div>
                    <div
                      className="gx-flex-row gx-mt-10"
                      style={{
                        justifyContent: "space-between"
                      }}
                    >
                      <Row className="gx-p-0">
                        <Col span={16}>
                          <div className="gx-addjob-step2-subpane-body-description gx-pr-10">
                            <Input
                              placeholder="Description (Optional)"
                              value={item.description}
                              onChange={e =>
                                this.onChangeItemDescription(e, index)
                              }
                            />
                          </div>
                        </Col>
                        <Col span={8}>
                          <div className="gx-addjob-step2-subpane-body-add gx-justify-content-end">
                            <Link
                              to="#"
                              style={{
                                display: "flex",
                                alignItems: "center",
                                padding: "0",
                                marginBottom: "0"
                              }}
                              onClick={() =>
                                this.onAddItemModalVisibleChange(true)
                              }
                            >
                              <i className="material-icons">add</i>
                              <span className="gx-font-weight-medium">
                                {this.props.kind + " item"}
                              </span>
                            </Link>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </Widget>
        ) : (
          <Widget
            styleName={`gx-card-full gx-dispatcher-job-panel gx-addjob-step2-subpane-mobile gx-addjob-step2-subpane-mobile-${this.props.kind}`}
          >
            <div className="gx-panel-title-bar ">
              <div className="gx-flex-row gx-align-items-center gx-justify-content-between gx-w-100">
                <h5>{this.props.kind.toUpperCase() + "S"}</h5>
                <i
                  className="material-icons"
                  onClick={() => this.onAddItemModalVisibleChange(true)}
                  style={{ color: "#257cde" }}
                >
                  add_box
                </i>
              </div>
            </div>
            <div className="gx-panel-content">
              {this.state.new_items.map((item, index) => (
                <div key={index}>
                  <div style={{ paddingBottom: "10px" }}>
                    <Input
                      value={item.name}
                      placeholder="Item name"
                      onChange={e => this.onChangeItemName(e, index)}
                    />
                  </div>
                  <div style={{ paddingBottom: "10px" }}>
                    <Input
                      placeholder="Description (Optional)"
                      value={item.description}
                      onChange={e => this.onChangeItemDescription(e, index)}
                    />
                  </div>
                  <div className="gx-line-item-detail gx-flex-row gx-align-items-center gx-justify-content-center gx-mb-10">
                    <Row gutter={10} className="gx-w-100">
                      <Col span={18} className="gutter-row gx-pl-0">
                        <h5>QTY</h5>
                      </Col>
                      <Col span={6} className="gutter-row gx-pr-0">
                        <InputNumber
                          min={1}
                          precision={1}
                          defaultValue={1}
                          value={item.qty}
                          style={{ direction: "rtl" }}
                          onChange={value => this.onChangeItemQty(value, index)}
                        />
                      </Col>
                    </Row>
                  </div>
                  <div className="gx-line-item-detail gx-flex-row gx-align-items-center gx-justify-content-center gx-mb-10">
                    <Row gutter={10} className="gx-w-100">
                      <Col span={18} className="gutter-row gx-pl-0">
                        <h5>Unit price</h5>
                      </Col>
                      <Col span={6} className="gutter-row gx-pr-0">
                        <InputNumber
                          min={0}
                          defaultValue={1.0}
                          value={item.rate}
                          precision={2}
                          formatter={value =>
                            (value = `$ ${value}`.replace(
                              /\B(?=(\d{3})+(?!\d))/g,
                              ","
                            ))
                          }
                          parser={value =>
                            value.replace(/\$\s?|(,*)[A-Za-z]/g, "")
                          }
                          onChange={value =>
                            this.onChangeItemRate(value, index)
                          }
                        />
                      </Col>
                    </Row>
                  </div>
                  <div className="gx-line-item-detail gx-flex-row gx-align-items-center gx-justify-content-center gx-mb-10">
                    <Row gutter={10} className="gx-w-100">
                      <Col span={18} className="gutter-row gx-pl-0">
                        <h5>Tax</h5>
                      </Col>
                      <Col span={6} className="gutter-row gx-pr-0">
                        <ButtonGroup className="gx-custom-toggle-buttons">
                          <Button
                            className={`gx-btn-toggle ${
                              item.taxable ? "gx-btn-toggle-yes" : ""
                            }`}
                            size="small"
                            onClick={() =>
                              this.onChangeItemTaxable(true, index)
                            }
                          >
                            {item.taxable ? (
                              <span>Yes</span>
                            ) : (
                              <ToggleButton></ToggleButton>
                            )}
                          </Button>
                          <Button
                            className={`gx-btn-toggle ${
                              !item.taxable ? "gx-btn-toggle-no" : ""
                            }`}
                            size="small"
                            onClick={() =>
                              this.onChangeItemTaxable(false, index)
                            }
                          >
                            {!item.taxable ? (
                              <span>No</span>
                            ) : (
                              <ToggleButton></ToggleButton>
                            )}
                          </Button>
                        </ButtonGroup>
                      </Col>
                    </Row>
                  </div>
                  <div className="gx-line-item-detail gx-flex-row gx-align-items-center gx-justify-content-center gx-mb-10">
                    <Row gutter={10} className="gx-w-100">
                      <Col span={18} className="gutter-row gx-pl-0">
                        <h5>Amount</h5>
                      </Col>
                      <Col span={6} className="gutter-row gx-pr-0">
                        <span className="gx-fs-13-20 gx-font-weight-bold">
                          {"$" + (item.qty * item.rate).toFixed(2)}
                        </span>
                      </Col>
                    </Row>
                  </div>
                  <div className="gx-line-item-detail gx-flex-row gx-align-items-center gx-justify-content-center gx-mb-10">
                    <Row gutter={10} className="gx-w-100">
                      <Col span={18} className="gutter-row gx-pl-0">
                        <Button
                          className="gx-flex-row gx-align-items-center gx-p-0"
                          type="link"
                        >
                          <i
                            className="material-icons"
                            style={{ paddingRight: "5px" }}
                          >
                            add
                          </i>
                          {this.props.kind + " item"}
                        </Button>
                      </Col>
                      <Col span={6} className="gutter-row gx-pr-0">
                        <i
                          className="material-icons gx-icon-action"
                          onClick={() => this.removeItem(index)}
                        >
                          cancel
                        </i>
                      </Col>
                    </Row>
                  </div>
                </div>
              ))}
            </div>
          </Widget>
        )}
        <Modal
          className="gx-modal-add-jobItem gx-customized-modal gx-p-0"
          title={
            <div className="gx-customized-modal-header">
              <div className="gx-flex-row gx-align-items-center gx-justify-content-center gx-w-100">
                <Row className="gx-w-100" gutter={10}>
                  <Col
                    span={6}
                    xs={12}
                    sm={12}
                    md={6}
                    lg={6}
                    xl={6}
                    className="gutter-row"
                  >
                    <div className="gx-customized-modal-title">
                      Add {this.props.kind.toLowerCase()}
                    </div>
                  </Col>
                  <Col
                    span={18}
                    xs={12}
                    sm={12}
                    md={18}
                    lg={18}
                    xl={18}
                    className="gutter-row"
                  >
                    <div className="gx-flex-row gx-align-items-center gx-justify-content-between gx-flex-nowrap">
                      <div className="gx-searchbox-wrapper">
                        <SearchBox
                          styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-jobitem-search-bar"
                          placeholder={`Search ${this.props.kind.toLowerCase()}`}
                          onChange={this.updateSearchText}
                          value={this.state.searchText}
                        />
                      </div>
                      <div className="gx-customized-modal-header-action gx-flex-row gx-align-items-center">
                        <Button
                          className="gx-btn-add-jobItem gx-m-0"
                          type="primary"
                          onClick={this.addItems}
                        >
                          Add to line items
                        </Button>
                        <i
                          className="material-icons gx-customized-modal-close gx-ml-20"
                          onClick={this.onAddItemModalClose}
                        >
                          clear
                        </i>
                      </div>
                    </div>
                  </Col>
                </Row>
              </div>
            </div>
          }
          visible={this.state.isAddItemModalVisible}
          centered
          width={"80%"}
          closable={false}
          onCancel={this.onAddItemModalClose}
          footer={null}
        >
          <div className="gx-w-100">
            <Row gutter={10}>
              <Col
                span={6}
                xs={0}
                sm={0}
                md={0}
                lg={6}
                xl={6}
                className="gutter-row"
              >
                <div className="gx-customized-modal-sidebar">
                  <div className="gx-customized-modal-sidebar-scroll">
                    <Menu
                      defaultSelectedKeys={["0"]}
                      mode="inline"
                      theme="light"
                    >
                      {this.props.categories.map((category, index) => (
                        <Menu.Item key={index}>
                          <div
                            className="gx-sidebar-item"
                            onClick={() => this.setSelectedCategory(category)}
                          >
                            <span className="gx-category-name">{category}</span>
                            <div className="gx-category-count">
                              {this.getMaterialCountByCategory(category)}
                            </div>
                          </div>
                        </Menu.Item>
                      ))}
                    </Menu>
                  </div>
                </div>
              </Col>
              <Col
                span={18}
                xs={24}
                sm={24}
                md={24}
                lg={18}
                xl={18}
                className="gutter-row"
              >
                <div className="gx-customized-modal-content">
                  <div className="gx-customized-modal-mobile-top-tabs gx-mt-10 gx-mx-20">
                    <Tabs className="gx-jobItem-tab-categories">
                      {this.props.categories.map((category, index) => (
                        <TabPane
                          tab={
                            <div
                              className="gx-sidebar-item"
                              onClick={() => this.setSelectedCategory(category)}
                            >
                              <IntlMessages id={category} />
                            </div>
                          }
                          key={index}
                        ></TabPane>
                      ))}
                    </Tabs>
                  </div>
                  <div className="gx-customized-modal-content-scroll">
                    <Row className="gx-w-100" gutter={10}>
                      {this.state.items
                        .filter(
                          item =>
                            item.name
                              .toLowerCase()
                              .includes(this.state.searchText.toLowerCase()) &&
                            item.category.includes(this.state.selected_category)
                        )
                        .map((item, index) => (
                          <Col
                            span={8}
                            xs={24}
                            sm={12}
                            md={12}
                            lg={12}
                            xl={8}
                            xxl={8}
                            key={index}
                          >
                            <Widget styleName="gx-jobItem-card gx-card-full gx-w-100">
                              <div className="gx-jobItem-card-body">
                                <div className="gx-jobItem-card-image gx-material-card-image">
                                  {item.image != "" ? (
                                    <img
                                      src={require("assets/images/material/" +
                                        item.image)}
                                    />
                                  ) : (
                                    <img
                                      src={require("assets/images/material/material-default.png")}
                                    />
                                  )}
                                </div>
                                <div className="gx-p-20">
                                  <div className="gx-jobItem-card-name">
                                    {item.name}
                                  </div>
                                  <div className="gx-jobItem-card-desc">
                                    {item.desc}
                                  </div>
                                  <div className="gx-jobItem-card-tags">
                                    {item.tags.map((tag, index) => (
                                      <Tag key={index}>{tag}</Tag>
                                    ))}
                                  </div>
                                </div>
                              </div>
                              <div className="gx-jobItem-card-footer">
                                <div className="gx-flex-row gx-align-items-cetner gx-justify-content-between gx-w-100">
                                  <div className="gx-flex-row gx-align-items-center">
                                    <span
                                      style={{
                                        fontSize: "13px",
                                        color: "#9399a2",
                                        marginRight: "10px"
                                      }}
                                    >
                                      Unit price:{" "}
                                    </span>
                                    <span className="gx-jobItem-card-price">
                                      ${parseFloat(item.price).toFixed(2)}
                                    </span>
                                  </div>
                                  <div className="gx-flex-row gx-align-items-center">
                                    <Button
                                      className="gx-customized-button gx-size-30 gx-quantity-btn gx-quantity-btn-minus"
                                      onClick={() => this.minusQTY(item.id)}
                                    >
                                      <i className="material-icons">remove</i>
                                    </Button>
                                    <div className="gx-jobItem-quantity">
                                      {item.quantity}
                                    </div>
                                    <Button
                                      className="gx-customized-button gx-size-30 gx-quantity-btn gx-quantity-btn-minus"
                                      onClick={() => this.plusQTY(item.id)}
                                    >
                                      <i className="material-icons">add</i>
                                    </Button>
                                  </div>
                                </div>
                              </div>
                            </Widget>
                          </Col>
                        ))}
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </Modal>
      </Auxiliary>
    );
  }
}
const mapStateToProps = ({ settings }) => {
  const {
    locale,
    navStyle,
    navCollapsed,
    width,
    pathname,
    currentPage
  } = settings;
  return { locale, navStyle, navCollapsed, width, pathname, currentPage };
};
export default connect(mapStateToProps, {
  switchLanguage,
  setCurrentPage,
  updateWindowWidth
})(injectIntl(JobSubPane));
