import React from "react";
import OverviewWelcome from "./OverViewWelcome";
import OverviewJobList from "./OverviewJobList";

const Overview = props => {
  return (
    <div className="gx-dashboard-overall-panel gx-h-100">
      <div className="gx-flex-1-same">
        <OverviewWelcome
          avatar={props.auth.avatar}
          name={props.auth.first_name + " " + props.auth.last_name}
        />
      </div>
      <div className="gx-flex-1-same">
        <OverviewJobList />
      </div>
    </div>
  );
};

export default Overview;
