import React, { Component } from "react";
import IntlMessages from "util/IntlMessages";
import { Avatar } from "antd";
import RoundPager from "components/RoundPager";
import { changeDateShortMonthFormatSS } from "util/DateTime";

const Item = ({ count, title, className }) => {
  return (
    <div className={className}>
      <div
        className="gx-media"
        style={{ paddingTop: "10px", paddingBottom: "10px" }}
      >
        <div className="gx-text-center gx-w-100">
          <Avatar
            className="count gx-fs-xl-nl gx-font-weight-semi-bold gx-mb-2"
            size="large"
          >
            {count}
          </Avatar>
          <div className="text gx-fs-13-20 gx-font-weight-medium">
            <IntlMessages id={title} />
          </div>
        </div>
      </div>
    </div>
  );
};

class OverviewWelcome extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      roundIdx: 0
    };
  }

  onChangeRoundItem(idx) {
    this.setState({ roundIdx: idx });
  }

  render() {
    const { name, avatar } = this.props;
    const { roundIdx } = this.state;
    return (
      <div className="gx-dashboard-overall-overview-welcome-panel">
        <div className="gx-mb-3 gx-text-header gx-flex-row gx-align-items-center gx-justify-content-between">
          <div className="gx-flex-row gx-align-items-center">
            <Avatar
              className={`gx-pointer gx-size-50 gx-mr-10 ${
                avatar == "" || avatar == undefined ? "gx-avatar-default" : ""
              }`}
              alt={avatar}
              src={avatar}
            >
              {avatar == "" || avatar == undefined ? (
                <span className="gx-fs-xxl">
                  {name.charAt(0).toUpperCase()}
                </span>
              ) : (
                <div />
              )}
            </Avatar>
            <div className="gx-flex-column gx-align-items-center">
              <div className="gx-fs-sm gx-font-weight-medium">
                <IntlMessages id="dashboard.overall.welcome" />
              </div>
              <div className="gx-fs-18 gx-font-weight-semi-bold">{name}</div>
            </div>
          </div>
          <div className="gx-dashboard-overall-overview-welcome-calendar gx-size-50">
            <div className="gx-flex-column gx-align-items-center gx-justify-content-center gx-h-100">
              <span className="gx-fs-md gx-lh-18 gx-text-primary gx-font-weight-bold">
                {changeDateShortMonthFormatSS(new Date()).split(" ")[0]}
              </span>
              <span className="gx-fs-md gx-lh-18 gx-text-primary gx-font-weight-bold">
                {changeDateShortMonthFormatSS(new Date())
                  .split(" ")[1]
                  .toUpperCase()}
              </span>
            </div>
          </div>
        </div>
        <div className="gx-div-align-center gx-justify-content-between gx-mb-3">
          <div className="gx-fs-md gx-font-weight-medium">
            <IntlMessages id="dashboard.overall.actionstotake" />
          </div>
          <RoundPager
            count={3}
            index={roundIdx}
            onChange={this.onChangeRoundItem.bind(this)}
          />
        </div>
        <div className="gx-mb-3">
          <div className="gx-dashboard-overall-overview-welcome-quickaction gx-div-align-center gx-mb-2">
            <div className="gx-dashboard-overall-overview-welcome-roundindex">
              1
            </div>
            <div className="gx-dashboard-overall-overview-welcome-action gx-fs-13-20 gx-font-weight-medium">
              <span>Reply 2 messages in chat</span>
            </div>
          </div>
          <div className="gx-dashboard-overall-overview-welcome-quickaction gx-div-align-center gx-mb-2">
            <div className="gx-dashboard-overall-overview-welcome-roundindex">
              2
            </div>
            <div className="gx-dashboard-overall-overview-welcome-action gx-fs-13-20 gx-font-weight-medium">
              <span>Schedule 5 jobs</span>
            </div>
          </div>
          <div className="gx-dashboard-overall-overview-welcome-quickaction gx-div-align-center gx-mb-2">
            <div className="gx-dashboard-overall-overview-welcome-roundindex">
              3
            </div>
            <div className="gx-dashboard-overall-overview-welcome-action gx-fs-13-20 gx-font-weight-medium">
              <span>Schedule 2 estimates</span>
            </div>
          </div>
          <div className="gx-dashboard-overall-overview-welcome-quickaction gx-div-align-center gx-mb-2">
            <div className="gx-dashboard-overall-overview-welcome-roundindex">
              4
            </div>
            <div className="gx-dashboard-overall-overview-welcome-action gx-fs-13-20 gx-font-weight-medium">
              <span>Send 3 invoice remainders</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OverviewWelcome;
