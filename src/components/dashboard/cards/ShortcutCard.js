import React, { Component } from "react";
import Widget from "components/Widget/index";
import IntlMessages from "util/IntlMessages";
import { List, Popover, Button, Checkbox } from "antd";

const ShortcutEditPopover = props => {
  return (
    <div className="gx-popover-dashboard-shortcut-edit-content">
      <div className="gx-popover-dashboard-shortcut-edit-content-header">
        <div className="gx-flex-column">
          <div className="gx-popover-dashboard-shortcut-edit-content-header-title">
            <IntlMessages id="dashboard.shortcuts.edit_shortcut_links" />
          </div>
          <div className="gx-popover-dashboard-shortcut-edit-content-header-desc">
            0{" "}
            <IntlMessages id="dashboard.shortcuts.more_links_can_be_selected" />
          </div>
        </div>
      </div>
      <div className="gx-popover-dashboard-shortcut-edit-content-body">
        <div className="gx-popover-dashboard-shortcut-edit-content-body-scroll">
          {props.links.map((item, index) => (
            <div key={index} className="gx-shortcut-link">
              <Checkbox
                checked={item.visible}
                onChange={() => props.updateVisibleShortcutLinks(item.id)}
              >
                {item.link}
              </Checkbox>
            </div>
          ))}
        </div>
      </div>
      <div className="gx-popover-dashboard-shortcut-edit-content-footer">
        <Button
          className="gx-btn-cancel"
          onClick={props.cancelVisibleShortcutLinks}
        >
          Cancel
        </Button>
        <Button className="gx-btn-save" onClick={props.setVisibleShortcutLinks}>
          Done
        </Button>
      </div>
    </div>
  );
};
class ShortcutCard extends Component {
  state = { links: [] };
  componentDidMount() {
    let temp = this.props.shortcutLinks;
    this.setState({ links: temp });
  }
  updateVisibleShortcutLinks = id => {
    let temp = this.state.links.map(item => {
      if (item.id == id) {
        let tempItem = { ...item };
        tempItem.visible = !tempItem.visible;
        return tempItem;
      }
      return item;
    });
    this.setState({ links: [...temp] });
  };
  setVisibleShortcutLinks = () => {
    this.props.setShortcutCardEditPopoverVisible(false);
    this.props.setVisibleShortcutLinks(this.state.links);
  };
  cancelVisibleShortcutLinks = () => {
    this.props.setShortcutCardEditPopoverVisible(false);
    this.setState({ links: this.props.shortcutLinks });
  };
  render() {
    return (
      <Widget
        title={
          <h4 className="gx-text-capitalize gx-mb-0">
            <IntlMessages id="dashboard.shortcuts" />
          </h4>
        }
        styleName="gx-dashboard-card"
        extra={
          <Popover
            overlayClassName="gx-popover-dashboard-shortcut-edit"
            placement="left"
            trigger="click"
            visible={this.props.isShortcutEditPopoverVisible}
            onVisibleChange={visible =>
              this.props.setShortcutCardEditPopoverVisible(visible)
            }
            content={
              <ShortcutEditPopover
                links={this.state.links}
                updateVisibleShortcutLinks={this.updateVisibleShortcutLinks}
                setVisibleShortcutLinks={this.setVisibleShortcutLinks}
                cancelVisibleShortcutLinks={this.cancelVisibleShortcutLinks}
                setShortcutCardEditPopoverVisible={
                  this.props.setShortcutCardEditPopoverVisible
                }
              />
            }
          >
            <i
              className="material-icons gx-icon-action gx-fs-xl"
              onClick={() => this.props.setShortcutCardEditPopoverVisible(true)}
            >
              edit
            </i>
          </Popover>
        }
      >
        <div className="gx-dashboard-card-shortcut-content-scroll">
          <List
            itemLayout="horizontal"
            dataSource={this.props.shortcutLinks.filter(
              item => item.visible == true
            )}
            renderItem={item => (
              <List.Item>
                <div
                  className="gx-dashboard-card-list"
                  style={{ marginTop: "-1px", marginBottom: "-1px" }}
                >
                  <div className="gx-div-align-center">
                    <i className="icon material-icons gx-mr-2">link</i>
                    <span className="text gx-fs-md">{item.link}</span>
                  </div>
                </div>
              </List.Item>
            )}
          />
        </div>
      </Widget>
    );
  }
}

export default ShortcutCard;
