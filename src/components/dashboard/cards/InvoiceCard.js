import React from "react";

import { Dropdown, Icon, Menu, List, Divider } from "antd";
import Widget from "components/Widget/index";
import TwoProgress from "components/Progress/TwoProgress";
import IntlMessages from "util/IntlMessages";

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="http://www.alipay.com/">1st menu item</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="http://www.taobao.com/">2nd menu item</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);
const stats = [
  { title: "Overdue", value: 8 },
  { title: "Due", value: 6 },
  { title: "Unsent", value: 4 }
];
const InvoiceCard = () => {
  return (
    <Widget
      title={
        <h4 className="gx-text-capitalize gx-mb-0">
          <IntlMessages id="dashboard.openinvoices" />
        </h4>
      }
      styleName="gx-dashboard-card"
      extra={<span className="gx-fs-sm gx-link">View all</span>}
    >
      <div className="gx-dashboard-card-hover">
        <div className="gx-mt-2 gx-mb-3 gx-font-weight-semi-bold">
          $ {"00.00"} Unpaid
        </div>
        <div className="ant-row-flex gx-w-100 gx-justify-content-between gx-align-items-center gx-mt-1 gx-mb-1">
          <div>
            <div className="gx-text-right gx-fs-xl gx-font-weight-medium gx-hover-red">
              $ {"00.00"}
            </div>
            <div className="gx-text-grey gx-text-left gx-fs-sm">Overdue</div>
          </div>
          <div className="">
            <div className="gx-fs-xl gx-font-weight-medium gx-hover-grey">
              $ {"00.00"}
            </div>
            <div className="gx-text-grey gx-fs-sm">Not due yet</div>
          </div>
        </div>
        <div className="gx-mt-1 gx-mb-2">
          <TwoProgress first="red" second="grey" progress={50} />
        </div>
      </div>

      <div className="gx-pt-1 gx-pb-1">
        <Divider />
      </div>

      <div className="gx-dashboard-card-stats">
        <div className="gx-mb-3 gx-font-weight-semi-bold">
          <IntlMessages id="dashboard.invoices_stats" />
        </div>
        <div className="gx-flex-row gx-align-items gx-flex-nowrap">
          {stats.map(stat => (
            <div className="gx-dashboard-card-stats-item gx-flex-column gx-align-items-center gx-justify-content-center">
              <span className="gx-dashboard-card-stats-item-value">
                {stat.value}
              </span>
              <span className="gx-dashboard-card-stats-item-title">
                {stat.title}
              </span>
            </div>
          ))}
        </div>
      </div>
    </Widget>
  );
};

export default InvoiceCard;
