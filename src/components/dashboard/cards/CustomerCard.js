import React from "react";
import Widget from "components/Widget/index";
import TwoProgress from "components/Progress/TwoProgress";
import IntlMessages from "util/IntlMessages";
import { List } from "antd";

const data = [
  {
    icon: "supervised_user_circle",
    title: "Total customers",
    value: "375"
  },
  {
    icon: "business",
    title: "Commercial customers",
    value: "173"
  },
  {
    icon: "home",
    title: "Residential customers",
    value: "202"
  },
  {
    icon: "check_box",
    title: "Recurring customers",
    value: "48"
  }
];

const stats = [
  { title: "Active", value: 256 },
  { title: "Recurring", value: 75 },
  { title: "Prospects", value: 28 }
];
const CustomerCard = () => {
  return (
    <Widget
      title={
        <h4 className="gx-text-capitalize gx-mb-0">
          <IntlMessages id="dashboard.customers" />
        </h4>
      }
      styleName="gx-dashboard-card hover-progress"
      extra={<i className="material-icons gx-icon-action">person_add</i>}
    >
      <div className="gx-border-box gx-mt-2 gx-mb-30 gx-p-20">
        <div className="gx-flex-row gx-align-items-center gx-w-100">
          <div className="gx-text-header gx-fs-xlxl gx-font-weight-medium gx-mr-20">
            8
          </div>
          <div>
            <span className="gx-fs-13-20 gx-text-grey">
              New customers this month
            </span>
            <br />
            <span className="gx-fs-13-20 gx-text-danger gx-font-weight-medium">
              50% less
            </span>
            <span className="gx-fs-13-20 gx-text-grey"> from last month</span>
          </div>
        </div>
      </div>
      <div className="gx-mb-30 gx-fs-md gx-font-weight-medium">
        <span>
          You have <span className="gx-text-primary">34 customers</span>{" "}
          genearting
        </span>
        <br />
        <div className="gx-flex-row gx-align-items-center">
          <span className="gx-text-success gx-flex-row gx-align-items-center">
            $5780.00{" "}
            <i className="material-icons gx-pointer gx-mr-1">trending_up</i>
          </span>{" "}
          <span>this month</span>
        </div>
      </div>
      <div className="gx-dashboard-card-stats">
        <div className="gx-flex-row gx-align-items gx-flex-nowrap">
          {stats.map(stat => (
            <div className="gx-dashboard-card-stats-item gx-flex-column gx-align-items-center gx-justify-content-center">
              <span className="gx-dashboard-card-stats-item-value">
                {stat.value}
              </span>
              <span className="gx-dashboard-card-stats-item-title">
                {stat.title}
              </span>
            </div>
          ))}
        </div>
      </div>
    </Widget>
  );
};

export default CustomerCard;
