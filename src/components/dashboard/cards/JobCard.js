import React from "react";
import Widget from "components/Widget/index";
import IntlMessages from "util/IntlMessages";
import { List, Menu, Dropdown, Icon, Divider, Button } from "antd";

const data = [
  {
    icon: "monetization_on",
    title: "Total jobs value",
    value: "$5780.00"
  },
  {
    icon: "event",
    title: "Scheduled jobs",
    value: "73"
  },
  {
    icon: "cancel",
    title: "Canceled jobs",
    value: "9"
  },
  {
    icon: "check_box",
    title: "Completed jobs",
    value: "65"
  }
];

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a href="http://www.alipay.com/">1st menu item</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a href="http://www.taobao.com/">2nd menu item</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3">3rd menu item</Menu.Item>
  </Menu>
);

const JobCard = () => {
  return (
    <Widget
      title={
        <h4 className="gx-text-capitalize gx-mb-0">
          <IntlMessages id="dashboard.jobs" />
        </h4>
      }
      styleName="gx-dashboard-card hover-progress"
      extra={
        <Dropdown overlay={menu} trigger={["click"]}>
          <span className="gx-text-grey gx-fs-sm">
            This month <Icon type="down" />
          </span>
        </Dropdown>
      }
    >
      <List
        itemLayout="horizontal"
        dataSource={data}
        renderItem={item => (
          <List.Item>
            <div className="gx-dashboard-card-list">
              <div className="gx-div-align-center">
                <i className="icon material-icons gx-mr-2">{item.icon}</i>
                <span className="text gx-fs-md">{item.title}</span>
              </div>
              <div className="value gx-fs-md gx-font-weight-medium">
                {item.value}
              </div>
            </div>
          </List.Item>
        )}
      />
      <Divider className="gx-mt-0 gx-mb-30" />
      <Button className="gx-dashboard-card-btn gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-center">
        <IntlMessages id="dashboard.jobs.see_all_jobs" />
      </Button>
    </Widget>
  );
};

export default JobCard;
