export const data = [
  {
    id: 1,
    avatar: "",
    title: "Mr.",
    first_name: "Peter",
    last_name: "Jackson",
    email: "peter@jackson.com",
    phone: {
      mobile: "+1(705)-255-1111",
      primary_phone: "(613)-966-1111",
      work: ""
    },
    birthday: "01/01/1974",
    balance: 100.0,
    status: "Active",
    starred: false,
    customer_info: {
      customer_type: "Commercial",
      lead_source: "Business card",
      creation_date: "01/01/2018",
      created_by: "Peter Jonson" //later it should be user id
    },
    address: [
      {
        id: 0,
        title: "Home address1",
        street: "2655 Speers Road",
        unit: "",
        city: "Oakville",
        state: "Ontario",
        zipcode: "L6J3X4",
        alarmcode: "88590",
        property_key: "",
        lat: 43.4030085,
        lng: -79.7314425
      },
      {
        id: 1,
        title: "Office address",
        street: "496 Hochelaga",
        unit: "",
        city: "Laval",
        state: "Quebec",
        zipcode: "H7P3H6",
        alarmcode: "",
        property_key:
          "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
        lat: 45.5841211,
        lng: -73.5285817
      }
    ],
    communication: {
      name_format: ["Mr.", "last_name"],
      email_communication: true,
      sms_communication: true,
      preferred_language: "English - US"
    },
    notes: [],
    jobs: [],
    estimates: [],
    invoices: [],
    files: [],
    financial_activities: {
      overdue_invoices: [],
      non_invoiced_jobs: [],
      not_due_yet: []
    },
    financial_info: {
      automatic_invoice: "Weekly",
      payment_term: "Due after 7 days",
      bill_to: "",
      taxable: true,
      discount_rate: "5%"
    },
    billing_info: {
      billing_name: "Kevin Jonson",
      street: "496 Hochelaga",
      unit: "",
      city: "Laval",
      state: "Quebec",
      zipcode: "H7P3H6",
      invoice_to: "kevin@website.com",
      lat: 45.5841211,
      lng: -73.5285817
    },
    payment_history: []
  },
  {
    id: 2,
    avatar: "",
    title: "Mr.",
    first_name: "Peter",
    last_name: "Jonson",
    email: "peter@jonson.com",
    phone: {
      mobile: "+1(705)-255-1111",
      primary_phone: "(613)-966-1111",
      work: ""
    },
    birthday: "01/01/1974",
    balance: 100.0,
    status: "Active",
    starred: false,
    customer_info: {
      customer_type: "Commercial",
      lead_source: "Business card",
      creation_date: "01/01/2018",
      created_by: "Peter Jonson" //later it should be user id
    },
    address: [
      {
        id: 0,
        title: "Home address2",
        street: "2655 Speers Road",
        unit: "",
        city: "Oakville",
        state: "Ontario",
        zipcode: "L6J3X4",
        alarmcode: "88590",
        property_key: "",
        lat: 43.4030085,
        lng: -79.7314425
      },
      {
        id: 1,
        title: "Office address",
        street: "496 Hochelaga",
        unit: "",
        city: "Laval",
        state: "Quebec",
        zipcode: "H7P3H6",
        alarmcode: "",
        property_key:
          "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
        lat: 45.5841211,
        lng: -73.5285817
      }
    ],
    communication: {
      name_format: ["Mr.", "last_name"],
      email_communication: true,
      sms_communication: true,
      preferred_language: "English - US"
    },
    notes: [],
    jobs: [],
    estimates: [],
    invoices: [],
    files: [],
    financial_activities: {
      overdue_invoices: [],
      non_invoiced_jobs: [],
      not_due_yet: []
    },
    financial_info: {
      automatic_invoice: "Weekly",
      payment_term: "Due after 7 days",
      bill_to: "",
      taxable: true,
      discount_rate: "5%"
    },
    billing_info: {
      billing_name: "Kevin Jonson",
      street: "496 Hochelaga",
      unit: "",
      city: "Laval",
      state: "Quebec",
      zipcode: "H7P3H6",
      invoice_to: "kevin@website.com",
      lat: 45.5841211,
      lng: -73.5285817
    },
    payment_history: []
  },
  {
    id: 3,
    avatar: "",
    title: "Mr.",
    first_name: "Robert",
    last_name: "Brannon",
    email: "robert@brannon.com",
    phone: {
      mobile: "+1(705)-255-1111",
      primary_phone: "(613)-966-1111",
      work: ""
    },
    birthday: "01/01/1974",
    balance: 100.0,
    status: "Active",
    starred: false,
    customer_info: {
      customer_type: "Commercial",
      lead_source: "Business card",
      creation_date: "01/01/2018",
      created_by: "Peter Jonson" //later it should be user id
    },
    address: [
      {
        id: 0,
        title: "Home address3",
        street: "2655 Speers Road",
        unit: "",
        city: "Oakville",
        state: "Ontario",
        zipcode: "L6J3X4",
        alarmcode: "88590",
        property_key: "",
        lat: 43.4030085,
        lng: -79.7314425
      },
      {
        id: 1,
        title: "Office address",
        street: "496 Hochelaga",
        unit: "",
        city: "Laval",
        state: "Quebec",
        zipcode: "H7P3H6",
        alarmcode: "",
        property_key:
          "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
        lat: 45.5841211,
        lng: -73.5285817
      }
    ],
    communication: {
      name_format: ["Mr.", "last_name"],
      email_communication: true,
      sms_communication: true,
      preferred_language: "English - US"
    },
    notes: [],
    jobs: [],
    estimates: [],
    invoices: [],
    files: [],
    financial_activities: {
      overdue_invoices: [],
      non_invoiced_jobs: [],
      not_due_yet: []
    },
    financial_info: {
      automatic_invoice: "Weekly",
      payment_term: "Due after 7 days",
      bill_to: "",
      taxable: true,
      discount_rate: "5%"
    },
    billing_info: {
      billing_name: "Kevin Jonson",
      street: "496 Hochelaga",
      unit: "",
      city: "Laval",
      state: "Quebec",
      zipcode: "H7P3H6",
      invoice_to: "kevin@website.com",
      lat: 45.5841211,
      lng: -73.5285817
    },
    payment_history: []
  },
  {
    id: 4,
    avatar: "",
    title: "Mr.",
    first_name: "Mike",
    last_name: "Wattson",
    email: "mike@wattson.com",
    phone: {
      mobile: "+1(705)-255-1111",
      primary_phone: "(613)-966-1111",
      work: ""
    },
    birthday: "01/01/1974",
    balance: 100.0,
    status: "Active",
    starred: false,
    customer_info: {
      customer_type: "Commercial",
      lead_source: "Business card",
      creation_date: "01/01/2018",
      created_by: "Peter Jonson" //later it should be user id
    },
    address: [
      {
        id: 0,
        title: "Home address4",
        street: "2655 Speers Road",
        unit: "",
        city: "Oakville",
        state: "Ontario",
        zipcode: "L6J3X4",
        alarmcode: "88590",
        property_key: "",
        lat: 43.4030085,
        lng: -79.7314425
      },
      {
        id: 1,
        title: "Office address",
        street: "496 Hochelaga",
        unit: "",
        city: "Laval",
        state: "Quebec",
        zipcode: "H7P3H6",
        alarmcode: "",
        property_key:
          "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
        lat: 45.5841211,
        lng: -73.5285817
      }
    ],
    communication: {
      name_format: ["Mr.", "last_name"],
      email_communication: true,
      sms_communication: true,
      preferred_language: "English - US"
    },
    notes: [],
    jobs: [],
    estimates: [],
    invoices: [],
    files: [],
    financial_activities: {
      overdue_invoices: [],
      non_invoiced_jobs: [],
      not_due_yet: []
    },
    financial_info: {
      automatic_invoice: "Weekly",
      payment_term: "Due after 7 days",
      bill_to: "",
      taxable: true,
      discount_rate: "5%"
    },
    billing_info: {
      billing_name: "Kevin Jonson",
      street: "496 Hochelaga",
      unit: "",
      city: "Laval",
      state: "Quebec",
      zipcode: "H7P3H6",
      invoice_to: "kevin@website.com",
      lat: 45.5841211,
      lng: -73.5285817
    },
    payment_history: []
  },
  {
    id: 5,
    avatar: "",
    title: "Mrs.",
    first_name: "Maria",
    last_name: "Carie",
    email: "maria@carie.com",
    phone: {
      mobile: "+1(705)-255-1111",
      primary_phone: "(613)-966-1111",
      work: ""
    },
    birthday: "01/01/1974",
    balance: 100.0,
    status: "Active",
    starred: false,
    customer_info: {
      customer_type: "Commercial",
      lead_source: "Business card",
      creation_date: "01/01/2018",
      created_by: "Peter Jonson" //later it should be user id
    },
    address: [
      {
        id: 0,
        title: "Home address5",
        street: "2655 Speers Road",
        unit: "",
        city: "Oakville",
        state: "Ontario",
        zipcode: "L6J3X4",
        alarmcode: "88590",
        property_key: "",
        lat: 43.4030085,
        lng: -79.7314425
      },
      {
        id: 1,
        title: "Office address",
        street: "496 Hochelaga",
        unit: "",
        city: "Laval",
        state: "Quebec",
        zipcode: "H7P3H6",
        alarmcode: "",
        property_key:
          "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
        lat: 45.5841211,
        lng: -73.5285817
      }
    ],
    communication: {
      name_format: ["Mr.", "last_name"],
      email_communication: true,
      sms_communication: true,
      preferred_language: "English - US"
    },
    notes: [],
    jobs: [],
    estimates: [],
    invoices: [],
    files: [],
    financial_activities: {
      overdue_invoices: [],
      non_invoiced_jobs: [],
      not_due_yet: []
    },
    financial_info: {
      automatic_invoice: "Weekly",
      payment_term: "Due after 7 days",
      bill_to: "",
      taxable: true,
      discount_rate: "5%"
    },
    billing_info: {
      billing_name: "Kevin Jonson",
      street: "496 Hochelaga",
      unit: "",
      city: "Laval",
      state: "Quebec",
      zipcode: "H7P3H6",
      invoice_to: "kevin@website.com",
      lat: 45.5841211,
      lng: -73.5285817
    },
    payment_history: []
  }
];
