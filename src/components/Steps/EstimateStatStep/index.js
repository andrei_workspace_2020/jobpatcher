import React, { Component } from "react";

import { Button, Popover, Select, Input, DatePicker, TimePicker } from "antd";
import IntlMessages from "util/IntlMessages";
import { injectIntl } from "react-intl";
import { getEstimateDisplayInfo } from "util/EstimateStatus";
import { changeDateTimeFormat } from "util/DateTime";
import Auxiliary from "util/Auxiliary";
import ButtonGroup from "antd/lib/button/button-group";

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};
const { Option } = Select;
class EstimateStatStep extends Component {
  state = {
    isOnmywayPopoverVisible: false,
    isFinishPopoverVisible: false,
    isApprovalActionPopoverVisible: false,
    isApprovalAcceptPopoverVisible: false,
    isApprovalRefusePopoverVisible: false,
    isConvertPopoverVisible: false,
    notify_onmyway: this.props.steps_data[1].notify,
    notify_finish: this.props.steps_data[2].notify,
  };
  onChangeStep = step => {
    this.onOnmywayPopoverVisibleChange(false);
    this.onFinishPopoverVisibleChange(false);
    this.onApprovalActionPopoverVisibleChange(false);
    this.onConvertPopoverVisibleChange(false);
    this.props.onChangeStep(step);
  };
  onOnmywayPopoverVisibleChange = visible => {
    this.setState({ isOnmywayPopoverVisible: visible });
  };
  onFinishPopoverVisibleChange = visible => {
    this.setState({ isFinishPopoverVisible: visible });
  };
  onApprovalActionPopoverVisibleChange = visible => {
    this.setState({ isApprovalActionPopoverVisible: visible });
  };
  onApprovalAcceptPopoverVisibleChange = visible => {
    this.setState({ isApprovalAcceptPopoverVisible: visible });
  };
  onApprovalRefusePopoverVisibleChange = visible => {
    this.setState({ isApprovalRefusePopoverVisible: visible });
  };
  onConvertPopoverVisibleChange = visible => {
    this.setState({ isConvertPopoverVisible: visible });
  };
  onOnmywayToggleChange = value => {
    this.setState({ notify_onmyway: value });
  };
  onFinishToggleChange = value => {
    this.setState({ notify_finish: value });
  };
  render() {
    const step1Info = getEstimateDisplayInfo(this.props.steps_data[0].title);
    const step2Info = getEstimateDisplayInfo(this.props.steps_data[1].title);
    const step3Info = getEstimateDisplayInfo(this.props.steps_data[2].title);
    const step4Info = getEstimateDisplayInfo(this.props.steps_data[3].title);
    const step5Info = getEstimateDisplayInfo(this.props.steps_data[4].title);
    const step6Info = getEstimateDisplayInfo(this.props.steps_data[5].title);
    return (
      <div className="gx-job-stat-steps">
        <div
          className="gx-pointer gx-job-stat-steps-item-wrapper"
          onClick={() => this.onChangeStep(1)}
        >
          <div
            className={`gx-job-stat-steps-item ${
              this.props.current >= 1 ? "check" : ""
            }`}
          >
            <div className="gx-job-stat-steps-icon">
              <i className="material-icons">{step1Info.icon}</i>
            </div>
            <div className="gx-mt-10">
              <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                <IntlMessages id={step1Info.title} />
              </span>
            </div>
            {this.props.current >= 1 ? (
              <Auxiliary>
                {this.props.times && this.props.times[0] ? (
                  <div className="gx-fs-11 gx-text-normal gx-mt-1">
                    {changeDateTimeFormat(this.props.times[0])}
                  </div>
                ) : (
                  <div />
                )}
              </Auxiliary>
            ) : (
              <div className="gx-fs-11 gx-text-grey gx-mt-1">
                <IntlMessages id={step1Info.action} />
              </div>
            )}
          </div>
        </div>
        <div className="gx-pointer gx-job-stat-steps-item-wrapper">
          <div
            className={`gx-job-stat-steps-item ${
              this.props.current >= 2 ? "check" : ""
            }`}
          >
            <Popover
              overlayClassName="gx-estimate-popover"
              placement="bottom"
              trigger="click"
              visible={this.state.isOnmywayPopoverVisible}
              onVisibleChange={visible =>
                this.onOnmywayPopoverVisibleChange(visible)
              }
              content={
                <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                  <div className="gx-popover-title gx-flex-row gx-flex-nowrap gx-align-items-center">
                    <img
                      className="gx-mr-10"
                      src={require("assets/images/estimate/ontheway.png")}
                    />
                    <div className="gx-flex-column">
                      <span className="gx-fs-lg gx-font-weight-bold">
                        On my way
                      </span>
                      <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                        This will make you visible on GPS tracking map
                      </span>
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-mt-10">
                    <div className="gx-customized-content-field-title">
                      On my way at
                    </div>
                    <div className="gx-customized-content-field-value">
                      <DatePicker
                        className="gx-w-100"
                        format="DD/MM/YYYY"
                        placeholder="DD / MM / YYYY"
                        suffixIcon={
                          <i className="material-icons">date_range</i>
                        }
                      />
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-mt-10">
                    <div className="gx-customized-content-field-value">
                      <TimePicker className="gx-w-100" />
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-mt-10">
                    <div className="gx-customized-content-field-value gx-flex-row gx-align-items-center">
                      <ButtonGroup className="gx-custom-toggle-buttons">
                        <Button
                          className={`gx-btn-toggle ${
                            this.state.notify_onmyway ? "gx-btn-toggle-yes" : ""
                          }`}
                          size="small"
                          onClick={() => this.onOnmywayToggleChange(true)}
                        >
                          {this.state.notify_onmyway ? (
                            <span>Yes</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                        <Button
                          className={`gx-btn-toggle ${
                            !this.state.notify_onmyway ? "gx-btn-toggle-no" : ""
                          }`}
                          size="small"
                          onClick={() => this.onOnmywayToggleChange(false)}
                        >
                          {!this.state.notify_onmyway ? (
                            <span>No</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                      </ButtonGroup>
                      <span className="gx-text-grey">Notify customer</span>
                    </div>
                  </div>
                  <div
                    className="gx-popover-action"
                    style={{ marginTop: "15px" }}
                  >
                    <Button
                      type="default"
                      className="gx-btn-cancel gx-mr-10"
                      onClick={() => this.onOnmywayPopoverVisibleChange(false)}
                    >
                      Cancel
                    </Button>
                    <Button
                      type="primary"
                      className="gx-btn-confirm"
                      onClick={() => this.onChangeStep(2)}
                    >
                      On my way
                    </Button>
                  </div>
                </div>
              }
            >
              <div className="gx-job-stat-steps-icon">
                <i className="material-icons">{step2Info.icon}</i>
              </div>
            </Popover>
            <div className="gx-mt-10">
              <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                <IntlMessages id={step2Info.title} />
              </span>
            </div>
            {this.props.current >= 2 ? (
              <Auxiliary>
                {this.props.times && this.props.times[1] ? (
                  <div className="gx-fs-11 gx-text-normal gx-mt-1">
                    {changeDateTimeFormat(this.props.times[1])}
                  </div>
                ) : (
                  <div />
                )}
              </Auxiliary>
            ) : (
              <div className="gx-fs-11 gx-text-grey gx-mt-1">
                <IntlMessages id={step2Info.action} />
              </div>
            )}
          </div>
          <div
            className={`gx-job-stat-steps-seperator ${
              this.props.current >= 2 ? "passed" : ""
            }`}
          ></div>
        </div>
        <div className="gx-pointer gx-job-stat-steps-item-wrapper">
          <div
            className={`gx-job-stat-steps-item ${
              this.props.current >= 3 ? "check" : ""
            }`}
          >
            <Popover
              overlayClassName="gx-estimate-popover"
              placement="bottom"
              trigger="click"
              visible={this.state.isFinishPopoverVisible}
              onVisibleChange={visible =>
                this.onFinishPopoverVisibleChange(visible)
              }
              content={
                <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                  <div className="gx-popover-title gx-flex-row gx-flex-nowrap gx-align-items-center">
                    <img
                      className="gx-mr-10"
                      src={require("assets/images/estimate/finish.png")}
                    />
                    <div className="gx-flex-column">
                      <span className="gx-fs-lg gx-font-weight-bold">
                        Finish estimate
                      </span>
                      <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                        This will stop estimate creation and record time
                      </span>
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-mt-10">
                    <div className="gx-customized-content-field-title">
                      Finish estimate at
                    </div>
                    <div className="gx-customized-content-field-value">
                      <DatePicker
                        className="gx-w-100"
                        format="DD/MM/YYYY"
                        placeholder="DD / MM / YYYY"
                        suffixIcon={
                          <i className="material-icons">date_range</i>
                        }
                      />
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-mt-10">
                    <div className="gx-customized-content-field-value">
                      <TimePicker className="gx-w-100" />
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-mt-10">
                    <div className="gx-customized-content-field-value gx-flex-row gx-align-items-center">
                      <ButtonGroup className="gx-custom-toggle-buttons">
                        <Button
                          className={`gx-btn-toggle ${
                            this.state.notify_finish ? "gx-btn-toggle-yes" : ""
                          }`}
                          size="small"
                          onClick={() => this.onFinishToggleChange(true)}
                        >
                          {this.state.notify_finish ? (
                            <span>Yes</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                        <Button
                          className={`gx-btn-toggle ${
                            !this.state.notify_finish ? "gx-btn-toggle-no" : ""
                          }`}
                          size="small"
                          onClick={() => this.onFinishToggleChange(false)}
                        >
                          {!this.state.notify_finish ? (
                            <span>No</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                      </ButtonGroup>
                      <span className="gx-text-grey">Notify customer</span>
                    </div>
                  </div>
                  <div
                    className="gx-popover-action"
                    style={{ marginTop: "15px" }}
                  >
                    <Button
                      type="default"
                      className="gx-btn-cancel gx-mr-10"
                      onClick={() => this.onFinishPopoverVisibleChange(false)}
                    >
                      Cancel
                    </Button>
                    <Button
                      type="primary"
                      className="gx-btn-confirm"
                      onClick={() => this.onChangeStep(3)}
                    >
                      Finish
                    </Button>
                  </div>
                </div>
              }
            >
              <div className="gx-job-stat-steps-icon">
                <i className="material-icons">{step3Info.icon}</i>
              </div>
            </Popover>
            <div className="gx-mt-10">
              <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                <IntlMessages id={step3Info.title} />
              </span>
            </div>
            {this.props.current >= 3 ? (
              <Auxiliary>
                {this.props.times && this.props.times[2] ? (
                  <div className="gx-fs-11 gx-text-normal gx-mt-1">
                    {changeDateTimeFormat(this.props.times[2])}
                  </div>
                ) : (
                  <div />
                )}
              </Auxiliary>
            ) : (
              <div className="gx-fs-11 gx-text-grey gx-mt-1">
                <IntlMessages id={step3Info.action} />
              </div>
            )}
          </div>
          <div
            className={`gx-job-stat-steps-seperator ${
              this.props.current >= 3 ? "passed" : ""
            }`}
          ></div>
        </div>
        <div
          className="gx-pointer gx-job-stat-steps-item-wrapper"
          onClick={() => {
            this.onChangeStep(4);
            this.props.history.push("/estimates/send");
          }}
        >
          <div
            className={`gx-job-stat-steps-item ${
              this.props.current >= 4 ? "check" : ""
            }`}
          >
            <div className="gx-job-stat-steps-icon">
              <i className="material-icons">{step4Info.icon}</i>
            </div>
            <div className="gx-mt-10">
              <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                <IntlMessages id={step4Info.title} />
              </span>
            </div>
            {this.props.current >= 4 ? (
              <Auxiliary>
                {this.props.times && this.props.times[3] ? (
                  <div className="gx-fs-11 gx-text-normal gx-mt-1">
                    {changeDateTimeFormat(this.props.times[3])}
                  </div>
                ) : (
                  <div />
                )}
              </Auxiliary>
            ) : (
              <div className="gx-fs-11 gx-text-grey gx-mt-1">
                <IntlMessages id={step4Info.action} />
              </div>
            )}
          </div>
          <div
            className={`gx-job-stat-steps-seperator ${
              this.props.current >= 4 ? "passed" : ""
            }`}
          ></div>
        </div>
        <div className="gx-pointer gx-job-stat-steps-item-wrapper">
          <div
            className={`gx-job-stat-steps-item ${
              this.props.current >= 5 ? "check" : ""
            }`}
          >
            <Popover
              overlayClassName={`gx-popover-estimate-approval-action ${
                this.state.isApprovalAcceptPopoverVisible ||
                this.state.isApprovalRefusePopoverVisible
                  ? "gx-estimate-popover"
                  : ""
              }`}
              placement="bottom"
              trigger="click"
              visible={this.state.isApprovalActionPopoverVisible}
              onVisibleChange={visible =>
                this.onApprovalActionPopoverVisibleChange(visible)
              }
              content={
                this.state.isApprovalAcceptPopoverVisible ? (
                  <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                    <div className="gx-popover-title gx-flex-row gx-flex-nowrap gx-align-items-center">
                      <img
                        className="gx-mr-10"
                        src={require("assets/images/estimate/accept.png")}
                      />
                      <div className="gx-flex-column">
                        <span className="gx-fs-lg gx-font-weight-bold">
                          Approve estimate
                        </span>
                        <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                          Mark estimate as approved
                        </span>
                      </div>
                    </div>
                    <div
                      className="gx-popover-action"
                      style={{ marginTop: "15px" }}
                    >
                      <Button
                        type="default"
                        className="gx-btn-cancel gx-mr-10"
                        onClick={() =>
                          this.onApprovalAcceptPopoverVisibleChange(false)
                        }
                      >
                        Cancel
                      </Button>
                      <Button
                        type="primary"
                        className="gx-btn-confirm"
                        onClick={() => this.onChangeStep(5)}
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                ) : this.state.isApprovalRefusePopoverVisible ? (
                  <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                    <div className="gx-popover-title gx-flex-row gx-flex-nowrap gx-align-items-center">
                      <img
                        className="gx-mr-10"
                        src={require("assets/images/estimate/refuse.png")}
                      />
                      <div className="gx-flex-column">
                        <span className="gx-fs-lg gx-font-weight-bold">
                          Refuse estimate
                        </span>
                        <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                          Mark estimate as refused
                        </span>
                      </div>
                    </div>
                    <div className="gx-customized-content-field gx-mt-10">
                      <div className="gx-customized-content-field-value">
                        <Select
                          className="gx-w-100"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          placeholder="Chosen another provider"
                        >
                          <Option value="AAAAA">AAAAA</Option>
                          <Option value="BBBBB">BBBBB</Option>
                          <Option value="CCCCC">CCCCC</Option>
                        </Select>
                      </div>
                    </div>
                    <div
                      className="gx-popover-action"
                      style={{ marginTop: "15px" }}
                    >
                      <Button
                        type="default"
                        className="gx-btn-cancel gx-mr-10"
                        onClick={() =>
                          this.onApprovalRefusePopoverVisibleChange(false)
                        }
                      >
                        Cancel
                      </Button>
                      <Button
                        type="primary"
                        className="gx-btn-confirm"
                        onClick={() =>
                          this.onApprovalRefusePopoverVisibleChange(false)
                        }
                      >
                        Confirm
                      </Button>
                    </div>
                  </div>
                ) : this.state.isApprovalActionPopoverVisible ? (
                  <div>
                    <div
                      className="gx-menuitem"
                      onClick={() => {
                        this.onApprovalAcceptPopoverVisibleChange(true);
                      }}
                    >
                      <i className="material-icons">check_circle</i>
                      <span>Approve estimate</span>
                    </div>
                    <div
                      className="gx-menuitem"
                      onClick={() => {
                        this.onApprovalRefusePopoverVisibleChange(true);
                      }}
                    >
                      <i className="material-icons">pan_tool</i>
                      <span>Refuse estimate</span>
                    </div>
                  </div>
                ) : (
                  <div />
                )
              }
            >
              <div className="gx-job-stat-steps-icon">
                <i className="material-icons">{step5Info.icon}</i>
              </div>
            </Popover>
            <div className="gx-mt-10 gx-flex-row gx-align-items-center gx-justify-content-center">
              <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                <IntlMessages id={step5Info.title} />
              </span>
              <i
                className={`material-icons ${
                  this.props.current >= 5 ? "gx-text-primary" : "gx-text-grey"
                }`}
                style={{ fontSize: "20px", lineHeight: "13px" }}
              >
                expand_more
              </i>
            </div>
            {this.props.current >= 5 ? (
              <Auxiliary>
                {this.props.times && this.props.times[4] ? (
                  <div className="gx-fs-11 gx-text-normal gx-mt-1">
                    {changeDateTimeFormat(this.props.times[4])}
                  </div>
                ) : (
                  <div />
                )}
              </Auxiliary>
            ) : (
              <div className="gx-fs-11 gx-text-grey gx-mt-1">
                <IntlMessages id={step5Info.action} />
              </div>
            )}
          </div>
          <div
            className={`gx-job-stat-steps-seperator ${
              this.props.current >= 5 ? "passed" : ""
            }`}
          ></div>
        </div>
        <div className="gx-pointer gx-job-stat-steps-item-wrapper">
          <div
            className={`gx-job-stat-steps-item ${
              this.props.current >= 6 ? "check" : ""
            }`}
          >
            <Popover
              overlayClassName="gx-estimate-popover"
              placement="bottom"
              trigger="click"
              visible={this.state.isConvertPopoverVisible}
              onVisibleChange={visible =>
                this.onConvertPopoverVisibleChange(visible)
              }
              content={
                <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                  <div className="gx-popover-title gx-flex-row gx-flex-nowrap gx-align-items-center">
                    <img
                      className="gx-mr-10"
                      src={require("assets/images/estimate/convert.png")}
                    />
                    <div className="gx-flex-column">
                      <span className="gx-fs-lg gx-font-weight-bold">
                        Convert to a job
                      </span>
                      <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                        Copy this estimate to a job
                      </span>
                    </div>
                  </div>
                  <div
                    className="gx-popover-action"
                    style={{ marginTop: "15px" }}
                  >
                    <Button
                      type="default"
                      className="gx-btn-cancel gx-mr-10"
                      onClick={() => this.onConvertPopoverVisibleChange(false)}
                    >
                      Cancel
                    </Button>
                    <Button
                      type="primary"
                      className="gx-btn-confirm"
                      onClick={() => this.onChangeStep(6)}
                    >
                      Confirm
                    </Button>
                  </div>
                </div>
              }
            >
              <div className="gx-job-stat-steps-icon">
                <i className="material-icons">{step6Info.icon}</i>
              </div>
            </Popover>
            <div className="gx-mt-10">
              <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                <IntlMessages id={step6Info.title} />
              </span>
            </div>
            {this.props.current >= 6 ? (
              <Auxiliary>
                {this.props.times && this.props.times[5] ? (
                  <div className="gx-fs-11 gx-text-normal gx-mt-1">
                    {changeDateTimeFormat(this.props.times[5])}
                  </div>
                ) : (
                  <div />
                )}
              </Auxiliary>
            ) : (
              <div className="gx-fs-11 gx-text-grey gx-mt-1">
                <IntlMessages id={step6Info.action} />
              </div>
            )}
          </div>
          <div
            className={`gx-job-stat-steps-seperator ${
              this.props.current >= 6 ? "passed" : ""
            }`}
          ></div>
        </div>
      </div>
    );
  }
}

export default injectIntl(EstimateStatStep);
