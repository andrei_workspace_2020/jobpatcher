import React, { Component } from "react";

import { Button, Popover, Select, Input, DatePicker, TimePicker } from "antd";
import IntlMessages from "util/IntlMessages";
import { injectIntl } from "react-intl";
import { getEstimateDisplayInfo } from "util/EstimateStatus";
import { changeDateTimeFormat } from "util/DateTime";
import Auxiliary from "util/Auxiliary";
import ButtonGroup from "antd/lib/button/button-group";

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};
const { Option } = Select;
class EstimateStatStep extends Component {
  state = {
    isPopoverVisible: false,
    selectedStep: ""
  };
  onChangeStep = step => {
    this.props.onChangeStep(step);
  };
  onPopoverVisibleChange = visible => {
    console.log(visible);
    this.setState({ isPopoverVisible: visible });
  };
  render() {
    return (
      <div className="gx-job-stat-steps">
        {this.props.steps_data.map((step, index) => {
          const stepInfo = getEstimateDisplayInfo(step);
          var style = "";
          if (index <= this.props.current - 1) style = "check";

          return (
            <div
              key={index}
              className="gx-pointer gx-job-stat-steps-item-wrapper"
              onClick={() => {
                this.setState({ selectedStep: step });
                this.onPopoverVisibleChange(true);
              }}
            >
              <div className={"gx-job-stat-steps-item " + style}>
                <Popover
                  overlayClassName="gx-estimate-popover"
                  placement="bottom"
                  trigger="click"
                  visible={
                    this.state.selectedStep === step
                      ? this.state.isPopoverVisible
                      : false
                  }
                  onVisibleChange={visible =>
                    this.onPopoverVisibleChange(visible)
                  }
                  content={
                    this.state.selectedStep === "on the way" ? (
                      <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                        <div className="gx-popover-title gx-flex-row gx-flex-nowrap gx-align-items-center">
                          <img
                            className="gx-mr-10"
                            src={require("assets/images/estimate/ontheway.png")}
                          />
                          <div className="gx-flex-column">
                            <span className="gx-fs-lg gx-font-weight-bold">
                              On my way
                            </span>
                            <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                              This will make you visible on GPS tracking map
                            </span>
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-mt-10">
                          <div className="gx-customized-content-field-title">
                            On my way at
                          </div>
                          <div className="gx-customized-content-field-value">
                            <DatePicker
                              className="gx-w-100"
                              format="DD/MM/YYYY"
                              placeholder="DD / MM / YYYY"
                              suffixIcon={
                                <i className="material-icons">date_range</i>
                              }
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-mt-10">
                          <div className="gx-customized-content-field-value">
                            <TimePicker className="gx-w-100" />
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-mt-10">
                          <div className="gx-customized-content-field-value gx-flex-row gx-align-items-center">
                            <ButtonGroup className="gx-custom-toggle-buttons">
                              <Button
                                className={`gx-btn-toggle ${
                                  this.props.notify_onmyway
                                    ? "gx-btn-toggle-yes"
                                    : ""
                                }`}
                                size="small"
                              >
                                {this.props.notify_onmyway ? (
                                  <span>Yes</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                              <Button
                                className={`gx-btn-toggle ${
                                  !this.props.notify_onmyway
                                    ? "gx-btn-toggle-no"
                                    : ""
                                }`}
                                size="small"
                              >
                                {!this.props.notify_onmyway ? (
                                  <span>No</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                            </ButtonGroup>
                            <span className="gx-text-grey">
                              Notify customer
                            </span>
                          </div>
                        </div>
                        <div
                          className="gx-popover-action"
                          style={{ marginTop: "15px" }}
                        >
                          <Button
                            type="default"
                            className="gx-btn-cancel gx-mr-10"
                            onClick={() => this.onPopoverVisibleChange(false)}
                          >
                            Cancel
                          </Button>
                          <Button
                            type="primary"
                            className="gx-btn-confirm"
                            onClick={() => {
                              this.onPopoverVisibleChange(false);
                              this.props.onChangeStep(index + 1);
                            }}
                          >
                            On my way
                          </Button>
                        </div>
                      </div>
                    ) : this.state.selectedStep === "finish" ? (
                      <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                        <div className="gx-popover-title gx-flex-row gx-flex-nowrap gx-align-items-center">
                          <img
                            className="gx-mr-10"
                            src={require("assets/images/estimate/finish.png")}
                          />
                          <div className="gx-flex-column">
                            <span className="gx-fs-lg gx-font-weight-bold">
                              Finish estimate
                            </span>
                            <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                              This will stop estimate creation and record time
                            </span>
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-mt-10">
                          <div className="gx-customized-content-field-title">
                            Finish estimate at
                          </div>
                          <div className="gx-customized-content-field-value">
                            <DatePicker
                              className="gx-w-100"
                              format="DD/MM/YYYY"
                              placeholder="DD / MM / YYYY"
                              suffixIcon={
                                <i className="material-icons">date_range</i>
                              }
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-mt-10">
                          <div className="gx-customized-content-field-value">
                            <TimePicker className="gx-w-100" />
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-mt-10">
                          <div className="gx-customized-content-field-value gx-flex-row gx-align-items-center">
                            <ButtonGroup className="gx-custom-toggle-buttons">
                              <Button
                                className={`gx-btn-toggle ${
                                  this.props.notify_finish
                                    ? "gx-btn-toggle-yes"
                                    : ""
                                }`}
                                size="small"
                              >
                                {this.props.notify_finish ? (
                                  <span>Yes</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                              <Button
                                className={`gx-btn-toggle ${
                                  !this.props.notify_finish
                                    ? "gx-btn-toggle-no"
                                    : ""
                                }`}
                                size="small"
                              >
                                {!this.props.notify_finish ? (
                                  <span>No</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                            </ButtonGroup>
                            <span className="gx-text-grey">
                              Notify customer
                            </span>
                          </div>
                        </div>
                        <div
                          className="gx-popover-action"
                          style={{ marginTop: "15px" }}
                        >
                          <Button
                            type="default"
                            className="gx-btn-cancel gx-mr-10"
                            onClick={() => this.onPopoverVisibleChange(false)}
                          >
                            Cancel
                          </Button>
                          <Button
                            type="primary"
                            className="gx-btn-confirm"
                            onClick={() => {
                              this.onPopoverVisibleChange(false);
                              this.props.onChangeStep(index + 1);
                            }}
                          >
                            Finish
                          </Button>
                        </div>
                      </div>
                    ) : this.state.selectedStep === "approval" ? (
                      <div />
                    ) : (
                      <div />
                    )
                  }
                >
                  <div className="gx-job-stat-steps-icon">
                    <i className="material-icons">{stepInfo.icon}</i>
                  </div>
                </Popover>
                <div className="gx-mt-10">
                  <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                    <IntlMessages id={stepInfo.title} />
                  </span>
                </div>
                {index <= this.props.current - 1 && (
                  <Auxiliary>
                    {this.props.times && this.props.times[index] ? (
                      <div className="gx-fs-11 gx-text-normal gx-mt-1">
                        {changeDateTimeFormat(this.props.times[index])}
                      </div>
                    ) : (
                      <div />
                    )}
                  </Auxiliary>
                )}
                {index > this.props.current - 1 && (
                  <div className="gx-fs-11 gx-text-grey gx-mt-1">
                    <IntlMessages id={stepInfo.action} />
                  </div>
                )}
              </div>

              {index > 0 && (
                <div
                  className={
                    "gx-job-stat-steps-seperator" +
                    (index <= this.props.current - 1 ? " passed" : "")
                  }
                ></div>
              )}
            </div>
          );
        })}
      </div>
    );
  }
}

export default injectIntl(EstimateStatStep);
