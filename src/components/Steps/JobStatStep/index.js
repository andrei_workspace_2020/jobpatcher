import React, { Component } from "react";

import { Button } from "antd";
import IntlMessages from "util/IntlMessages";
import { getJobDisplayInfo } from "util/JobStatus";
import { changeDateTimeFormat } from "util/DateTime";
import Auxiliary from "util/Auxiliary";

const onChangeStep = (props, step) => {
  props.onChangeStep(step);
};
const JobStatStep = props => {
  return (
    <div className="gx-job-stat-steps">
      {props.steps_data.map((step, index) => {
        const stepInfo = getJobDisplayInfo(step);
        var style = "";
        if (index <= props.current - 1) style = "check";

        return (
          <div
            key={index}
            className="gx-pointer gx-job-stat-steps-item-wrapper"
            onClick={() => onChangeStep(props, index + 1)}
          >
            <div className={"gx-job-stat-steps-item " + style}>
              <div className="gx-job-stat-steps-icon">
                <i className="material-icons">{stepInfo.icon}</i>
              </div>
              <div className="gx-mt-10">
                <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-job-stat-steps-status">
                  <IntlMessages id={stepInfo.title} />
                </span>
              </div>
              {index <= props.current - 1 && (
                <Auxiliary>
                  {props.times && props.times[index] ? (
                    <div className="gx-fs-11 gx-text-normal gx-mt-1">
                      {changeDateTimeFormat(props.times[index])}
                    </div>
                  ) : (
                    <div className="gx-fs-11 gx-text-grey gx-mt-1 gx-flex-row gx-align-items gx-justify-content-center">
                      <Button className="gx-btn-undo">
                        <span className="gx-fs-13-20 gx-font-weight-medium">
                          <IntlMessages id="job.action.undo" />
                        </span>
                      </Button>
                    </div>
                  )}
                </Auxiliary>
              )}
              {index > props.current - 1 && (
                <div className="gx-fs-11 gx-text-grey gx-mt-1">
                  <IntlMessages id={stepInfo.action} />
                </div>
              )}
            </div>

            {index > 0 && (
              <div
                className={
                  "gx-job-stat-steps-seperator" +
                  (index <= props.current - 1 ? " passed" : "")
                }
              ></div>
            )}
          </div>
        );
      })}
    </div>
  );
};

export default JobStatStep;
