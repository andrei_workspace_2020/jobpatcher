import React, { Component } from "react";
import NotificationItem from "./NotificationItem";
import { notifications } from "./data";
import CustomScrollbars from "util/CustomScrollbars";
import Auxiliary from "util/Auxiliary";
import { Link } from "react-router-dom";

class MailNotification extends Component {
  onChatPopoverVisibleChange = visible => {
    console.log("AAAAAAA");
    this.props.onChatPopoverVisibleChange(visible);
  };
  render() {
    return (
      <Auxiliary>
        <div className="gx-popover-header">
          <h3 className="gx-mb-0">Employee chat</h3>
          <img src={require("assets/images/icon-settings.png")} />
        </div>
        <div className="gx-popover-scroll">
          <ul className="gx-sub-popover">
            {notifications.map((notification, index) => (
              <NotificationItem
                key={index}
                notification={notification}
                onChatPopoverVisibleChange={this.onChatPopoverVisibleChange}
              />
            ))}
          </ul>
        </div>
        <Link to="/chat" onClick={() => this.onChatPopoverVisibleChange(false)}>
          <div
            className="gx-flex-row gx-align-items-center gx-justify-content-center"
            style={{
              height: "50px",
              fontWeight: "500",
              borderRadius: "0px 0px 10px 10px"
            }}
          >
            View chatting page
          </div>
        </Link>
      </Auxiliary>
    );
  }
}

export default MailNotification;
