import React from "react";
import { Avatar } from "antd";
import { Link } from "react-router-dom";

const NotificationItem = props => {
  const { image, badge, name, time, message } = props.notification;
  return (
    <Link to="/chat" onClick={() => props.onChatPopoverVisibleChange(false)}>
      <li className="gx-media">
        <div className="gx-user-thumb gx-mr-3">
          <Avatar
            className={`gx-size-30 ${image == "" ? "gx-avatar-default" : ""}`}
            alt={image}
            src={image}
          >
            {name.charAt(0)}
          </Avatar>
        </div>
        <div className="gx-media-body">
          <div className="gx-flex-row gx-justify-content-between gx-align-items-center">
            <h5 className="gx-text-capitalize gx-user-name gx-mb-0 gx-flex-row gx-align-items-center gx-font-weight-medium">
              <span>{name}</span>
              {badge > 0 ? (
                <span
                  className="gx-badge gx-badge-danger gx-text-white gx-mb-0"
                  style={{
                    marginLeft: "5px",
                    fontSize: "12px",
                    padding: "2px 5px"
                  }}
                >
                  {badge}
                </span>
              ) : null}
            </h5>
            <span className="gx-meta-date">
              <small>{time}</small>
            </span>
          </div>
          <p
            className={`gx-fs-sm gx-mb-0 ${badge > 0 ? "message-unread" : ""}`}
          >
            {message}
          </p>
        </div>
      </li>
    </Link>
  );
};

export default NotificationItem;
