import React, { Component } from "react";

import ActivityListItem from "components/List/ActivityListItem";
import IntlMessages from "util/IntlMessages";
import { Button, Popover } from "antd";
import { Link } from "react-router-dom";
import Widget from "components/Widget";

const data = [
  {
    alert_color: "yellow",
    activity: (
      <span>
        An invoice worth $200.00 needs to be sent for job{" "}
        <Link to="#">#00546</Link>
      </span>
    )
  },
  {
    alert_color: "red",
    activity: (
      <span>
        Invoice <Link to="#">#0380</Link> has become overdue
      </span>
    )
  }
];

class FinancialActivityTab extends Component {
  state = {};

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { className, title, balance } = this.props;
    return (
      <div className={"gx-customer-tab " + className} style={{ zIndex: "100" }}>
        <div className="gx-customer-tab-header gx-flex-row gx-align-items-center gx-justifiy-content-between">
          <h5 className="gx-text-uppercase">
            <IntlMessages id={title} />
          </h5>
          <Popover
            overlayClassName="gx-popover-new-financial-activity"
            placement="bottomLeft"
            trigger="click"
            content={
              <div>
                <div className="gx-menuitem">Create an invoice</div>
                <div className="gx-menuitem">Record a payment</div>
                <div className="gx-menuitem">Add a deposit</div>
              </div>
            }
          >
            <Button className="gx-customized-button gx-flex-row gx-align-items-center gx-justify-content-between">
              New <i className="material-icons">expand_more</i>
            </Button>
          </Popover>
        </div>
        <div className={className + "-scroll"}>
          <div className="gx-customer-tab-content">
            <Widget styleName="gx-card-full gx-mb-0 gx-financial-activities-card">
              <div className="gx-panel-title-bar">
                <h5>Current balance</h5>
                <h5>{balance}</h5>
              </div>
              <div className="gx-panel-content">
                {data.length > 0 ? (
                  data.map((item, index) => (
                    <div
                      key={index}
                      className="gx-financial-activity-item gx-flex-row gx-flex-nowrap"
                    >
                      <i
                        className="material-icons gx-financial-activity-icon"
                        style={{
                          color:
                            item.alert_color == "yellow"
                              ? "#efb51c"
                              : item.alert_color == "red"
                              ? "#f55555"
                              : ""
                        }}
                      >
                        warning
                      </i>
                      <span className="gx-fs-sm">{item.activity}</span>
                    </div>
                  ))
                ) : (
                  <div className="gx-flex-column gx-align-items-center gx-justify-content-center gx-pt-20">
                    <div>
                      <img
                        src={require("assets/images/customer/no_financial_activities.png")}
                      />
                    </div>
                    <span
                      className="gx-mt-10 gx-font-size-13-20"
                      style={{ color: "#9399a2" }}
                    >
                      Everything is paid successfully.
                    </span>
                  </div>
                )}
              </div>
            </Widget>
          </div>
        </div>
      </div>
    );
  }
}

export default FinancialActivityTab;
