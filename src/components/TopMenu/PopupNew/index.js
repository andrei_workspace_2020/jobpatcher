import React from "react";
import Auxiliary from "util/Auxiliary";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

const PopupNew = props => {
  return (
    <Auxiliary>
      <div className="gx-popover-no-padding" style={{ minWidth: "185px" }}>
        <div className="gx-mt-3 gx-mb-3">
          <div
            className="gx-menuitem"
            onClick={() => {
              props.showNewCustomerDlg();
              props.onVisibleNewPopover(false);
            }}
          >
            <i className="material-icons gx-mr-10">account_circle</i>
            <IntlMessages id="topmenu.new.customer" />
          </div>
          <Link to="/jobs/add" onClick={() => props.onVisibleNewPopover(false)}>
            <div className="gx-menuitem">
              <i className="material-icons gx-mr-10">build</i>
              <IntlMessages id="topmenu.new.job" />
            </div>
          </Link>
          <Link
            to="/customers"
            onClick={() => props.onVisibleNewPopover(false)}
          >
            <div className="gx-menuitem">
              <i className="material-icons gx-mr-10">assignment_turned_in</i>
              <IntlMessages id="topmenu.new.estimate" />
            </div>
          </Link>
        </div>
      </div>
    </Auxiliary>
  );
};

export default PopupNew;
