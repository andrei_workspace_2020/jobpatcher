import React, { Component } from "react";
import { injectIntl } from "react-intl";
import Widget from "components/Widget";
import { Popover } from "antd";

class CustomerCard extends Component {
  state = {};

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {
      data,
      intl: { formatMessage }
    } = this.props;
    return (
      <Widget styleName="gx-card-full gx-dispatcher-job-panel gx-card-customerinfo gx-m-0 gx-h-100">
        <div className="gx-panel-title-bar ">
          <h5>CUSTOMER</h5>
          <div
            className="gx-edit-circle"
            onClick={() => {
              this.props.showEditCustomerDlg(data);
              this.props.onSetEditablePanelKey("customer");
            }}
          >
            <i className="material-icons gx-icon-action">edit</i>
          </div>
        </div>
        <div className="gx-panel-content">
          <div
            className="gx-addjob-customer-widget"
            style={{ padding: "20px" }}
          >
            <div className="gx-customized-content-field gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-between">
              <div>
                <div className="gx-customized-content-field-title">
                  <span>Name</span>
                </div>
                <div className="gx-customized-content-field-value">
                  <span>
                    {this.props.data != undefined
                      ? this.props.data.first_name +
                        " " +
                        this.props.data.last_name
                      : ""}
                  </span>
                </div>
              </div>
              <i className="material-icons gx-addjob-customer-widget-icon">
                account_circle
              </i>
            </div>
            <div className="gx-customized-content-field gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-between">
              <div>
                <div className="gx-customized-content-field-title">
                  <span>Phone</span>
                </div>
                <div className="gx-customized-content-field-value">
                  <span>
                    {this.props.data != undefined
                      ? this.props.data.phone.primary_phone
                      : ""}
                  </span>
                </div>
              </div>
              <i className="material-icons gx-addjob-customer-widget-icon">
                local_phone
              </i>
            </div>
            <div className="gx-customized-content-field gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-between">
              <div>
                <div className="gx-customized-content-field-title">
                  <span>Email</span>
                </div>
                <div className="gx-customized-content-field-value">
                  <span>
                    {this.props.data != undefined ? this.props.data.email : ""}
                  </span>
                </div>
              </div>
              <i className="material-icons gx-addjob-customer-widget-icon">
                email
              </i>
            </div>
            <div
              className="gx-addjob-customer-widget-footer gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-between gx-mt-20"
              style={{
                height: 34,
                backgroundColor: "#f7f6f6",
                padding: 5,
                cursor: "pointer",
                borderRadius: "20px"
              }}
            >
              <div className="gx-flex-row gx-align-items-center">
                <i
                  className="material-icons gx-ml-10 gx-mr-10"
                  style={{
                    color: "#b5b5b5",
                    fontSize: 20
                  }}
                >
                  contacts
                </i>
                <span
                  className="gx-addjob-customer-widget-text gx-fs-13-20 gx-font-weight-medium"
                  style={{ color: "#b5b5b5" }}
                >
                  Customer jobs history
                </span>
              </div>
              <i
                className="material-icons"
                style={{
                  color: "#b5b5b5",
                  fontSize: 20
                }}
              >
                keyboard_arrow_right
              </i>
            </div>
          </div>
        </div>
      </Widget>
    );
  }
}

export default injectIntl(CustomerCard);
