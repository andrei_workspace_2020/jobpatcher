import React, { Component } from "react";
import { injectIntl } from "react-intl";
import Widget from "components/Widget";
import { Popover, Button, Input, Select } from "antd";
import { GoogleMap, withGoogleMap, Marker } from "react-google-maps";

const { Option } = Select;
const SimpleMapExampleGoogleMap = withGoogleMap(props => {
  return (
    <GoogleMap defaultZoom={16} center={props.center}>
      {props.markers.map((marker, index) => {
        return (
          <Marker
            key={index}
            position={{
              lat: parseFloat(marker.lat),
              lng: parseFloat(marker.lng)
            }}
          ></Marker>
        );
      })}
    </GoogleMap>
  );
});

class AddressCard extends Component {
  state = {
    isAddressFormChanged: false,
    selected_address: {
      id: "",
      title: "",
      street: "",
      unit: "",
      city: "",
      state: "",
      zip: "",
      alarmcode: "",
      property_key: ""
    },
    isAddressActionPopoverVisible: [],
    isAddressDeletePopoverVisible: false
  };

  constructor(props, context) {
    super(props, context);
  }
  componentDidMount() {
    let array = [];
    for (let i = 0; i < this.props.data.address.length; i++) array[i] = false;
    this.setState({ isAddressActionPopoverVisible: [...array] });
  }
  onSetAddressEditable = editable => {
    this.setState({ isAddressEditable: editable });
  };
  onAddressTitleChange = e => {
    let temp = this.state.selected_address;
    temp.title = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onStreetChange = e => {
    let temp = this.state.selected_address;
    temp.street = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onUnitChange = e => {
    let temp = this.state.selected_address;
    temp.unit = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onCityChange = e => {
    let temp = this.state.selected_address;
    temp.city = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onStateChange = value => {
    let temp = this.state.selected_address;
    temp.state = value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onZipcodeChange = e => {
    let temp = this.state.selected_address;
    temp.zipcode = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onAlarmcodeChange = e => {
    let temp = this.state.selected_address;
    temp.alarmcode = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onPropertyKeyChange = e => {
    let temp = this.state.selected_address;
    temp.property_key = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onSubmitAddress = () => {
    this.props.onSetEditablePanelKey("");
    this.props.onSubmitAddress(this.state.selected_address);
  };
  onAddressActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isAddressActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isAddressActionPopoverVisible: temp });
  };
  onAddressDeletePopoverVisibleChange = visible => {
    console.log("delete popover");
    this.setState({ isAddressDeletePopoverVisible: visible });
  };
  render() {
    const {
      data,
      intl: { formatMessage }
    } = this.props;
    return (
      <Widget styleName="gx-card-full gx-dispatcher-job-panel gx-card-address gx-m-0 gx-h-100">
        <div className="gx-panel-title-bar ">
          <h5>ADDRESS</h5>
          {this.props.editablePanelKey == "address" ? (
            <Button
              className={`gx-btn-save ${
                this.state.isAddressFormChanged ? "gx-btn-save-active" : ""
              }`}
              onClick={this.onSubmitAddress}
            >
              Save
            </Button>
          ) : (
            <div
              className="gx-edit-circle"
              onClick={() => {
                this.props.onSetEditablePanelKey("address");
                this.setState({
                  selected_address: { ...this.props.data.address[0] }
                });
              }}
            >
              <i className="material-icons gx-icon-action">edit</i>
            </div>
          )}
        </div>
        <div className="gx-panel-content">
          <div className="gx-panel-body-content-scroll">
            {this.props.editablePanelKey == "address" ? (
              <div className="gx-p-20 gx-w-100">
                <div className="gx-customized-content-field">
                  <div className={`gx-customized-content-field-title editable`}>
                    Address title
                  </div>
                  <div className="gx-customized-content-field-value">
                    <Input
                      value={this.state.selected_address.title}
                      onChange={this.onAddressTitleChange}
                    />
                  </div>
                </div>
                <div className="gx-customized-content-field">
                  <div className={`gx-customized-content-field-title editable`}>
                    Street
                  </div>
                  <div className="gx-customized-content-field-value">
                    <Input
                      id="street"
                      value={this.state.selected_address.street}
                      onChange={this.onStreetChange}
                    />
                  </div>
                </div>
                <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                  <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 3 ? "editable" : ""
                      }`}
                    >
                      Unit
                    </div>
                    <div className="gx-customized-content-field-value">
                      <Input
                        placeholder="Unit number"
                        value={this.state.selected_address.unit}
                        onChange={this.onUnitChange}
                      />
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-w-100">
                    <div
                      className={`gx-customized-content-field-title editable`}
                    >
                      City
                    </div>
                    <div className="gx-customized-content-field-value">
                      <Input
                        value={this.state.selected_address.city}
                        onChange={this.onCityChange}
                      />
                    </div>
                  </div>
                </div>
                <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                  <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                    <div
                      className={`gx-customized-content-field-title editable`}
                    >
                      State
                    </div>
                    <div className="gx-customized-content-field-value">
                      <Select
                        suffixIcon={
                          <i className="material-icons">expand_more</i>
                        }
                        value={this.state.selected_address.state}
                        onChange={this.onStateChange}
                      >
                        <Option value="Ontario">Ontario</Option>
                        <Option value="Quebec">Quebec</Option>
                      </Select>
                    </div>
                  </div>
                  <div className="gx-customized-content-field gx-w-100">
                    <div
                      className={`gx-customized-content-field-title editable`}
                    >
                      Zipcode
                    </div>
                    <div className="gx-customized-content-field-value">
                      <Input
                        value={this.state.selected_address.zipcode}
                        onChange={this.onZipcodeChange}
                      />
                    </div>
                  </div>
                </div>
                <div className="gx-customized-content-field">
                  <div className={`gx-customized-content-field-title editable`}>
                    Alarm code
                  </div>
                  <div className="gx-customized-content-field-value">
                    <Input
                      value={this.state.selected_address.alarmcode}
                      onChange={this.onAlarmcodeChange}
                    />
                  </div>
                </div>
                <div className="gx-customized-content-field">
                  <div className={`gx-customized-content-field-title editable`}>
                    Key note
                  </div>
                  <div className="gx-customized-content-field-value">
                    <Input
                      value={this.state.selected_address.property_key}
                      onChange={this.onPropertyKeyChange}
                    />
                  </div>
                </div>
              </div>
            ) : this.props.data.address.length > 0 ? (
              <div>
                <SimpleMapExampleGoogleMap
                  markers={this.props.data.address}
                  center={{
                    lat: parseFloat(this.props.data.address[0].lat),
                    lng: parseFloat(this.props.data.address[0].lng)
                  }}
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={
                    <div
                      className="gx-dispatch-gps-googlemap-container"
                      style={{ height: "150px" }}
                    />
                  }
                  mapElement={<div style={{ height: `100%` }} />}
                />
                {this.props.data.address.map((add, index) => (
                  <div
                    key={index}
                    className="address-info"
                    style={{ padding: "15px 10px 5px 20px" }}
                  >
                    <div className="gx-customized-content-field">
                      <div className="gx-customized-content-field-title">
                        <span>
                          {add.title !== ""
                            ? add.title
                            : "Address #" + (index + 1)}
                        </span>
                      </div>
                      <div className="gx-customized-content-field-value">
                        <table className="gx-w-100">
                          <tbody>
                            <tr>
                              <td width="50%">
                                <span>
                                  {add.street +
                                    " " +
                                    add.city +
                                    ", " +
                                    add.state +
                                    " " +
                                    add.zipcode}
                                </span>
                              </td>
                              <td width="50%">
                                <div className="gx-flex-row gx-align-items-center gx-justify-content-end">
                                  <div
                                    className={`gx-circle gx-size-30 gx-mr-10 ${
                                      add.alarmcode != "" ? "active" : ""
                                    }`}
                                  >
                                    <Popover
                                      overlayClassName="gx-popover-customer-alarmcode"
                                      placement="top"
                                      trigger="hover"
                                      content={
                                        <div
                                          className="gx-address-alarmcode-content"
                                          style={{
                                            padding: "10px 20px"
                                          }}
                                        >
                                          {add.alarmcode != "" ? (
                                            <div className="gx-flex-column gx-justify-content-center gx-align-items-center">
                                              <span
                                                style={{
                                                  color: "#9399a2",
                                                  fontSize: "13px",
                                                  fontWeight: "500",
                                                  marginBottom: "10px"
                                                }}
                                              >
                                                Alarm code
                                              </span>
                                              <span
                                                className="gx-custom-popover-content-value"
                                                style={{
                                                  color: "#4c586d",
                                                  fontSize: "18px",
                                                  fontWeight: "bold"
                                                }}
                                              >
                                                {add.alarmcode}
                                              </span>
                                            </div>
                                          ) : (
                                            <span
                                              style={{
                                                color: "#9399a2",
                                                fontSize: "13px",
                                                fontWeight: "500",
                                                marginBottom: "10px"
                                              }}
                                            >
                                              Not available
                                            </span>
                                          )}
                                        </div>
                                      }
                                    >
                                      <i className="material-icons">
                                        notification_important
                                      </i>
                                    </Popover>
                                  </div>
                                  <div
                                    className={`gx-circle gx-size-30 gx-mr-10 ${
                                      add.property_key != "" ? "active" : ""
                                    }`}
                                  >
                                    <Popover
                                      overlayClassName="gx-popover-customer-propertykey"
                                      placement="top"
                                      trigger="hover"
                                      content={
                                        <div
                                          className="gx-address-propertykey-content"
                                          style={{
                                            padding: "10px"
                                          }}
                                        >
                                          {add.property_key != "" ? (
                                            <div className="gx-flex-column gx-justify-content-center gx-align-items-center">
                                              <span
                                                style={{
                                                  color: "#9399a2",
                                                  fontSize: "13px",
                                                  fontWeight: "500",
                                                  marginBottom: "10px"
                                                }}
                                              >
                                                Property key note
                                              </span>
                                              <span
                                                className="gx-custom-popover-content-value gx-text-center"
                                                style={{
                                                  color: "#4c586d",
                                                  fontSize: "12px",
                                                  fontWeight: "500",
                                                  maxWidth: "250px",
                                                  lineHeight: "20px"
                                                }}
                                              >
                                                {add.property_key}
                                              </span>
                                            </div>
                                          ) : (
                                            <span
                                              style={{
                                                color: "#9399a2",
                                                fontSize: "13px",
                                                fontWeight: "500",
                                                marginBottom: "10px"
                                              }}
                                            >
                                              Not available
                                            </span>
                                          )}
                                        </div>
                                      }
                                    >
                                      <i className="material-icons">vpn_key</i>
                                    </Popover>
                                  </div>
                                  <Popover
                                    overlayClassName="gx-popover-customer-address-action"
                                    placement="bottomRight"
                                    content={
                                      !this.state
                                        .isAddressDeletePopoverVisible ? (
                                        <div>
                                          <div
                                            className="gx-menuitem"
                                            onClick={() => {
                                              this.setState({
                                                selected_address: {
                                                  ...this.props.data.address[
                                                    index
                                                  ]
                                                }
                                              });
                                              this.onAddressActionPopoverVisibleChange(
                                                false,
                                                index
                                              );
                                              this.props.onSetEditablePanelKey(
                                                "address"
                                              );
                                            }}
                                          >
                                            <i className="material-icons">
                                              edit
                                            </i>
                                            <span>Edit address</span>
                                          </div>
                                          <div
                                            className="gx-menuitem"
                                            onClick={() => {
                                              this.setState({
                                                selected_address: {
                                                  ...this.props.data.address[
                                                    index
                                                  ]
                                                }
                                              });
                                              // this.props.onDeleteAddress(
                                              //   add.id
                                              // );
                                              // this.onAddressActionPopoverVisibleChange(
                                              //   false,
                                              //   index
                                              // );
                                              this.onAddressDeletePopoverVisibleChange(
                                                true
                                              );
                                            }}
                                          >
                                            <i className="material-icons">
                                              delete
                                            </i>
                                            <span>Delete address</span>
                                          </div>
                                        </div>
                                      ) : (
                                        <div className="gx-popover-delete-address-content">
                                          <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                            <i
                                              className="gx-size-30 gx-mr-10 material-icons"
                                              style={{
                                                color: "#fc6262",
                                                fontSize: "30px"
                                              }}
                                            >
                                              warning
                                            </i>
                                            <div className="gx-flex-column">
                                              <span
                                                style={{
                                                  fontSize: "15px",
                                                  color: "#4c586d",
                                                  fontWeight: "bold"
                                                }}
                                              >
                                                Delete this address
                                              </span>
                                              <span
                                                style={{
                                                  fontSize: "13px",
                                                  color: "#9399a2",
                                                  fontWeight: "500"
                                                }}
                                              >
                                                This action can't be undone
                                              </span>
                                            </div>
                                          </div>
                                          <div
                                            className="gx-popover-action"
                                            style={{
                                              marginTop: "15px"
                                            }}
                                          >
                                            <Button
                                              type="default"
                                              className="gx-btn-cancel"
                                              onClick={() =>
                                                this.onAddressDeletePopoverVisibleChange(
                                                  index,
                                                  false
                                                )
                                              }
                                            >
                                              Cancel
                                            </Button>
                                            <Button
                                              type="primary"
                                              className="gx-btn-confirm"
                                              onClick={() => {
                                                this.onAddressDeletePopoverVisibleChange(
                                                  index,
                                                  false
                                                );
                                                this.onAddressActionPopoverVisibleChange(
                                                  index,
                                                  false
                                                );
                                                this.props.onDeleteAddress(
                                                  this.state.selected_address.id
                                                );
                                              }}
                                            >
                                              Confirm
                                            </Button>
                                          </div>
                                        </div>
                                      )
                                    }
                                    visible={
                                      this.state.isAddressActionPopoverVisible[
                                        index
                                      ]
                                    }
                                    onVisibleChange={visible =>
                                      this.onAddressActionPopoverVisibleChange(
                                        visible,
                                        index
                                      )
                                    }
                                    trigger="click"
                                  >
                                    <i
                                      className="material-icons gx-icon-action"
                                      style={{ fontSize: "26px" }}
                                    >
                                      more_vert
                                    </i>
                                  </Popover>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            ) : (
              <div
                className="gx-empty-note-container gx-flex-row gx-align-items-center gx-justify-content-center"
                style={{ height: "120px" }}
              >
                <span
                  className="gx-fs-lg gx-font-weight-medium"
                  style={{ color: "#cfd1d5" }}
                >
                  Nothing available
                </span>
              </div>
            )}
          </div>
        </div>
      </Widget>
    );
  }
}

export default injectIntl(AddressCard);
