import React, { Component } from "react";
import { Link } from "react-router-dom";
import Widget from "components/Widget/index";
import IntlMessages from "util/IntlMessages";
import { Button, Popover } from "antd";

class EmployeeJobCard extends Component {
  state = {
    isEmpActionPopoverVisible: false
  };
  constructor(props, context) {
    super(props, context);
  }
  onEmpActionPopoverVisibleChange = visible => {
    this.setState({ isEmpActionPopoverVisible: visible });
  };
  notAvailableWhenBlank(item) {
    if (item === "" || item == " ") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="customer.profile.notavailable" />
        </div>
      );
    }
    return item;
  }
  render() {
    return (
      <Widget styleName="gx-employee-card gx-py-0 gx-mb-20">
        <div
          className="gx-flex-row"
          style={{
            paddingTop: 20,
            paddingLeft: 20,
            paddingRight: 10,
            justifyContent: "space-between"
          }}
        >
          <div className="gx-flex-row">
            <div className="gx-employee-card-avatar gx-flex-0 gx-main-avatar gx-mr-3">
              {this.props.emp.avatar === "" ? (
                <span>
                  {this.props.emp.first_name.substr(0, 1).toUpperCase()}
                </span>
              ) : (
                <img
                  className="gx-employee-avatar"
                  src={require("assets/images/avatar/" + this.props.emp.avatar)}
                />
              )}
            </div>
            <div className="gx-mr-3">
              <div
                className="gx-employee-card-name gx-ss-fw-500"
                style={{ fontSize: "14px" }}
              >
                <Link to="#">
                  {this.props.emp.first_name + " " + this.props.emp.last_name}
                </Link>
              </div>
              <div className="gx-div-align-center gx-fs-md lh-26 gx-text-grey gx-ss-emloyee-card-position">
                {this.props.emp.employee_info.role}
              </div>
              <div className="gx-mb-12 paddingTop gx-ss-employee-card-btn-group">
                <Button className="gx-customized-button gx-mr-0">
                  <i className="material-icons" style={{ paddingTop: 6 }}>
                    message
                  </i>
                </Button>
                <Link to={`/dispatch/employees/profile/${this.props.emp.id}`}>
                  <Button className="gx-customized-button gx-pr-20">
                    View
                  </Button>
                </Link>
              </div>
            </div>
          </div>
          <div className="right-icon-more">
            <Popover
              content={
                <div style={{ width: "100%", height: "100%" }}>
                  <p
                    className="gx-editContent gx-ss-fs-13 gx-ss-fw-500"
                    onClick={() => {
                      this.onEmpActionPopoverVisibleChange(false);
                      this.props.showEditEmpDlg(this.props.emp);
                    }}
                  >
                    <i className="material-icons  gx-text-center icon-content">
                      edit
                    </i>
                    Edit employee
                  </p>
                  <p
                    className="gx-editContent gx-ss-fs-13 gx-ss-fw-500"
                    onClick={() => {
                      this.onEmpActionPopoverVisibleChange(false);
                      this.props.onTerminateEmp(this.props.emp);
                    }}
                  >
                    <i className="material-icons  gx-text-center icon-content ">
                      cancel
                    </i>
                    Terminate
                  </p>
                </div>
              }
              trigger="click"
              placement="bottomRight"
              arrowPointAtCenter
              overlayClassName="employee-popover"
              style={{ padding: 0 }}
              visible={this.state.isEmpActionPopoverVisible}
              onVisibleChange={visible =>
                this.onEmpActionPopoverVisibleChange(visible)
              }
            >
              <div
                className="more-icon"
                onClick={() => this.onEmpActionPopoverVisibleChange(true)}
              >
                <i
                  className="material-icons gx-w-100 gx-text-center"
                  style={{ fontSize: 27, color: "#a5abb5" }}
                >
                  more_vert
                </i>
              </div>
            </Popover>
          </div>
        </div>
        <div className="gx-employee-detail-container gx-px-20">
          <div className="customer-details-section">
            <div className="gx-div-align-center gx-fs-md gx-lh-18 gx-text-grey w-85 gx-ss-fs-12 gx-ss-fw-400">
              Email
            </div>
            <div className="customer-name gx-fs-md gx-lh-18 gx-ss-fs-12 gx-ss-fw-500">
              <span>{this.notAvailableWhenBlank(this.props.emp.email)}</span>
            </div>
          </div>
          <div className="customer-details-section">
            <div className="gx-div-align-center gx-fs-md gx-lh-18 gx-text-grey w-85 gx-ss-fs-12 gx-ss-fw-400">
              Mobile
            </div>
            <div className="customer-name gx-fs-md gx-lh-18 gx-ss-fs-12 gx-ss-fw-500">
              <span>
                {this.notAvailableWhenBlank(this.props.emp.phone.mobile)}
              </span>
            </div>
          </div>
          <div className="customer-details-section">
            <div className="gx-div-align-center gx-fs-md gx-lh-18 gx-text-grey w-85 gx-ss-fs-12 gx-ss-fw-400">
              Status
            </div>
            <div className="customer-name gx-fs-md gx-lh-18 gx-ss-fs-12 gx-ss-fw-500">
              {this.props.emp.jobs.length == 0 ? (
                "N/A"
              ) : (
                <div>
                  <span>
                    {this.props.emp.jobs[this.props.emp.jobs.length - 1]
                      .status + " job "}
                  </span>
                  <span
                    className="gx-text-primary"
                    style={{ cursor: "pointer" }}
                  >
                    {this.props.emp.jobs[this.props.emp.jobs.length - 1].id}
                  </span>
                </div>
              )}
            </div>
          </div>
          <div className="customer-details-section">
            <div className="gx-div-align-center gx-fs-md gx-lh-18 gx-text-grey w-85  gx-ss-fs-12 gx-ss-fw-400">
              Last Login
            </div>
            <div className="customer-name gx-fs-md gx-lh-18  gx-ss-fs-12 gx-ss-fw-500">
              {this.props.emp.last_login == "" ? (
                <div className="gx-flex-row gx-align-items-center">
                  <span
                    className="gx-text-green"
                    style={{ fontSize: 28, fontWeight: 600 }}
                  >
                    •&nbsp;
                  </span>
                  <span>Online</span>
                </div>
              ) : (
                <span>{this.props.emp.last_login}</span>
              )}
            </div>
          </div>
        </div>
        <div className="gx-d-md-block" style={{ height: 50, marginTop: 11 }}>
          <div
            className="gx-dispatch-job-card-worker-panel"
            style={{ height: 50 }}
          >
            <div className="gx-dispatch-job-card-worker-panel-info w-100">
              <div className="user-extra-Details">
                <div className="gx-border-right gx-employee-card-bottom-button">
                  <div className="gx-div-align-center gx-fs-md lh-26 gx-text-grey app-version gx-ss-fs-12 gx-ss-fw-400">
                    App Version:
                  </div>
                  {this.props.emp.employee_info.app_version == "Invited" ? (
                    <span className="gx-fs-md lh-26 gx-text-green  gx-ss-fs-12 gx-ss-fw-400">
                      {this.props.emp.employee_info.app_version}
                    </span>
                  ) : this.props.emp.employee_info.app_version == "" ? (
                    <span className="gx-fs-md lh-26 gx-ss-fs-12 gx-ss-fw-400 gx-ss-vers">
                      N/A
                    </span>
                  ) : (
                    <span className="gx-fs-md lh-26 gx-ss-fs-12 gx-ss-fw-400 gx-ss-vers">
                      {this.props.emp.employee_info.app_version}
                    </span>
                  )}
                </div>
                <div className="customer-details-section gx-extra-details">
                  <div className="gx-fs-md  gx-ss-fs-12 gx-ss-fw-400 gx-pl-20">
                    {this.props.emp.employee_info.app_version === "Invited" ? (
                      <span className="gx-text-primary">Resend</span>
                    ) : (
                      <span className="gx-text-green">Active</span>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Widget>
    );
  }
}

export default EmployeeJobCard;
