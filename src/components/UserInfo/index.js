import React, { Component } from "react";
import { connect } from "react-redux";
import { Avatar, Popover, Divider, Button } from "antd";
import { userSignOut } from "appRedux/actions/Auth";
import { Link, useHistory } from "react-router-dom";
import IntlMessages from "util/IntlMessages";
import IntlHtmlMessages from "util/IntlHtmlMessages";


class UserInfo extends Component {
  state = {  
    user : {
      avatar: "",
      name: this.props.auth.first_name+" "+this.props.auth.last_name,
      email: this.props.auth.email,
      trial: 8  
    },
    isUserPopoverVisible: false
  }
  signOut = ()=>{
    this.props.userSignOut();
  }
  componentDidMount() {
  }
  makeShortEmail = ()=>{
    let email = this.state.user.email;
    if(email.length>20)
      return email.substring(0, 20)+"...";
    else return this.state.user.email;
  }
  onUserPopoverVisibleChange = visible =>{
    this.setState({isUserPopoverVisible: visible});
  }
  render() {
    const {authUser} = this.props;
    
    const userMenuOptions = (
      <div
        className="gx-popover-no-padding"
        style={{ minWidth: "215px", backgroundColor: "white" }}
      >
        <div className="gx-p-3 gx-pb-2">
          <div className="gx-flex-row gx-flex-nowrap gx-flex">
          <Avatar
            className={`gx-pointer gx-size-30 gx-mr-10 ${
              this.state.user.avatar == ""
                ? "gx-avatar-default"
                : ""
            }`}
            alt={this.state.user.avatar}
            src={this.state.user.avatar}
          >
            {this.state.user.name.charAt(0).toUpperCase()}
          </Avatar>
            {/* <img
              alt="avatar"
              src={this.state.user.avatar}
              className="gx-avatar-img gx-size-36 gx-border-0 gx-mr-12"
            /> */}
            <div>
              <div className="gx-sub-title gx-fs-md gx-mb-1">{this.state.user.name}</div>
              <div className="gx-fs-sm">{this.makeShortEmail()}</div>
              <div className="gx-fs-sm gx-text-grey gx-mb-2">
                <IntlHtmlMessages
                  id="userinfo.trial"
                  values={{ value: this.state.user.trial }}
                />
              </div>

              <Button
                type="primary"
                style={{ height: "30px", width: "117px", boxShadow: "none" }}
              >
                <div className="gx-fs-13-20 gx-font-weight-medium">
                  <IntlHtmlMessages id="userinfo.upgrade" />
                </div>
              </Button>
            </div>
          </div>
        </div>
        <Divider className="gx-mt-2 gx-mb-2" />
        <div>
          <Link to="/settings/account/profile" onClick={()=>this.onUserPopoverVisibleChange(false)}>
            <div className="gx-menuitem gx-fs-13-20">
              <i className="material-icons gx-mr-10">account_box</i>
              <IntlMessages id="userinfo.myprofile" />
            </div>
          </Link>
          <Link to="/settings/general/profile" onClick={()=>this.onUserPopoverVisibleChange(false)}>
            <div className="gx-menuitem">
              <i className="material-icons gx-mr-10">business</i>
              <IntlMessages id="userinfo.company" />
            </div>
          </Link>
          <Link to="/settings/account/billings" onClick={()=>this.onUserPopoverVisibleChange(false)}>
            <div className="gx-menuitem">
              <i className="material-icons gx-mr-10">account_balance_wallet</i>
              <IntlMessages id="userinfo.billings" />
            </div>
          </Link>
          <Link to="/settings/general/region_language" onClick={()=>this.onUserPopoverVisibleChange(false)}>
            <div className="gx-menuitem">
              <i className="material-icons gx-mr-10">font_download</i>
              <div>
                <span className="gx-mr-1">
                  <IntlMessages id="userinfo.language" />
                </span>
                English
              </div>
            </div>
          </Link>
        </div>
        <Divider className="gx-mt-2 gx-mb-2" />
        <div className="gx-mb-3">
          <div className="gx-menuitem">
            <i className="material-icons gx-mr-10">feedback</i>
            <IntlMessages id="userinfo.feedback" />
          </div>
          <div className="gx-menuitem">
            <i className="material-icons gx-mr-10">lock</i>
            <IntlMessages id="userinfo.lockscreen" />
          </div>
          <div className="gx-menuitem" onClick={this.signOut}>
            <i className="material-icons gx-mr-10">input</i>
            <IntlMessages id="userinfo.signout" />
          </div>
        </div>
      </div>
    );
    return (
      <Popover
        overlayClassName="gx-popover-horizantal gx-top-profile-popover"
        placement="bottomRight"
        visible={this.state.isUserPopoverVisible}
        onVisibleChange={this.onUserPopoverVisibleChange}
        content={userMenuOptions}
        trigger="click"
      >
        <Avatar 
          src={this.state.user.avatar} 
          className={`gx-avatar gx-pointer ${this.state.user.avatar==""?"gx-avatar-default":""}`} 
          alt="">
            <h2 className="gx-font-weight-bold" style={{color: "white"}}>{this.state.user.name.charAt(0).toUpperCase()}</h2>
        </Avatar>
        <i className="material-icons">keyboard_arrow_down</i>
      </Popover>
    );
  }
}
const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser, isUserEmailExist } = auth;
  return { loader, alertMessage, showMessage, authUser, isUserEmailExist };
};
export default connect(
  mapStateToProps,
  { userSignOut }
)(UserInfo);
