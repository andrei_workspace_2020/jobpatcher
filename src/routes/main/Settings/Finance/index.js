import React, { Component } from "react";
import { Button, Input, DatePicker, Select, Card, Upload, Switch } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";

import TopMenu from "../TopMenu";

const Option = Select.Option;
const Dragger = Upload.Dragger;

class Finance extends Component {
  state = {
    isFormChanged: false,
    isOnlinePaymentSwitch: true,
    isStripActive: true
  };
  onStartsWithChange = e => {
    this.setState({ isFormChanged: true });
  };
  onInvoiceTermsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onFinancialYearStartsWithChange = e => {
    this.setState({ isFormChanged: true });
  };
  onTaxYearStartsWithChange = e => {
    this.setState({ isFormChanged: true });
  };
  onTaxPaymentChange = e => {
    this.setState({ isFormChanged: true });
  };
  onDefaultSelectionChange = e => {
    this.setState({ isFormChanged: true });
  };
  onOnlinePaymentSwitchChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ isOnlinePaymentSwitch: !this.state.isOnlinePaymentSwitch });
  };
  onStripChange = active => {
    this.setState({ isFormChanged: true });
    this.setState({ isStripActive: active });
  };
  render() {
    return (
      <div className="gx-main-content">
        <TopMenu currentPage="3" />
        <div className="gx-app-module gx-dispatch-module">
          <div className="gx-w-100">
            <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
              <Widget styleName="gx-card-full">
                <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
                  <div className="gx-w-100">
                    <div className="gx-settings-body-header gx-desktop-settings-body-header">
                      <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                        <div className="gx-settings-body-header-content">
                          <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                            Invoice settings
                          </p>
                          <Button
                            className={`gx-btn-settings-save ${
                              this.state.isFormChanged
                                ? "gx-btn-settings-save-active"
                                : ""
                            }`}
                            disabled={!this.state.isFormChanged}
                            type="secondary"
                          >
                            Save changes
                          </Button>
                        </div>
                      </div>
                    </div>
                    <div className="gx-settings-body-content gx-panel-content-scroll">
                      <div className="gx-settings-finance-body-content">
                        <div className="gx-settings-finance-details">
                          <div className="gx-settings-finance-invoice-settings">
                            <p
                              className="gx-mb-30 gx-font-weight-semi-bold"
                              style={{ fontSize: "15px" }}
                            >
                              Invoice settings
                            </p>
                            <div style={{ display: "flex", marginBottom: "" }}>
                              <div className="div-detail div-starts-with">
                                <label>Starts with</label>
                                <Select
                                  className="starts_with"
                                  placeholder="Select"
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  defaultValue="#00"
                                  onChange={this.onStartsWithChange}
                                >
                                  <Option value="#00">#00</Option>
                                  <Option value="#01">#01</Option>
                                  <Option value="#02">#02</Option>
                                  <Option value="#03">#03</Option>
                                  <Option value="#04">#04</Option>
                                  <Option value="#05">#05</Option>
                                </Select>
                              </div>
                              <div className="div-detail div-invoice-terms">
                                <label>Invoice terms</label>
                                <Select
                                  className="invoice_terms"
                                  placeholder="Select term"
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  defaultValue="net60"
                                  onChange={this.onInvoiceTermsChange}
                                >
                                  <Option value="net7">Net-7</Option>
                                  <Option value="net10">Net-10</Option>
                                  <Option value="net30">Net-30</Option>
                                  <Option value="net60">Net-60</Option>
                                  <Option value="net90">Net-90</Option>
                                </Select>
                              </div>
                            </div>
                          </div>
                          <div className="gx-settings-finance-tax-settings">
                            <p
                              className="gx-mb-30 gx-mt-20 gx-font-weight-semi-bold"
                              style={{ fontSize: "15px" }}
                            >
                              Tax settings
                            </p>
                            <div className="div-detail div-financial-year-starts-with">
                              <label>Financial year starts with</label>
                              <Select
                                className="financial_start"
                                placeholder="Select"
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                defaultValue="january"
                                onChange={this.onFinancialYearStartsWithChange}
                              >
                                <Option value="january">January</Option>
                                <Option value="febrary">Febraray</Option>
                                <Option value="march">March</Option>
                                <Option value="april">April</Option>
                                <Option value="may">May</Option>
                                <Option value="june">June</Option>
                                <Option value="july">July</Option>
                                <Option value="august">August</Option>
                                <Option value="september">September</Option>
                                <Option value="october">October</Option>
                                <Option value="november">November</Option>
                                <Option value="december">December</Option>
                              </Select>
                            </div>
                            <div className="div-detail div-tax-year-starts-with">
                              <label>Financial year starts with</label>
                              <Select
                                className="tax_start"
                                placeholder="Select"
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                defaultValue="january"
                                onChange={this.onTaxYearStartsWithChange}
                              >
                                <Option value="january">January</Option>
                                <Option value="febrary">Febraray</Option>
                                <Option value="march">March</Option>
                                <Option value="april">April</Option>
                                <Option value="may">May</Option>
                                <Option value="june">June</Option>
                                <Option value="july">July</Option>
                                <Option value="august">August</Option>
                                <Option value="september">September</Option>
                                <Option value="october">October</Option>
                                <Option value="november">November</Option>
                                <Option value="december">December</Option>
                              </Select>
                            </div>
                            <div style={{ display: "flex", marginBottom: "" }}>
                              <div className="div-detail div-tax-payment">
                                <label>Tax payment</label>
                                <Select
                                  className="tax_payment"
                                  placeholder="Select tax payment"
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  defaultValue="quarterly"
                                  onChange={this.onTaxPaymentChange}
                                >
                                  <Option value="quarterly">Quarterly</Option>
                                  <Option value="semi-quarterly">
                                    Semi Quarterly
                                  </Option>
                                </Select>
                              </div>
                              <div className="div-detail div-default-selection">
                                <label>Default selection</label>
                                <Select
                                  className="default_selection"
                                  placeholder="Select selection"
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  defaultValue="exclusiveoftax"
                                  onChange={this.onExclusiveOfTaxChange}
                                >
                                  <Option value="exclusiveoftax">
                                    Exclusive of tax
                                  </Option>
                                </Select>
                              </div>
                            </div>
                          </div>
                          <div className="gx-settings-finance-online-payment">
                            <p
                              className="gx-mb-30 gx-mt-10 gx-font-weight-semi-bold"
                              style={{ fontSize: "15px" }}
                            >
                              Online payment
                            </p>
                            <div className="div-detail div-online-payment">
                              <Switch
                                defaultChecked={
                                  this.state.isOnlinePaymentSwitch
                                }
                                onChange={this.onOnlinePaymentSwitchChange}
                              />
                              <span
                                className={
                                  !this.state.isOnlinePaymentSwitch
                                    ? "disabled-text"
                                    : ""
                                }
                              >
                                Accept online payment
                              </span>
                            </div>
                            <div className="div-detail div-strip">
                              <Button
                                onClick={() => this.onStripChange(true)}
                                className={`btn_strip_connected ${
                                  this.state.isStripActive
                                    ? "btn_strip_connected_clicked"
                                    : ""
                                }`}
                              >
                                <div className="btn-prefix">S</div>
                                <div className="btn-text">
                                  Strip is connected
                                </div>
                              </Button>
                              <Button
                                onClick={() => this.onStripChange(false)}
                                className={`btn_strip_deactivate ${
                                  !this.state.isStripActive
                                    ? "btn_strip_deactivate_clicked"
                                    : ""
                                }`}
                              >
                                Deactivate
                              </Button>
                            </div>
                          </div>
                        </div>
                        <Button
                          className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                            this.state.isFormChanged
                              ? "gx-btn-settings-save-active"
                              : ""
                          }`}
                          disabled={!this.state.isFormChanged}
                          type="secondary"
                        >
                          Save changes
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </Widget>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(Finance);
