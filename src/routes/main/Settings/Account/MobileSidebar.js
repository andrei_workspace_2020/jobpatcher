import React, { Component } from "react";
import { Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

const TabPane = Tabs.TabPane;

const MENU_ITEMS = [
  {
    title: "sidebar.settings.account.profile",
    icon: "account_box",
    link: "/settings/account/profile"
  },
  {
    title: "sidebar.settings.account.security",
    icon: "lock",
    link: "/settings/account/security"
  },
  {
    title: "sidebar.settings.account.notifications",
    icon: "notifications_active",
    link: "/settings/account/notifications"
  },
  {
    title: "sidebar.settings.account.billings",
    icon: "account_balance_wallet",
    link: "/settings/account/billings"
  }
];

class MobileSidebar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Tabs className="gx-dispatch-sidebar" defaultActiveKey={currentPage}>
        {MENU_ITEMS.map((menuItem, index) => (
          <TabPane
            tab={
              <Link to={menuItem.link}>
                <div className="gx-sidebar-item">
                  <i className="material-icons">{menuItem.icon}</i>
                  <IntlMessages id={menuItem.title} />
                </div>
              </Link>
            }
            key={index}
          ></TabPane>
        ))}
      </Tabs>
    );
  }
}

export default MobileSidebar;
