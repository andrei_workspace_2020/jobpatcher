import React, { Component } from "react";
import { Button, Input, DatePicker, Select, Card, message, Upload, notification, Popover } from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Widget from "components/Widget";
import MobileSideBar from "./MobileSidebar";
import Sidebar from "./Sidebar";
import { hideMessage, showAuthLoader, updatePassword } from "appRedux/actions/Auth";

import "assets/custom.css";
import "assets/settings-custom.css";

class Security extends Component {
  state = {
    isFormChanged: false,
    email_address: JSON.parse(localStorage.getItem("user_info")).email,
    current_password: "",
    new_password: "",
    repeat_password: "",
    error_oldpassword: false,
    error_newpassword: false,
    error_text_oldpassword: "",
    error_text_newpassword: "",
  };
  onEmailAddressChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ email_address: e.target.value });
  };
  onCurrentPasswordChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ current_password: e.target.value });
    this.setState({error_oldpassword: false});
  };
  onNewPasswordChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ new_password: e.target.value });
    this.setState({error_newpassword: false});
  };
  onRepeatPasswordChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ repeat_password: e.target.value });
    this.setState({error_newpassword: false});
  };
  // onOldPasswordErrorPopoverVisibleChange = visible=>{
  //   this.setState({error_oldpassword: visible});
  // }
  // onNewPasswordErrorPopoverVisibleChange = visible=>{
  //   this.setState({error_newpassword: visible});
  // }
  onSubmit = () =>{
    console.log(this.state);
    if(this.state.current_password!=JSON.parse(localStorage.getItem("user_info")).password){
      this.setState({
        error_oldpassword: true, 
        error_text_oldpassword: "You have entered a wrong password!"
      });
    }else if(this.state.old_password==""){
      this.setState({
        error_oldpassword: true, 
        error_text_oldpassword: "Old password is required!"
      });
    }
    if(this.state.new_password!=this.state.repeat_password){
      this.setState({
        error_newpassword: true, 
        error_text_newpassword: "Please check a new password again!"
      });
    }else if(this.state.new_password==""){
      this.setState({
        error_newpassword: true, 
        error_text_newpassword: "New password is required!"
      });
    }else{
      this.props.updatePassword({
        email: this.state.email_address, 
        password: this.state.new_password
      });
      
      message.success("Your password is successfully updated!");

      this.setState({
        error_oldpassword: false,
        error_newpassword: false,
        error_text_oldpassword: "",
        error_text_newpassword: "",
        current_password: "",
        new_password: "",
        repeat_password: "",
        isFormChanged: false
      });
    }
  }
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    Account settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSideBar
                      className="gx-mobile-settings-sidebar"
                      currentPage="1"
                    ></MobileSideBar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="1"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Security settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                      onClick={this.onSubmit}
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-account-body-content">
                  <div className="gx-settings-account-details">
                    <div className="gx-settings-account-security-details">
                      <p
                        className="gx-mb-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Login information
                      </p>
                      <div className="div-detail">
                        <div className="more-action">
                          <label>Email address</label>
                        </div>
                        <Input
                          className="email_address"
                          placeholder="Enter email address"
                          value={this.state.email_address}
                          onChange={this.onEmailAddressChange}
                        />
                      </div>
                    </div>
                    <div className="gx-settings-account-password-details">
                      <p
                        className="gx-mb-30 gx-mt-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Change password
                      </p>
                      <div className="div-detail">
                        <label>Current password</label>
                        <Popover 
                          overlayClassName="gx-input-error-popover"
                          visible={this.state.error_oldpassword}
                          // onVisibleChange={visible=>this.onOldPasswordErrorPopoverVisibleChange(visible)}
                          content={<span className="gx-error-text gx-fs-sm gx-font-weight-medium">{this.state.error_text_oldpassword}</span>}
                          placement="right"
                        >
                          <Input
                            className={`current_password ${this.state.error_oldpassword?"error":""}`}
                            placeholder="Enter current password"
                            type="password"
                            value={this.state.current_password}
                            onChange={this.onCurrentPasswordChange}
                          />
                        </Popover>
                      </div>
                      <div className="div-detail">
                        <label>New password</label>
                        <Popover 
                          overlayClassName="gx-input-error-popover"
                          visible={this.state.error_newpassword}
                          // onVisibleChange={visible=>this.onNewPasswordErrorPopoverVisibleChange(visible)}
                          content={<span className="gx-error-text gx-fs-sm gx-font-weight-medium">{this.state.error_text_newpassword}</span>}
                          placement="right"
                        >
                          <Input
                            className={`new_password ${this.state.error_newpassword?"error":""}`}
                            placeholder="Enter new password"
                            type="password"
                            value={this.state.new_password}
                            onChange={this.onNewPasswordChange}
                          ></Input>
                        </Popover>
                      </div>
                      <div className="div-detail">
                        <label>Repeat password</label>
                        <Popover 
                          overlayClassName="gx-input-error-popover"
                          visible={this.state.error_newpassword}
                          // onVisibleChange={visible=>this.onNewPasswordErrorPopoverVisibleChange(visible)}
                          content={<span className="gx-error-text gx-fs-sm gx-font-weight-medium">{this.state.error_text_newpassword}</span>}
                          placement="right"
                        >
                          <Input
                            className={`repeat_password ${this.state.error_newpassword?"error":""}`}
                            placeholder="Enter password again"
                            type="password"
                            value={this.state.repeat_password}
                            onChange={this.onRepeatPasswordChange}
                          ></Input>
                        </Popover>
                      </div>
                    </div>
                  </div>
                  <div className="gx-settings-account-security-auth">
                    <Card className="gx-card gx-pl-20 gx-pr-20 gx-pt-20 gx-pb-20">
                      <div className="gx-flex-nowrap gx-align-items-center">
                        <div className="div-authentication-image">
                          <div className="circle">
                            <img
                              src={require("assets/images/security-auth.png")}
                            ></img>
                          </div>
                        </div>
                        <div className="div-authentication-body">
                          <span className="auth-title">
                            Two-factor authentication
                          </span>
                          <span className="auth-description">
                            Give your account an extra layer of security
                          </span>
                          <Link
                            className="auth-link"
                            to="/settings/account/security"
                          >
                            <span>Click here to activate</span>
                          </Link>
                        </div>
                      </div>
                    </Card>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}
const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser, userInfo } = auth;
  return { loader, alertMessage, showMessage, authUser, userInfo };
};
export default connect(
  mapStateToProps,{
    hideMessage,
    showAuthLoader,
    updatePassword
  }
)(injectIntl(Security));
