import React, { Component } from "react";
import {
  Input,
  DatePicker,
  Tabs,
  Menu,
  Button,
  Table,
  Modal,
  Card
} from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import moment from "moment";
import IntlMessages from "util/IntlMessages";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const { TabPane } = Tabs;

const columns = [
  {
    title: "Date",
    dataIndex: "date",
    key: "date"
  },
  {
    title: "Plan",
    dataIndex: "plan",
    key: "plan"
  },
  {
    title: "Period",
    dataIndex: "period",
    key: "period"
  },
  {
    title: "Amount",
    dataIndex: "amount",
    key: "amount",
    render: (text, record) => (
      <span className="amount-value">
        <span>{record.amount}</span>
        <Button className="btn-table-action">
          <i className="material-icons">more_vert</i>
        </Button>
      </span>
    )
  }
];

const billing_history = [
  {
    key: "1",
    date: "March 28, 2019",
    plan: "Free trial",
    period: "March 28 - April 12",
    amount: "$00.00"
  },
  {
    key: "2",
    date: "April 13, 2019",
    plan: "Pro plan",
    period: "April 13 - May 13",
    amount: "$49.99"
  }
];

class Billings extends Component {
  state = {
    manageModalVisible: false,
    firstname: "Peter",
    lastname: "Jackson",
    card_number: "**** **** **** 5026",
    expiration_date: "25/2",
    security_code: ""
  };
  setManageModalVisible(visible) {
    console.log(visible);
    this.setState({ manageModalVisible: visible });
  }
  onFirstNameChange = e => {
    this.setState({ firstname: e.target.value });
  };
  onLastNameChange = e => {
    this.setState({ lastname: e.target.value });
  };
  onCardNumberChange = e => {
    this.setState({ card_number: e.target.value });
  };
  onExpirationDateChange = (date, dateString) => {
    if (dateString == "") dateString = null;
    this.setState({ expiration_date: dateString });
  };
  onSecurityCodeChange = e => {
    this.setState({ security_code: e.target.value });
  };
  onResetForm = () => {
    this.setState({ firstname: "" });
    this.setState({ lastname: "" });
    this.setState({ card_number: "" });
    this.setState({ security_code: "" });
  };
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    Account settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="3"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="3"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Billings settings
                    </p>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-settings-billings-body-content gx-panel-content-scroll">
                <div className="gx-settings-account-body-content">
                  <div className="gx-settings-account-billing-body-content">
                    <div className="gx-settings-account-billing-details">
                      <p
                        className="gx-mb-15 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Billing details
                      </p>
                      <Card className="gx-card billings-details-card">
                        <div className="div-billing-details div-membership-plan">
                          <span className="billing-details">
                            <label>Current membership plan</label>
                            <span>
                              <span
                                style={{
                                  fontSize: "14px",
                                  fontWeight: "500",
                                  color: "#132144"
                                }}
                              >
                                Team
                              </span>{" "}
                              - for 3 users - $59 / month
                            </span>
                          </span>
                          <Button
                            className="btn-billing-details btn-membership-change"
                            type="secondary"
                          >
                            Change
                          </Button>
                        </div>
                        <div className="div-billing-details div-next-billing-date">
                          <span className="billing-details">
                            <label>Next billing date</label>
                            <span>May 13, 2019</span>
                          </span>
                          <Button
                            className="btn-billing-details btn-next-billing-date"
                            type="secondary"
                          >
                            Cancel
                          </Button>
                        </div>
                        <div className="div-billing-details div-payment-method">
                          <span className="billing-details">
                            <label>Payment method</label>
                            <span>
                              <img
                                src={require("assets/images/visa.png")}
                                style={{ marginRight: "8px" }}
                              />
                              **** **** **** 5026
                            </span>
                          </span>
                          <Button
                            className="btn-billing-details"
                            type="secondary"
                            onClick={() => this.setManageModalVisible(true)}
                          >
                            Manage
                          </Button>
                          <Modal
                            className="modal-credit-card"
                            title={
                              <div>
                                <p>Update your credit card</p>
                                <i
                                  className="material-icons"
                                  onClick={() =>
                                    this.setManageModalVisible(false)
                                  }
                                >
                                  clear
                                </i>
                              </div>
                            }
                            closable={false}
                            centered
                            visible={this.state.manageModalVisible}
                            footer={[null, null]}
                            onCancel={() => this.setManageModalVisible(false)}
                          >
                            <Tabs defaultActiveKey="1">
                              <TabPane
                                tab={
                                  <span className="payment-visa">
                                    <img
                                      src={require("assets/images/tab-visa.png")}
                                    />
                                  </span>
                                }
                                key="1"
                              >
                                <div className="modal-creditcard-form">
                                  <div className="form-group form-firstname">
                                    <label>First name</label>
                                    <Input
                                      type="text"
                                      className="form-control"
                                      placeholder="Enter first name"
                                      value={this.state.firstname}
                                      onChange={this.onFirstNameChange}
                                    ></Input>
                                  </div>
                                  <div className="form-group form-lastname">
                                    <label>Last name</label>
                                    <Input
                                      type="text"
                                      placeholder="Enter last name"
                                      value={this.state.lastname}
                                      onChange={this.onLastNameChange}
                                    ></Input>
                                  </div>
                                  <div className="form-group form-cardnumber">
                                    <label>Card number</label>
                                    <Input
                                      type="text"
                                      placeholder="Enter card number"
                                      prefix={
                                        <img
                                          src={require("assets/images/visa.png")}
                                        />
                                      }
                                      suffix={
                                        <i className="material-icons">
                                          check_circle_outline
                                        </i>
                                      }
                                      onChange={this.onCardNumberChange}
                                      value={this.state.card_number}
                                    ></Input>
                                  </div>
                                  <div className="form-group form-expirationdate">
                                    <label>Expiration date</label>
                                    <DatePicker
                                      placeholder="DD / MM"
                                      format="DD/MM"
                                      suffixIcon={<span>DD / MM</span>}
                                      value={moment(
                                        this.state.expiration_date,
                                        "DD/MM"
                                      )}
                                      allowClear={false}
                                      onChange={this.onExpirationDateChange}
                                    />
                                  </div>
                                  <div className="form-group form-securitycode">
                                    <label>Security code</label>
                                    <Input
                                      type="text"
                                      placeholder="Enter CVV"
                                      suffix={
                                        <i className="material-icons">
                                          help_outline
                                        </i>
                                      }
                                      onChange={this.onSecurityCodeChange}
                                      value={this.state.security_code}
                                    ></Input>
                                  </div>
                                  <div className="form-group form-action">
                                    <Button
                                      className="btn-modal-cancel"
                                      type="default"
                                      onClick={() =>
                                        this.setManageModalVisible(false)
                                      }
                                    >
                                      Cancel
                                    </Button>
                                    <Button
                                      className="btn-modal-save"
                                      type="primary"
                                      onClick={() =>
                                        this.setManageModalVisible(false)
                                      }
                                    >
                                      Save card
                                    </Button>
                                  </div>
                                  <Link
                                    to="#"
                                    className="btn-reset-form"
                                    onClick={this.onResetForm}
                                  >
                                    <i className="material-icons">
                                      delete_sweep
                                    </i>
                                    <span>Delete saved card</span>
                                  </Link>
                                </div>
                              </TabPane>
                              <TabPane
                                tab={
                                  <span className="payment-mastercard">
                                    <img
                                      src={require("assets/images/tab-mastercard.png")}
                                    />
                                  </span>
                                }
                                key="2"
                              >
                                Content of Tab Pane 2
                              </TabPane>
                              <TabPane
                                tab={
                                  <span className="payment-amex">
                                    <img
                                      src={require("assets/images/tab-amex.png")}
                                    />
                                  </span>
                                }
                                key="3"
                              >
                                Content of Tab Pane 3
                              </TabPane>
                              <TabPane
                                tab={
                                  <span className="payment-discover">
                                    <img
                                      src={require("assets/images/tab-discover.png")}
                                    />
                                  </span>
                                }
                                key="4"
                              >
                                Content of Tab Pane 4
                              </TabPane>
                            </Tabs>
                          </Modal>
                        </div>
                      </Card>
                    </div>
                    <div className="gx-settings-account-billing-history">
                      <div className="gx-mt-10 div-billing-history-title">
                        <p
                          className="gx-font-weight-semi-bold"
                          style={{ fontSize: "15px" }}
                        >
                          Billing history
                        </p>
                        <Button className="btn-print">
                          <i className="material-icons">print</i>
                        </Button>
                      </div>
                      <Card className="gx-card billing-history-card">
                        <Table
                          className="table-reponsive"
                          columns={columns}
                          dataSource={billing_history}
                          pagination={false}
                        ></Table>
                      </Card>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(Billings);
