import React, { Component } from "react";
import { Button, Switch, Checkbox, Select } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const { Option } = Select;
const checkOptions = ["Email", "Web", "SMS"];

class Notifications extends Component {
  state = {
    isFormChanged: false,
    notification1Switch: false,
    notification2Switch: true,
    notification3Switch: true,
    reminder1Switch: false,
    reminder2Switch: true,
    reminder3Switch: false,
    activity1Switch: false,
    activity2Switch: true,
    activity3Switch: true
  };
  onNotification1SwitchChange = e => {
    this.setState({ notification1Switch: !this.state.notification1Switch });
    this.setState({ isFormChanged: true });
  };
  onNotification1ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onNotification2SwitchChange = e => {
    this.setState({ notification2Switch: !this.state.notification2Switch });
    this.setState({ isFormChanged: true });
  };
  onNotification2ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onNotification3SwitchChange = e => {
    this.setState({ notification3Switch: !this.state.notification3Switch });
    this.setState({ isFormChanged: true });
  };
  onNotification3ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onReminder1SwitchChange = e => {
    this.setState({ reminder1Switch: !this.state.reminder1Switch });
    this.setState({ isFormChanged: true });
  };
  onReminder1ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onReminder2SwitchChange = e => {
    this.setState({ reminder2Switch: !this.state.reminder2Switch });
    this.setState({ isFormChanged: true });
  };
  onReminder2ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onReminder3SwitchChange = e => {
    this.setState({ reminder3Switch: !this.state.reminder3Switch });
    this.setState({ isFormChanged: true });
  };
  onReminder3ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onReminder1TimeChange = e => {
    this.setState({ isFormChanged: true });
  };
  onReminder2TimeChange = e => {
    this.setState({ isFormChanged: true });
  };
  onReminder3TimeChange = e => {
    this.setState({ isFormChanged: true });
  };

  onActivity1SwitchChange = e => {
    this.setState({ activity1Switch: !this.state.activity1Switch });
    this.setState({ isFormChanged: true });
  };
  onActivity1ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onActivity2SwitchChange = e => {
    this.setState({ activity2Switch: !this.state.activity2Switch });
    this.setState({ isFormChanged: true });
  };
  onActivity2ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  onActivity3SwitchChange = e => {
    this.setState({ activity3Switch: !this.state.activity3Switch });
    this.setState({ isFormChanged: true });
  };
  onActivity3ObjectsChange = e => {
    this.setState({ isFormChanged: true });
  };
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    Account settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="2"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="2"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-notifications-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Notifications settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-account-notifications-body-content">
                  <div className="gx-settings-notifications-details">
                    <div className="gx-settings-general-notification-details">
                      <p>General notifications</p>
                      <div className="notification-switches">
                        <div className="notification-switch notification-case">
                          <Switch
                            defaultChecked={this.state.notification1Switch}
                            onChange={this.onNotification1SwitchChange}
                          />
                          <span
                            className={
                              !this.state.notification1Switch
                                ? "disabled-text"
                                : ""
                            }
                          >
                            Get notification when a new customer, estimate or
                            job is created
                          </span>
                        </div>
                        <div className="notification-object">
                          <Checkbox.Group
                            disabled={!this.state.notification1Switch}
                            options={checkOptions}
                            defaultValue={["Web"]}
                            onChange={this.onNotification1ObjectsChange}
                          />
                        </div>
                      </div>
                      <div className="notification-switches">
                        <div className="notification-switch notification-case">
                          <Switch
                            defaultChecked={this.state.notification2Switch}
                            onChange={this.onNotification2SwitchChange}
                          />
                          <span
                            className={
                              !this.state.notification2Switch
                                ? "disabled-text"
                                : ""
                            }
                          >
                            Get notification when changes made on schedule page
                          </span>
                        </div>
                        <div className="notification-object">
                          <Checkbox.Group
                            disabled={!this.state.notification2Switch}
                            options={checkOptions}
                            defaultValue={["Email", "Web"]}
                            onChange={this.onNotification2ObjectsChange}
                          />
                        </div>
                      </div>
                      <div className="notification-switches">
                        <div className="notification-switch notification-case">
                          <Switch
                            defaultChecked={this.state.notification3Switch}
                            onChange={this.onNotification3SwitchChange}
                          />
                          <span
                            className={
                              !this.state.notification3Switch
                                ? "disabled-text"
                                : ""
                            }
                          >
                            Get notification when new chat message is received
                          </span>
                        </div>
                        <div className="notification-object">
                          <Checkbox.Group
                            disabled={!this.state.notification3Switch}
                            options={checkOptions}
                            defaultValue={["Email", "Web", "SMS"]}
                            onChange={this.onNotification3ObjectsChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="gx-settings-reminder-details">
                      <p>Reminders</p>
                      <div className="reminder-switches">
                        <div className="reminder-switch reminder-case">
                          <Switch
                            defaultChecked={this.state.reminder1Switch}
                            onChange={this.onReminder1SwitchChange}
                          />
                          <div>
                            <span
                              className={
                                !this.state.reminder1Switch
                                  ? "disabled-text"
                                  : ""
                              }
                            >
                              Get reminder
                              <Select
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                disabled={!this.state.reminder1Switch}
                                defaultValue="30 Minutes"
                                onChange={this.onReminder1TimeChange}
                              >
                                <Option value="10 Minutes">10 Minutes</Option>
                                <Option value="20 Minutes">20 Minutes</Option>
                                <Option value="30 Minutes">30 Minutes</Option>
                                <Option value="40 Minutes">40 Minutes</Option>
                                <Option value="50 Minutes">50 Minutes</Option>
                              </Select>
                            </span>
                            <span
                              className={
                                !this.state.reminder1Switch
                                  ? "disabled-text"
                                  : ""
                              }
                            >
                              before any scheduled event
                            </span>
                          </div>
                        </div>
                        <div className="reminder-object">
                          <Checkbox.Group
                            disabled={!this.state.reminder1Switch}
                            options={checkOptions}
                            defaultValue={["Web"]}
                            onChange={this.onReminder1ObjectsChange}
                          />
                        </div>
                      </div>
                      <div className="reminder-switches">
                        <div className="reminder-switch reminder-case">
                          <Switch
                            defaultChecked={this.state.reminder2Switch}
                            onChange={this.onReminder2SwitchChange}
                          />
                          <div>
                            <span
                              className={
                                !this.state.reminder2Switch
                                  ? "disabled-text"
                                  : ""
                              }
                            >
                              Get reminder
                              <Select
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                disabled={!this.state.reminder2Switch}
                                defaultValue="30 Minutes"
                                onChange={this.onReminder2TimeChange}
                              >
                                <Option value="10 Minutes">10 Minutes</Option>
                                <Option value="20 Minutes">20 Minutes</Option>
                                <Option value="30 Minutes">30 Minutes</Option>
                                <Option value="40 Minutes">40 Minutes</Option>
                                <Option value="50 Minutes">50 Minutes</Option>
                              </Select>
                            </span>
                            <span
                              className={
                                !this.state.reminder2Switch
                                  ? "disabled-text"
                                  : ""
                              }
                            >
                              before any scheduled estimate
                            </span>
                          </div>
                        </div>
                        <div className="reminder-object">
                          <Checkbox.Group
                            disabled={!this.state.reminder2Switch}
                            options={checkOptions}
                            defaultValue={["Web", "SMS"]}
                            onChange={this.onReminder2ObjectsChange}
                          />
                        </div>
                      </div>
                      <div className="reminder-switches">
                        <div className="reminder-switch reminder-case">
                          <Switch
                            defaultChecked={this.state.reminder3Switch}
                            onChange={this.onReminder3SwitchChange}
                          />
                          <div>
                            <span
                              className={
                                !this.state.reminder3Switch
                                  ? "disabled-text"
                                  : ""
                              }
                            >
                              Get reminder
                              <Select
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                disabled={!this.state.reminder3Switch}
                                defaultValue="30 Minutes"
                                onChange={this.onReminder3TimeChange}
                              >
                                <Option value="10 Minutes">10 Minutes</Option>
                                <Option value="20 Minutes">20 Minutes</Option>
                                <Option value="30 Minutes">30 Minutes</Option>
                                <Option value="40 Minutes">40 Minutes</Option>
                                <Option value="50 Minutes">50 Minutes</Option>
                              </Select>
                            </span>
                            <span
                              className={
                                !this.state.reminder3Switch
                                  ? "disabled-text"
                                  : ""
                              }
                            >
                              before any scheduled job
                            </span>
                          </div>
                        </div>
                        <div className="reminder-object">
                          <Checkbox.Group
                            disabled={!this.state.reminder3Switch}
                            options={checkOptions}
                            defaultValue={["Web", "SMS"]}
                            onChange={this.onReminder3ObjectsChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="gx-settings-customers-activities-details">
                      <p>Customers activities</p>
                      <div className="activity-switches">
                        <div className="activity-switch activity-case">
                          <Switch
                            defaultChecked={this.state.activity1Switch}
                            onChange={this.onActivity1SwitchChange}
                          />
                          <span
                            className={
                              !this.state.activity1Switch ? "disabled-text" : ""
                            }
                          >
                            Get notification when a customer open email or click
                            any link inside
                          </span>
                        </div>
                        <div className="activity-object">
                          <Checkbox.Group
                            disabled={!this.state.activity1Switch}
                            options={checkOptions}
                            defaultValue={["Web"]}
                            onChange={this.onActivity1ObjectsChange}
                          />
                        </div>
                      </div>
                      <div className="activity-switches">
                        <div className="activity-switch activity-case">
                          <Switch
                            defaultChecked={this.state.activity2Switch}
                            onChange={this.onActivity2SwitchChange}
                          />
                          <span
                            className={
                              !this.state.activity2Switch ? "disabled-text" : ""
                            }
                          >
                            Get notification when customer is paid the invoice
                          </span>
                        </div>
                        <div className="activity-object">
                          <Checkbox.Group
                            disabled={!this.state.activity2Switch}
                            options={checkOptions}
                            defaultValue={["Email", "Web", "SMS"]}
                            onChange={this.onActivity2ObjectsChange}
                          />
                        </div>
                      </div>
                      <div className="activity-switches">
                        <div className="activity-switch activity-case">
                          <Switch
                            defaultChecked={this.state.activity3Switch}
                            onChange={this.onActivity3SwitchChange}
                          />
                          <span
                            className={
                              !this.state.activity3Switch ? "disabled-text" : ""
                            }
                          >
                            Get notification when payment became overdue
                          </span>
                        </div>
                        <div className="activity-object">
                          <Checkbox.Group
                            disabled={!this.state.activity3Switch}
                            options={checkOptions}
                            defaultValue={["Email", "Web"]}
                            onChange={this.onActivity3ObjectsChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(Notifications);
