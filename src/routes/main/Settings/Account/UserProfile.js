import React, { Component } from "react";
import { Button, Input, DatePicker, Select, Card, Upload } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const Option = Select.Option;
const Dragger = Upload.Dragger;

class UserProfile extends Component {
  state = {
    isFormChanged: false,
    first_name: JSON.parse(localStorage.getItem('user_info')).first_name,
    last_name: JSON.parse(localStorage.getItem('user_info')).last_name,
    phone_number: JSON.parse(localStorage.getItem('user_info')).phone_number,
    birthday: "",
    gender: "",
    address: "",
    state: "",
    zipcode: "",
    country: "",
    profile_first_letter: JSON.parse(localStorage.getItem('user_info')).first_name.charAt(0).toUpperCase()
  };
  onFirstNameChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ first_name: e.target.value });
    this.setState({
      profile_first_letter: e.target.value.charAt(0).toUpperCase()
    });
  };
  onLastNameChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ last_name: e.target.value });
  };
  onPhoneNumberChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ phone_number: e.target.value });
  };
  onBirthdayChange = e => {
    this.setState({ isFormChanged: true });
  };
  onGenderChange = e => {
    this.setState({ isFormChanged: true });
  };
  onAddressChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ address: e.target.value });
  };
  onStateChange = e => {
    this.setState({ isFormChanged: true });
  };
  onZipcodeChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ zipcode: e.target.value });
  };
  onCountryChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ country: true });
  };

  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    Account settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="0"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="0"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      User profile settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-account-body-content">
                  <div className="gx-settings-account-details">
                    <div className="gx-settings-account-personal-details">
                      <p
                        className="gx-mb-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Personal details
                      </p>
                      <div className="gx-flex-row gx-flex-nowrap">
                        <div className="div-detail div-full-name gx-w-50">
                          <label>First name</label>
                          <Input
                            className="full_name"
                            placeholder="First Name"
                            value={this.state.first_name}
                            onChange={this.onFirstNameChange}
                          />
                        </div>
                        <div className="div-detail div-full-name gx-w-50 gx-ml-10">
                          <label>Last name</label>
                          <Input
                            className="full_name"
                            placeholder="Last Name"
                            value={this.state.last_name}
                            onChange={this.onLastNameChange}
                          />
                        </div>
                      </div>
                      <div className="div-detail div-job-title">
                        <label>Phone number</label>
                        <Input
                          className="job_title"
                          placeholder="Phone number"
                          onChange={this.onPhoneNumberChange}
                        />
                      </div>
                      <div>
                        <div className="div-detail div-date-birth">
                          <label>Date of birth</label>
                          <DatePicker
                            className="date_birth"
                            placeholder="DD / MM / YY"
                            format="DD/MM/YY"
                            suffixIcon={
                              <i className="material-icons">date_range</i>
                            }
                            onChange={this.onBirthdayChange}
                          />
                        </div>
                        <div className="div-detail div-gender">
                          <label>Gender</label>
                          <Select
                            className="gender"
                            placeholder="Select Gender"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            onChange={this.onGenderChange}
                          >
                            <Option value="male">Male</Option>
                            <Option value="female">Female</Option>
                          </Select>
                        </div>
                      </div>
                    </div>
                    <div className="gx-settings-account-location-details">
                      <p
                        className="gx-mb-30 gx-mt-20 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Location details
                      </p>
                      <div className="div-detail div-street-address">
                        <label>Street address</label>
                        <Input
                          className="full_address"
                          placeholder="Full Address"
                          onChange={this.onAddressChange}
                        />
                      </div>
                      <div>
                        <div className="div-detail div-state">
                          <label>State</label>
                          <Select
                            className="state"
                            placeholder="Select State"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            onChange={this.onStateChange}
                          >
                            <Option value="state1">State 1</Option>
                            <Option value="state2">State 2</Option>
                          </Select>
                        </div>
                        <div className="div-detail div-zipcode">
                          <label>Zip code</label>
                          <Input
                            className="zipcode"
                            placeholder="Enter here"
                            onChange={this.onZipcodeChange}
                          />
                        </div>
                      </div>
                      <div className="div-detail div-country">
                        <label>Country</label>
                        <Select
                          className="country"
                          placeholder="Select Country"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onCountryChange}
                        >
                          <Option value="canada">Canada</Option>
                          <Option value="russia">Russia</Option>
                        </Select>
                      </div>
                    </div>
                  </div>
                  <div className="gx-settings-account-image-upload">
                    <Card className="gx-card gx-pl-20 gx-pr-20 gx-pt-20 gx-pb-20">
                      <div className="gx-media gx-flex-nowrap gx-align-items-center">
                        <div className="div-profile-image">
                          <span className="circle">
                            {this.state.profile_first_letter}
                          </span>
                        </div>
                        <div className="gx-media-body">
                          <Upload>
                            <h5
                              className="gx-wall-user-title"
                              style={{
                                fontSize: "13px",
                                color: "#257cde",
                                fontWeight: "bold",
                                cursor: "pointer"
                              }}
                            >
                              Change photo
                            </h5>
                          </Upload>
                        </div>
                      </div>
                      <Dragger>
                        <p className="ant-upload-drag-icon">
                          <img src={require("assets/images/upload.png")} />
                        </p>
                        <p className="ant-upload-text">
                          Drag photo here or <a>Browse</a>
                        </p>
                      </Dragger>
                    </Card>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(UserProfile);
