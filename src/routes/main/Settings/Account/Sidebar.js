import React, { Component } from "react";
import { Menu, Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

class Sidebar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Menu defaultSelectedKeys={[currentPage]} mode="inline" theme="light">
        <Menu.Item key="0">
          <Link to="/settings/account/profile">
            <div className="gx-sidebar-item">
              <i className="material-icons">account_box</i>
              <IntlMessages id="sidebar.settings.account.profile" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to="/settings/account/security">
            <div className="gx-sidebar-item">
              <i className="material-icons">lock</i>
              <IntlMessages id="sidebar.settings.account.security" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/settings/account/notifications">
            <div className="gx-sidebar-item">
              <i className="material-icons">notifications_active</i>
              <IntlMessages id="sidebar.settings.account.notifications" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/settings/account/billings">
            <div className="gx-sidebar-item">
              <i className="material-icons">account_balance_wallet</i>
              <IntlMessages id="sidebar.settings.account.billings" />
            </div>
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}

export default Sidebar;
