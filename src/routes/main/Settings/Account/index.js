import React from "react";
import { Route, Switch } from "react-router-dom";
import UserProfile from "./UserProfile";
import Security from "./Security";
import Notifications from "./Notifications";
import Billings from "./Billings";
import TopMenu from "../TopMenu";

const Account = ({ match }) => (
  <div className="gx-main-content">
    <TopMenu currentPage="1" />
    <div className="gx-app-module gx-dispatch-module">
      <div className="gx-w-100">
        <Switch>
          <Route path={`${match.url}/profile`} component={UserProfile} />
          <Route path={`${match.url}/security`} component={Security} />
          <Route
            path={`${match.url}/notifications`}
            component={Notifications}
          />
          <Route path={`${match.url}/billings`} component={Billings} />
        </Switch>
      </div>
    </div>
  </div>
);

export default Account;
