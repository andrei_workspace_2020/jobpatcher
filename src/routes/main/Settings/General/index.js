import React from "react";
import { Route, Switch } from "react-router-dom";
import CompanyProfile from "./CompanyProfile";
import RegionLanguage from "./RegionLanguage";
import TimeCurrency from "./TimeCurrency";
import RolePermission from "./RolePermission";
import ManageUsers from "./ManageUsers";
import TopMenu from "../TopMenu";

const General = ({ match }) => (
  <div className="gx-main-content">
    <TopMenu currentPage="0" />
    <div className="gx-app-module gx-dispatch-module">
      <div className="gx-w-100">
        <Switch>
          <Route path={`${match.url}/profile`} component={CompanyProfile} />
          <Route
            path={`${match.url}/region_language`}
            component={RegionLanguage}
          />
          <Route path={`${match.url}/time_currency`} component={TimeCurrency} />
          <Route
            path={`${match.url}/role_permission`}
            component={RolePermission}
          />
          <Route path={`${match.url}/manage_users`} component={ManageUsers} />
        </Switch>
      </div>
    </div>
  </div>
);

export default General;
