import React, { Component } from "react";
import { Button, Input, Select, Card, Upload } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const Option = Select.Option;
const Dragger = Upload.Dragger;

class RegionLanguage extends Component {
  state = {
    isFormChanged: false,
    timezone: "canada",
    time_format: "quebec",
    hour_format: "",
    date_format: "en-us",
    firstday_of_week: "",
    currency: ""
  };
  onTimezoneChange = e => {
    this.setState({ isFormChanged: true });
  };
  onTimeFormatChange = e => {
    this.setState({ isFormChanged: true });
  };
  onHourFormatChange = e => {
    this.setState({ isFormChanged: true });
  };
  onDateFormatChange = e => {
    this.setState({ isFormChanged: true });
  };
  onFirstDayOfWeekChange = e => {
    this.setState({ isFormChanged: true });
  };
  onCurrencyChange = e => {
    this.setState({ isFormChanged: true });
  };
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    General settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="2"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="2"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Time and currency settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-general-body-content">
                  <div className="gx-settings-general-details">
                    <div className="gx-settings-general-time-settings">
                      <p
                        className="gx-mb-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Time settings
                      </p>
                      <div className="div-detail div-timezone">
                        <label>Timezone</label>
                        <Select
                          className="timezone"
                          placeholder="Select timezone"
                          defaultValue="gmt-4"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onTimezoneChange}
                        >
                          <Option value="gmt-4">
                            (GMT-4) GMT Blainville, Canada
                          </Option>
                          <Option value="gmt+10">
                            (GMT+10) Vladivostok, Russia
                          </Option>
                        </Select>
                      </div>
                      <div style={{ display: "flex" }}>
                        <div className="div-detail div-timeformat">
                          <label>Time format</label>
                          <Select
                            className="time_format"
                            placeholder="Select time format"
                            defaultValue="H:MMTT"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            onChange={this.onTimeFormatChange}
                          >
                            <Option value="H:MMTT">H : MM TT</Option>
                            <Option value="HH:MMTT">HH : MM TT</Option>
                          </Select>
                        </div>
                        <div className="div-detail div-hourformat">
                          <label>Hour format</label>
                          <Select
                            className="hour_format"
                            placeholder="Select hour format"
                            defaultValue="12hours"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            onChange={this.onTimeFormatChange}
                          >
                            <Option value="12hours">12-Hours</Option>
                            <Option value="24hours">24-Hours</Option>
                          </Select>
                        </div>
                      </div>
                    </div>
                    <div className="gx-settings-general-date-settings">
                      <p
                        className="gx-mb-30 gx-mt-20 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Date settings
                      </p>
                      <div className="div-detail div-date-format">
                        <label>Date format</label>
                        <Select
                          className="date_format"
                          placeholder="Select date format"
                          defaultValue="DD/MM/YYYY"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onDateFormatChange}
                        >
                          <Option value="DD/MM/YYYY">DD/MM/YYYY</Option>
                          <Option value="DD-MM-YYYY">DD-MM-YYYY</Option>
                          <Option value="YYYY/MM/DD">YYYY/MM/DD</Option>
                          <Option value="YYYY-MM-DD">YYYY-MM-DD</Option>
                        </Select>
                      </div>
                      <div className="div-detail div-firstday-of-week">
                        <label>First day of week</label>
                        <Select
                          className="firstday_of_week"
                          placeholder="Select first day of week"
                          defaultValue="monday"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onFirstDayOfWeekChange}
                        >
                          <Option value="sunday">Sunday</Option>
                          <Option value="monday">Monday</Option>
                        </Select>
                      </div>
                    </div>
                    <div className="gx-settings-general-currency-settings">
                      <p
                        className="gx-mb-30 gx-mt-20 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Currency settings
                      </p>
                      <div className="div-detail div-currency">
                        <label>Default currency</label>
                        <Select
                          className="currency"
                          placeholder="Select currency"
                          defaultValue="cad"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onCurrencyChange}
                        >
                          <Option value="cad">Canadian dollar-CAD</Option>
                          <Option value="aud">Australian dollar-AUD</Option>
                          <Option value="usd">US dollar-USD</Option>
                          <Option value="hkd">HongKong dollar-HKD</Option>
                        </Select>
                      </div>
                    </div>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}
export default injectIntl(RegionLanguage);
