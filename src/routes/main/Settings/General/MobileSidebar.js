import React, { Component } from "react";
import { Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

const TabPane = Tabs.TabPane;

const MENU_ITEMS = [
  {
    title: "sidebar.settings.general.profile",
    icon: "business",
    link: "/settings/general/profile"
  },
  {
    title: "sidebar.settings.general.region_language",
    icon: "font_download",
    link: "/settings/general/region_language"
  },
  {
    title: "sidebar.settings.general.time_currency",
    icon: "access_time",
    link: "/settings/general/time_currency"
  },
  {
    title: "sidebar.settings.general.role_permission",
    icon: "verified_user",
    link: "/settings/general/role_permission"
  },
  {
    title: "sidebar.settings.general.manage_users",
    icon: "supervised_user_circle",
    link: "/settings/general/manage_users"
  }
];

class MobileSidebar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Tabs className="gx-dispatch-sidebar" defaultActiveKey={currentPage}>
        {MENU_ITEMS.map((menuItem, index) => (
          <TabPane
            tab={
              <Link to={menuItem.link}>
                <div className="gx-sidebar-item">
                  <i className="material-icons">{menuItem.icon}</i>
                  <IntlMessages id={menuItem.title} />
                </div>
              </Link>
            }
            key={index}
          ></TabPane>
        ))}
      </Tabs>
    );
  }
}

export default MobileSidebar;
