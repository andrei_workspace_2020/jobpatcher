import React, { Component } from "react";
import { Button, Table, Card, Popover } from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const TablePopover = () => {
  return (
    <div>
      <div className="gx-menuitem">
        <i className="material-icons gx-mr-10">settings_applications</i>
        <span>Edit role</span>
      </div>
      <div className="gx-menuitem">
        <i className="material-icons gx-mr-10">verified_user</i>
        <span>Edit permission</span>
      </div>
      <div className="gx-menuitem">
        <i className="material-icons gx-mr-10">person_add</i>
        <span>Assign user</span>
      </div>
      <div className="gx-menuitem">
        <i className="material-icons gx-mr-10">delete_sweep</i>
        <span>Delete role</span>
      </div>
    </div>
  );
};
const columns = [
  {
    title: "User name",
    dataIndex: "username",
    key: "username",
    render: (text, record) => (
      <Link to="#">
        <img className="img-user-avatar" src={record.avatar} />
        <span>{record.username}</span>
      </Link>
    )
  },
  {
    title: "User role",
    dataIndex: "role",
    key: "role"
  },
  {
    title: "Permission level",
    dataIndex: "permission_level",
    key: "permission_level",
    render: (text, record) => (
      <span className="action-more">
        <span>{record.permission_level}</span>
        <Popover
          overlayClassName="user-table-popover"
          placement="bottom"
          content={<TablePopover />}
          trigger="click"
        >
          <Button className="btn-table-action">
            <i className="material-icons">more_vert</i>
          </Button>
        </Popover>
      </span>
    )
  }
];

const user_roles = [
  {
    key: "1",
    username: "Homer Cole",
    avatar: require("assets/images/avatar-user1.png"),
    role: "Administrator",
    permission_level: "Global access"
  },
  {
    key: "2",
    username: "Peter Jaclsom",
    avatar: require("assets/images/avatar-user2.png"),
    role: "Dispatcher",
    permission_level: "Limited access"
  },
  {
    key: "3",
    username: "Steven Rodberg",
    avatar: require("assets/images/avatar-user3.png"),
    role: "Field tech",
    permission_level: "Limited access"
  }
];
class ManageUsers extends Component {
  state = {
    isFormChanged: false
  };
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    General settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="4"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="4"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Manage users
                    </p>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-general-body-content">
                  <div className="gx-settings-user-accounts">
                    <p
                      className="gx-mb-30 gx-font-weight-semi-bold"
                      style={{ fontSize: "15px" }}
                    >
                      User accounts
                    </p>
                    <Card>
                      <Table
                        className="table-user-accounts"
                        columns={columns}
                        dataSource={user_roles}
                        pagination={false}
                      ></Table>
                    </Card>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}
export default injectIntl(ManageUsers);
