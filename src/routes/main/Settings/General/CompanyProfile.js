import React, { Component } from "react";
import { Button, Input, DatePicker, Select, Card, Upload } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const Option = Select.Option;
const Dragger = Upload.Dragger;

class CompanyProfile extends Component {
  state = {
    isFormChanged: false,
    company_name: JSON.parse(localStorage.getItem("user_info")).company_name,
    registered_name: "Clean Master Pvt.Ltd",
    industry: JSON.parse(localStorage.getItem("user_info")).service_industry,
    address: "",
    city: "",
    state: "",
    zipcode: ""
  };
  componentDidMount(){
  }
  onCompanyNameChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ company_name: e.target.value });
  };
  onRegisteredNameChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ registered_name: e.target.value });
  };
  onIndustryChange = e => {
    this.setState({ isFormChanged: true });
  };
  onAddressChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ address: e.target.value });
    console.log("address: " + e.target.value);
  };
  onCityChange = e => {
    this.setState({ isFormChanged: true });
  };
  onStateChange = e => {
    this.setState({ isFormChanged: true });
  };
  onZipcodeChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ zipcode: e.target.value });
    console.log("zipcode: " + e.target.value);
  };

  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    General settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="0"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="0"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Company profile settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-general-body-content">
                  <div className="gx-settings-general-details">
                    <div className="gx-settings-general-company-details">
                      <p
                        className="gx-mb-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Company details
                      </p>
                      <div className="div-detail div-company-name">
                        <label>Company name</label>
                        <Input
                          className="company_name"
                          placeholder="Company Name"
                          value={this.state.company_name}
                          onChange={this.onCompanyNameChange}
                        />
                      </div>
                      <div className="div-detail div-registered-name">
                        <label>Registered name</label>
                        <Input
                          className="registered_name"
                          placeholder="Registered name"
                          value={this.state.registered_name}
                          onChange={this.onRegisteredNameChange}
                        />
                      </div>
                      <div className="div-detail div-industry">
                        <label>Industry</label>
                        <Select
                          className="industry"
                          placeholder="Select industry"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onIndustryChange}
                          value={this.state.industry}
                        >
                          <Option value="a">AAAAAAAA</Option>
                          <Option value="b">BBBBBBBB</Option>
                        </Select>
                      </div>
                    </div>
                    <div className="gx-settings-general-location-details">
                      <p
                        className="gx-mb-30 gx-mt-20 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Company address
                      </p>
                      <div className="div-detail div-street-address">
                        <label>Street address</label>
                        <Input
                          className="full_address"
                          placeholder="Full address"
                          onChange={this.onAddressChange}
                        />
                      </div>
                      <div className="div-detail div-city">
                        <label>City</label>
                        <Input
                          className="city"
                          placeholder="Enter city"
                          onChange={this.onAddressChange}
                        />
                      </div>
                      <div>
                        <div className="div-detail div-state">
                          <label>State</label>
                          <Select
                            className="state"
                            placeholder="Select state"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            onChange={this.onStateChange}
                          >
                            <Option value="state1">State 1</Option>
                            <Option value="state2">State 2</Option>
                          </Select>
                        </div>
                        <div className="div-detail div-zipcode">
                          <label>Zip code</label>
                          <Input
                            className="zipcode"
                            placeholder="Type here"
                            onChange={this.onZipcodeChange}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="gx-settings-general-image-upload">
                    <Card className="gx-card gx-pl-20 gx-pr-20 gx-pt-20 gx-pb-20">
                      <div className="gx-media gx-flex-nowrap gx-align-items-center">
                        <div className="div-profile-image">
                          <span className="circle">
                            <i className="material-icons">business</i>
                          </span>
                        </div>
                        <div className="gx-media-body">
                          <Upload>
                            <h5
                              className="gx-wall-company-title"
                              style={{
                                fontSize: "13px",
                                color: "#257cde",
                                fontWeight: "bold",
                                cursor: "pointer"
                              }}
                            >
                              Change logo
                            </h5>
                          </Upload>
                        </div>
                      </div>
                      <Dragger>
                        <p className="ant-upload-drag-icon">
                          <img src={require("assets/images/upload.png")} />
                        </p>
                        <p className="ant-upload-text">
                          Drag logo here or <a>Browse</a>
                        </p>
                      </Dragger>
                    </Card>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(CompanyProfile);
