import React, { Component } from "react";
import { Menu, Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

class Sidebar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Menu defaultSelectedKeys={[currentPage]} mode="inline" theme="light">
        <Menu.Item key="0">
          <Link to="/settings/general/profile">
            <div className="gx-sidebar-item">
              <i className="material-icons">business</i>
              <IntlMessages id="sidebar.settings.general.profile" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to="/settings/general/region_language">
            <div className="gx-sidebar-item">
              <i className="material-icons">font_download</i>
              <IntlMessages id="sidebar.settings.general.region_language" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/settings/general/time_currency">
            <div className="gx-sidebar-item">
              <i className="material-icons">access_time</i>
              <IntlMessages id="sidebar.settings.general.time_currency" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/settings/general/role_permission">
            <div className="gx-sidebar-item">
              <i className="material-icons">verified_user</i>
              <IntlMessages id="sidebar.settings.general.role_permission" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="4">
          <Link to="/settings/general/manage_users">
            <div className="gx-sidebar-item">
              <i className="material-icons">supervised_user_circle</i>
              <IntlMessages id="sidebar.settings.general.manage_users" />
            </div>
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}

export default Sidebar;
