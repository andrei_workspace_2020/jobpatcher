import React, { Component } from "react";
import { Button, Input, Select, Card, Upload } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const Option = Select.Option;
const Dragger = Upload.Dragger;

class RegionLanguage extends Component {
  state = {
    isFormChanged: false,
    country: "canada",
    state: "quebec",
    zipcode: "",
    language: "en-us"
  };
  onCountryChange = e => {
    this.setState({ isFormChanged: true });
  };
  onStateChange = e => {
    this.setState({ isFormChanged: true });
  };
  onZipCodeChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ zipcode: e.target.value });
  };
  onLanguageChange = e => {
    this.setState({ isFormChanged: true });
  };
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    General settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="1"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="1"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Region and language settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-general-body-content">
                  <div className="gx-settings-general-details">
                    <div className="gx-settings-general-regional-settings">
                      <p
                        className="gx-mb-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Regional settings
                      </p>
                      <div className="div-detail div-country">
                        <label>Country</label>
                        <Select
                          className="country"
                          placeholder="Select country"
                          defaultValue="canada"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onCountryChange}
                        >
                          <Option value="canada">Canada</Option>
                          <Option value="russia">Russia</Option>
                        </Select>
                      </div>
                      <div style={{ display: "flex" }}>
                        <div className="div-detail div-state">
                          <label>State</label>
                          <Select
                            className="state"
                            placeholder="Select state"
                            defaultValue="quebec"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            onChange={this.onStateChange}
                          >
                            <Option value="quebec">Quebec</Option>
                            <Option value="aa">AAA</Option>
                            <Option value="bb">BBB</Option>
                            <Option value="cc">CCC</Option>
                          </Select>
                        </div>
                        <div className="div-detail div-zipcode">
                          <label>Zip code</label>
                          <Input
                            className="zipcode"
                            placeholder="Enter code"
                            onChange={this.onZipCodeChange}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="gx-settings-general-language-settings">
                      <p
                        className="gx-mb-30 gx-mt-20 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Language settings
                      </p>
                      <div className="div-detail div-language">
                        <label>Default language</label>
                        <Select
                          className="language"
                          placeholder="Select language"
                          defaultValue="en-us"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onLanguageChange}
                        >
                          <Option value="en-us">English(United states)</Option>
                          <Option value="en-uk">English(United kingdom)</Option>
                        </Select>
                      </div>
                    </div>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(RegionLanguage);
