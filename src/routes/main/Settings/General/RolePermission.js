import React, { Component } from "react";
import { Button, Table, Card, Popover, Modal, Switch, Checkbox } from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const checkOptions = ["View", "Create", "Edit", "Delete"];
const TablePopover = props => {
  console.log(props);
  return (
    <div>
      <div className="gx-menuitem">
        <i className="material-icons gx-mr-10">settings_applications</i>
        <span>Edit role</span>
      </div>
      <div
        className="gx-menuitem"
        onClick={() => props.setVisibleEditPermissionModal(true)}
      >
        <i className="material-icons gx-mr-10">verified_user</i>
        <span>Edit permission</span>
      </div>
      <Modal
        className="modal-edit-permission"
        title={
          <div className="gx-flex-row gx-justify-content-between gx-align-items-center">
            <p className="gx-mb-0">Edit permissions</p>
            <i
              className="material-icons gx-icon-action"
              onClick={() => props.setVisibleEditPermissionModal(false)}
            >
              clear
            </i>
          </div>
        }
        centered
        closable={false}
        zIndex={9999}
        visible={props.isModalVisible}
        footer={[null, null]}
        onCancel={() => props.setVisibleEditPermissionModal(false)}
      >
        <div className="modal-content">
          <Card className="gx-card">
            <div className="modal-content-header">
              <div className="modal-content-header-text">
                <span>Functions</span>
                <span>Permissions</span>
              </div>
              <Button
                type="secondary"
                className={`btn-modal-save btn-desktop-modal-save ${
                  props.isFormChanged ? "btn-modal-save-active" : ""
                }`}
                disabled={!props.isFormChanged}
                onClick={() => props.setVisibleEditPermissionModal(false)}
              >
                Save
              </Button>
            </div>
            <div className="modal-content-body">
              <div className="div-permission div-permission-customer">
                <div className="div-switch">
                  <Switch
                    defaultChecked={props.isCustomerSwitch}
                    onChange={props.onCustomerSwitchChange}
                  />
                  <span
                    className={!props.isCustomerSwitch ? "disabled-text" : ""}
                  >
                    Customer
                  </span>
                </div>
                <div className="div-check-permissions">
                  <Checkbox.Group
                    disabled={!props.isCustomerSwitch}
                    options={checkOptions}
                    onChange={props.onCustomerPermissionChange}
                  />
                </div>
              </div>
              <div className="div-permission div-permission-mobile-app">
                <div className="div-switch">
                  <Switch
                    defaultChecked={props.isMobileAppSwitch}
                    onChange={props.onMobileAppSwitchChange}
                  />
                  <span
                    className={!props.isMobileSwitch ? "disabled-text" : ""}
                  >
                    Mobile app
                  </span>
                </div>
                <div className="div-check-permissions">
                  <Checkbox.Group
                    disabled={!props.isMobileAppSwitch}
                    options={checkOptions}
                    onChange={props.onMobileAppPermissionChange}
                  />
                </div>
              </div>
              <div className="div-permission div-permission-chat">
                <div className="div-switch">
                  <Switch
                    defaultChecked={props.isChatSwitch}
                    onChange={props.onChatSwitchChange}
                  />
                  <span className={!props.isChatSwitch ? "disabled-text" : ""}>
                    Chat
                  </span>
                </div>
                <div className="div-check-permissions">
                  <Checkbox.Group
                    disabled={!props.isChatSwitch}
                    options={checkOptions}
                    defaultValue={["View", "Create", "Edit"]}
                    onChange={props.onChatPermissionChange}
                  />
                </div>
              </div>
              <div className="div-permission div-permission-schedule">
                <div className="div-switch">
                  <Switch
                    defaultChecked={props.isScheduleSwitch}
                    onChange={props.onScheduleSwitchChange}
                  />
                  <span
                    className={!props.isScheduleSwitch ? "disabled-text" : ""}
                  >
                    Schedule
                  </span>
                </div>
                <div className="div-check-permissions">
                  <Checkbox.Group
                    disabled={!props.isScheduleSwitch}
                    options={checkOptions}
                    onChange={props.onSchedulePermissionChange}
                  />
                </div>
              </div>
              <div className="div-permission div-permission-estimate">
                <div className="div-switch">
                  <Switch
                    defaultChecked={props.isEstimateSwitch}
                    onChange={props.onEstimateSwitchChange}
                  />
                  <span
                    className={!props.isEstimateSwitch ? "disabled-text" : ""}
                  >
                    Estimate
                  </span>
                </div>
                <div className="div-check-permissions">
                  <Checkbox.Group
                    disabled={!props.isEstimateSwitch}
                    options={checkOptions}
                    defaultValue={["View", "Edit"]}
                    onChange={props.onEstimatePermissionChange}
                  />
                </div>
              </div>
              <div className="div-permission div-permission-job">
                <div className="div-switch">
                  <Switch
                    defaultChecked={props.isJobSwitch}
                    onChange={props.onJobSwitchChange}
                  />
                  <span className={!props.isJobSwitch ? "disabled-text" : ""}>
                    Job
                  </span>
                </div>
                <div className="div-check-permissions">
                  <Checkbox.Group
                    disabled={!props.isJobSwitch}
                    options={checkOptions}
                    onChange={props.onJobPermissionChange}
                  />
                </div>
              </div>
              <div className="div-permission div-permission-invoice">
                <div className="div-switch">
                  <Switch
                    defaultChecked={props.isInvoiceSwitch}
                    onChange={props.onInvoiceSwitchChange}
                  />
                  <span
                    className={!props.isInvoiceSwitch ? "disabled-text" : ""}
                  >
                    Invoice
                  </span>
                </div>
                <div className="div-check-permissions">
                  <Checkbox.Group
                    disabled={!props.isInvoiceSwitch}
                    options={checkOptions}
                    defaultValue={["View", "Edit"]}
                    onChange={props.onInvoicePermissionChange}
                  />
                </div>
              </div>
            </div>
          </Card>
          <Button
            type="secondary"
            className={`btn-modal-save btn-mobile-modal-save ${
              props.isFormChanged ? "btn-modal-save-active" : ""
            }`}
            disabled={!props.isFormChanged}
            onClick={() => props.setVisibleEditPermissionModal(false)}
          >
            Save changes
          </Button>
        </div>
      </Modal>
      <div className="gx-menuitem">
        <i className="material-icons gx-mr-10">person_add</i>
        <span>Assign user</span>
      </div>
      <div className="gx-menuitem">
        <i className="material-icons gx-mr-10">delete_sweep</i>
        <span>Delete role</span>
      </div>
    </div>
  );
};
const columns = (
  isModalVisible,
  setVisibleEditPermissionModal,
  isFormChanged,
  isCustomerSwitch,
  isMobileAppSwitch,
  isChatSwitch,
  isScheduleSwitch,
  isEstimateSwitch,
  isJobSwitch,
  isInvoiceSwitch,
  onCustomerSwitchChange,
  onMobileAppSwitchChange,
  onChatSwitchChange,
  onScheduleSwitchChange,
  onEstimateSwitchChange,
  onJobSwitchChange,
  onInvoiceSwitchChange,
  onCustomerPermissionChange,
  onMobileAppPermissionChange,
  onChatPermissionChange,
  onSchedulePermissionChange,
  onEstimatePermissionChange,
  onJobPermissionChange,
  onInvoicePermissionChange
) => {
  return [
    {
      title: "Role name",
      dataIndex: "role",
      key: "role"
    },
    {
      title: "Assigned users",
      dataIndex: "assigned_users",
      key: "assigned_users",
      render: (text, record) => <Link to="#">{record.assigned_users}</Link>
    },
    {
      title: "Permission level",
      dataIndex: "permission_level",
      key: "permission_level",
      render: (text, record) => (
        <span className="action-more">
          <span>{record.permission_level}</span>
          <Popover
            overlayClassName={
              "role-table-popover role-table-popover-" + record.key
            }
            placement="bottom"
            content={
              <TablePopover
                isModalVisible={isModalVisible}
                setVisibleEditPermissionModal={setVisibleEditPermissionModal}
                index={record.key}
                isFormChanged={isFormChanged}
                isCustomerSwitch={isCustomerSwitch}
                isMobileAppSwitch={isMobileAppSwitch}
                isChatSwitch={isChatSwitch}
                isScheduleSwitch={isScheduleSwitch}
                isEstimateSwitch={isEstimateSwitch}
                isJobSwitch={isJobSwitch}
                isInvoiceSwitch={isInvoiceSwitch}
                onCustomerSwitchChange={onCustomerSwitchChange}
                onMobileAppSwitchChange={onMobileAppSwitchChange}
                onChatSwitchChange={onChatSwitchChange}
                onScheduleSwitchChange={onScheduleSwitchChange}
                onEstimateSwitchChange={onEstimateSwitchChange}
                onJobSwitchChange={onJobSwitchChange}
                onInvoiceSwitchChange={onInvoiceSwitchChange}
                onCustomerPermissionChange={onCustomerPermissionChange}
                onMobileAppPermissionChange={onMobileAppPermissionChange}
                onChatPermissionChange={onChatPermissionChange}
                onSchedulePermissionChange={onSchedulePermissionChange}
                onEstimatePermissionChange={onEstimatePermissionChange}
                onJobPermissionChange={onJobPermissionChange}
                onInvoicePermissionChange={onInvoicePermissionChange}
              />
            }
            trigger="click"
          >
            <Button className="btn-table-action">
              <i className="material-icons">more_vert</i>
            </Button>
          </Popover>
        </span>
      )
    }
  ];
};

const user_roles = [
  {
    key: "1",
    role: "Administrator",
    assigned_users: "01 Active user",
    permission_level: "Global access"
  },
  {
    key: "2",
    role: "Field tech",
    assigned_users: "02 Active users",
    permission_level: "Limited access"
  },
  {
    key: "3",
    role: "Field tech",
    assigned_users: "03 Active users",
    permission_level: "Limited access"
  }
];
class RolePermission extends Component {
  state = {
    isFormChanged: false,
    editPermissionModalVisible: false,
    isCustomerSwitch: false,
    isMobileAppSwitch: false,
    isChatSwitch: true,
    isScheduleSwitch: false,
    isEstimateSwitch: true,
    isJobSwitch: false,
    isInvoiceSwitch: true
  };
  setVisibleEditPermissionModal = visible => {
    console.log("open modal");
    this.setState({ isFormChanged: false });
    this.setState({ editPermissionModalVisible: visible });
    this.setState({ popoverVisible: false });
  };
  setVisiblePopover = visible => {
    this.setState({
      popoverVisible: visible
    });
  };
  onCustomerSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({ isCustomerSwitch: !this.state.isCustomerSwitch });
  };
  onMobileAppSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({ isMobileAppSwitch: !this.state.isMobileAppSwitch });
  };
  onChatSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({ isChatSwitch: !this.state.isChatSwitch });
  };
  onScheduleSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({ isScheduleSwitch: !this.state.isScheduleSwitch });
  };
  onEstimateSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({ isEstimateSwitch: !this.state.isEstimateSwitch });
  };
  onJobSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({ isJobSwitch: !this.state.isJobSwitch });
  };
  onInvoiceSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({ isInvoiceSwitch: !this.state.isInvoiceSwitch });
  };
  onCustomerPermissionChange = () => {
    this.setState({ isFormChanged: true });
  };
  onMobileAppPermissionChange = () => {
    this.setState({ isFormChanged: true });
  };
  onChatPermissionChange = () => {
    this.setState({ isFormChanged: true });
  };
  onSchedulePermissionChange = () => {
    this.setState({ isFormChanged: true });
  };
  onEstimatePermissionChange = () => {
    this.setState({ isFormChanged: true });
  };
  onJobPermissionChange = () => {
    this.setState({ isFormChanged: true });
  };
  onInvoicePermissionChange = () => {
    this.setState({ isFormChanged: true });
  };
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    General settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="3"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="3"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Role and permission settings
                    </p>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-general-body-content">
                  <div className="gx-settings-role-permission-details">
                    <p
                      className="gx-mb-30 gx-font-weight-semi-bold"
                      style={{ fontSize: "15px" }}
                    >
                      Manage user roles
                    </p>
                    <Card>
                      <Table
                        className="table-reponsive "
                        columns={columns(
                          this.state.editPermissionModalVisible,
                          this.setVisibleEditPermissionModal,
                          this.state.isFormChanged,
                          this.state.isCustomerSwitch,
                          this.state.isMobileAppSwitch,
                          this.state.isChatSwitch,
                          this.state.isScheduleSwitch,
                          this.state.isEstimateSwitch,
                          this.state.isJobSwitch,
                          this.state.isInvoiceSwitch,
                          this.onCustomerSwitchChange,
                          this.onMobileAppSwitchChange,
                          this.onChatSwitchChange,
                          this.onScheduleSwitchChange,
                          this.onEstimateSwitchChange,
                          this.onJobSwitchChange,
                          this.onInvoiceSwitchChange,
                          this.onCustomerPermissionChange,
                          this.onMobileAppPermissionChange,
                          this.onChatPermissionChange,
                          this.onSchedulePermissionChange,
                          this.onEstimatePermissionChange,
                          this.onJobPermissionChange,
                          this.onInvoicePermissionChange
                        )}
                        dataSource={user_roles}
                        pagination={false}
                      ></Table>
                    </Card>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}
export default injectIntl(RolePermission);
