import React, { Component } from "react";
import { Button, Input, DatePicker, Select, Card, Upload, Switch } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";

import TopMenu from "../TopMenu";

const Option = Select.Option;
const Dragger = Upload.Dragger;

class Dispatch extends Component {
  state = {
    isFormChanged: false,
    isSMSScheduleSwitch: true,
    isNotifyAdminSwitch: false,
    isAllowSwitch: false,
    schedule_start_day: "monday",
    opening_hour: "9am",
    closing_hour: "5pm"
  };
  onScheduleStartDayChange = e => {
    this.setState({ isFormChanged: true });
  };
  onOpeningHourChange = e => {
    this.setState({ isFormChanged: true });
  };
  onClosingHourChange = e => {
    this.setState({ isFormChanged: true });
  };
  onSMSScheduleSwitchChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ isSMSScheduleSwitch: !this.state.isSMSScheduleSwitch });
  };
  onNotifyAdminSwitchChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ isNotifyAdminSwitch: !this.state.isNotifyAdminSwitch });
  };
  onAllowSwitchChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ isAllowSwitch: !this.state.isAllowSwitch });
  };
  onScheduledToChange = e => {
    this.setState({ isFormChanged: true });
  };
  onAllowToChange = e => {
    this.setState({ isFormChanged: true });
  };
  render() {
    return (
      <div className="gx-main-content">
        <TopMenu currentPage="2" />
        <div className="gx-app-module gx-dispatch-module">
          <div className="gx-w-100">
            <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
              <Widget styleName="gx-card-full">
                <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
                  <div className="gx-w-100">
                    <div className="gx-settings-body-header gx-desktop-settings-body-header">
                      <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                        <div className="gx-settings-body-header-content">
                          <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                            Dispatch settings
                          </p>
                          <Button
                            className={`gx-btn-settings-save ${
                              this.state.isFormChanged
                                ? "gx-btn-settings-save-active"
                                : ""
                            }`}
                            disabled={!this.state.isFormChanged}
                            type="secondary"
                          >
                            Save changes
                          </Button>
                        </div>
                      </div>
                    </div>
                    <div className="gx-settings-body-content gx-panel-content-scroll">
                      <div className="gx-settings-dispatch-body-content">
                        <div className="gx-settings-dispatch-details">
                          <div className="gx-settings-dispatch-schedule-time">
                            <p
                              className="gx-mb-30 gx-font-weight-semi-bold"
                              style={{ fontSize: "15px" }}
                            >
                              Schedule time
                            </p>
                            <div className="div-detail div-schedule-start-day">
                              <label>Schedule start day</label>
                              <Select
                                className="schedule_start_day"
                                placeholder="Select start day"
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                defaultValue="monday"
                                onChange={this.onScheduleStartDayChange}
                              >
                                <Option value="monday">Monday</Option>
                                <Option value="tuesday">Tuesday</Option>
                                <Option value="wednesday">Wednesday</Option>
                                <Option value="thursday">Thursday</Option>
                                <Option value="friday">Friday</Option>
                                <Option value="saturday">Saturday</Option>
                                <Option value="sunday">Sunday</Option>
                              </Select>
                            </div>
                            <div style={{ display: "flex", marginBottom: "" }}>
                              <div className="div-detail div-opening-hour">
                                <label>Opening hour</label>
                                <Select
                                  className="opening_hour"
                                  placeholder="Select hour"
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  defaultValue="9am"
                                  onChange={this.onOpeningHourChange}
                                >
                                  <Option value="1am">1:00 AM</Option>
                                  <Option value="2am">2:00 AM</Option>
                                  <Option value="3am">3:00 AM</Option>
                                  <Option value="4am">4:00 AM</Option>
                                  <Option value="5am">5:00 AM</Option>
                                  <Option value="6am">6:00 AM</Option>
                                  <Option value="7am">7:00 AM</Option>
                                  <Option value="8am">8:00 AM</Option>
                                  <Option value="9am">9:00 AM</Option>
                                  <Option value="10am">10:00 AM</Option>
                                </Select>
                              </div>
                              <div className="div-detail div-closing-hour">
                                <label>Closing hour</label>
                                <Select
                                  className="closing_hour"
                                  placeholder="Select hour"
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  defaultValue="5pm"
                                  onChange={this.onClosingHourChange}
                                >
                                  <Option value="1pm">1:00 PM</Option>
                                  <Option value="2pm">2:00 PM</Option>
                                  <Option value="3pm">3:00 PM</Option>
                                  <Option value="4pm">4:00 PM</Option>
                                  <Option value="5pm">5:00 PM</Option>
                                  <Option value="6pm">6:00 PM</Option>
                                  <Option value="7pm">7:00 PM</Option>
                                  <Option value="8pm">8:00 PM</Option>
                                  <Option value="9pm">9:00 PM</Option>
                                  <Option value="10pm">10:00 PM</Option>
                                </Select>
                              </div>
                            </div>
                          </div>
                          <div className="gx-settings-dispatch-schedule-alerts">
                            <p
                              className="gx-mb-30 gx-mt-20 gx-font-weight-semi-bold"
                              style={{ fontSize: "15px" }}
                            >
                              Schedule alerts
                            </p>
                            <div className="div-detail div-sms-schedule">
                              <Switch
                                defaultChecked={this.state.isSMSScheduleSwitch}
                                onChange={this.onSMSScheduleSwitchChange}
                              />
                              <div>
                                <span
                                  className={
                                    !this.state.isSMSScheduleSwitch
                                      ? "disabled-text"
                                      : ""
                                  }
                                >
                                  SMS schedule to
                                  <Select
                                    className="scheduled_to"
                                    placeholder="Select"
                                    suffixIcon={
                                      <i className="material-icons">
                                        expand_more
                                      </i>
                                    }
                                    defaultValue="assigned"
                                    disabled={!this.state.isSMSScheduleSwitch}
                                    onChange={this.onScheduledToChange}
                                  >
                                    <Option value="assigned">Assigned</Option>
                                    <Option value="assigned1">Assigned1</Option>
                                    <Option value="assigned2">Assigned2</Option>
                                    <Option value="assigned3">Assigned3</Option>
                                  </Select>
                                  <span>employees</span>
                                </span>
                              </div>
                            </div>
                            <div className="div-detail div-notify-admin">
                              <Switch
                                defaultChecked={this.state.isNotifyAdminSwitch}
                                onChange={this.onNotifyAdminSwitchChange}
                              />
                              <div>
                                <span
                                  className={
                                    !this.state.isNotifyAdminSwitch
                                      ? "disabled-text"
                                      : ""
                                  }
                                >
                                  Notify admin if not scheduled in
                                  <Select
                                    className="scheduled_hours"
                                    placeholder="Select"
                                    suffixIcon={
                                      <i className="material-icons">
                                        expand_more
                                      </i>
                                    }
                                    defaultValue="2hours"
                                    disabled={!this.state.isNotifyAdminSwitch}
                                    onChange={this.onScheduledToChange}
                                  >
                                    <Option value="1hour">An hour</Option>
                                    <Option value="2hours">2 hours</Option>
                                    <Option value="3hours">3 hours</Option>
                                    <Option value="4hours">4 hours</Option>
                                    <Option value="5hours">5 hours</Option>
                                    <Option value="6hours">6 hours</Option>
                                    <Option value="7hours">7 hours</Option>
                                    <Option value="8hours">8 hours</Option>
                                    <Option value="9hours">9 hours</Option>
                                    <Option value="10hours">10 hours</Option>
                                  </Select>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className="gx-settings-dispatch-accessibility">
                            <p
                              className="gx-mb-30 gx-mt-30 gx-font-weight-semi-bold"
                              style={{ fontSize: "15px" }}
                            >
                              Accessibility
                            </p>
                            <div className="div-detail div-accessibility">
                              <Switch
                                defaultChecked={this.state.isAllowSwitch}
                                onChange={this.onAllowSwitchChange}
                              />
                              <div>
                                <span
                                  className={
                                    !this.state.isAllowSwitch
                                      ? "disabled-text"
                                      : ""
                                  }
                                >
                                  Allow
                                  <Select
                                    className="allow_to"
                                    placeholder="Select"
                                    suffixIcon={
                                      <i className="material-icons">
                                        expand_more
                                      </i>
                                    }
                                    defaultValue="everybody"
                                    disabled={!this.state.isAllowSwitch}
                                    onChange={this.onAllowToChange}
                                  >
                                    <Option value="everybody">Everybody</Option>
                                    <Option value="somebody">Somebody</Option>
                                  </Select>
                                  <span>to view schedule</span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <Button
                          className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                            this.state.isFormChanged
                              ? "gx-btn-settings-save-active"
                              : ""
                          }`}
                          disabled={!this.state.isFormChanged}
                          type="secondary"
                        >
                          Save changes
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </Widget>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(Dispatch);
