import React, { Component } from "react";
import { Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

const TabPane = Tabs.TabPane;

const MENU_ITEMS = [
  {
    title: "sidebar.settings.communication.send_receive",
    icon: "send",
    link: "/settings/communication/send_receive"
  },
  {
    title: "sidebar.settings.communication.email_templates",
    icon: "email",
    link: "/settings/communication/email_templates"
  },
  {
    title: "sidebar.settings.communication.sms_templates",
    icon: "sms",
    link: "/settings/communication/sms_templates"
  }
];

class MobileSidebar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Tabs className="gx-dispatch-sidebar" defaultActiveKey={currentPage}>
        {MENU_ITEMS.map((menuItem, index) => (
          <TabPane
            tab={
              <Link to={menuItem.link}>
                <div className="gx-sidebar-item">
                  <i className="material-icons">{menuItem.icon}</i>
                  <IntlMessages id={menuItem.title} />
                </div>
              </Link>
            }
            key={index}
          ></TabPane>
        ))}
      </Tabs>
    );
  }
}

export default MobileSidebar;
