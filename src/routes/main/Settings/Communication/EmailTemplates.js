import React, { Component } from "react";
import {
  Icon,
  Button,
  Collapse,
  Input,
  Select,
  Modal,
  Switch,
  Card
} from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const { Panel } = Collapse;
const { TextArea } = Input;

const dumy_subject = "Your login details for the customer dashboard";
const dumy_message =
  "Hi {customer_first_name},\n\nGreat news!\nYou can now login and view information via our secure web portal\nPlease find below your login details to gain access";
class EmailTemplates extends Component {
  state = {
    isFormChanged: false,
    isCustomerSwitch: true,
    isEditMessageModalVisible: false,
    customer_email_subject: dumy_subject,
    customer_email_message: dumy_message,
    modalTemplateMessage: "",
    whichMessageTemplate: "",
    cursorAt: 0,
    isEstimateSwitch: true,
    estimate_email_subject: dumy_subject,
    estimate_email_message: dumy_message,
    isJobStatusSwitch: true,
    job_status_email_subject: dumy_subject,
    job_status_email_message: dumy_message,
    isInvoiceSwitch: true,
    invoice_email_subject: dumy_subject,
    invoice_email_message: dumy_message,
    isFinancialStatementSwitch: true,
    financial_statement_email_subject: dumy_subject,
    financial_statement_email_message: dumy_message
  };
  onCollapseChange = key => {
    console.log(key);
  };
  onCustomerSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({
      isCustomerSwitch: !this.state.isCustomerSwitch
    });
  };
  onCustomerSubjectChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ customer_email_subject: e.target.value });
  };
  onCustomerMessageChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ customer_email_message: e.target.value });
  };
  onCustomerResetMessage = () => {
    this.setState({ isFormChanged: true });
    this.setState({ customer_email_message: "" });
  };
  onEstimateSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({
      isEstimateSwitch: !this.state.isEstimateSwitch
    });
  };
  onEstimateSubjectChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ estimate_email_subject: e.target.value });
  };
  onEstimateMessageChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ estimate_email_message: e.target.value });
  };
  onEstimateResetMessage = () => {
    this.setState({ isFormChanged: true });
    this.setState({ estimate_email_message: "" });
  };
  onJobStatusSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({
      isJobStatusSwitch: !this.state.isJobStatusSwitch
    });
  };
  onJobStatusSubjectChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ job_status_email_subject: e.target.value });
  };
  onJobStatusMessageChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ job_status_email_message: e.target.value });
  };
  onJobStatusResetMessage = () => {
    this.setState({ isFormChanged: true });
    this.setState({ job_status_email_message: "" });
  };
  onInvoiceSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({
      isInvoiceSwitch: !this.state.isInvoiceSwitch
    });
  };
  onInvoiceSubjectChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ invoice_email_subject: e.target.value });
  };
  onInvoiceMessageChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ invoice_email_message: e.target.value });
  };
  onInvoiceResetMessage = () => {
    this.setState({ isFormChanged: true });
    this.setState({ invoice_email_message: "" });
  };
  onFinancialStatementSwitchChange = () => {
    this.setState({ isFormChanged: true });
    this.setState({
      isFinancialStatementSwitch: !this.state.isFinancialStatementSwitch
    });
  };
  onFinancialStatementSubjectChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ financial_statement_email_subject: e.target.value });
  };
  onFinancialStatementMessageChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ financial_statement_email_message: e.target.value });
  };
  onFinancialStatementResetMessage = () => {
    this.setState({ isFormChanged: true });
    this.setState({ financial_statement_email_message: "" });
  };
  openEditMessageModal = (type, msg) => {
    this.setState({ whichMessageTemplate: type });
    this.setState({ isEditMessageModalVisible: true });
    this.setState({ modalTemplateMessage: msg });
  };
  setVisibleEditMessageModal = visible => {
    if (!visible) {
      this.setState({ whichMessageTemplate: "" });
      this.setState({ cursorAt: 0 });
    }
    this.setState({ isEditMessageModalVisible: visible });
  };
  onModalMessageChange = e => {
    this.setState({ modalTemplateMessage: e.target.value });
  };
  updateMessageTemplate = () => {
    switch (this.state.whichMessageTemplate) {
      case "customer":
        this.setState({
          customer_email_message: this.state.modalTemplateMessage
        });
        break;
      case "estimate":
        this.setState({
          estimate_email_message: this.state.modalTemplateMessage
        });
        break;
      case "jobstatus":
        this.setState({
          job_status_email_message: this.state.modalTemplateMessage
        });
        break;
      case "invoice":
        this.setState({
          invoice_email_message: this.state.modalTemplateMessage
        });
        break;
      case "financial":
        this.setState({
          financial_statement_email_message: this.state.modalTemplateMessage
        });
        break;
    }
    this.setState({ isEditMessageModalVisible: false });
    this.setState({ whichMessageTemplate: "" });
  };
  insertTag = (e, type) => {
    console.log(e.target.value);
    let textBeforeCursor = this.state.modalTemplateMessage.substring(
      0,
      this.state.cursorAt
    );
    let textAfterCursor = this.state.modalTemplateMessage.substring(
      this.state.cursorAt,
      this.state.modalTemplateMessage.length
    );
    this.setState({
      modalTemplateMessage:
        textBeforeCursor + "{" + type + "}" + textAfterCursor
    });
    this.setState({ cursorAt: this.state.cursorAt + type.length + 2 });
  };
  render() {
    const {
      customer_email_message,
      estimate_email_message,
      job_status_email_message,
      invoice_email_message,
      financial_statement_email_message
    } = this.state;

    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    Communication settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="1"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="1"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Email templates settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-communication-body-content">
                  <div className="gx-settings-email-templates-details">
                    <Collapse
                      accordion
                      defaultActiveKey={["1"]}
                      onChange={this.onCollapseChange}
                      expandIconPosition="right"
                      expandIcon={({ isActive }) => (
                        <i className="material-icons">
                          {isActive ? "-" : "+"}{" "}
                        </i>
                      )}
                    >
                      <Panel header="Customer account" key="1">
                        <div className="collapse-item-content-body">
                          <div className="div-customer-account">
                            <div className="div-detail div-customer-account">
                              <Switch
                                defaultChecked={this.state.isCustomerSwitch}
                                onChange={this.onCustomerSwitchChange}
                              />
                              <span
                                className={
                                  !this.state.isCustomerSwitch
                                    ? "disabled-text"
                                    : ""
                                }
                              >
                                Customer account is created
                              </span>
                            </div>
                            <div className="div-detail div-subject">
                              <label>Subject</label>
                              <Input
                                className="subject"
                                onChange={this.onCustomerSubjectChange}
                                value={this.state.customer_email_subject}
                              ></Input>
                            </div>
                            <div className="div-detail div-message">
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center"
                                }}
                              >
                                <label>Message</label>
                                <a onClick={this.onCustomerResetMessage}>
                                  Reset
                                </a>
                              </div>
                              <TextArea
                                rows="5"
                                className="subject"
                                readOnly
                                onClick={() => {
                                  return this.openEditMessageModal(
                                    "customer",
                                    customer_email_message
                                  );
                                }}
                                value={customer_email_message}
                              ></TextArea>
                            </div>
                          </div>
                        </div>
                      </Panel>
                      <Panel header="Estimate to customer" key="2">
                        <div className="collapse-item-content-body">
                          <div className="div-customer-account">
                            <div className="div-detail div-customer-account">
                              <Switch
                                defaultChecked={this.state.isEstimateSwitch}
                                onChange={this.onEstimateSwitchChange}
                              />
                              <span
                                className={
                                  !this.state.isEstimateSwitch
                                    ? "disabled-text"
                                    : ""
                                }
                              >
                                Customer account is created
                              </span>
                            </div>
                            <div className="div-detail div-subject">
                              <label>Subject</label>
                              <Input
                                className="subject"
                                onChange={this.onEstimateSubjectChange}
                                value={this.state.estimate_email_subject}
                              ></Input>
                            </div>
                            <div className="div-detail div-message">
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center"
                                }}
                              >
                                <label>Message</label>
                                <a onClick={this.onEstimateResetMessage}>
                                  Reset
                                </a>
                              </div>
                              <TextArea
                                rows="5"
                                className="subject"
                                readOnly
                                onClick={() => {
                                  return this.openEditMessageModal(
                                    "estimate",
                                    estimate_email_message
                                  );
                                }}
                                value={this.state.estimate_email_message}
                              ></TextArea>
                            </div>
                          </div>
                        </div>
                      </Panel>
                      <Panel header="Job status to customer" key="3">
                        <div className="collapse-item-content-body">
                          <div className="div-customer-account">
                            <div className="div-detail div-customer-account">
                              <Switch
                                defaultChecked={this.state.isJobStatusSwitch}
                                onChange={this.onJobStatusSwitchChange}
                              />
                              <span
                                className={
                                  !this.state.isJobStatusSwitch
                                    ? "disabled-text"
                                    : ""
                                }
                              >
                                Customer account is created
                              </span>
                            </div>
                            <div className="div-detail div-subject">
                              <label>Subject</label>
                              <Input
                                className="subject"
                                onChange={this.onJobStatusSubjectChange}
                                value={this.state.job_status_email_subject}
                              ></Input>
                            </div>
                            <div className="div-detail div-message">
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center"
                                }}
                              >
                                <label>Message</label>
                                <a onClick={this.onJobStatusResetMessage}>
                                  Reset
                                </a>
                              </div>
                              <TextArea
                                rows="5"
                                className="subject"
                                readOnly
                                onClick={() => {
                                  return this.openEditMessageModal(
                                    "jobstatus",
                                    job_status_email_message
                                  );
                                }}
                                value={this.state.job_status_email_message}
                              ></TextArea>
                            </div>
                          </div>
                        </div>
                      </Panel>
                      <Panel header="Invoice to customer" key="4">
                        <div className="collapse-item-content-body">
                          <div className="div-customer-account">
                            <div className="div-detail div-customer-account">
                              <Switch
                                defaultChecked={this.state.isInvoiceSwitch}
                                onChange={this.onInvoiceSwitchChange}
                              />
                              <span
                                className={
                                  !this.state.isInvoiceSwitch
                                    ? "disabled-text"
                                    : ""
                                }
                              >
                                Customer account is created
                              </span>
                            </div>
                            <div className="div-detail div-subject">
                              <label>Subject</label>
                              <Input
                                className="subject"
                                onChange={this.onInvoiceSubjectChange}
                                value={this.state.invoice_email_subject}
                              ></Input>
                            </div>
                            <div className="div-detail div-message">
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center"
                                }}
                              >
                                <label>Message</label>
                                <a onClick={this.onInvoiceResetMessage}>
                                  Reset
                                </a>
                              </div>
                              <TextArea
                                rows="5"
                                className="subject"
                                readOnly
                                onClick={() => {
                                  return this.openEditMessageModal(
                                    "invoice",
                                    invoice_email_message
                                  );
                                }}
                                value={this.state.invoice_email_message}
                              ></TextArea>
                            </div>
                          </div>
                        </div>
                      </Panel>
                      <Panel header="Financial statement" key="5">
                        <div className="collapse-item-content-body">
                          <div className="div-customer-account">
                            <div className="div-detail div-customer-account">
                              <Switch
                                defaultChecked={
                                  this.state.isFinancialStatementSwitch
                                }
                                onChange={this.onFinancialStatementSwitchChange}
                              />
                              <span
                                className={
                                  !this.state.isFinancialStatementSwitch
                                    ? "disabled-text"
                                    : ""
                                }
                              >
                                Customer account is created
                              </span>
                            </div>
                            <div className="div-detail div-subject">
                              <label>Subject</label>
                              <Input
                                className="subject"
                                onChange={
                                  this.onFinancialStatementSubjectChange
                                }
                                value={
                                  this.state.financial_statement_email_subject
                                }
                              ></Input>
                            </div>
                            <div className="div-detail div-message">
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between",
                                  alignItems: "center"
                                }}
                              >
                                <label>Message</label>
                                <a
                                  onClick={
                                    this.onFinancialStatementResetMessage
                                  }
                                >
                                  Reset
                                </a>
                              </div>
                              <TextArea
                                rows="5"
                                className="subject"
                                readOnly
                                onClick={() => {
                                  return this.openEditMessageModal(
                                    "financial",
                                    financial_statement_email_message
                                  );
                                }}
                                value={
                                  this.state.financial_statement_email_message
                                }
                              ></TextArea>
                            </div>
                          </div>
                        </div>
                      </Panel>
                    </Collapse>
                    <Modal
                      className="modal-edit-email-message"
                      title={
                        <div>
                          <p>Customer account email template</p>
                          <i
                            className="material-icons"
                            onClick={() =>
                              this.setVisibleEditMessageModal(false)
                            }
                          >
                            clear
                          </i>
                        </div>
                      }
                      centered
                      closable={false}
                      zIndex={9999}
                      visible={this.state.isEditMessageModalVisible}
                      onOk={this.updateMessageTemplate}
                      okText="Save"
                      onCancel={() => this.setVisibleEditMessageModal(false)}
                    >
                      <Card className="gx-card card-email-message">
                        <div className="card-header">
                          <p>EDIT EMAIL TEXT</p>
                        </div>
                        <div className="card-body">
                          <TextArea
                            ref={el => {
                              this.input = el;
                            }}
                            rows="5"
                            value={this.state.modalTemplateMessage}
                            onChange={this.onModalMessageChange}
                            onClick={e => {
                              this.setState({
                                cursorAt: e.target.selectionStart
                              });
                            }}
                          ></TextArea>
                          <div className="div-tags">
                            <div className="div-tags-column">
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "customer_name")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Customer name</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "employee_name")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Employee name</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "company_name")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Company name</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "schedule_time")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Schedule time</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e => this.insertTag(e, "job_stage")}
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Job stage</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "invoice_number")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Invoice number</p>
                              </div>
                            </div>
                            <div className="div-tags-column">
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "last_payment")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Last payment</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e => this.insertTag(e, "active_job")}
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Active job</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "active_estimate")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Active estimate</p>
                              </div>
                              <div className="div-tag">
                                <Button
                                  className="btn-add-tag"
                                  onClick={e =>
                                    this.insertTag(e, "active_invoice")
                                  }
                                >
                                  <span>+</span>
                                  <span>Add</span>
                                </Button>
                                <p>Active invoice</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Card>
                    </Modal>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(EmailTemplates);
