import React, { Component } from "react";
import { Button, Input, Select, Card, Upload } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";

const Option = Select.Option;
const Dragger = Upload.Dragger;

class SendReceive extends Component {
  state = {
    isFormChanged: false,
    send_email_from: "",
    send_sms_from: "",
    receive_email_at: "email@company.com",
    receive_sms_at: ""
  };
  onSendEmailFromChange = e => {
    this.setState({ isFormChanged: true });
  };
  onSendSMSFromChange = e => {
    this.setState({ isFormChanged: true });
  };
  onReceiveEmailAtChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ receive_email_at: e.target.value });
  };
  onReceiveSMSAtChange = e => {
    this.setState({ isFormChanged: true });
    this.setState({ receive_sms_at: e.target.value });
  };
  render() {
    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    Communication settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="0"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="0"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      Region and language settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-communication-body-content">
                  <div className="gx-settings-communication-details">
                    <div className="gx-settings-communication-send">
                      <p
                        className="gx-mb-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Send email and SMS
                      </p>
                      <div className="div-detail div-send-email-from">
                        <label>Send email from</label>
                        <Select
                          className="send_email_from"
                          placeholder="Select email"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          defaultValue="company@jobpathcer.com"
                          onChange={this.onSendEmailFromChange}
                        >
                          <Option value="company@jobpatcher.com">
                            company@jobpatcher.com
                          </Option>
                          <Option value="support@jobpatcher.com">
                            support@jobpatcher.com
                          </Option>
                        </Select>
                      </div>
                      <div className="div-detail div-send-sms-from">
                        <label>Send SMS from</label>
                        <Select
                          className="send_sms_from"
                          placeholder="Select phone number"
                          defaultValue="24563447743"
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          onChange={this.onSendSMSFromChange}
                        >
                          <Option value="24563447743">+1(456)344-7743</Option>
                          <Option value="89841542820">+7(984)154-2820</Option>
                        </Select>
                      </div>
                    </div>
                    <div className="gx-settings-communication-receive">
                      <p
                        className="gx-mb-30 gx-mt-30 gx-font-weight-semi-bold"
                        style={{ fontSize: "15px" }}
                      >
                        Receive email and SMS
                      </p>
                      <div className="div-detail div-receive-email-at">
                        <label>Receive emails at</label>
                        <Input
                          className="receive_email_at"
                          placeholder="Enter email"
                          value={this.state.receive_email_at}
                          onChange={this.onReceiveEmailAtChange}
                        />
                      </div>
                      <div className="div-detail div-receive-sms-at">
                        <label>Receive SMS at</label>
                        <Input
                          className="receive_sms_at"
                          placeholder="Enter phone number"
                          value={this.state.receive_sms_at}
                          onChange={this.onReceiveSMSAtChange}
                        />
                      </div>
                    </div>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(SendReceive);
