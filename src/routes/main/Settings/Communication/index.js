import React from "react";
import { Route, Switch } from "react-router-dom";
import SendReceive from "./SendReceive";
import EmailTemplates from "./EmailTemplates";
import SMSTemplates from "./SMSTemplates";
import TopMenu from "../TopMenu";

const General = ({ match }) => (
  <div className="gx-main-content">
    <TopMenu currentPage="4" />
    <div className="gx-app-module gx-dispatch-module">
      <div className="gx-w-100">
        <Switch>
          <Route path={`${match.url}/send_receive`} component={SendReceive} />
          <Route
            path={`${match.url}/email_templates`}
            component={EmailTemplates}
          />
          <Route path={`${match.url}/sms_templates`} component={SMSTemplates} />
        </Switch>
      </div>
    </div>
  </div>
);

export default General;
