import React, { Component } from "react";
import { Icon, Button, Card, Input, Select, Switch, Modal } from "antd";
import { injectIntl } from "react-intl";

import Widget from "components/Widget";
import MobileSidebar from "./MobileSidebar";
import Sidebar from "./Sidebar";
import CaretPositioning from "./EditCaretPositioning";

const { TextArea } = Input;
const dummy_sms_text =
  "Hello Peter, John Alpha and Leana Melona from CleanHome are scheduled to arrive Tue, Aug 6 at 17:30.";

class SMSTemplates extends Component {
  state = {
    isFormChanged: false,
    job_scheduled_sms: dummy_sms_text,
    isEditMessageModalVisible: false,
    modalTemplateMessage: "",
    modalPreviewTemplateMessage: "",
    whichMessageTemplate: "",
    caretPosition: { start: 0, end: 0 },
    jobscheduled_sms_message: dummy_sms_text,
    ontheway_sms_message: dummy_sms_text,
    jobcanceled_sms_message: dummy_sms_text
  };
  openEditMessageModal = (type, msg) => {
    this.setState({ whichMessageTemplate: type });
    this.setState({ isEditMessageModalVisible: true });
    this.setState({ modalTemplateMessage: msg });
    this.setState({ modalPreviewTemplateMessage: msg });
  };
  setVisibleEditMessageModal = visible => {
    if (!visible) {
      this.setState({ whichMessageTemplate: "" });
      this.setState({ caretPosition: { start: 0, end: 0 } });
    }
    this.setState({ isEditMessageModalVisible: visible });
  };
  convertStringToTag(text) {
    text = text.replace(/{/g, "<span class='tag-highlighted'>{");
    text = text.replace(/}/g, "}</span>");
    return text;
  }
  convertToPreviewMessage(text) {
    text = text.replace(/{customer_name}/gi, "Peter");
    text = text.replace(/{employee_name}/gi, "John Alpha");
    text = text.replace(/{company_name}/gi, "CleanHome");
    text = text.replace(/{schedule_time}/gi, "Tue, Aug 6 at 17:30");
    text = text.replace(/{job_stage}/gi, "");
    text = text.replace(/{invoice_number}/gi, "");
    return text;
  }
  onModalMessageChange = e => {
    let savedCaretPosition = CaretPositioning.saveSelection(e.currentTarget);
    this.setState(
      {
        modalTemplateMessage: this.convertStringToTag(e.target.textContent),
        modalPreviewTemplateMessage: this.convertToPreviewMessage(
          e.target.textContent
        ),
        caretPosition: savedCaretPosition
      },
      () => {
        CaretPositioning.restoreSelection(
          document.getElementById("editable"),
          this.state.caretPosition
        );
      }
    );
  };
  updateMessageTemplate = () => {
    switch (this.state.whichMessageTemplate) {
      case "jobscheduled":
        this.setState({
          customer_email_message: this.state.modalTemplateMessage
        });
        break;
      case "ontheway":
        this.setState({
          estimate_email_message: this.state.modalTemplateMessage
        });
        break;
      case "jobstajobtus":
        this.setState({
          job_status_email_message: this.state.modalTemplateMessage
        });
        break;
    }
    this.setState({ isEditMessageModalVisible: false });
    this.setState({ whichMessageTemplate: "" });
  };
  insertTag = (e, type) => {
    console.log(this.state.caretPosition.start);
    let textBeforeCursor = this.state.modalTemplateMessage.substring(
      0,
      this.state.caretPosition.start
    );
    let textAfterCursor = this.state.modalTemplateMessage.substring(
      this.state.caretPosition.start,
      this.state.modalTemplateMessage.length
    );
    this.setState(
      {
        modalTemplateMessage:
          textBeforeCursor +
          "<span class='tag-highlighted'>{" +
          type +
          "}</span>" +
          textAfterCursor
      },
      () => {
        this.setState({
          modalPreviewTemplateMessage: this.convertToPreviewMessage(
            document.getElementById("editable").textContent
          )
        });
      }
    );
    this.setState({
      caretPosition: this.state.caretPosition.start + type.length + 2
    });
  };
  getCaretPosition = e => {
    if (window.getSelection && window.getSelection().getRangeAt) {
      var range = window.getSelection().getRangeAt(0);
      var selectedObj = window.getSelection();
      var rangeCount = 0;
      var childNodes = selectedObj.anchorNode.parentNode.childNodes;
      for (var i = 0; i < childNodes.length; i++) {
        if (childNodes[i] == selectedObj.anchorNode) {
          break;
        }
        if (childNodes[i].outerHTML) {
          rangeCount += childNodes[i].outerHTML.length;
        } else if (childNodes[i].nodeType == 3) {
          rangeCount += childNodes[i].textContent.length;
        }
      }
      return range.startOffset + rangeCount;
    }
    return -1;
  };
  render() {
    const {
      jobscheduled_sms_message,
      ontheway_sms_message,
      jobcanceled_sms_message
    } = this.state;

    return (
      <div className="gx-dispatch-module-content gx-mt-30 gx-ml-30 gx-mr-30">
        <Widget styleName="gx-card-full">
          <div className="gx-dashboard-overall-panel gx-h-100 gx-pt-0">
            <div className="gx-customer-sidenav page-settings-sidenav">
              <div
                className="gx-dashboard-overall-overview-welcome-panel gx-pr-0 gx-pl-0 gx-pb-0 gx-pt-0"
                style={{ width: "256px" }}
              >
                <div className="gx-text-header">
                  <div className="gx-pl-30 gx-font-weight-semi-bold gx-nav-title">
                    Communication settings
                  </div>
                  <div className="gx-mobile-settings-sidebar">
                    <MobileSidebar
                      className="gx-mobile-settings-sidebar"
                      currentPage="2"
                    ></MobileSidebar>
                  </div>
                  <div className="gx-desktop-settings-sidebar">
                    <Sidebar currentPage="2"></Sidebar>
                  </div>
                </div>
              </div>
            </div>
            <div className="gx-w-100">
              <div className="gx-settings-body-header gx-desktop-settings-body-header">
                <div className="gx-customer-tab-header gx-pl-0 gx-pr-0">
                  <div className="gx-settings-body-header-content gx-settings-sms-body-header-content">
                    <p className="gx-fs-18 gx-font-weight-semi-bold gx-settings-body-header-content-title">
                      SMS templates settings
                    </p>
                    <Button
                      className={`gx-btn-settings-save ${
                        this.state.isFormChanged
                          ? "gx-btn-settings-save-active"
                          : ""
                      }`}
                      disabled={!this.state.isFormChanged}
                      type="secondary"
                    >
                      Save changes
                    </Button>
                  </div>
                </div>
              </div>
              <div className="gx-settings-body-content gx-panel-content-scroll">
                <div className="gx-settings-communication-body-content gx-settings-communication-sms-body-content">
                  <div className="gx-settings-sms-templates-details">
                    <div className="div-detail">
                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">date_range</i>
                            <span className="card-header-title">
                              Job scheduled SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-job-scheduled"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "jobscheduled",
                                jobscheduled_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>

                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">local_shipping</i>
                            <span className="card-header-title">
                              On the way SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-on-the-way"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "ontheway",
                                ontheway_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>

                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">cancel</i>
                            <span className="card-header-title">
                              Job canceled SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-job-canceled"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "jobcanceled",
                                jobcanceled_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>
                    </div>
                    <div className="div-detail">
                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">date_range</i>
                            <span className="card-header-title">
                              Job scheduled SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-job-scheduled"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "jobscheduled",
                                jobscheduled_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>

                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">local_shipping</i>
                            <span className="card-header-title">
                              On the way SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-on-the-way"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "ontheway",
                                ontheway_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>

                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">cancel</i>
                            <span className="card-header-title">
                              Job canceled SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-job-canceled"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "jobcanceled",
                                jobcanceled_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>
                    </div>
                    <div className="div-detail">
                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">date_range</i>
                            <span className="card-header-title">
                              Job scheduled SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-job-scheduled"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "jobscheduled",
                                jobscheduled_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>

                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">local_shipping</i>
                            <span className="card-header-title">
                              On the way SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-on-the-way"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "ontheway",
                                ontheway_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>

                      <Card className="gx-card sms-card">
                        <div className="card-header">
                          <div className="card-header-title-wrapper">
                            <i className="material-icons">cancel</i>
                            <span className="card-header-title">
                              Job canceled SMS
                            </span>
                          </div>
                          <Button
                            className="btn-sms-edit btn-sms-edit-job-canceled"
                            onClick={() => {
                              return this.openEditMessageModal(
                                "jobcanceled",
                                jobcanceled_sms_message
                              );
                            }}
                          >
                            <i className="material-icons">edit</i>
                          </Button>
                        </div>
                        <div className="card-body">
                          <div className="div-sms div-sms-top">
                            <p className="sms-text">
                              {this.state.job_scheduled_sms}
                            </p>
                            <br />
                            <p className="sms-text">Check job details...</p>
                          </div>
                          <div className="div-sms div-sms-bottom">
                            <div>
                              <img
                                src={require("assets/images/sms-user1.png")}
                              />
                              <img
                                src={require("assets/images/sms-user2.png")}
                              />
                            </div>
                            <p>Tab to see job details</p>
                          </div>
                        </div>
                      </Card>
                      <Modal
                        className="modal-edit-sms-message"
                        title={
                          <div>
                            <p>Edit SMS TEXT</p>
                            <i
                              className="material-icons"
                              onClick={() =>
                                this.setVisibleEditMessageModal(false)
                              }
                            >
                              clear
                            </i>
                          </div>
                        }
                        centered
                        closable={false}
                        zIndex={9999}
                        visible={this.state.isEditMessageModalVisible}
                        onOk={this.updateMessageTemplate}
                        okText="Save"
                        onCancel={() => this.setVisibleEditMessageModal(false)}
                      >
                        <Card className="gx-card card-sms-message">
                          <div className="card-header">
                            <p>EDIT SMS TEXT</p>
                          </div>
                          <div className="card-body">
                            {/* <TextArea
                              ref={el => {
                                this.input = el;
                              }}
                              rows="5"
                              value={this.state.modalTemplateMessage}
                              onChange={this.onModalMessageChange}
                              onClick={e => {
                                this.setState({
                                  caretPosition: e.target.selectionStart
                                });
                              }}
                            ></TextArea> */}
                            <div
                              className="modal_sms_message_textarea"
                              contentEditable
                              id="editable"
                              onInput={this.onModalMessageChange}
                              onClick={e => {
                                let caret = this.getCaretPosition(e);
                                console.log(caret);
                                this.setState({
                                  caretPosition: { start: caret, end: caret }
                                });
                              }}
                              dangerouslySetInnerHTML={{
                                __html: this.state.modalTemplateMessage
                              }}
                            ></div>
                            <div className="div-tags">
                              <div className="div-tags-column">
                                <div className="div-tag">
                                  <Button
                                    className="btn-add-tag"
                                    onClick={e =>
                                      this.insertTag(e, "customer_name")
                                    }
                                  >
                                    <span>+</span>
                                    <span>Add</span>
                                  </Button>
                                  <p>Customer name</p>
                                </div>
                                <div className="div-tag">
                                  <Button
                                    className="btn-add-tag"
                                    onClick={e =>
                                      this.insertTag(e, "employee_name")
                                    }
                                  >
                                    <span>+</span>
                                    <span>Add</span>
                                  </Button>
                                  <p>Employee name</p>
                                </div>
                                <div className="div-tag">
                                  <Button
                                    className="btn-add-tag"
                                    onClick={e =>
                                      this.insertTag(e, "company_name")
                                    }
                                  >
                                    <span>+</span>
                                    <span>Add</span>
                                  </Button>
                                  <p>Company name</p>
                                </div>
                                <div className="div-tag">
                                  <Button
                                    className="btn-add-tag"
                                    onClick={e =>
                                      this.insertTag(e, "schedule_time")
                                    }
                                  >
                                    <span>+</span>
                                    <span>Add</span>
                                  </Button>
                                  <p>Schedule time</p>
                                </div>
                                <div className="div-tag">
                                  <Button
                                    className="btn-add-tag"
                                    onClick={e =>
                                      this.insertTag(e, "job_stage")
                                    }
                                  >
                                    <span>+</span>
                                    <span>Add</span>
                                  </Button>
                                  <p>Job stage</p>
                                </div>
                                <div className="div-tag">
                                  <Button
                                    className="btn-add-tag"
                                    onClick={e =>
                                      this.insertTag(e, "invoice_number")
                                    }
                                  >
                                    <span>+</span>
                                    <span>Add</span>
                                  </Button>
                                  <p>Invoice number</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Card>
                        <Card className="gx-card card-sms-preview">
                          <div className="card-header">
                            <p>LIVE SMS PREVIEW</p>
                          </div>
                          <div className="card-body">
                            <div className="div-preview-sms div-preview-sms-top">
                              <p className="sms-text">
                                {this.state.modalPreviewTemplateMessage}
                              </p>
                              <br />
                              <p className="sms-text">Check job details...</p>
                            </div>
                            <div className="div-preview-sms div-preview-sms-bottom">
                              <div>
                                <img
                                  src={require("assets/images/sms-user1.png")}
                                />
                                <img
                                  src={require("assets/images/sms-user2.png")}
                                />
                              </div>
                              <p>Tab to see job details</p>
                            </div>
                          </div>
                        </Card>
                      </Modal>
                    </div>
                  </div>
                  <Button
                    className={`gx-btn-settings-save gx-mobile-btn-settings-save ${
                      this.state.isFormChanged
                        ? "gx-btn-settings-save-active"
                        : ""
                    }`}
                    disabled={!this.state.isFormChanged}
                    type="secondary"
                  >
                    Save changes
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}

export default injectIntl(SMSTemplates);
