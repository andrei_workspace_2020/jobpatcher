import React, { Component } from "react";
import { Menu, Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

class Sidebar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Menu defaultSelectedKeys={[currentPage]} mode="inline" theme="light">
        <Menu.Item key="0">
          <Link to="/settings/communication/send_receive">
            <div className="gx-sidebar-item">
              <i className="material-icons">send</i>
              <IntlMessages id="sidebar.settings.communication.send_receive" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to="/settings/communication/email_templates">
            <div className="gx-sidebar-item">
              <i className="material-icons">email</i>
              <IntlMessages id="sidebar.settings.communication.email_templates" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/settings/communication/sms_templates">
            <div className="gx-sidebar-item">
              <i className="material-icons">sms</i>
              <IntlMessages id="sidebar.settings.communication.sms_templates" />
            </div>
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}

export default Sidebar;
