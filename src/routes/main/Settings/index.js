import React from "react";
import { Route, Switch } from "react-router-dom";
import General from "./General";
import Account from "./Account";
import Dispatch from "./Dispatch";
import Finance from "./Finance";
import Communication from "./Communication";
import "assets/settings-custom.css";

const Settings = ({ match }) => (
  <div className="gx-main-content-wrapper gx-settings-content-wrapper">
    <Switch>
      <Route path="/settings/general" component={General} />
      <Route path="/settings/account" component={Account} />
      <Route path="/settings/dispatch" component={Dispatch} />
      <Route path="/settings/finance" component={Finance} />
      <Route path="/settings/communication" component={Communication} />
    </Switch>
  </div>
);

export default Settings;
