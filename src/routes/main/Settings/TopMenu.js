import React, { Component } from "react";
import { Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

const TabPane = Tabs.TabPane;

const MENU_ITEMS = [
  {
    title: "topmenu.menu.general",
    link: "/settings/general/profile"
  },
  {
    title: "topmenu.menu.account",
    link: "/settings/account/profile"
  },
  {
    title: "topmenu.menu.dispatch",
    link: "/settings/dispatch"
  },
  {
    title: "topmenu.menu.finance",
    link: "/settings/finance"
  },
  {
    title: "topmenu.menu.communication",
    link: "/settings/communication/send_receive"
  }
];

class TopMenu extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Tabs className="gx-dispatch-topmenu" defaultActiveKey={currentPage}>
        {MENU_ITEMS.map((menuItem, index) => (
          <TabPane
            tab={
              <Link to={menuItem.link}>
                <IntlMessages id={menuItem.title} />
              </Link>
            }
            key={index}
          ></TabPane>
        ))}
      </Tabs>
    );
  }
}

export default TopMenu;
