/*global google*/
import React, { Component } from "react";
import { Button, Row, Col, Input, InputNumber, Form, Tabs } from "antd";
import Widget from "components/Widget";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import ButtonGroup from "antd/lib/button/button-group";

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

const { TextArea } = Input;
const { TabPane } = Tabs;

class AddPaymentDlg extends Component {
  state = {
    activePanelKey: -1,
    payment: "",
    amount: 0,
    email_receipt: "",
    payment_note: "",
    notify_customer: false
  };
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {}
  componentWillUnmount() {}
  componentDidUpdate(prevProps) {}
  componentWillReceiveProps(nextProps) {}

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
      }
    });
  };
  onPaymentChange = key => {
    this.setState({ activePanelKey: key });
    this.setState({
      amount: 0,
      email_receipt: "",
      payment_note: "",
      notify_customer: false
    });
    this.props.form.setFieldsValue({ amount: 0 });
    this.props.form.setFieldsValue({ email_receipt: "" });
    this.props.form.setFieldsValue({ payment_note: "" });
    this.props.form.setFieldsValue({ notify_customer: false });
  };
  onNotifyCustomerChange = flag => {
    this.setState({ notify_customer: flag });
  };
  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="gx-customer-add-payment gx-edit-customer-dlg">
        <Form className="gx-form-row0" onSubmit={this.onSubmit}>
          <div className="gx-customized-modal-content gx-p-0">
            <div className="gx-customized-modal-content-block gx-m-30">
              <Row>
                <Col
                  xs={24}
                  sm={24}
                  md={24}
                  lg={24}
                  xl={24}
                  xxl={24}
                  className="gutter-row gx-p-0"
                >
                  <Widget styleName="gx-customer-details gx-card-full gx-h-100 gx-mb-0">
                    <Tabs
                      className="gx-card-tabs"
                      defaultActiveKey="1"
                      onChange={this.onPaymentChange}
                    >
                      <TabPane tab="CASH" key="1">
                        <div className="gx-p-20">
                          <Row gutter={10}>
                            <Col lg={8} sm={8} xs={24} className=" gutter-row">
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.amount" />
                                </div>
                                {getFieldDecorator("amount", {
                                  initialValue: this.state.amount,
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please write amount!"
                                    }
                                  ]
                                })(
                                  <InputNumber
                                    min={0}
                                    precision={2}
                                    formatter={value =>
                                      (value = `$ ${value}`.replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        ","
                                      ))
                                    }
                                    parser={value =>
                                      value.replace(/\$\s?|(,*)[A-Za-z]/g, "")
                                    }
                                    onChange={this.onHourlyRateChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                            <Col
                              lg={16}
                              sm={16}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.email_receipt" />
                                </div>
                                {getFieldDecorator("email_receipt", {
                                  validateTrigger: "onSubmit",
                                  initialValue: this.state.email_receipt,
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please write email receipt!"
                                    },
                                    {
                                      type: "email",
                                      message:
                                        "Please write valid email address!"
                                    }
                                  ]
                                })(
                                  <Input
                                    placeholder={formatMessage({
                                      id:
                                        "customer.addpaymentdlg.content.placeholder.enter_email"
                                    })}
                                    onChange={this.onEmailReceiptChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={10}>
                            <Col
                              lg={24}
                              sm={24}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.payment_note" />
                                </div>
                                {getFieldDecorator("payment_note", {
                                  initialValue: this.state.payment_note
                                })(
                                  <TextArea
                                    rows={4}
                                    placeholder={formatMessage({
                                      id:
                                        "customer.addpaymentdlg.content.placeholder.type_note"
                                    })}
                                    onChange={this.onPaymentNoteChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={10}>
                            <Col
                              lg={24}
                              sm={24}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                                  <ButtonGroup className="gx-custom-toggle-buttons">
                                    <Button
                                      className={`gx-btn-toggle ${
                                        this.state.notify_customer
                                          ? "gx-btn-toggle-yes"
                                          : ""
                                      }`}
                                      size="small"
                                      onClick={() =>
                                        this.onNotifyCustomerChange(true)
                                      }
                                    >
                                      {this.state.notify_customer ? (
                                        <span>Yes</span>
                                      ) : (
                                        <ToggleButton></ToggleButton>
                                      )}
                                    </Button>
                                    <Button
                                      className={`gx-btn-toggle ${
                                        !this.state.notify_customer
                                          ? "gx-btn-toggle-no"
                                          : ""
                                      }`}
                                      size="small"
                                      onClick={() =>
                                        this.onNotifyCustomerChange(false)
                                      }
                                    >
                                      {!this.state.notify_customer ? (
                                        <span>No</span>
                                      ) : (
                                        <ToggleButton></ToggleButton>
                                      )}
                                    </Button>
                                  </ButtonGroup>
                                  <span
                                    className={`gx-mr-30 gx-fs-sm ${
                                      this.state.notify_customer
                                        ? "gx-font-weight-medium"
                                        : ""
                                    }`}
                                    style={{
                                      color: this.state.notify_customer
                                        ? "#4c586d"
                                        : "#9399a2"
                                    }}
                                  >
                                    Notify customer
                                  </span>
                                </div>
                              </Form.Item>
                            </Col>
                          </Row>
                        </div>
                      </TabPane>
                      <TabPane tab="E-TRANSFER" key="2">
                        <div className="gx-p-20">
                          <Row gutter={10}>
                            <Col lg={8} sm={8} xs={24} className=" gutter-row">
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.amount" />
                                </div>
                                {getFieldDecorator("amount", {
                                  initialValue: this.state.amount
                                })(
                                  <InputNumber
                                    min={0}
                                    precision={2}
                                    formatter={value =>
                                      (value = `$ ${value}`.replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        ","
                                      ))
                                    }
                                    parser={value =>
                                      value.replace(/\$\s?|(,*)[A-Za-z]/g, "")
                                    }
                                    onChange={this.onHourlyRateChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                            <Col
                              lg={16}
                              sm={16}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.email_receipt" />
                                </div>
                                {getFieldDecorator("email", {
                                  validateTrigger: "onSubmit",
                                  initialValue: this.state.email_receipt,
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please write email receipt!"
                                    },
                                    {
                                      type: "email",
                                      message:
                                        "Please write valid email address!"
                                    }
                                  ]
                                })(
                                  <Input
                                    placeholder={formatMessage({
                                      id:
                                        "customer.addpaymentdlg.content.placeholder.enter_email"
                                    })}
                                    onChange={this.onEmailReceiptChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={10}>
                            <Col
                              lg={24}
                              sm={24}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.payment_note" />
                                </div>
                                {getFieldDecorator("payment_note", {
                                  initialValue: this.state.payment_note
                                })(
                                  <TextArea
                                    rows={4}
                                    placeholder={formatMessage({
                                      id:
                                        "customer.addpaymentdlg.content.placeholder.type_note"
                                    })}
                                    onChange={this.onPaymentNoteChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={10}>
                            <Col
                              lg={24}
                              sm={24}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                                  <ButtonGroup className="gx-custom-toggle-buttons">
                                    <Button
                                      className={`gx-btn-toggle ${
                                        this.state.notify_customer
                                          ? "gx-btn-toggle-yes"
                                          : ""
                                      }`}
                                      size="small"
                                      onClick={() =>
                                        this.onNotifyCustomerChange(true)
                                      }
                                    >
                                      {this.state.notify_customer ? (
                                        <span>Yes</span>
                                      ) : (
                                        <ToggleButton></ToggleButton>
                                      )}
                                    </Button>
                                    <Button
                                      className={`gx-btn-toggle ${
                                        !this.state.notify_customer
                                          ? "gx-btn-toggle-no"
                                          : ""
                                      }`}
                                      size="small"
                                      onClick={() =>
                                        this.onNotifyCustomerChange(false)
                                      }
                                    >
                                      {!this.state.notify_customer ? (
                                        <span>No</span>
                                      ) : (
                                        <ToggleButton></ToggleButton>
                                      )}
                                    </Button>
                                  </ButtonGroup>
                                  <span
                                    className={`gx-mr-30 gx-fs-sm ${
                                      this.state.notify_customer
                                        ? "gx-font-weight-medium"
                                        : ""
                                    }`}
                                    style={{
                                      color: this.state.notify_customer
                                        ? "#4c586d"
                                        : "#9399a2"
                                    }}
                                  >
                                    Notify customer
                                  </span>
                                </div>
                              </Form.Item>
                            </Col>
                          </Row>
                        </div>
                      </TabPane>
                      <TabPane tab="CHECK" key="3">
                        <div className="gx-p-20">
                          <Row gutter={10}>
                            <Col lg={8} sm={8} xs={24} className=" gutter-row">
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.amount" />
                                </div>
                                {getFieldDecorator("amount", {
                                  initialValue: this.state.amount
                                })(
                                  <InputNumber
                                    min={0}
                                    precision={2}
                                    formatter={value =>
                                      (value = `$ ${value}`.replace(
                                        /\B(?=(\d{3})+(?!\d))/g,
                                        ","
                                      ))
                                    }
                                    parser={value =>
                                      value.replace(/\$\s?|(,*)[A-Za-z]/g, "")
                                    }
                                    onChange={this.onHourlyRateChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                            <Col
                              lg={16}
                              sm={16}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.email_receipt" />
                                </div>
                                {getFieldDecorator("email", {
                                  validateTrigger: "onSubmit",
                                  initialValue: this.state.email_receipt,
                                  rules: [
                                    {
                                      required: true,
                                      message: "Please write email receipt!"
                                    },
                                    {
                                      type: "email",
                                      message:
                                        "Please write valid email address!"
                                    }
                                  ]
                                })(
                                  <Input
                                    placeholder={formatMessage({
                                      id:
                                        "customer.addpaymentdlg.content.placeholder.enter_email"
                                    })}
                                    onChange={this.onEmailReceiptChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={10}>
                            <Col
                              lg={24}
                              sm={24}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-customized-modal-content-field-title">
                                  <IntlMessages id="customer.addpaymentdlg.content.label.payment_note" />
                                </div>
                                {getFieldDecorator("payment_note", {
                                  initialValue: this.state.payment_note
                                })(
                                  <TextArea
                                    rows={4}
                                    placeholder={formatMessage({
                                      id:
                                        "customer.addpaymentdlg.content.placeholder.type_note"
                                    })}
                                    onChange={this.onPaymentNoteChange}
                                  />
                                )}
                              </Form.Item>
                            </Col>
                          </Row>
                          <Row gutter={10}>
                            <Col
                              lg={24}
                              sm={24}
                              xs={24}
                              className=" gutter-row"
                            >
                              <Form.Item>
                                <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                                  <ButtonGroup className="gx-custom-toggle-buttons">
                                    <Button
                                      className={`gx-btn-toggle ${
                                        this.state.notify_customer
                                          ? "gx-btn-toggle-yes"
                                          : ""
                                      }`}
                                      size="small"
                                      onClick={() =>
                                        this.onNotifyCustomerChange(true)
                                      }
                                    >
                                      {this.state.notify_customer ? (
                                        <span>Yes</span>
                                      ) : (
                                        <ToggleButton></ToggleButton>
                                      )}
                                    </Button>
                                    <Button
                                      className={`gx-btn-toggle ${
                                        !this.state.notify_customer
                                          ? "gx-btn-toggle-no"
                                          : ""
                                      }`}
                                      size="small"
                                      onClick={() =>
                                        this.onNotifyCustomerChange(false)
                                      }
                                    >
                                      {!this.state.notify_customer ? (
                                        <span>No</span>
                                      ) : (
                                        <ToggleButton></ToggleButton>
                                      )}
                                    </Button>
                                  </ButtonGroup>
                                  <span
                                    className={`gx-mr-30 gx-fs-sm ${
                                      this.state.notify_customer
                                        ? "gx-font-weight-medium"
                                        : ""
                                    }`}
                                    style={{
                                      color: this.state.notify_customer
                                        ? "#4c586d"
                                        : "#9399a2"
                                    }}
                                  >
                                    Notify customer
                                  </span>
                                </div>
                              </Form.Item>
                            </Col>
                          </Row>
                        </div>
                      </TabPane>
                      <TabPane tab="OTHER" key="4">
                        <div className="gx-p-20">
                          <Row gutter={10}>
                            <Col lg={24} sm={24} xs={24} className="gutter-row">
                              <Tabs
                                className="gx-tabs-other-payments"
                                defaultActiveKey="4"
                                onChange={this.onPaymentChange}
                              >
                                <TabPane tab="Home financing" key="4">
                                  <Row gutter={10}>
                                    <Col
                                      lg={8}
                                      sm={8}
                                      xs={24}
                                      className="gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.amount" />
                                        </div>
                                        {getFieldDecorator("amount", {
                                          initialValue: this.state.amount
                                        })(
                                          <InputNumber
                                            min={0}
                                            precision={2}
                                            formatter={value =>
                                              (value = `$ ${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              ))
                                            }
                                            parser={value =>
                                              value.replace(
                                                /\$\s?|(,*)[A-Za-z]/g,
                                                ""
                                              )
                                            }
                                            onChange={this.onHourlyRateChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col
                                      lg={16}
                                      sm={16}
                                      xs={24}
                                      className="gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.email_receipt" />
                                        </div>
                                        {getFieldDecorator("email", {
                                          validateTrigger: "onSubmit",
                                          initialValue: this.state
                                            .email_receipt,
                                          rules: [
                                            {
                                              required: true,
                                              message:
                                                "Please write email receipt!"
                                            },
                                            {
                                              type: "email",
                                              message:
                                                "Please write valid email address!"
                                            }
                                          ]
                                        })(
                                          <Input
                                            placeholder={formatMessage({
                                              id:
                                                "customer.addpaymentdlg.content.placeholder.enter_email"
                                            })}
                                            onChange={this.onEmailReceiptChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                  <Row gutter={10}>
                                    <Col
                                      lg={24}
                                      sm={24}
                                      xs={24}
                                      className=" gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.payment_note" />
                                        </div>
                                        {getFieldDecorator("payment_note", {
                                          initialValue: this.state.payment_note
                                        })(
                                          <TextArea
                                            rows={4}
                                            placeholder={formatMessage({
                                              id:
                                                "customer.addpaymentdlg.content.placeholder.type_note"
                                            })}
                                            onChange={this.onPaymentNoteChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                  <Row gutter={10}>
                                    <Col
                                      lg={24}
                                      sm={24}
                                      xs={24}
                                      className=" gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                                          <ButtonGroup className="gx-custom-toggle-buttons">
                                            <Button
                                              className={`gx-btn-toggle ${
                                                this.state.notify_customer
                                                  ? "gx-btn-toggle-yes"
                                                  : ""
                                              }`}
                                              size="small"
                                              onClick={() =>
                                                this.onNotifyCustomerChange(
                                                  true
                                                )
                                              }
                                            >
                                              {this.state.notify_customer ? (
                                                <span>Yes</span>
                                              ) : (
                                                <ToggleButton></ToggleButton>
                                              )}
                                            </Button>
                                            <Button
                                              className={`gx-btn-toggle ${
                                                !this.state.notify_customer
                                                  ? "gx-btn-toggle-no"
                                                  : ""
                                              }`}
                                              size="small"
                                              onClick={() =>
                                                this.onNotifyCustomerChange(
                                                  false
                                                )
                                              }
                                            >
                                              {!this.state.notify_customer ? (
                                                <span>No</span>
                                              ) : (
                                                <ToggleButton></ToggleButton>
                                              )}
                                            </Button>
                                          </ButtonGroup>
                                          <span
                                            className={`gx-mr-30 gx-fs-sm ${
                                              this.state.notify_customer
                                                ? "gx-font-weight-medium"
                                                : ""
                                            }`}
                                            style={{
                                              color: this.state.notify_customer
                                                ? "#4c586d"
                                                : "#9399a2"
                                            }}
                                          >
                                            Notify customer
                                          </span>
                                        </div>
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                </TabPane>
                                <TabPane tab="Credit card" key="5">
                                  <Row gutter={10}>
                                    <Col
                                      lg={8}
                                      sm={8}
                                      xs={24}
                                      className="gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.amount" />
                                        </div>
                                        {getFieldDecorator("amount", {
                                          initialValue: this.state.amount
                                        })(
                                          <InputNumber
                                            min={0}
                                            precision={2}
                                            formatter={value =>
                                              (value = `$ ${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              ))
                                            }
                                            parser={value =>
                                              value.replace(
                                                /\$\s?|(,*)[A-Za-z]/g,
                                                ""
                                              )
                                            }
                                            onChange={this.onHourlyRateChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col
                                      lg={16}
                                      sm={16}
                                      xs={24}
                                      className="gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.email_receipt" />
                                        </div>
                                        {getFieldDecorator("email", {
                                          validateTrigger: "onSubmit",
                                          initialValue: this.state
                                            .email_receipt,
                                          rules: [
                                            {
                                              required: true,
                                              message:
                                                "Please write email receipt!"
                                            },
                                            {
                                              type: "email",
                                              message:
                                                "Please write valid email address!"
                                            }
                                          ]
                                        })(
                                          <Input
                                            placeholder={formatMessage({
                                              id:
                                                "customer.addpaymentdlg.content.placeholder.enter_email"
                                            })}
                                            onChange={this.onEmailReceiptChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                  <Row gutter={10}>
                                    <Col
                                      lg={24}
                                      sm={24}
                                      xs={24}
                                      className=" gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.payment_note" />
                                        </div>
                                        {getFieldDecorator("payment_note", {
                                          initialValue: this.state.payment_note
                                        })(
                                          <TextArea
                                            rows={4}
                                            placeholder={formatMessage({
                                              id:
                                                "customer.addpaymentdlg.content.placeholder.type_note"
                                            })}
                                            onChange={this.onPaymentNoteChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                  <Row gutter={10}>
                                    <Col
                                      lg={24}
                                      sm={24}
                                      xs={24}
                                      className=" gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                                          <ButtonGroup className="gx-custom-toggle-buttons">
                                            <Button
                                              className={`gx-btn-toggle ${
                                                this.state.notify_customer
                                                  ? "gx-btn-toggle-yes"
                                                  : ""
                                              }`}
                                              size="small"
                                              onClick={() =>
                                                this.onNotifyCustomerChange(
                                                  true
                                                )
                                              }
                                            >
                                              {this.state.notify_customer ? (
                                                <span>Yes</span>
                                              ) : (
                                                <ToggleButton></ToggleButton>
                                              )}
                                            </Button>
                                            <Button
                                              className={`gx-btn-toggle ${
                                                !this.state.notify_customer
                                                  ? "gx-btn-toggle-no"
                                                  : ""
                                              }`}
                                              size="small"
                                              onClick={() =>
                                                this.onNotifyCustomerChange(
                                                  false
                                                )
                                              }
                                            >
                                              {!this.state.notify_customer ? (
                                                <span>No</span>
                                              ) : (
                                                <ToggleButton></ToggleButton>
                                              )}
                                            </Button>
                                          </ButtonGroup>
                                          <span
                                            className={`gx-mr-30 gx-fs-sm ${
                                              this.state.notify_customer
                                                ? "gx-font-weight-medium"
                                                : ""
                                            }`}
                                            style={{
                                              color: this.state.notify_customer
                                                ? "#4c586d"
                                                : "#9399a2"
                                            }}
                                          >
                                            Notify customer
                                          </span>
                                        </div>
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                </TabPane>
                                <TabPane tab="Other" key="6">
                                  <Row gutter={10}>
                                    <Col
                                      lg={8}
                                      sm={8}
                                      xs={24}
                                      className="gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.amount" />
                                        </div>
                                        {getFieldDecorator("amount", {
                                          initialValue: this.state.amount
                                        })(
                                          <InputNumber
                                            min={0}
                                            precision={2}
                                            formatter={value =>
                                              (value = `$ ${value}`.replace(
                                                /\B(?=(\d{3})+(?!\d))/g,
                                                ","
                                              ))
                                            }
                                            parser={value =>
                                              value.replace(
                                                /\$\s?|(,*)[A-Za-z]/g,
                                                ""
                                              )
                                            }
                                            onChange={this.onHourlyRateChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                    <Col
                                      lg={16}
                                      sm={16}
                                      xs={24}
                                      className="gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.email_receipt" />
                                        </div>
                                        {getFieldDecorator("email", {
                                          validateTrigger: "onSubmit",
                                          initialValue: this.state
                                            .email_receipt,
                                          rules: [
                                            {
                                              required: true,
                                              message:
                                                "Please write email receipt!"
                                            },
                                            {
                                              type: "email",
                                              message:
                                                "Please write valid email address!"
                                            }
                                          ]
                                        })(
                                          <Input
                                            placeholder={formatMessage({
                                              id:
                                                "customer.addpaymentdlg.content.placeholder.enter_email"
                                            })}
                                            onChange={this.onEmailReceiptChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                  <Row gutter={10}>
                                    <Col
                                      lg={24}
                                      sm={24}
                                      xs={24}
                                      className=" gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-customized-modal-content-field-title">
                                          <IntlMessages id="customer.addpaymentdlg.content.label.payment_note" />
                                        </div>
                                        {getFieldDecorator("payment_note", {
                                          initialValue: this.state.payment_note
                                        })(
                                          <TextArea
                                            rows={4}
                                            placeholder={formatMessage({
                                              id:
                                                "customer.addpaymentdlg.content.placeholder.type_note"
                                            })}
                                            onChange={this.onPaymentNoteChange}
                                          />
                                        )}
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                  <Row gutter={10}>
                                    <Col
                                      lg={24}
                                      sm={24}
                                      xs={24}
                                      className=" gutter-row"
                                    >
                                      <Form.Item>
                                        <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                                          <ButtonGroup className="gx-custom-toggle-buttons">
                                            <Button
                                              className={`gx-btn-toggle ${
                                                this.state.notify_customer
                                                  ? "gx-btn-toggle-yes"
                                                  : ""
                                              }`}
                                              size="small"
                                              onClick={() =>
                                                this.onNotifyCustomerChange(
                                                  true
                                                )
                                              }
                                            >
                                              {this.state.notify_customer ? (
                                                <span>Yes</span>
                                              ) : (
                                                <ToggleButton></ToggleButton>
                                              )}
                                            </Button>
                                            <Button
                                              className={`gx-btn-toggle ${
                                                !this.state.notify_customer
                                                  ? "gx-btn-toggle-no"
                                                  : ""
                                              }`}
                                              size="small"
                                              onClick={() =>
                                                this.onNotifyCustomerChange(
                                                  false
                                                )
                                              }
                                            >
                                              {!this.state.notify_customer ? (
                                                <span>No</span>
                                              ) : (
                                                <ToggleButton></ToggleButton>
                                              )}
                                            </Button>
                                          </ButtonGroup>
                                          <span
                                            className={`gx-mr-30 gx-fs-sm ${
                                              this.state.notify_customer
                                                ? "gx-font-weight-medium"
                                                : ""
                                            }`}
                                            style={{
                                              color: this.state.notify_customer
                                                ? "#4c586d"
                                                : "#9399a2"
                                            }}
                                          >
                                            Notify customer
                                          </span>
                                        </div>
                                      </Form.Item>
                                    </Col>
                                  </Row>
                                </TabPane>
                              </Tabs>
                            </Col>
                          </Row>
                        </div>
                      </TabPane>
                    </Tabs>
                  </Widget>
                </Col>
              </Row>
            </div>
            <div className="gx-customized-modal-footer">
              <div className="gx-flex-row gx-flex-nowrap">
                <Button
                  className="gx-edit-customer-dlg-btn"
                  type="primary"
                  htmlType="submit"
                >
                  <IntlMessages id="add_payment" />
                </Button>
                <Button
                  className="gx-edit-customer-dlg-btn "
                  style={{
                    color: "white",
                    backgroundColor: "#a5abb5",
                    border: "none"
                  }}
                  onClick={this.props.onCancel}
                >
                  <IntlMessages id="cancel" />
                </Button>
              </div>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

AddPaymentDlg = Form.create()(AddPaymentDlg);
export default injectIntl(AddPaymentDlg);
