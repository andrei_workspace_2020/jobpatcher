/*global google*/
import React, { Component } from "react";
import { Button, Row, Col, Select, Input, Form } from "antd";
import Widget from "components/Widget";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import ButtonGroup from "antd/lib/button/button-group";

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

const Option = Select.Option;

const { TextArea } = Input;

class EditCustomerDlg extends Component {
  constructor(props, context) {
    super(props, context);
    this.autocomplete = null;
    console.log(props);
    if (props.data != undefined) {
      this.state = {
        customer: { ...props.data },
        address: {
          id: props.data.address.length > 0 ? props.data.address[0].id : "",
          title:
            props.data.address.length > 0 ? props.data.address[0].title : "",
          street:
            props.data.address.length > 0 ? props.data.address[0].street : "",
          unit: props.data.address.length > 0 ? props.data.address[0].unit : "",
          city: props.data.address.length > 0 ? props.data.address[0].city : "",
          state:
            props.data.address.length > 0 ? props.data.address[0].state : "",
          zipcode:
            props.data.address.length > 0 ? props.data.address[0].zipcode : "",
          alarmcode:
            props.data.address.length > 0
              ? props.data.address[0].alarmcode
              : "",
          property_key:
            props.data.address.length > 0
              ? props.data.address[0].property_key
              : "",
          lat: props.data.address.length > 0 ? props.data.address[0].lat : "",
          lng: props.data.address.length > 0 ? props.data.address[0].lng : ""
        }
      };
    } else {
      this.state = {
        customer: {
          id: "",
          avatar: "",
          title: "",
          name: "",
          first_name: "",
          last_name: "",
          company_name: "",
          email: "",
          phone: {
            mobile: "",
            primary_phone: "",
            work: ""
          },
          birthday: "01/01/1974",
          balance: 0.0,
          status: "Active",
          starred: false,
          customer_info: {
            customer_type: "",
            lead_source: "",
            creation_date: "",
            created_by: "" //later it should be user id
          },
          address: [],
          communication: {
            name_format: ["", ""],
            email_communication: true,
            sms_communication: false,
            preferred_language: "English - US"
          },
          notes: [],
          jobs: [],
          estimates: [],
          invoices: [],
          files: [],
          financial_activities: {
            overdue_invoices: [],
            non_invoiced_jobs: [],
            not_due_yet: []
          },
          financial_info: {
            automatic_invoice: "",
            payment_term: "",
            bill_to: "",
            taxable: false,
            discount_rate: ""
          },
          billing_info: {
            billing_name: "",
            street: "",
            unit: "",
            city: "",
            state: "",
            zipcode: "",
            invoice_to: ""
          },
          payment_history: []
        },
        address: {
          id: "",
          title: "",
          street: "",
          unit: "",
          city: "",
          state: "",
          zipcode: "",
          alarmcode: "",
          property_key: "",
          lat: "",
          lng: ""
        }
      };
    }
  }
  componentDidMount() {
    let inputNode = document.getElementById("street_address");
    if (inputNode) {
      var options = {
        types: ["address"],
        componentRestrictions: {
          country: "ca"
        }
      };
      this.autoComplete = new google.maps.places.Autocomplete(
        inputNode,
        options
      );
      this.autoComplete.addListener("place_changed", this.onPlaceChange);
    }
  }
  componentWillUnmount() {
    // you need to unbind the same listener that was binded.
    // if (this.autoComplete != null) {
    //   google.maps.event.clearInstanceListeners(this.autoComplete);
    // }
  }
  componentDidUpdate(prevProps) {
    // if (this.props.isModalVisible) {
    //   google.maps.event.clearInstanceListeners(this.autoComplete);
    // }
  }

  componentWillReceiveProps(nextProps) {}

  onPlaceChange = () => {
    let place = this.autoComplete.getPlace();
    let location = place.geometry.location;
    let temp = { ...this.state.address };
    temp.lat = location.lat();
    temp.lng = location.lng();
    temp.street = "";
    for (var i = 0; i < place.address_components.length; i++) {
      for (var j = 0; j < place.address_components[i].types.length; j++) {
        if (place.address_components[i].types[j] == "street_number") {
          temp.street += place.address_components[i].long_name + " ";
        } else if (place.address_components[i].types[j] == "route") {
          temp.street += place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "locality") {
          // or administrative_area_level_2
          temp.city = place.address_components[i].long_name;
        } else if (
          place.address_components[i].types[j] == "administrative_area_level_1"
        ) {
          temp.state = place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "postal_code") {
          temp.zipcode = place.address_components[i].long_name;
        }
      }
    }
    this.setState({ address: temp });
    this.props.form.setFieldsValue({ street_address: temp.street });
    this.props.form.setFieldsValue({ city: temp.city });
    this.props.form.setFieldsValue({ state: temp.state });
    this.props.form.setFieldsValue({ zipcode: temp.zipcode });
  };
  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (this.state.address.street != "") {
          let temp = { ...this.state.customer };
          if (this.state.address.id == "") {
            let temp1 = { ...this.state.address };
            temp1.id = 0;
            temp1.title = "Address #1";
            this.setState({ address: temp1 }, () => {
              temp.address.push(this.state.address);
            });
          } else temp.address[0] = { ...this.state.address };
          this.setState({ customer: temp }, () => {
            this.props.onSave(this.state.customer);
          });
        } else this.props.onSave(this.state.customer);
      }
    });
  };
  onTitleChange = value => {
    let temp = { ...this.state.customer };
    temp.title = value;
    this.setState({ customer: temp });
  };
  onFirstNameChange = e => {
    let temp = { ...this.state.customer };
    temp.first_name = e.target.value;
    this.setState({ customer: temp });
    this.props.form.setFieldsValue({ first_name: e.target.value });
  };
  onLastNameChange = e => {
    let temp = { ...this.state.customer };
    temp.last_name = e.target.value;
    this.setState({ customer: temp });
    this.props.form.setFieldsValue({ last_name: e.target.value });
  };
  onCompanyNameChange = e => {
    let temp = { ...this.state.customer };
    temp.company_name = e.target.value;
    this.setState({ customer: temp });
    this.props.form.setFieldsValue({ company_name: e.target.value });
  };
  onEmailChange = e => {
    let temp = { ...this.state.customer };
    temp.email = e.target.value;
    this.setState({ customer: temp });
    this.props.form.setFieldsValue({ email: e.target.value });
  };
  onPhoneNumberChange = e => {
    let temp = { ...this.state.customer };
    temp.phone.primary_phone = e.target.value;
    this.setState({ customer: temp });
    this.props.form.setFieldsValue({ phone_number: e.target.value });
  };
  onEmailNotificationChange = value => {
    let temp = { ...this.state.customer };
    temp.communication.email_communication = value;
    this.setState({ customer: temp });
  };
  onSMSNotificationChange = value => {
    let temp = { ...this.state.customer };
    temp.communication.sms_communication = value;
    this.setState({ customer: temp });
  };
  onStreetAddressChange = e => {
    let temp = { ...this.state.address };
    temp.street = e.target.value;
    this.setState({ address: { ...temp } });
    this.props.form.setFieldsValue({ street_address: e.target.value });
  };
  onUnitNumberChange = e => {
    let temp = { ...this.state.address };
    temp.unit = e.target.value;
    this.setState({ address: { ...temp } });
    this.props.form.setFieldsValue({ unit: e.target.value });
  };
  onCityChange = e => {
    let temp = { ...this.state.address };
    temp.city = e.target.value;
    this.setState({ address: { ...temp } });
    this.props.form.setFieldsValue({ city: e.target.value });
  };
  onStateChange = value => {
    let temp = { ...this.state.address };
    temp.state = value;
    this.setState({ address: { ...temp } });
    this.props.form.setFieldsValue({ state: value });
  };
  onZipCodeChange = e => {
    let temp = { ...this.state.address };
    temp.zipcode = e.target.value;
    this.setState({ address: { ...temp } });
    this.props.form.setFieldsValue({ zipcode: e.target.value });
  };
  onAlarmCodeChange = e => {
    let temp = { ...this.state.address };
    temp.alarmcode = e.target.value;
    this.setState({ address: { ...temp } });
    this.props.form.setFieldsValue({ alarmcode: e.target.value });
  };
  onPropertyKeyChange = e => {
    let temp = { ...this.state.address };
    temp.property_key = e.target.value;
    this.setState({ address: { ...temp } });
    this.props.form.setFieldsValue({ property_key: e.target.value });
  };

  checkCompanyName = (rule, value, callback) => {
    const { form } = this.props;
    if (!value && !form.getFieldValue("company_name")) {
      callback("Ooops!");
    } else {
      callback();
    }
  };

  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { getFieldDecorator } = this.props.form;
    const mobileFormatter = e =>
      e.target.value.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3");

    //
    return (
      <div className="gx-edit-customer-dlg">
        <Form className="gx-form-row0" onSubmit={this.onSubmit}>
          <div
            className="gx-customized-modal-content"
            style={{ paddingBottom: "0" }}
          >
            <div className="gx-customized-modal-content-block gx-m-30">
              <Row>
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  xxl={12}
                  className="gutter-row gx-p-0"
                >
                  <Widget styleName="gx-customer-details gx-card-full gx-h-100 gx-mb-0">
                    <div className="gx-panel-title-bar">
                      <div className="gx-customized-modal-content-title">
                        <i
                          className="material-icons"
                          style={{ fontSize: "28px" }}
                        >
                          account_circle
                        </i>
                        &nbsp;
                        <IntlMessages id="customer.customerdlg.content.customer_details" />
                      </div>
                    </div>
                    <div className="gx-panel-content">
                      <Row gutter={10}>
                        <Col lg={6} sm={6} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("title", {
                              initialValue: this.state.customer.title
                            })(
                              <Select
                                className="gx-w-100"
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.title"
                                })}
                                onChange={this.onTitleChange}
                              >
                                <Option value="">No title</Option>
                                <Option value="Mr.">Mr.</Option>
                                <Option value="Ms.">Ms.</Option>
                                <Option value="Mrs.">Mrs.</Option>
                                <Option value="Miss.">Miss.</Option>
                                <Option value="Dr.">Dr.</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </Col>
                        <Col lg={9} sm={9} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("first_name", {
                              validateTrigger: "onSubmit",
                              initialValue: this.state.customer.first_name,
                              rules: [
                                {
                                  required:
                                    this.props.form.getFieldValue(
                                      "company_name"
                                    ) != "" &&
                                    this.props.form.getFieldValue(
                                      "company_name"
                                    ) != undefined
                                      ? false
                                      : true,
                                  message: "Please write first name!"
                                }
                              ]
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.first_name"
                                })}
                                onChange={this.onFirstNameChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                        <Col lg={9} sm={9} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("last_name", {
                              validateTrigger: "onSubmit",
                              initialValue: this.state.customer.last_name,
                              rules: [
                                {
                                  required:
                                    this.props.form.getFieldValue(
                                      "company_name"
                                    ) != "" &&
                                    this.props.form.getFieldValue(
                                      "company_name"
                                    ) != undefined
                                      ? false
                                      : true,
                                  message: "Please write last name!"
                                }
                              ]
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.last_name"
                                })}
                                onChange={this.onLastNameChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("company_name", {
                              validateTrigger: "onSubmit",
                              initialValue: this.state.customer.company_name,
                              rules: [
                                {
                                  required:
                                    this.props.form.getFieldValue(
                                      "first_name"
                                    ) != "" &&
                                    this.props.form.getFieldValue(
                                      "last_name"
                                    ) != ""
                                      ? false
                                      : true,
                                  message: "Please write company name!"
                                }
                              ]
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.company_name"
                                })}
                                onChange={this.onCompanyNameChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="customer.customerdlg.content.label.email_address" />
                            </div>
                            {getFieldDecorator("email", {
                              validateTrigger: "onSubmit",
                              initialValue: this.state.customer.email,
                              rules: [
                                {
                                  type: "email",
                                  message: "Please write valid email address!"
                                }
                              ]
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.enter_email"
                                })}
                                onChange={this.onEmailChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="customer.customerdlg.content.label.phone_number" />
                            </div>
                            {getFieldDecorator("phone_number", {
                              initialValue: this.state.customer.phone
                                .primary_phone,
                              getValueFromEvent: mobileFormatter
                            })(
                              <Input
                                maxLength={14}
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.enter_number"
                                })}
                                onChange={this.onPhoneNumberChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                            <ButtonGroup className="gx-custom-toggle-buttons">
                              <Button
                                className={`gx-btn-toggle ${
                                  this.state.customer.communication
                                    .email_communication
                                    ? "gx-btn-toggle-yes"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onEmailNotificationChange(true)
                                }
                              >
                                {this.state.customer.communication
                                  .email_communication ? (
                                  <span>Yes</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                              <Button
                                className={`gx-btn-toggle ${
                                  !this.state.customer.communication
                                    .email_communication
                                    ? "gx-btn-toggle-no"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onEmailNotificationChange(false)
                                }
                              >
                                {!this.state.customer.communication
                                  .email_communication ? (
                                  <span>No</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                            </ButtonGroup>
                            <span
                              className={`gx-mr-30 gx-fs-sm ${
                                this.state.customer.communication
                                  .email_communication
                                  ? "gx-font-weight-medium"
                                  : ""
                              }`}
                              style={{
                                color: this.state.customer.communication
                                  .email_communication
                                  ? "#4c586d"
                                  : "#9399a2"
                              }}
                            >
                              Enable email notifications
                            </span>
                          </div>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <div className="gx-flex-row gx-align-items-center">
                            <ButtonGroup className="gx-custom-toggle-buttons">
                              <Button
                                className={`gx-btn-toggle ${
                                  this.state.customer.communication
                                    .sms_communication
                                    ? "gx-btn-toggle-yes"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onSMSNotificationChange(true)
                                }
                              >
                                {this.state.customer.communication
                                  .sms_communication ? (
                                  <span>Yes</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                              <Button
                                className={`gx-btn-toggle ${
                                  !this.state.customer.communication
                                    .sms_communication
                                    ? "gx-btn-toggle-no"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onSMSNotificationChange(false)
                                }
                              >
                                {!this.state.customer.communication
                                  .sms_communication ? (
                                  <span>No</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                            </ButtonGroup>
                            <span
                              className={`gx-mr-30 gx-fs-sm ${
                                this.state.customer.communication
                                  .sms_communication
                                  ? "gx-font-weight-medium"
                                  : ""
                              }`}
                              style={{
                                color: this.state.customer.communication
                                  .sms_communication
                                  ? "#4c586d"
                                  : "#9399a2"
                              }}
                            >
                              Enable SMS notifications
                            </span>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Widget>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  xxl={12}
                  className="gutter-row gx-p-0"
                >
                  <Widget styleName="gx-property-address gx-card-full gx-h-100 gx-mb-0">
                    <div className="gx-panel-title-bar">
                      <div className="gx-customized-modal-content-title">
                        <i
                          className="material-icons"
                          style={{ fontSize: "28px" }}
                        >
                          room
                        </i>
                        &nbsp;
                        <IntlMessages id="customer.customerdlg.content.property_address" />
                      </div>
                    </div>
                    <Input type="hidden" value={this.state.address.id} />
                    <Input type="hidden" value={this.state.address.title} />
                    <div className="gx-panel-content">
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("street_address", {
                              initialValue: this.state.address.street
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.street_address"
                                })}
                                onChange={this.onStreetAddressChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("unit", {
                              initialValue: this.state.address.unit
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.unit_number"
                                })}
                                onChange={this.onUnitNumberChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("city", {
                              initialValue: this.state.address.city
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.city"
                                })}
                                onChange={this.onCityChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("state", {
                              initialValue: this.state.address.state
                            })(
                              <Select
                                className="gx-w-100"
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.state"
                                })}
                                onChange={this.onStateChange}
                              >
                                <Option value="Ontario">Ontario</Option>
                                <Option value="Quebec">Quebec</Option>
                              </Select>
                            )}
                          </Form.Item>
                        </Col>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("zipcode", {
                              initialValue: this.state.address.zipcode
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.zip_code"
                                })}
                                onChange={this.onZipCodeChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="customer.customerdlg.content.label.alarm_code" />
                            </div>
                            {getFieldDecorator("alarmcode", {
                              initialValue: this.state.address.alarmcode
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.enter_code"
                                })}
                                onChange={this.onAlarmCodeChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="customer.customerdlg.content.label.property_key" />
                            </div>
                            {getFieldDecorator("property_key", {
                              initialValue: this.state.address.property_key
                            })(
                              <TextArea
                                placeholder={formatMessage({
                                  id:
                                    "customer.customerdlg.content.placeholder.key_note"
                                })}
                                rows={3}
                                onChange={this.onPropertyKeyChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                    </div>
                  </Widget>
                </Col>
              </Row>
            </div>
            <div className="gx-customized-modal-footer">
              <div className="gx-flex-row gx-flex-nowrap">
                {this.props.data === undefined && (
                  <Button
                    className="gx-edit-customer-dlg-btn"
                    type="primary"
                    htmlType="submit"
                  >
                    <IntlMessages id="save_view" />
                  </Button>
                )}
                {this.props.data !== undefined && (
                  <Button
                    className="gx-edit-customer-dlg-btn"
                    type="primary"
                    htmlType="submit"
                  >
                    <IntlMessages id="update" />
                  </Button>
                )}
                <Button
                  className="gx-edit-customer-dlg-btn "
                  style={{
                    color: "white",
                    backgroundColor: "#a5abb5",
                    border: "none"
                  }}
                  onClick={this.props.onCancel}
                >
                  <IntlMessages id="cancel" />
                </Button>
              </div>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

EditCustomerDlg = Form.create()(EditCustomerDlg);
export default injectIntl(EditCustomerDlg);
