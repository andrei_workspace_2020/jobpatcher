import React, { Component } from "react";
import { Route } from "react-router-dom";
import {
  Button,
  Select,
  Modal,
  Input,
  Tag,
  Tabs,
  Row,
  Col,
  Menu,
  Popover,
  Upload
} from "antd";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import IntlHtmlMessages from "util/IntlHtmlMessages";
import SearchBox from "components/SearchBox";
import Widget from "components/Widget";
import ButtonGroup from "antd/lib/button/button-group";

import TopMenu from "../TopMenu";

const { Option } = Select;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const Dragger = Upload.Dragger;

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

class Services extends Component {
  state = {
    new_service: {
      category: "",
      name: "",
      image: "",
      desc: "",
      tags: ["Taxable"],
      price: 0,
      unit: "",
      taxable: true
    },
    new_category: "",
    updated_category: "",
    current_category: "",
    popover_selected_category: "",
    isCategoryEditable: false,
    searchText: "",
    categories: [
      "All services",
      "Carpet cleaning",
      "Home cleaning",
      "Window cleaning",
      "Plumbing",
      "Landscaping & Lawn"
    ],
    services: [
      {
        id: 1,
        category: "Carpet cleaning",
        name: "1 hour carpet cleaning",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["Residental", "Taxable"],
        price: 30.0
      },
      {
        id: 2,
        category: "Carpet cleaning",
        name: "2 hours carpet cleaning",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["Residental", "Taxable"],
        price: 50.0
      },
      {
        id: 3,
        category: "Home cleaning",
        name: "1 hour home cleaning",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["Residental", "Taxable"],
        price: 80.0
      },
      {
        id: 4,
        category: "Home cleaning",
        name: "3 hours home cleaning",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["Residental", "Taxable"],
        price: 120.0
      },
      {
        id: 5,
        category: "Window cleaning",
        name: "2 hours window cleaning",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["Residental", "Taxable"],
        price: 50.0
      },
      {
        id: 6,
        category: "Window cleaning",
        name: "1 hour window cleaning",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["Residental", "Taxable"],
        price: 80.0
      },
      {
        id: 7,
        category: "Window cleaning",
        name: "3 hours window cleaning",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["Residental", "Taxable"],
        price: 120.0
      },
      {
        id: 8,
        category: "Plumbing",
        name: "Adapter",
        image: "",
        desc:
          "Adaptors connect pipes that are not the same type. Because an adaptor can be male or female",
        tags: ["Modal: EL 423836 XV5"],
        price: 120.0
      },
      {
        id: 9,
        category: "Plumbing",
        name: "Plug and cap",
        image: "",
        desc:
          "Plugs and caps are both used to close up pip openings during inspections and repairs.",
        tags: ["Modal: EL 423836 XV5"],
        price: 120.0
      },
      {
        id: 10,
        category: "Plumbing",
        name: "Barb",
        image: "",
        desc:
          "A barb connects hoses to pipes. It is usually male at one end. The other has a barbed tube.",
        tags: ["Modal: EL 423836 XV5"],
        price: 120.0
      },
      {
        id: 11,
        category: "Plumbing",
        name: "Valve",
        image: "",
        desc:
          "Plugs and caps are both used to close up pipe opneings during inspections and repairs.",
        tags: ["Modal: EL 423836 XV5"],
        price: 120.0
      }
    ],
    isVisibleAddCategoryPopover: false,
    isVisibleAddCategoryModal: false,
    isVisibleAddServiceModal: false,
    isVisibleEditCategoryPopover: [],
    isVisibleCategoryActionPopover: [],
    isVisibleDeleteCategoryPopover: [],

    isServiceActionPopoverVisible: [],
    isServiceEditPopoverVisible: [],
    isServiceMovePopoverVisible: [],
    isServiceDeletePopoverVisible: [],
    selected_taxable: true
  };
  componentDidMount() {
    if (this.state.categories.length > 0) {
      this.setState({ current_category: this.state.categories[0] });
      var array = [];
      for (var i = 0; i < this.state.categories.length; i++) array[i] = false;
      console.log(array);
      this.setState({ isVisibleCategoryActionPopover: [...array] });
      this.setState({ isVisibleEditCategoryPopover: [...array] });
      this.setState({ isVisibleDeleteCategoryPopover: [...array] });
      for (var i = 0; i < this.state.services.length; i++) array[i] = false;
      this.setState({ isServiceActionPopoverVisible: [...array] });
      this.setState({ isServiceEditPopoverVisible: [...array] });
      this.setState({ isServiceMovePopoverVisible: [...array] });
      this.setState({ isServiceDeletePopoverVisible: [...array] });
    }
  }
  getServiceCountByCategory = category => {
    var array = [];
    if (category == "All services") array = this.state.services;
    else array = this.state.services.filter(item => item.category == category);
    return array.length;
  };
  onVisibleAddCategoryPopoverChange = visible => {
    this.setState({ isVisibleAddCategoryPopover: visible });
  };
  onVisibleAddCategoryModalChange = visible => {
    this.setState({ isVisibleAddCategoryModal: visible });
  };
  onVisibleAddServiceModalChange = visible => {
    this.setState({ isVisibleAddServiceModal: visible });
  };
  onVisibleCategoryActionPopoverChange = (visible, index) => {
    console.log(this.state.isVisibleEditCategoryPopover);
    var array = this.state.isVisibleCategoryActionPopover;
    array[index] = visible;
    this.setState({ isVisibleCategoryActionPopover: array });
  };
  onVisibleEditCategoryPopoverChange = (visible, index) => {
    var array = this.state.isVisibleEditCategoryPopover;
    array[index] = visible;
    this.setState({ isVisibleEditCategoryPopover: array });
  };
  onVisibleDeleteCategoryPopoverChange = (visible, index) => {
    var array = this.state.isVisibleDeleteCategoryPopover;
    array[index] = visible;
    this.setState({ isVisibleDeleteCategoryPopover: array });
  };
  saveNewCategory = () => {
    this.setState({ isVisibleAddCategoryPopover: false });
    this.setState({ isVisibleAddCategoryModal: false });
    this.setState(
      {
        categories: [...this.state.categories, this.state.new_category]
      },
      () => {
        this.setState({ new_category: "" });
      }
    );
  };
  updateCategory = index => {
    var array = this.state.categories;
    array[index] = this.state.updated_category;
    this.setState({ categories: array });

    array = this.state.isVisibleEditCategoryPopover;
    array[index] = false;
    this.setState({ isVisibleEditCategoryPopover: array });
  };
  deleteCategory = index => {
    console.log(index);
    var array = this.state.isVisibleDeleteCategoryPopover;
    array[index] = false;
    this.setState({ isVisibleDeleteCategoryPopover: array });
    array = this.state.isVisibleCategoryActionPopover;
    array[index] = false;
    this.setState({ isVisibleCategoryActionPopover: array });
    array = this.state.categories;
    array.splice(index, 1);
    this.setState({ categories: array });
  };
  onNewCategoryChange = e => {
    this.setState({ new_category: e.target.value });
  };
  onUpdateCategoryChange = e => {
    this.setState({ updated_category: e.target.value });
  };
  onNewServiceCategoryChange = value => {
    var temp = this.state.new_service;
    temp.category = value;
    this.setState({ new_service: temp });
  };
  onNewServiceNameChange = e => {
    var temp = this.state.new_service;
    temp.name = e.target.value;
    this.setState({ new_service: temp });
  };
  onNewServiceDescChange = e => {
    var temp = this.state.new_service;
    temp.desc = e.target.value;
    this.setState({ new_service: temp });
  };
  onNewServicePriceChange = e => {
    var temp = this.state.new_service;
    temp.price = parseFloat(e.target.value).toFixed(2);
    console.log(temp.price);
    this.setState({ new_service: temp });
  };
  onNewServiceUnitChange = value => {
    var temp = this.state.new_service;
    temp.unit = value;
    this.setState({ new_service: temp });
  };
  onNewServiceTypeChange = value => {
    var temp = this.state.new_service;
    temp.tags.push(value);
    this.setState({ new_service: temp });
  };
  onNewServiceTaxableChange = (checked, e) => {
    console.log(checked);
    var temp = this.state.new_service;
    if (checked) {
      temp.tags.push("Taxable");
    } else {
      var index = temp.tags.indexOf("Taxable");
      if (index > -1) temp.tags.slice(index, 1);
    }
    this.setState({ new_service: temp });
  };
  addNewService = () => {
    this.setState({
      services: [...this.state.services, this.state.new_service]
    });
    this.setState({ isVisibleAddServiceModal: false });
  };
  filterBySearchText = e => {
    this.setState({ searchText: e.target.value });
  };
  onServiceActionPopoverVisibleChange = (index, visible) => {
    let temp = this.state.isServiceActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isServiceActionPopoverVisible: temp });
  };
  onServiceEditPopoverVisibleChange = (index, visible) => {
    let temp = this.state.isServiceEditPopoverVisible;
    temp[index] = visible;
    this.setState({ isServiceEditPopoverVisible: temp });
  };
  onServiceMovePopoverVisibleChange = (index, visible) => {
    let temp = this.state.isServiceMovePopoverVisible;
    temp[index] = visible;
    this.setState({ isServiceMovePopoverVisible: temp });
  };
  onServiceDeletePopoverVisibleChange = (index, visible) => {
    let temp = this.state.isServiceDeletePopoverVisible;
    temp[index] = visible;
    this.setState({ isServiceDeletePopoverVisible: temp });
  };
  updateServiceCategory = id => {
    let temp = this.state.services;
    temp.map(mat => {
      if (mat.id == id) mat.category = this.state.popover_selected_category;
    });
    this.setState({ services: temp }, () => console.log(this.state.services));
  };
  onDeleteService = id => {
    let temp = this.state.services;
    let index = 0;
    temp.map((item, i) => {
      if (item.id === id) index = i;
    });
    temp.splice(index, 1);
    this.setState({ services: temp });
  };
  onTaxableChange = taxable => {
    this.setState({ selected_taxable: taxable });
  };
  render() {
    let filter_services = this.state.services.filter(item =>
      item.name.includes(this.state.searchText)
    );
    if (this.state.current_category != "All services")
      filter_services = this.state.services.filter(
        service =>
          service.name.includes(this.state.searchText) &&
          service.category == this.state.current_category
      );
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="gx-main-content">
        <TopMenu currentPage="1" />
        <div className="gx-app-module gx-services-module">
          <div className="gx-w-100">
            <div className="gx-services-module-scroll">
              <div
                className="gx-panel-content gx-services-panel-content"
                style={{ backgroundColor: "transparent" }}
              >
                <div className="gx-panel-content-scroll">
                  <div className="gx-service-header-mobile">
                    <div className="gx-service-header-actions-mobile gx-mb-10">
                      <Button
                        type="default"
                        className="gx-btn-new-category gx-customized-button gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-center gx-flex-nowrap"
                        onClick={() =>
                          this.onVisibleAddCategoryModalChange(true)
                        }
                      >
                        <i
                          className="material-icons"
                          style={{ marginRight: "5px" }}
                        >
                          add
                        </i>
                        <IntlMessages id="sales.services.new_category" />
                      </Button>
                      <Button
                        type="primary"
                        className="gx-btn-new-service gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-center gx-flex-nowrap"
                        onClick={() =>
                          this.onVisibleAddServiceModalChange(true)
                        }
                      >
                        <i
                          className="material-icons"
                          style={{ marginRight: "5px" }}
                        >
                          add
                        </i>
                        <IntlMessages id="sales.services.new_service" />
                      </Button>
                    </div>
                    <div className="gx-service-filter-mobile gx-mb-10">
                      <SearchBox
                        styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-services-search-bar gx-w-100"
                        placeholder={"Search services"}
                        onChange={evt =>
                          this.setState({
                            searchText: evt.target.value
                          })
                        }
                        value={this.state.searchText}
                      />
                    </div>
                  </div>
                  <div className="gx-services-tab-categories-mobile">
                    <Tabs className="gx-services-tab-categories">
                      {this.state.categories.map((category, index) => (
                        <TabPane
                          tab={
                            <div
                              className="gx-sidebar-item"
                              onClick={() =>
                                this.setState({
                                  current_category: category
                                })
                              }
                            >
                              <IntlMessages id={category} />
                            </div>
                          }
                          key={index}
                        ></TabPane>
                      ))}
                    </Tabs>
                  </div>
                  <Widget styleName="gx-card-full gx-services-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
                    <div className="gx-services-content-header-mobile">
                      <span className="gx-fs-lg gx-font-weight-medium">
                        {this.getServiceCountByCategory(
                          this.state.current_category
                        ) > 1
                          ? this.getServiceCountByCategory(
                              this.state.current_category
                            ) + " Service items"
                          : this.getServiceCountByCategory(
                              this.state.current_category
                            ) == 1
                          ? this.getServiceCountByCategory(
                              this.state.current_category
                            ) + " Service item"
                          : "No available service items"}
                      </span>
                    </div>
                    <div className="gx-services-content">
                      <div className="gx-services-sidebar">
                        <div className="gx-services-sidebar-header gx-px-20 gx-flex-row gx-align-items-center gx-custom-flex-justify-content-space-between">
                          <Popover
                            overlayClassName="popover-add-category"
                            placement="bottomRight"
                            visible={this.state.isVisibleAddCategoryPopover}
                            onVisibleChange={
                              this.onVisibleAddCategoryPopoverChange
                            }
                            content={
                              <div className="popover-add-category-content">
                                <div className="gx-popover-title">
                                  <span>
                                    <IntlMessages id="sales.services.add_new_category" />
                                  </span>
                                </div>
                                <div className="gx-popover-category">
                                  <label>
                                    <IntlMessages id="sales.services.category_name" />
                                  </label>
                                  <Input
                                    className="gx-category-input"
                                    style={{ marginTop: "10px" }}
                                    value={this.state.new_category}
                                    onChange={this.onNewCategoryChange}
                                  ></Input>
                                </div>
                                <div className="gx-popover-actions">
                                  <Button
                                    type="default"
                                    className="gx-btn-cancel-category"
                                    onClick={() =>
                                      this.onVisibleAddCategoryPopoverChange(
                                        false
                                      )
                                    }
                                  >
                                    <IntlMessages id="sales.services.cancel" />
                                  </Button>
                                  <Button
                                    type="primary"
                                    className="gx-btn-save-category"
                                    onClick={this.saveNewCategory}
                                  >
                                    <IntlMessages id="sales.services.save" />
                                  </Button>
                                </div>
                              </div>
                            }
                            trigger="click"
                          >
                            <Button
                              type="primary"
                              className="gx-btn-new-category gx-m-0 gx-flex-row gx-align-items-center gx-flex-nowrap"
                            >
                              <i
                                className="material-icons"
                                style={{ marginRight: "5px" }}
                              >
                                add
                              </i>
                              <IntlMessages id="sales.services.new_category" />
                            </Button>
                          </Popover>
                          <Button
                            onClick={() =>
                              this.setState({
                                isCategoryEditable: !this.state
                                  .isCategoryEditable
                              })
                            }
                            type="default"
                            className="gx-btn-edit-category gx-mb-0 gx-ml-20 gx-p-0 gx-flex-row gx-align-items-center gx-custom-flex-justify-content-center"
                          >
                            <i className="material-icons icon-action">create</i>
                          </Button>
                        </div>
                        <div className="gx-services-sidebar-body">
                          <div className="gx-services-sidebar-body-scroll">
                            <Menu
                              defaultSelectedKeys={["0"]}
                              mode="inline"
                              theme="light"
                            >
                              {this.state.categories.map((category, index) => (
                                <Menu.Item
                                  key={index}
                                  onClick={() =>
                                    this.setState({
                                      current_category: category
                                    })
                                  }
                                >
                                  <div className="gx-sidebar-item">
                                    <span className="gx-category-name">
                                      {category}
                                    </span>
                                    {!this.state.isCategoryEditable ? (
                                      <div className="gx-category-count">
                                        {this.getServiceCountByCategory(
                                          category
                                        )}
                                      </div>
                                    ) : (
                                      <div className="gx-category-action">
                                        <Popover
                                          overlayClassName="popover-category-action popover-add-category popover-edit-category popover-delete-category"
                                          placement="bottom"
                                          visible={
                                            this.state
                                              .isVisibleCategoryActionPopover[
                                              index
                                            ]
                                          }
                                          onVisibleChange={visible =>
                                            this.onVisibleCategoryActionPopoverChange(
                                              visible,
                                              index
                                            )
                                          }
                                          content={
                                            this.state
                                              .isVisibleEditCategoryPopover[
                                              index
                                            ] == true ? (
                                              <div className="popover-edit-category-content">
                                                <div className="gx-popover-title">
                                                  <span>
                                                    <IntlMessages id="sales.services.edit_category" />
                                                  </span>
                                                </div>
                                                <div className="gx-popover-category">
                                                  <label>
                                                    <IntlMessages id="sales.services.category_name" />
                                                  </label>
                                                  <Input
                                                    className="gx-category-input"
                                                    style={{
                                                      marginTop: "10px"
                                                    }}
                                                    value={
                                                      this.state
                                                        .updated_category
                                                    }
                                                    onChange={
                                                      this
                                                        .onUpdateCategoryChange
                                                    }
                                                  ></Input>
                                                </div>
                                                <div className="gx-popover-actions">
                                                  <Button
                                                    type="default"
                                                    className="gx-btn-cancel-category"
                                                    onClick={() =>
                                                      this.onVisibleEditCategoryPopoverChange(
                                                        false,
                                                        index
                                                      )
                                                    }
                                                  >
                                                    <IntlMessages id="sales.services.cancel" />
                                                  </Button>
                                                  <Button
                                                    type="primary"
                                                    className="gx-btn-save-category"
                                                    onClick={() =>
                                                      this.updateCategory(index)
                                                    }
                                                  >
                                                    <IntlMessages id="sales.services.update" />
                                                  </Button>
                                                </div>
                                              </div>
                                            ) : this.state
                                                .isVisibleDeleteCategoryPopover[
                                                index
                                              ] == true ? (
                                              <div className="popover-delete-category-content">
                                                <div className="gx-popover-title">
                                                  <span>
                                                    <IntlMessages id="sales.services.delete_category" />
                                                  </span>
                                                </div>
                                                <div className="gx-popover-category">
                                                  <label>
                                                    <IntlMessages id="sales.services.delete.confirm" />
                                                  </label>
                                                  <Input
                                                    className="gx-category-input"
                                                    style={{
                                                      marginTop: "10px"
                                                    }}
                                                    value={
                                                      this.state.categories[
                                                        index
                                                      ]
                                                    }
                                                    disabled={true}
                                                  ></Input>
                                                </div>
                                                <div className="gx-popover-actions">
                                                  <Button
                                                    type="default"
                                                    className="gx-btn-cancel-category"
                                                    onClick={() =>
                                                      this.onVisibleDeleteCategoryPopoverChange(
                                                        false,
                                                        index
                                                      )
                                                    }
                                                  >
                                                    <IntlMessages id="sales.services.cancel" />
                                                  </Button>
                                                  <Button
                                                    type="danger"
                                                    className="gx-btn-delete-category"
                                                    onClick={() =>
                                                      this.deleteCategory(index)
                                                    }
                                                  >
                                                    <IntlMessages id="sales.services.delete" />
                                                  </Button>
                                                </div>
                                              </div>
                                            ) : (
                                              <div className="popover-category-action-content">
                                                <div
                                                  className="gx-menuitem"
                                                  onClick={() => {
                                                    this.onVisibleEditCategoryPopoverChange(
                                                      true,
                                                      index
                                                    );
                                                    this.setState({
                                                      updated_category: this
                                                        .state.categories[index]
                                                    });
                                                  }}
                                                >
                                                  <i className="material-icons gx-mr-10">
                                                    edit
                                                  </i>
                                                  <IntlMessages id="sales.services.edit_category" />
                                                </div>
                                                <div
                                                  className="gx-menuitem"
                                                  onClick={() => {
                                                    this.onVisibleDeleteCategoryPopoverChange(
                                                      true,
                                                      index
                                                    );
                                                  }}
                                                >
                                                  <i className="material-icons gx-mr-10">
                                                    delete_sweep
                                                  </i>
                                                  <IntlMessages id="sales.services.delete_category" />
                                                </div>
                                              </div>
                                            )
                                          }
                                          trigger="click"
                                        >
                                          <i className="material-icons gx-icon-action">
                                            more_horiz
                                          </i>
                                        </Popover>
                                      </div>
                                    )}
                                  </div>
                                </Menu.Item>
                              ))}
                            </Menu>
                          </div>
                        </div>
                      </div>
                      <div className="gx-services-body gx-w-100">
                        <div className="gx-service-body-content-header">
                          <div className="gx-service-body-content-header-desktop">
                            {/* <div
                              className="gx-service-by-hours"
                              style={{ width: "200px" }}
                            >
                              <Select
                                className="gx-w-100"
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                placeholder={
                                  <IntlMessages id="sales.services.services_by_hours" />
                                }
                              >
                                <Option value="1">1 hour</Option>
                                <Option value="2">2 hours</Option>
                                <Option value="3">3 hours</Option>
                                <Option value="4">4 hours</Option>
                                <Option value="5">5 hours</Option>
                              </Select>
                            </div> */}
                            <span className="gx-fs-lg gx-font-weight-medium">
                              {this.getServiceCountByCategory(
                                this.state.current_category
                              ) > 1
                                ? this.getServiceCountByCategory(
                                    this.state.current_category
                                  ) + " Service items"
                                : this.getServiceCountByCategory(
                                    this.state.current_category
                                  ) == 1
                                ? this.getServiceCountByCategory(
                                    this.state.current_category
                                  ) + " Service item"
                                : "No available service items"}
                            </span>
                            <div className="gx-flex-row gx-flex-nowrap">
                              <SearchBox
                                styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-services-search-bar gx-mr-10"
                                placeholder={"Search services"}
                                onChange={this.filterBySearchText}
                                value={this.state.searchText}
                              />
                              <Button
                                type="primary"
                                className="gx-m-0 gx-flex-row gx-flex-nowrap gx-align-items-center"
                                onClick={() =>
                                  this.onVisibleAddServiceModalChange(true)
                                }
                              >
                                <i
                                  className="material-icons"
                                  style={{ fontSize: "20px" }}
                                >
                                  add
                                </i>
                                <IntlMessages id="sales.services.new_service" />
                              </Button>
                            </div>
                          </div>
                        </div>
                        <div className="gx-service-body-content-body ">
                          <Row>
                            {filter_services.map((service, index) => (
                              <Col
                                span={6}
                                xs={24}
                                sm={12}
                                md={12}
                                lg={12}
                                xl={8}
                                xxl={6}
                                key={index}
                              >
                                <Widget styleName="gx-service-card gx-card-full">
                                  <div className="gx-service-card-body">
                                    <div className="gx-service-card-image">
                                      {service.image != "" ? (
                                        <img
                                          src={require("assets/images/service/" +
                                            service.image)}
                                        />
                                      ) : (
                                        <img
                                          src={require("assets/images/service/service-default.png")}
                                        />
                                      )}
                                    </div>
                                    <div className="gx-p-20">
                                      <div className="gx-service-card-name">
                                        {service.name}
                                      </div>
                                      <div className="gx-service-card-desc">
                                        {service.desc}
                                      </div>
                                      <div className="gx-service-card-tags">
                                        {service.tags.map((tag, index) => (
                                          <Tag key={index}>{tag}</Tag>
                                        ))}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="gx-service-card-footer">
                                    <div className="gx-flex-row gx-align-items-center">
                                      <span
                                        style={{
                                          fontSize: "13px",
                                          color: "#9399a2",
                                          marginRight: "10px"
                                        }}
                                      >
                                        Service price:{" "}
                                      </span>
                                      <span className="gx-service-card-price">
                                        ${parseFloat(service.price).toFixed(2)}
                                      </span>
                                    </div>
                                    <Popover
                                      overlayClassName="gx-popover-material-action"
                                      placement="right"
                                      trigger="click"
                                      visible={
                                        this.state
                                          .isServiceActionPopoverVisible[index]
                                      }
                                      onVisibleChange={visible =>
                                        this.onServiceActionPopoverVisibleChange(
                                          index,
                                          visible
                                        )
                                      }
                                      content={
                                        this.state.isServiceEditPopoverVisible[
                                          index
                                        ] ? (
                                          <div />
                                        ) : this.state
                                            .isServiceMovePopoverVisible[
                                            index
                                          ] ? (
                                          <div className="gx-popover-material-content gx-popover-material-move-content">
                                            <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                              <i
                                                className="gx-size-30 gx-mr-10 material-icons"
                                                style={{
                                                  color: "#f7c43d",
                                                  fontSize: "30px"
                                                }}
                                              >
                                                directions
                                              </i>
                                              <div className="gx-flex-column">
                                                <span
                                                  style={{
                                                    fontSize: "15px",
                                                    color: "#4c586d",
                                                    fontWeight: "bold"
                                                  }}
                                                >
                                                  Move this service
                                                </span>
                                                <span
                                                  style={{
                                                    fontSize: "13px",
                                                    color: "#9399a2",
                                                    fontWeight: "500"
                                                  }}
                                                >
                                                  Move to different category
                                                </span>
                                              </div>
                                            </div>
                                            <div
                                              className="gx-popover-body"
                                              style={{ marginTop: "15px" }}
                                            >
                                              <Select
                                                className="gx-w-100"
                                                placeholder="Select a category"
                                                suffixIcon={
                                                  <i className="material-icons">
                                                    expand_more
                                                  </i>
                                                }
                                                value={
                                                  this.state
                                                    .popover_selected_category
                                                }
                                                onChange={value =>
                                                  this.setState({
                                                    popover_selected_category: value
                                                  })
                                                }
                                              >
                                                {this.state.categories.map(
                                                  (cat, index) => {
                                                    if (cat != "All services")
                                                      return (
                                                        <Option
                                                          value={cat}
                                                          key={index}
                                                        >
                                                          {cat}
                                                        </Option>
                                                      );
                                                  }
                                                )}
                                              </Select>
                                            </div>
                                            <div
                                              className="gx-popover-action"
                                              style={{ marginTop: "15px" }}
                                            >
                                              <Button
                                                type="default"
                                                className="gx-btn-cancel"
                                                onClick={() =>
                                                  this.onServiceMovePopoverVisibleChange(
                                                    index,
                                                    false
                                                  )
                                                }
                                              >
                                                Cancel
                                              </Button>
                                              <Button
                                                type="primary"
                                                className="gx-btn-confirm"
                                                onClick={() => {
                                                  this.onServiceMovePopoverVisibleChange(
                                                    index,
                                                    false
                                                  );
                                                  this.updateServiceCategory(
                                                    service.id
                                                  );
                                                }}
                                              >
                                                Confirm
                                              </Button>
                                            </div>
                                          </div>
                                        ) : this.state
                                            .isServiceDeletePopoverVisible[
                                            index
                                          ] ? (
                                          <div className="gx-popover-material-content gx-popover-material-delete-content">
                                            <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                              <i
                                                className="gx-size-30 gx-mr-10 material-icons"
                                                style={{
                                                  color: "#fc6262",
                                                  fontSize: "30px"
                                                }}
                                              >
                                                warning
                                              </i>
                                              <div className="gx-flex-column">
                                                <span
                                                  style={{
                                                    fontSize: "15px",
                                                    color: "#4c586d",
                                                    fontWeight: "bold"
                                                  }}
                                                >
                                                  Delete this service
                                                </span>
                                                <span
                                                  style={{
                                                    fontSize: "13px",
                                                    color: "#9399a2",
                                                    fontWeight: "500"
                                                  }}
                                                >
                                                  This action can't be undone
                                                </span>
                                              </div>
                                            </div>
                                            <div
                                              className="gx-popover-action"
                                              style={{ marginTop: "15px" }}
                                            >
                                              <Button
                                                type="default"
                                                className="gx-btn-cancel"
                                                onClick={() =>
                                                  this.onServiceDeletePopoverVisibleChange(
                                                    index,
                                                    false
                                                  )
                                                }
                                              >
                                                Cancel
                                              </Button>
                                              <Button
                                                type="primary"
                                                className="gx-btn-confirm"
                                                onClick={() => {
                                                  this.onServiceDeletePopoverVisibleChange(
                                                    index,
                                                    false
                                                  );
                                                  this.onDeleteService(
                                                    service.id
                                                  );
                                                  this.onServiceActionPopoverVisibleChange(
                                                    index,
                                                    false
                                                  );
                                                }}
                                              >
                                                Confirm
                                              </Button>
                                            </div>
                                          </div>
                                        ) : (
                                          <div>
                                            <div
                                              className="gx-menuitem"
                                              onClick={() =>
                                                this.onServiceEditPopoverVisibleChange(
                                                  index,
                                                  true
                                                )
                                              }
                                            >
                                              <i className="material-icons">
                                                edit
                                              </i>
                                              <span>Edit service</span>
                                            </div>
                                            <div
                                              className="gx-menuitem"
                                              onClick={() => {
                                                this.onServiceMovePopoverVisibleChange(
                                                  index,
                                                  true
                                                );
                                                this.setState({
                                                  popover_selected_category:
                                                    service.category
                                                });
                                              }}
                                            >
                                              <i className="material-icons">
                                                directions
                                              </i>
                                              <span>Move service</span>
                                            </div>
                                            <div
                                              className="gx-menuitem"
                                              onClick={() => {
                                                this.onServiceDeletePopoverVisibleChange(
                                                  index,
                                                  true
                                                );
                                                this.setState({
                                                  popover_selected_category: ""
                                                });
                                              }}
                                            >
                                              <i className="material-icons">
                                                delete
                                              </i>
                                              <span>Delete service</span>
                                            </div>
                                          </div>
                                        )
                                      }
                                    >
                                      <i className="material-icons gx-icon-action">
                                        more_vert
                                      </i>
                                    </Popover>
                                  </div>
                                </Widget>
                              </Col>
                            ))}
                          </Row>
                        </div>
                      </div>
                    </div>
                  </Widget>
                  <Modal
                    className="modal-add-service"
                    title={
                      <div className="gx-flex-row gx-align-items-center gx-custom-flex-justify-content-space-between">
                        <IntlMessages id="sales.services.add_new_service_item" />
                        <i
                          className="material-icons gx-icon-action"
                          onClick={() =>
                            this.onVisibleAddServiceModalChange(false)
                          }
                        >
                          clear
                        </i>
                      </div>
                    }
                    closable={false}
                    centered
                    visible={this.state.isVisibleAddServiceModal}
                    footer={[null, null]}
                    onCancel={() => this.onVisibleAddServiceModalChange(false)}
                  >
                    <div className="service-body">
                      <div className="service-details">
                        <div className="service-details-control">
                          <IntlMessages id="sales.services.choose_category" />
                          <Select
                            className="gx-w-100"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            placeholder={
                              <IntlMessages id="sales.services.choose_category" />
                            }
                            onChange={this.onNewServiceCategoryChange}
                          >
                            {this.state.categories.map((category, index) => (
                              <Option key={index} value={category}>
                                {category}
                              </Option>
                            ))}
                          </Select>
                        </div>
                        <div className="service-details-control">
                          <IntlMessages id="sales.services.title" />
                          <Input
                            placeholder={formatMessage({
                              id: "sales.services.title"
                            })}
                            onChange={this.onNewServiceNameChange}
                          />
                        </div>
                        <div className="service-details-control">
                          <IntlMessages id="sales.services.description" />
                          <TextArea
                            rows="4"
                            placeholder={formatMessage({
                              id: "sales.services.max_120_characters"
                            })}
                            onChange={this.onNewServiceDescChange}
                          />
                        </div>
                        <div className="service-details-control">
                          <IntlMessages id="sales.services.pricing" />
                          <div className="service-details-control service-details-pricing">
                            <Input
                              placeholder={formatMessage({
                                id: "sales.services.enter_amount"
                              })}
                              onChange={this.onNewServicePriceChange}
                            />
                            <Select
                              defaultValue="per_unit"
                              suffixIcon={
                                <i className="material-icons">expand_more</i>
                              }
                              onChange={this.onNewServiceUnitChange}
                            >
                              <Option value="per_unit">Per unit</Option>
                            </Select>
                          </div>
                        </div>
                        <div className="service-details-control">
                          <div className="service-details-control">
                            <IntlMessages id="sales.services.service_type" />
                            <div className="gx-w-100 gx-flex-row">
                              <Select
                                className="service-details-type"
                                placeholder={
                                  <IntlMessages id="sales.services.select_type" />
                                }
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                onChange={this.onNewServiceTypeChange}
                                style={{ marginRight: "10px" }}
                              >
                                <Option value="Residential">Residential</Option>
                              </Select>
                              <div
                                className="service-details-taxable gx-flex-row gx-align-items-center"
                                style={{
                                  justifyContent: "center"
                                }}
                              >
                                <ButtonGroup className="gx-custom-toggle-buttons">
                                  <Button
                                    className={`gx-btn-toggle ${
                                      this.state.selected_taxable
                                        ? "gx-btn-toggle-yes"
                                        : ""
                                    }`}
                                    size="small"
                                    onClick={() => this.onTaxableChange(true)}
                                  >
                                    {this.state.selected_taxable ? (
                                      <span>Yes</span>
                                    ) : (
                                      <ToggleButton></ToggleButton>
                                    )}
                                  </Button>
                                  <Button
                                    className={`gx-btn-toggle ${
                                      !this.state.selected_taxable
                                        ? "gx-btn-toggle-no"
                                        : ""
                                    }`}
                                    size="small"
                                    onClick={() => this.onTaxableChange(false)}
                                  >
                                    {!this.state.selected_taxable ? (
                                      <span>No</span>
                                    ) : (
                                      <ToggleButton></ToggleButton>
                                    )}
                                  </Button>
                                </ButtonGroup>
                                <IntlMessages id="sales.services.taxable" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="service-image">
                        <Widget styleName="gx-card-full">
                          <div className="gx-p-20">
                            <div
                              className="gx-service-image gx-flex-row gx-align-items-center"
                              style={{
                                backgroundColor: "#fbbbbb",
                                justifyContent: "center",
                                height: "120px"
                              }}
                            >
                              <img
                                src={require("assets/images/service/service-default.png")}
                              />
                            </div>
                            <Button
                              type="default"
                              className="gx-btn-change-icon gx-my-10 gx-w-100"
                            >
                              <IntlMessages id="sales.services.change_icon" />
                            </Button>
                            <Dragger>
                              <div
                                className="gx-flex-column gx-justify-content-center gx-align-items-center"
                                style={{ color: "#a5abb5" }}
                              >
                                <img
                                  style={{ marginRight: "15px" }}
                                  src={require("assets/images/upload.png")}
                                />
                                <div className="gx-fs-13-20 gx-font-weight-medium gx-mt-10">
                                  <IntlHtmlMessages id="sales.services.dragdrop" />
                                </div>
                              </div>
                            </Dragger>
                          </div>
                        </Widget>
                      </div>
                    </div>
                    <div className="service-footer">
                      <div className="service-details-action">
                        <Button
                          type="default"
                          className="gx-btn-cancel"
                          onClick={() =>
                            this.setState({ isVisibleAddServiceModal: false })
                          }
                        >
                          <IntlMessages id="sales.services.cancel" />
                        </Button>
                        <Button
                          type="primary"
                          className="gx-btn-save"
                          onClick={this.addNewService}
                        >
                          <IntlMessages id="sales.services.save" />
                        </Button>
                      </div>
                    </div>
                  </Modal>
                  <Modal
                    className="modal-add-category"
                    closable={false}
                    centered
                    visible={this.state.isVisibleAddCategoryModal}
                    footer={[null, null]}
                    onCancel={() => this.onVisibleAddCategoryModalChange(false)}
                  >
                    <div className="modal-add-category-content">
                      <div className="gx-category-title">
                        <span>
                          <IntlMessages id="sales.services.add_new_category" />
                        </span>
                      </div>
                      <div className="gx-category-form">
                        <label>
                          <IntlMessages id="sales.services.category_name" />
                        </label>
                        <Input
                          placeholder={formatMessage({
                            id: "sales.services.enter_name"
                          })}
                          className="gx-category-input"
                          style={{ marginTop: "10px" }}
                          value={this.state.new_category}
                          onChange={this.onNewCategoryChange}
                        />
                      </div>
                      <div className="gx-category-actions">
                        <Button
                          type="default"
                          className="gx-btn-cancel-category"
                          onClick={() =>
                            this.onVisibleAddCategoryModalChange(false)
                          }
                        >
                          <IntlMessages id="sales.services.cancel" />
                        </Button>
                        <Button
                          type="primary"
                          className="gx-btn-save-category"
                          onClick={this.saveNewCategory}
                        >
                          <IntlMessages id="sales.services.save" />
                        </Button>
                      </div>
                    </div>
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(Services);
