import React, { Component } from "react";
import { Route } from "react-router-dom";
import {
  Button,
  Select,
  Modal,
  Input,
  Tag,
  Tabs,
  Row,
  Col,
  Menu,
  Popover,
  Upload
} from "antd";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import IntlHtmlMessages from "util/IntlHtmlMessages";
import SearchBox from "components/SearchBox";
import Widget from "components/Widget";
import ButtonGroup from "antd/lib/button/button-group";

import TopMenu from "../TopMenu";

const { Option } = Select;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const Dragger = Upload.Dragger;

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

class Materials extends Component {
  state = {
    new_material: {
      category: "",
      name: "",
      image: "",
      desc: "",
      tags: ["Taxable"],
      price: 0,
      unit: "",
      model: "",
      taxable: true
    },
    new_category: "",
    updated_category: "",
    current_category: "",
    popover_selected_category: "",
    isCategoryEditable: false,
    searchText: "",
    categories: ["All materials", "Steel", "Copper", "Plastic"],
    materials: [
      {
        id: 1,
        category: "Steel",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 30.0
      },
      {
        id: 2,
        category: "Steel",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0
      },
      {
        id: 3,
        category: "Copper",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0
      },
      {
        id: 4,
        category: "Copper",
        name: "Valve",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0
      },
      {
        id: 5,
        category: "Plastic",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0
      },
      {
        id: 6,
        category: "Plastic",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0
      },
      {
        id: 7,
        category: "Plastic",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0
      }
    ],
    isVisibleAddCategoryPopover: false,
    isVisibleAddCategoryModal: false,
    isVisibleAddMaterialModal: false,
    isVisibleEditCategoryPopover: [],
    isVisibleCategoryActionPopover: [],
    isVisibleDeleteCategoryPopover: [],

    isMaterialActionPopoverVisible: [],
    isMaterialEditPopoverVisible: [],
    isMaterialMovePopoverVisible: [],
    isMaterialDeletePopoverVisible: [],
    selected_taxable: true
  };
  componentDidMount() {
    if (this.state.categories.length > 0) {
      this.setState({ current_category: this.state.categories[0] });
      var array = [];
      for (var i = 0; i < this.state.categories.length; i++) array[i] = false;
      console.log(array);
      this.setState({ isVisibleCategoryActionPopover: [...array] });
      this.setState({ isVisibleEditCategoryPopover: [...array] });
      this.setState({ isVisibleDeleteCategoryPopover: [...array] });
      for (var i = 0; i < this.state.materials.length; i++) array[i] = false;
      this.setState({ isMaterialActionPopoverVisible: [...array] });
      this.setState({ isMaterialEditPopoverVisible: [...array] });
      this.setState({ isMaterialMovePopoverVisible: [...array] });
      this.setState({ isMaterialDeletePopoverVisible: [...array] });
    }
  }
  getMaterialCountByCategory = category => {
    let array = [];
    if (category == "All materials") array = this.state.materials;
    else array = this.state.materials.filter(item => item.category == category);
    return array.length;
  };
  onVisibleAddCategoryPopoverChange = visible => {
    this.setState({ isVisibleAddCategoryPopover: visible });
  };
  onVisibleAddCategoryModalChange = visible => {
    this.setState({ isVisibleAddCategoryModal: visible });
  };
  onVisibleAddMaterialModalChange = visible => {
    this.setState({ isVisibleAddMaterialModal: visible });
  };
  onVisibleCategoryActionPopoverChange = (visible, index) => {
    console.log(this.state.isVisibleEditCategoryPopover);
    var array = this.state.isVisibleCategoryActionPopover;
    array[index] = visible;
    this.setState({ isVisibleCategoryActionPopover: array });
  };
  onVisibleEditCategoryPopoverChange = (visible, index) => {
    var array = this.state.isVisibleEditCategoryPopover;
    array[index] = visible;
    this.setState({ isVisibleEditCategoryPopover: array });
  };
  onVisibleDeleteCategoryPopoverChange = (visible, index) => {
    var array = this.state.isVisibleDeleteCategoryPopover;
    array[index] = visible;
    this.setState({ isVisibleDeleteCategoryPopover: array });
  };
  saveNewCategory = () => {
    this.setState({ isVisibleAddCategoryPopover: false });
    this.setState({ isVisibleAddCategoryModal: false });
    this.setState(
      {
        categories: [...this.state.categories, this.state.new_category]
      },
      () => {
        this.setState({ new_category: "" });
      }
    );
  };
  updateCategory = index => {
    var array = this.state.categories;
    array[index] = this.state.updated_category;
    this.setState({ categories: array });

    array = this.state.isVisibleEditCategoryPopover;
    array[index] = false;
    this.setState({ isVisibleEditCategoryPopover: array });
  };
  deleteCategory = index => {
    console.log(index);
    var array = this.state.isVisibleDeleteCategoryPopover;
    array[index] = false;
    this.setState({ isVisibleDeleteCategoryPopover: array });
    array = this.state.isVisibleCategoryActionPopover;
    array[index] = false;
    this.setState({ isVisibleCategoryActionPopover: array });
    array = this.state.categories;
    array.splice(index, 1);
    this.setState({ categories: array });
  };
  onNewCategoryChange = e => {
    this.setState({ new_category: e.target.value });
  };
  onUpdateCategoryChange = e => {
    this.setState({ updated_category: e.target.value });
  };
  onNewMaterialCategoryChange = value => {
    var temp = this.state.new_material;
    temp.category = value;
    this.setState({ new_material: temp });
  };
  onNewMaterialNameChange = e => {
    var temp = this.state.new_material;
    temp.name = e.target.value;
    this.setState({ new_material: temp });
  };
  onNewMaterialDescChange = e => {
    var temp = this.state.new_material;
    temp.desc = e.target.value;
    this.setState({ new_material: temp });
  };
  onNewMaterialPriceChange = e => {
    var temp = this.state.new_material;
    temp.price = parseFloat(e.target.value).toFixed(2);
    console.log(temp.price);
    this.setState({ new_material: temp });
  };
  onNewMaterialUnitChange = value => {
    var temp = this.state.new_material;
    temp.unit = value;
    this.setState({ new_material: temp });
  };
  onNewMaterialModelChange = e => {
    var temp = this.state.new_material;
    temp.model = e.target.value;
    this.setState({ new_material: temp });
  };
  onNewMaterialTaxableChange = (checked, e) => {
    console.log(checked);
    var temp = this.state.new_material;
    if (checked) {
      temp.tags.push("Taxable");
    } else {
      var index = temp.tags.indexOf("Taxable");
      if (index > -1) temp.tags.slice(index, 1);
    }
    this.setState({ new_material: temp });
  };
  addNewMaterial = () => {
    var temp = this.state.new_material;
    if (temp.model != "") {
      temp.tags.push("Model: " + temp.model);
      this.setState({ new_material: temp });
    }
    this.setState({
      materials: [...this.state.materials, temp]
    });
    this.setState({ isVisibleAddMaterialModal: false });
  };
  filterBySearchText = e => {
    this.setState({ searchText: e.target.value });
  };
  onMaterialActionPopoverVisibleChange = (index, visible) => {
    let temp = this.state.isMaterialActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isMaterialActionPopoverVisible: temp });
  };
  onMaterialEditPopoverVisibleChange = (index, visible) => {
    let temp = this.state.isMaterialEditPopoverVisible;
    temp[index] = visible;
    this.setState({ isMaterialEditPopoverVisible: temp });
  };
  onMaterialMovePopoverVisibleChange = (index, visible) => {
    let temp = this.state.isMaterialMovePopoverVisible;
    temp[index] = visible;
    this.setState({ isMaterialMovePopoverVisible: temp });
  };
  onMaterialDeletePopoverVisibleChange = (index, visible) => {
    let temp = this.state.isMaterialDeletePopoverVisible;
    temp[index] = visible;
    this.setState({ isMaterialDeletePopoverVisible: temp });
  };
  updateMaterialCategory = id => {
    let temp = this.state.materials;
    temp.map(mat => {
      if (mat.id == id) mat.category = this.state.popover_selected_category;
    });
    this.setState({ materials: temp }, () => console.log(this.state.materials));
  };
  onDeleteMaterial = id => {
    let temp = this.state.materials;
    let index = 0;
    temp.map((item, i) => {
      if (item.id === id) index = i;
    });
    temp.splice(index, 1);
    this.setState({ materials: temp });
  };
  onTaxableChange = taxable => {
    this.setState({ selected_taxable: taxable });
  };
  render() {
    let filter_materials = this.state.materials.filter(item =>
      item.name.includes(this.state.searchText)
    );
    if (this.state.current_category != "All materials")
      filter_materials = this.state.materials.filter(
        material =>
          material.name.includes(this.state.searchText) &&
          material.category == this.state.current_category
      );
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="gx-main-content">
        <TopMenu currentPage="2" />
        <div className="gx-app-module gx-services-module">
          <div className="gx-w-100">
            <div className="gx-services-module-scroll">
              <div
                className="gx-panel-content gx-services-panel-content"
                style={{ backgroundColor: "transparent" }}
              >
                <div className="gx-panel-content-scroll">
                  <div className="gx-service-header-mobile">
                    <div className="gx-service-header-actions-mobile gx-mb-10">
                      <Button
                        type="default"
                        className="gx-btn-new-category gx-customized-button gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-center gx-flex-nowrap"
                        onClick={() =>
                          this.onVisibleAddCategoryModalChange(true)
                        }
                      >
                        <i
                          className="material-icons"
                          style={{ marginRight: "5px" }}
                        >
                          add
                        </i>
                        <IntlMessages id="sales.materials.new_category" />
                      </Button>
                      <Button
                        type="primary"
                        className="gx-btn-new-service gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-center gx-flex-nowrap"
                        onClick={() =>
                          this.onVisibleAddMaterialModalChange(true)
                        }
                      >
                        <i
                          className="material-icons"
                          style={{ marginRight: "5px" }}
                        >
                          add
                        </i>
                        <IntlMessages id="sales.materials.new_material" />
                      </Button>
                    </div>
                    <div className="gx-service-filter-mobile gx-mb-10">
                      <SearchBox
                        styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-services-search-bar gx-w-100"
                        placeholder={"Search services"}
                        onChange={evt =>
                          this.setState({
                            searchText: evt.target.value
                          })
                        }
                        value={this.state.searchText}
                      />
                    </div>
                  </div>
                  <div className="gx-services-tab-categories-mobile">
                    <Tabs className="gx-services-tab-categories">
                      {this.state.categories.map((category, index) => (
                        <TabPane
                          tab={
                            <div
                              className="gx-sidebar-item"
                              onClick={() =>
                                this.setState({
                                  current_category: category
                                })
                              }
                            >
                              <IntlMessages id={category} />
                            </div>
                          }
                          key={index}
                        ></TabPane>
                      ))}
                    </Tabs>
                  </div>
                  <Widget styleName="gx-card-full gx-services-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
                    <div className="gx-services-content-header-mobile">
                      <span className="gx-fs-lg gx-font-weight-medium">
                        {this.getMaterialCountByCategory(
                          this.state.current_category
                        ) > 1
                          ? this.getMaterialCountByCategory(
                              this.state.current_category
                            ) + " Material items"
                          : this.getMaterialCountByCategory(
                              this.state.current_category
                            ) == 1
                          ? this.getMaterialCountByCategory(
                              this.state.current_category
                            ) + " Material item"
                          : "No available material items"}
                      </span>
                    </div>
                    <div className="gx-services-content">
                      <div className="gx-services-sidebar">
                        <div className="gx-services-sidebar-header gx-px-20 gx-flex-row gx-align-items-center gx-custom-flex-justify-content-space-between">
                          <Popover
                            overlayClassName="popover-add-category"
                            placement="bottomRight"
                            visible={this.state.isVisibleAddCategoryPopover}
                            onVisibleChange={
                              this.onVisibleAddCategoryPopoverChange
                            }
                            content={
                              <div className="popover-add-category-content">
                                <div className="gx-popover-title">
                                  <span>
                                    <IntlMessages id="sales.materials.add_new_category" />
                                  </span>
                                </div>
                                <div className="gx-popover-category">
                                  <label>
                                    <IntlMessages id="sales.materials.category_name" />
                                  </label>
                                  <Input
                                    className="gx-category-input"
                                    style={{ marginTop: "10px" }}
                                    value={this.state.new_category}
                                    onChange={this.onNewCategoryChange}
                                  ></Input>
                                </div>
                                <div className="gx-popover-actions">
                                  <Button
                                    type="default"
                                    className="gx-btn-cancel-category"
                                    onClick={() =>
                                      this.onVisibleAddCategoryPopoverChange(
                                        false
                                      )
                                    }
                                  >
                                    <IntlMessages id="sales.materials.cancel" />
                                  </Button>
                                  <Button
                                    type="primary"
                                    className="gx-btn-save-category"
                                    onClick={this.saveNewCategory}
                                  >
                                    <IntlMessages id="sales.materials.save" />
                                  </Button>
                                </div>
                              </div>
                            }
                            trigger="click"
                          >
                            <Button
                              type="primary"
                              className="gx-btn-new-category gx-m-0 gx-flex-row gx-align-items-center gx-flex-nowrap"
                            >
                              <i
                                className="material-icons"
                                style={{ marginRight: "5px" }}
                              >
                                add
                              </i>
                              <IntlMessages id="sales.materials.new_category" />
                            </Button>
                          </Popover>
                          <Button
                            onClick={() =>
                              this.setState({
                                isCategoryEditable: !this.state
                                  .isCategoryEditable
                              })
                            }
                            type="default"
                            className="gx-btn-edit-category gx-m-0 gx-p-0 gx-flex-row gx-align-items-center gx-custom-flex-justify-content-center"
                          >
                            <i className="material-icons icon-action">create</i>
                          </Button>
                        </div>
                        <div className="gx-services-sidebar-body">
                          <div className="gx-services-sidebar-body-scroll">
                            <Menu
                              defaultSelectedKeys={["0"]}
                              mode="inline"
                              theme="light"
                            >
                              {this.state.categories.map((category, index) => (
                                <Menu.Item
                                  key={index}
                                  onClick={() =>
                                    this.setState({
                                      current_category: category
                                    })
                                  }
                                >
                                  <div className="gx-sidebar-item">
                                    <span className="gx-category-name">
                                      {category}
                                    </span>
                                    {!this.state.isCategoryEditable ? (
                                      <div className="gx-category-count">
                                        {this.getMaterialCountByCategory(
                                          category
                                        )}
                                      </div>
                                    ) : (
                                      <div className="gx-category-action">
                                        <Popover
                                          overlayClassName="popover-category-action popover-add-category popover-edit-category popover-delete-category"
                                          placement="bottom"
                                          visible={
                                            this.state
                                              .isVisibleCategoryActionPopover[
                                              index
                                            ]
                                          }
                                          onVisibleChange={visible =>
                                            this.onVisibleCategoryActionPopoverChange(
                                              visible,
                                              index
                                            )
                                          }
                                          content={
                                            this.state
                                              .isVisibleEditCategoryPopover[
                                              index
                                            ] == true ? (
                                              <div className="popover-edit-category-content">
                                                <div className="gx-popover-title">
                                                  <span>
                                                    <IntlMessages id="sales.materials.edit_category" />
                                                  </span>
                                                </div>
                                                <div className="gx-popover-category">
                                                  <label>
                                                    <IntlMessages id="sales.materials.category_name" />
                                                  </label>
                                                  <Input
                                                    className="gx-category-input"
                                                    style={{
                                                      marginTop: "10px"
                                                    }}
                                                    value={
                                                      this.state
                                                        .updated_category
                                                    }
                                                    onChange={
                                                      this
                                                        .onUpdateCategoryChange
                                                    }
                                                  ></Input>
                                                </div>
                                                <div className="gx-popover-actions">
                                                  <Button
                                                    type="default"
                                                    className="gx-btn-cancel-category"
                                                    onClick={() =>
                                                      this.onVisibleEditCategoryPopoverChange(
                                                        false,
                                                        index
                                                      )
                                                    }
                                                  >
                                                    <IntlMessages id="sales.materials.cancel" />
                                                  </Button>
                                                  <Button
                                                    type="primary"
                                                    className="gx-btn-save-category"
                                                    onClick={() =>
                                                      this.updateCategory(index)
                                                    }
                                                  >
                                                    <IntlMessages id="sales.materials.update" />
                                                  </Button>
                                                </div>
                                              </div>
                                            ) : this.state
                                                .isVisibleDeleteCategoryPopover[
                                                index
                                              ] == true ? (
                                              <div className="popover-delete-category-content">
                                                <div className="gx-popover-title">
                                                  <span>
                                                    <IntlMessages id="sales.materials.delete_category" />
                                                  </span>
                                                </div>
                                                <div className="gx-popover-category">
                                                  <label>
                                                    <IntlMessages id="sales.materials.delete.confirm" />
                                                  </label>
                                                  <Input
                                                    className="gx-category-input"
                                                    style={{
                                                      marginTop: "10px"
                                                    }}
                                                    value={
                                                      this.state.categories[
                                                        index
                                                      ]
                                                    }
                                                    disabled={true}
                                                  ></Input>
                                                </div>
                                                <div className="gx-popover-actions">
                                                  <Button
                                                    type="default"
                                                    className="gx-btn-cancel-category"
                                                    onClick={() =>
                                                      this.onVisibleDeleteCategoryPopoverChange(
                                                        false,
                                                        index
                                                      )
                                                    }
                                                  >
                                                    <IntlMessages id="sales.materials.cancel" />
                                                  </Button>
                                                  <Button
                                                    type="danger"
                                                    className="gx-btn-delete-category"
                                                    onClick={() =>
                                                      this.deleteCategory(index)
                                                    }
                                                  >
                                                    <IntlMessages id="sales.materials.delete" />
                                                  </Button>
                                                </div>
                                              </div>
                                            ) : (
                                              <div className="popover-category-action-content">
                                                <div
                                                  className="gx-menuitem"
                                                  onClick={() => {
                                                    this.onVisibleEditCategoryPopoverChange(
                                                      true,
                                                      index
                                                    );
                                                    this.setState({
                                                      updated_category: this
                                                        .state.categories[index]
                                                    });
                                                  }}
                                                >
                                                  <i className="material-icons gx-mr-10">
                                                    edit
                                                  </i>
                                                  <IntlMessages id="sales.materials.edit_category" />
                                                </div>
                                                <div
                                                  className="gx-menuitem"
                                                  onClick={() => {
                                                    this.onVisibleDeleteCategoryPopoverChange(
                                                      true,
                                                      index
                                                    );
                                                  }}
                                                >
                                                  <i className="material-icons gx-mr-10">
                                                    delete_sweep
                                                  </i>
                                                  <IntlMessages id="sales.materials.delete_category" />
                                                </div>
                                              </div>
                                            )
                                          }
                                          trigger="click"
                                        >
                                          <i className="material-icons gx-icon-action">
                                            more_horiz
                                          </i>
                                        </Popover>
                                      </div>
                                    )}
                                  </div>
                                </Menu.Item>
                              ))}
                            </Menu>
                          </div>
                        </div>
                      </div>
                      <div className="gx-services-body gx-w-100">
                        <div className="gx-service-body-content-header">
                          <div className="gx-service-body-content-header-desktop">
                            {/* <div
                              className="gx-service-by-hours"
                              style={{ width: "200px" }}
                            >
                              <Select
                                className="gx-w-100"
                                suffixIcon={
                                  <i className="material-icons">expand_more</i>
                                }
                                placeholder={
                                  <IntlMessages id="sales.materials.materials_by_hours" />
                                }
                              >
                                <Option value="1">1 hour</Option>
                                <Option value="2">2 hours</Option>
                                <Option value="3">3 hours</Option>
                                <Option value="4">4 hours</Option>
                                <Option value="5">5 hours</Option>
                              </Select>
                            </div> */}
                            <span className="gx-fs-lg gx-font-weight-medium">
                              {this.getMaterialCountByCategory(
                                this.state.current_category
                              ) > 1
                                ? this.getMaterialCountByCategory(
                                    this.state.current_category
                                  ) + " Material items"
                                : this.getMaterialCountByCategory(
                                    this.state.current_category
                                  ) == 1
                                ? this.getMaterialCountByCategory(
                                    this.state.current_category
                                  ) + " Material item"
                                : "No available material items"}
                            </span>
                            <div className="gx-flex-row gx-flex-nowrap">
                              <SearchBox
                                styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-services-search-bar gx-mr-10"
                                placeholder="Search materials"
                                onChange={this.filterBySearchText}
                                value={this.state.searchText}
                              />
                              <Button
                                type="primary"
                                className="gx-m-0 gx-flex-row gx-flex-nowrap gx-align-items-center"
                                onClick={() =>
                                  this.onVisibleAddMaterialModalChange(true)
                                }
                              >
                                <i
                                  className="material-icons"
                                  style={{ fontSize: "20px" }}
                                >
                                  add
                                </i>
                                <IntlMessages id="sales.materials.new_material" />
                              </Button>
                            </div>
                          </div>
                        </div>
                        <div className="gx-service-body-content-body ">
                          <Row>
                            {filter_materials.map((material, index) => (
                              <Col
                                span={6}
                                xs={24}
                                sm={12}
                                md={12}
                                lg={12}
                                xl={8}
                                xxl={6}
                                key={index}
                              >
                                <Widget styleName="gx-service-card gx-card-full">
                                  <div className="gx-service-card-body">
                                    <div className="gx-service-card-image gx-material-card-image">
                                      {material.image != "" ? (
                                        <img
                                          src={require("assets/images/material/" +
                                            material.image)}
                                        />
                                      ) : (
                                        <img
                                          src={require("assets/images/material/material-default.png")}
                                        />
                                      )}
                                    </div>
                                    <div className="gx-p-20">
                                      <div className="gx-service-card-name">
                                        {material.name}
                                      </div>
                                      <div className="gx-service-card-desc">
                                        {material.desc}
                                      </div>
                                      <div className="gx-service-card-tags">
                                        {material.tags.map((tag, index) => (
                                          <Tag key={index}>{tag}</Tag>
                                        ))}
                                      </div>
                                    </div>
                                  </div>
                                  <div className="gx-service-card-footer">
                                    <div className="gx-flex-row gx-align-items-center">
                                      <span
                                        style={{
                                          fontSize: "13px",
                                          color: "#9399a2",
                                          marginRight: "10px"
                                        }}
                                      >
                                        Unit price:{" "}
                                      </span>
                                      <span className="gx-service-card-price">
                                        ${parseFloat(material.price).toFixed(2)}
                                      </span>
                                    </div>
                                    <Popover
                                      overlayClassName="gx-popover-material-action"
                                      placement="right"
                                      trigger="click"
                                      visible={
                                        this.state
                                          .isMaterialActionPopoverVisible[index]
                                      }
                                      onVisibleChange={visible =>
                                        this.onMaterialActionPopoverVisibleChange(
                                          index,
                                          visible
                                        )
                                      }
                                      content={
                                        this.state.isMaterialEditPopoverVisible[
                                          index
                                        ] ? (
                                          <div />
                                        ) : this.state
                                            .isMaterialMovePopoverVisible[
                                            index
                                          ] ? (
                                          <div className="gx-popover-material-content gx-popover-material-move-content">
                                            <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                              <i
                                                className="gx-size-30 gx-mr-10 material-icons"
                                                style={{
                                                  color: "#f7c43d",
                                                  fontSize: "30px"
                                                }}
                                              >
                                                directions
                                              </i>
                                              <div className="gx-flex-column">
                                                <span
                                                  style={{
                                                    fontSize: "15px",
                                                    color: "#4c586d",
                                                    fontWeight: "bold"
                                                  }}
                                                >
                                                  Move this material
                                                </span>
                                                <span
                                                  style={{
                                                    fontSize: "13px",
                                                    color: "#9399a2",
                                                    fontWeight: "500"
                                                  }}
                                                >
                                                  Move to different category
                                                </span>
                                              </div>
                                            </div>
                                            <div
                                              className="gx-popover-body"
                                              style={{ marginTop: "15px" }}
                                            >
                                              <Select
                                                className="gx-w-100"
                                                placeholder="Select a category"
                                                suffixIcon={
                                                  <i className="material-icons">
                                                    expand_more
                                                  </i>
                                                }
                                                value={
                                                  this.state
                                                    .popover_selected_category
                                                }
                                                onChange={value =>
                                                  this.setState({
                                                    popover_selected_category: value
                                                  })
                                                }
                                              >
                                                {this.state.categories.map(
                                                  (cat, index) => {
                                                    if (cat != "All materials")
                                                      return (
                                                        <Option
                                                          value={cat}
                                                          key={index}
                                                        >
                                                          {cat}
                                                        </Option>
                                                      );
                                                  }
                                                )}
                                              </Select>
                                            </div>
                                            <div
                                              className="gx-popover-action"
                                              style={{ marginTop: "15px" }}
                                            >
                                              <Button
                                                type="default"
                                                className="gx-btn-cancel"
                                                onClick={() =>
                                                  this.onMaterialMovePopoverVisibleChange(
                                                    index,
                                                    false
                                                  )
                                                }
                                              >
                                                Cancel
                                              </Button>
                                              <Button
                                                type="primary"
                                                className="gx-btn-confirm"
                                                onClick={() => {
                                                  this.onMaterialMovePopoverVisibleChange(
                                                    index,
                                                    false
                                                  );
                                                  this.updateMaterialCategory(
                                                    material.id
                                                  );
                                                }}
                                              >
                                                Confirm
                                              </Button>
                                            </div>
                                          </div>
                                        ) : this.state
                                            .isMaterialDeletePopoverVisible[
                                            index
                                          ] ? (
                                          <div className="gx-popover-material-content gx-popover-material-delete-content">
                                            <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                              <i
                                                className="gx-size-30 gx-mr-10 material-icons"
                                                style={{
                                                  color: "#fc6262",
                                                  fontSize: "30px"
                                                }}
                                              >
                                                warning
                                              </i>
                                              <div className="gx-flex-column">
                                                <span
                                                  style={{
                                                    fontSize: "15px",
                                                    color: "#4c586d",
                                                    fontWeight: "bold"
                                                  }}
                                                >
                                                  Delete this material
                                                </span>
                                                <span
                                                  style={{
                                                    fontSize: "13px",
                                                    color: "#9399a2",
                                                    fontWeight: "500"
                                                  }}
                                                >
                                                  This action can't be undone
                                                </span>
                                              </div>
                                            </div>
                                            <div
                                              className="gx-popover-action"
                                              style={{ marginTop: "15px" }}
                                            >
                                              <Button
                                                type="default"
                                                className="gx-btn-cancel"
                                                onClick={() =>
                                                  this.onMaterialDeletePopoverVisibleChange(
                                                    index,
                                                    false
                                                  )
                                                }
                                              >
                                                Cancel
                                              </Button>
                                              <Button
                                                type="primary"
                                                className="gx-btn-confirm"
                                                onClick={() => {
                                                  this.onMaterialDeletePopoverVisibleChange(
                                                    index,
                                                    false
                                                  );
                                                  this.onDeleteMaterial(
                                                    material.id
                                                  );
                                                  this.onMaterialActionPopoverVisibleChange(
                                                    index,
                                                    false
                                                  );
                                                }}
                                              >
                                                Confirm
                                              </Button>
                                            </div>
                                          </div>
                                        ) : (
                                          <div>
                                            <div
                                              className="gx-menuitem"
                                              onClick={() =>
                                                this.onMaterialEditPopoverVisibleChange(
                                                  index,
                                                  true
                                                )
                                              }
                                            >
                                              <i className="material-icons">
                                                edit
                                              </i>
                                              <span>Edit material</span>
                                            </div>
                                            <div
                                              className="gx-menuitem"
                                              onClick={() => {
                                                this.onMaterialMovePopoverVisibleChange(
                                                  index,
                                                  true
                                                );
                                                this.setState({
                                                  popover_selected_category:
                                                    material.category
                                                });
                                              }}
                                            >
                                              <i className="material-icons">
                                                directions
                                              </i>
                                              <span>Move material</span>
                                            </div>
                                            <div
                                              className="gx-menuitem"
                                              onClick={() => {
                                                this.onMaterialDeletePopoverVisibleChange(
                                                  index,
                                                  true
                                                );
                                                this.setState({
                                                  popover_selected_category: ""
                                                });
                                              }}
                                            >
                                              <i className="material-icons">
                                                delete
                                              </i>
                                              <span>Delete material</span>
                                            </div>
                                          </div>
                                        )
                                      }
                                    >
                                      <i className="material-icons gx-icon-action">
                                        more_vert
                                      </i>
                                    </Popover>
                                  </div>
                                </Widget>
                              </Col>
                            ))}
                          </Row>
                        </div>
                      </div>
                    </div>
                  </Widget>
                  <Modal
                    className="modal-add-service"
                    title={
                      <div className="gx-flex-row gx-align-items-center gx-custom-flex-justify-content-space-between">
                        <IntlMessages id="sales.materials.add_new_material_item" />
                        <i
                          className="material-icons gx-icon-action"
                          onClick={() =>
                            this.onVisibleAddMaterialModalChange(false)
                          }
                        >
                          clear
                        </i>
                      </div>
                    }
                    closable={false}
                    centered
                    visible={this.state.isVisibleAddMaterialModal}
                    footer={[null, null]}
                    onCancel={() => this.onVisibleAddMaterialModalChange(false)}
                  >
                    <div className="service-body">
                      <div className="service-details">
                        <div className="service-details-control">
                          <IntlMessages id="sales.materials.choose_category" />
                          <Select
                            className="gx-w-100"
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            placeholder={
                              <IntlMessages id="sales.materials.choose_category" />
                            }
                            onChange={this.onNewMaterialCategoryChange}
                          >
                            {this.state.categories.map((category, index) => (
                              <Option key={index} value={category}>
                                {category}
                              </Option>
                            ))}
                          </Select>
                        </div>
                        <div className="service-details-control">
                          <IntlMessages id="sales.materials.title" />
                          <Input
                            placeholder={formatMessage({
                              id: "sales.materials.title"
                            })}
                            onChange={this.onNewMaterialNameChange}
                          />
                        </div>
                        <div className="service-details-control">
                          <IntlMessages id="sales.materials.description" />
                          <TextArea
                            rows="4"
                            placeholder={formatMessage({
                              id: "sales.materials.max_120_characters"
                            })}
                            maxLength={120}
                            onChange={this.onNewMaterialDescChange}
                          />
                        </div>
                        <div className="service-details-control">
                          <IntlMessages id="sales.materials.pricing" />
                          <div className="service-details-control service-details-pricing">
                            <Input
                              placeholder={formatMessage({
                                id: "sales.materials.enter_amount"
                              })}
                              onChange={this.onNewMaterialPriceChange}
                            />
                            <Select
                              defaultValue="per_unit"
                              suffixIcon={
                                <i className="material-icons">expand_more</i>
                              }
                              onChange={this.onNewMaterialUnitChange}
                            >
                              <Option value="per_unit">Per unit</Option>
                            </Select>
                          </div>
                        </div>
                        <div className="service-details-control">
                          <div className="service-details-control">
                            <IntlMessages id="sales.materials.material_type" />
                            <div className="gx-w-100 gx-flex-row">
                              <Input
                                className="service-details-type gx-mb-10"
                                placeholder={formatMessage({
                                  id: "sales.materials.model"
                                })}
                                onChange={this.onNewMaterialModelChange}
                                style={{ marginRight: "10px" }}
                              ></Input>
                              <div
                                className="service-details-taxable gx-mb-10 gx-flex-row gx-align-items-center"
                                style={{
                                  justifyContent: "center"
                                }}
                              >
                                <ButtonGroup className="gx-custom-toggle-buttons">
                                  <Button
                                    className={`gx-btn-toggle ${
                                      this.state.selected_taxable
                                        ? "gx-btn-toggle-yes"
                                        : ""
                                    }`}
                                    size="small"
                                    onClick={() => this.onTaxableChange(true)}
                                  >
                                    {this.state.selected_taxable ? (
                                      <span>Yes</span>
                                    ) : (
                                      <ToggleButton></ToggleButton>
                                    )}
                                  </Button>
                                  <Button
                                    className={`gx-btn-toggle ${
                                      !this.state.selected_taxable
                                        ? "gx-btn-toggle-no"
                                        : ""
                                    }`}
                                    size="small"
                                    onClick={() => this.onTaxableChange(false)}
                                  >
                                    {!this.state.selected_taxable ? (
                                      <span>No</span>
                                    ) : (
                                      <ToggleButton></ToggleButton>
                                    )}
                                  </Button>
                                </ButtonGroup>
                                <IntlMessages id="sales.materials.taxable" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="service-image">
                        <Widget styleName="gx-card-full">
                          <div className="gx-p-20">
                            <div
                              className="gx-service-image gx-flex-row gx-align-items-center"
                              style={{
                                backgroundColor: "#f0f0f0",
                                justifyContent: "center",
                                height: "120px"
                              }}
                            >
                              <img
                                src={require("assets/images/material/material-default.png")}
                              />
                            </div>
                            <Button
                              type="default"
                              className="gx-btn-change-icon gx-my-10 gx-w-100"
                            >
                              <IntlMessages id="sales.materials.change_icon" />
                            </Button>
                            <Dragger>
                              <div
                                className="gx-flex-column gx-justify-content-center gx-align-items-center"
                                style={{ color: "#a5abb5" }}
                              >
                                <img
                                  style={{ marginRight: "15px" }}
                                  src={require("assets/images/upload.png")}
                                />
                                <div className="gx-fs-13-20 gx-font-weight-medium gx-mt-10">
                                  <IntlHtmlMessages id="sales.materials.dragdrop" />
                                </div>
                              </div>
                            </Dragger>
                          </div>
                        </Widget>
                      </div>
                    </div>
                    <div className="service-footer">
                      <div className="service-details-action">
                        <Button
                          type="default"
                          className="gx-btn-cancel"
                          onClick={() =>
                            this.setState({ isVisibleAddMaterialModal: false })
                          }
                        >
                          <IntlMessages id="sales.materials.cancel" />
                        </Button>
                        <Button
                          type="primary"
                          className="gx-btn-save"
                          onClick={this.addNewMaterial}
                        >
                          <IntlMessages id="sales.materials.save" />
                        </Button>
                      </div>
                    </div>
                  </Modal>
                  <Modal
                    className="modal-add-category"
                    closable={false}
                    centered
                    visible={this.state.isVisibleAddCategoryModal}
                    footer={[null, null]}
                    onCancel={() => this.onVisibleAddCategoryModalChange(false)}
                  >
                    <div className="modal-add-category-content">
                      <div className="gx-category-title">
                        <span>
                          <IntlMessages id="sales.materials.add_new_category" />
                        </span>
                      </div>
                      <div className="gx-category-form">
                        <label>
                          <IntlMessages id="sales.materials.category_name" />
                        </label>
                        <Input
                          placeholder={formatMessage({
                            id: "sales.materials.enter_name"
                          })}
                          className="gx-category-input"
                          style={{ marginTop: "10px" }}
                          value={this.state.new_category}
                          onChange={this.onNewCategoryChange}
                        />
                      </div>
                      <div className="gx-category-actions">
                        <Button
                          type="default"
                          className="gx-btn-cancel-category"
                          onClick={() =>
                            this.onVisibleAddCategoryModalChange(false)
                          }
                        >
                          <IntlMessages id="sales.materials.cancel" />
                        </Button>
                        <Button
                          type="primary"
                          className="gx-btn-save-category"
                          onClick={this.saveNewCategory}
                        >
                          <IntlMessages id="sales.materials.save" />
                        </Button>
                      </div>
                    </div>
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(Materials);
