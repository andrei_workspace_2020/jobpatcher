import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Input, Select, Card, Row, Col, Button, Popover, Collapse} from "antd";
import { Link } from "react-router-dom";
import IntlMessages from "util/IntlMessages";
import { type } from "os";

const { Panel } = Collapse;
const { Option } = Select;

class ActiveEstimates extends Component {
  state = {
    new_estimates: [
      {
        name: "Mark Zoddila",
        estimate: "#00532",
        amount: 500.0,
        created: "6 Auguest-9:37 AM"
      },
      {
        name: "Mark Zoddila",
        estimate: "#00532",
        amount: 500.0,
        created: "6 Auguest-9:37 AM"
      },
      {
        name: "Mark Zoddila",
        estimate: "#00532",
        amount: 500.0,
        created: "6 Auguest-9:37 AM"
      },
      {
        name: "Mark Zoddila",
        estimate: "#00532",
        amount: 500.0,
        created: "6 Auguest-9:37 AM"
      }
    ],
    scheduled: [
      {
        name: "Peter Jonson",
        estimate: "#00538",
        amount: 500.0,
        scheduled: "8 Auguest-14:30 PM"
      },
      {
        name: "Peter Jonson",
        estimate: "#00538",
        amount: 500.0,
        scheduled: "8 Auguest-14:30 PM"
      },
      {
        name: "Peter Jonson",
        estimate: "#00538",
        amount: 500.0,
        scheduled: "8 Auguest-14:30 PM"
      },
      {
        name: "Peter Jonson",
        estimate: "#00538",
        amount: 500.0,
        scheduled: "8 Auguest-14:30 PM"
      }
    ],
    under_review: [
      {
        name: "Kevin Windberg",
        estimate: "#00526",
        amount: 500.0,
        sent_on: "5 Auguest-13:25 PM"
      },
      {
        name: "Kevin Windberg",
        estimate: "#00526",
        amount: 500.0,
        sent_on: "5 Auguest-13:25 PM"
      }
    ],
    accepted_estimates: [
      {
        name: "Mark Boldzone",
        estimate: "#00537",
        amount: 500.0,
        accepted: "7 Auguest-15:43 PM"
      },
      {
        name: "Mark Boldzone",
        estimate: "#00537",
        amount: 500.0,
        accepted: "7 Auguest-15:43 PM"
      },
      {
        name: "Mark Boldzone",
        estimate: "#00537",
        amount: 500.0,
        accepted: "7 Auguest-15:43 PM"
      }
    ],
    width: window.innerWidth,
    activePanelKey: window.innerWidth<768? ["1"] : ["1", "2"],
    isNewEstimateActionPopoverVisible: [],
    isNewEstimateEditPopoverVisible: [],
    isNewEstimateRefusePopoverVisible: [],
    isNewEstimateDeletePopoverVisible: [],
    isScheduledActionPopoverVisible: [],
    isScheduledEditPopoverVisible: [],
    isScheduledDeletePopoverVisible: [],
    isUnderReviewActionPopoverVisible: [],
    isUnderReviewEditPopoverVisible: [],
    isUnderReviewDeletePopoverVisible: [],
    isAcceptedEstimatesActionPopoverVisible: [],
    isAcceptedEstimatesEditPopoverVisible: [],
    isAcceptedEstimatesDeletePopoverVisible: [],

  };
  componentDidMount(){
    this.setState({ width: window.innerWidth });
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);

    let array = [];
    for(let i=0; i<this.state.new_estimates.length; i++) array[i] = false;
    this.setState({isNewEstimateActionPopoverVisible: [...array]});
    this.setState({isNewEstimateEditPopoverVisible: [...array]});
    this.setState({isNewEstimateRefusePopoverVisible: [...array]});
    this.setState({isNewEstimateDeletePopoverVisible: [...array]});
  }
  updateDimensions = () => {
    this.setState({width: window.innerWidth});
  };
  startNewEstimate = (e, index) => {
    var array = this.state.new_estimates;
    var obj = array[index];
    array.splice(index, 1);
    this.setState({ new_estimates: array });
    this.setState({
      under_review: [
        ...this.state.under_review,
        {
          name: obj.name,
          estimate: obj.estimate,
          amount: obj.amount,
          sent_on: obj.created
        }
      ]
    });
  };
  startScheduledEstimate = (e, index) => {
    var array = this.state.scheduled;
    var obj = array[index];
    array.splice(index, 1);
    this.setState({ scheduled: array });
    this.setState({
      under_review: [
        ...this.state.under_review,
        {
          name: obj.name,
          estimate: obj.estimate,
          amount: obj.amount,
          sent_on: obj.scheduled
        }
      ]
    });
  };
  getNewEstimateTotal = () => {
    var total = 0;
    if (this.state.new_estimates.length > 0) {
      for (var i = 0; i < this.state.new_estimates.length; i++)
        total += this.state.new_estimates[i].amount;
    }
    return total.toFixed(2);
  };
  getScheduledTotal = () => {
    var total = 0;
    if (this.state.scheduled.length > 0) {
      for (var i = 0; i < this.state.scheduled.length; i++)
        total += this.state.scheduled[i].amount;
    }
    return total.toFixed(2);
  };
  getUnderReviewTotal = () => {
    var total = 0;
    if (this.state.under_review.length > 0) {
      for (var i = 0; i < this.state.under_review.length; i++)
        total += this.state.under_review[i].amount;
    }
    return total.toFixed(2);
  };
  getAcceptedEstimateTotal = () => {
    var total = 0;
    if (this.state.accepted_estimates.length > 0) {
      for (var i = 0; i < this.state.accepted_estimates.length; i++)
        total += this.state.accepted_estimates[i].amount;
    }
    return total.toFixed(2);
  };
  onCollapseChange = key => {
    let added = 0;
    let element;
    let temp = [];
    if(this.state.activePanelKey.length < key.length){
      added = 1;
      temp = key;
    } else{
      added = -1;
      temp = this.state.activePanelKey;
    }
    for(let i=0; i<temp.length; i++)
      if(!(key.includes(temp[i]) && this.state.activePanelKey.includes(temp[i])))
        element = temp[i];
    if(this.state.width >= 768){
      if(added==1 && (element==1 || element==2))
        this.setState({ activePanelKey: ["1", "2"] });
      else if(added==-1 && (element==1 || element==2))
        this.setState({ activePanelKey: ["3", "4"] });
      else if(added==1 && (element==3 || element == 4))
        this.setState({ activePanelKey: ["3", "4"] });
      else if(added==-1 && (element==3 || element == 4))
        this.setState({ activePanelKey: ["1", "2"] });
    }
  };
  onNewEstimateActionPopoverVisibleChange = (index, visible) =>{
    let temp = this.state.isNewEstimateActionPopoverVisible;
    temp[index] = visible;
    this.setState({isNewEstimateActionPopoverVisible: temp});
  }
  onNewEstimateEditPopoverVisibleChange = (index, visible) =>{
    let temp = this.state.isNewEstimateEditPopoverVisible;
    temp[index] = visible;
    this.setState({isNewEstimateEditPopoverVisible: temp});
  }
  onNewEstimateRefusePopoverVisibleChange = (index, visible) =>{
    let temp = this.state.isNewEstimateRefusePopoverVisible;
    temp[index] = visible;
    this.setState({isNewEstimateRefusePopoverVisible: temp});
  }
  onNewEstimateDeletePopoverVisibleChange = (index, visible) =>{
    let temp = this.state.isNewEstimateDeletePopoverVisible;
    temp[index] = visible;
    this.setState({isNewEstimateDeletePopoverVisible: temp});
  }
  onDeleteNewEstimate = (index)=>{
    let temp = this.state.new_estimates;
    temp.splice(index, 1);
    this.setState({new_estimates: temp});
  }
  render() {
    return (
      <div>
        <div className="gx-panel-estimate-content gx-flex-row gx-justify-content-between">
          <Row className="gx-m-0 gx-w-100">
            <Col
              span={6}
              xs={24}
              sm={6}
              md={6}
              lg={6}
              xl={6}
              xxl={6}
              className="gx-p-0"
            >
              <Card className="gx-card gx-card-estimates gx-card-estimates-new-estimates">
                <div className="card-header">
                  <div className="card-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="card-header-mark card-header-mark-new-estimates"></span>
                    <div>
                      <div className="card-title">New estimates</div>
                      <div className="card-total-value">
                        <span>{this.state.new_estimates.length} estimate</span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getNewEstimateTotal()} value</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  {this.state.new_estimates.map((item, index) => (
                    <Card
                      key={index}
                      className="gx-card gx-child-card-estimates gx-child-card-estimates-new-estimates"
                    >
                      <div className="child-card-body">
                        <div className="user-details">
                          <div className="user-details-top">
                            <div className="user-avatar">
                              <i className="material-icons">account_circle</i>
                              <div className="gx-flex-column">
                                <Link to="#">
                                  <span className="user-avatar-name">
                                    {item.name}
                                  </span>
                                </Link>
                                <div className="user-estimate-details">
                                  <span>{"$"+item.amount}</span>
                                  <span className="dot">
                                    <i className="material-icons">lens</i>
                                  </span>
                                  <span>{item.created}</span>
                                </div>
                              </div>
                            </div>
                            <Popover
                              overlayClassName="gx-estimate-popover"
                              placement="bottom"
                              visible={this.state.isNewEstimateActionPopoverVisible[index]}
                              onVisibleChange = {(visible)=>this.onNewEstimateActionPopoverVisibleChange(index, visible)}
                              content={
                                this.state.isNewEstimateEditPopoverVisible[index]
                                ? <div/>
                                : this.state.isNewEstimateRefusePopoverVisible[index]
                                ? (
                                  <div className="gx-popover-estimate-content gx-popover-new-estimate-refuse-content">
                                    <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                      <i className="gx-size-30 gx-mr-10 material-icons" style={{color: "#f7c43d", fontSize: "30px"}}>pan_tool</i>
                                      <div className="gx-flex-column">
                                        <span style={{fontSize: "15px", color: "#4c586d", fontWeight: "bold"}}>Mark as refused</span>
                                        <span style={{fontSize: "13px", color: "#9399a2", fontWeight: "500"}}>Please select a reason</span>
                                      </div>
                                    </div>
                                    <div className="gx-popover-body" style={{marginTop: "15px"}}>
                                      <Select
                                        className="gx-refused-reason gx-w-100"
                                        placeholder="Select a reason"
                                        suffixIcon={
                                          <i className="material-icons">expand_more</i>
                                        }
                                      >
                                        <Option value="provider">Chosen another provider</Option>
                                        <Option value="price">Price is too high</Option>
                                      </Select>
                                    </div>
                                    <div className="gx-popover-action" style={{marginTop: "15px"}}>
                                      <Button
                                        type="default"
                                        className="gx-btn-cancel"
                                        onClick={()=>this.onNewEstimateRefusePopoverVisibleChange(index, false)}
                                      >
                                        Cancel
                                      </Button>
                                      <Button
                                        type="primary"
                                        className="gx-btn-confirm"
                                        onClick={()=>this.onNewEstimateRefusePopoverVisibleChange(index, false)}
                                      >
                                        Confirm
                                      </Button>
                                    </div>
                                  </div>
                                  )
                                : this.state.isNewEstimateDeletePopoverVisible[index]
                                ? (
                                  <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                                    <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                      <i className="gx-size-30 gx-mr-10 material-icons" style={{color: "#fc6262", fontSize: "30px"}}>warning</i>
                                      <div className="gx-flex-column">
                                        <span style={{fontSize: "15px", color: "#4c586d", fontWeight: "bold"}}>Delete this estimate</span>
                                        <span style={{fontSize: "13px", color: "#9399a2", fontWeight: "500"}}>This action can't be undone</span>
                                      </div>
                                    </div>
                                    <div className="gx-popover-action" style={{marginTop: "15px"}}>
                                      <Button
                                        type="default"
                                        className="gx-btn-cancel"
                                        onClick={()=>this.onNewEstimateDeletePopoverVisibleChange(index, false)}
                                      >
                                        Cancel
                                      </Button>
                                      <Button
                                        type="primary"
                                        className="gx-btn-confirm"
                                        onClick={()=>{
                                          this.onNewEstimateDeletePopoverVisibleChange(index, false);
                                          this.onDeleteNewEstimate(index);
                                          this.onNewEstimateActionPopoverVisibleChange(index, false);
                                        }}
                                      >
                                        Confirm
                                      </Button>
                                    </div>
                                  </div>
                                  )
                                : (
                                  <div>
                                    <div
                                      className="gx-menuitem"
                                      onClick={()=>this.onNewEstimateEditPopoverVisibleChange(index, true)}
                                    >
                                      <i className="material-icons">edit</i>
                                      <span>Edit estimate</span>
                                    </div>
                                    <div
                                      className="gx-menuitem"
                                      onClick={()=>this.onNewEstimateRefusePopoverVisibleChange(index, true)}
                                    >
                                      <i className="material-icons">pan_tool</i>
                                      <span>Refuse estimate</span>
                                    </div>
                                    <div
                                      className="gx-menuitem"
                                      onClick={()=>this.onNewEstimateDeletePopoverVisibleChange(index, true)}
                                    >
                                      <i className="material-icons">delete</i>
                                      <span>Delete estiamte</span>
                                    </div>
                                  </div>
                                  )
                              }
                              trigger="click"
                            >
                              <Button className="btn-user-more">
                                <i className="material-icons">more_vert</i>
                              </Button>
                            </Popover>
                          </div>
                          {/* <div className="user-details-body">
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Estimate
                              </div>
                              <div
                                className="user-details-child-item-value"
                                style={{ color: "#257cde" }}
                              >
                                {item.estimate}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Amount
                              </div>
                              <div className="user-details-child-item-value">
                                {item.amount}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Created
                              </div>
                              <div className="user-details-child-item-value">
                                {item.created}
                              </div>
                            </div>
                          </div> */}
                        </div>
                      </div>
                      <div className="child-card-footer">
                        <div
                          className="action action-start-estimate"
                          onClick={e => this.startNewEstimate(e, index)}
                        >
                          Start estimate
                        </div>
                        <div className="action action-schedule-estimate">
                          Schedule now
                        </div>
                      </div>
                    </Card>
                  ))}
                </div>
              </Card>
            </Col>
            <Col
              span={6}
              xs={24}
              sm={6}
              md={6}
              lg={6}
              xl={6}
              xxl={6}
              className="gx-p-0"
            >
              <Card className="gx-card gx-card-estimates gx-card-estimates-scheduled">
                <div className="card-header">
                  <div className="card-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="card-header-mark card-header-mark-scheduled"></span>
                    <div>
                      <div className="card-title">Scheduled</div>
                      <div className="card-total-value">
                        <span>{this.state.scheduled.length} estimate</span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getScheduledTotal()} value</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  {this.state.scheduled.map((item, index) => (
                    <Card
                      key={index}
                      className="gx-card gx-child-card-estimates gx-child-card-estimates-scheduled"
                    >
                      <div className="child-card-body">
                        <div className="user-details">
                          <div className="user-details-top">
                            <div className="user-avatar">
                              <i className="material-icons">account_circle</i>
                              <div className="gx-flex-column">
                                <Link to="#">
                                  <span className="user-avatar-name">
                                    {item.name}
                                  </span>
                                </Link>
                                <div className="user-estimate-details">
                                  <span>{"$"+item.amount}</span>
                                  <span className="dot">
                                    <i className="material-icons">lens</i>
                                  </span>
                                  <span>{item.scheduled}</span>
                                </div>
                              </div>
                            </div>
                            <Popover
                              overlayClassName="gx-estimate-popover"
                              placement="bottom"
                              content={
                                <div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">edit</i>
                                    <span>Edit estimate</span>
                                  </div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">pan_tool</i>
                                    <span>Refuse estimate</span>
                                  </div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">delete</i>
                                    <span>Delete estiamte</span>
                                  </div>
                                </div>
                              }
                              trigger="click"
                            >
                              <Button className="btn-user-more">
                                <i className="material-icons">more_vert</i>
                              </Button>
                            </Popover>
                          </div>
                          {/* <div className="user-details-body">
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Estimate
                              </div>
                              <div
                                className="user-details-child-item-value"
                                style={{ color: "#257cde" }}
                              >
                                {item.estimate}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Amount
                              </div>
                              <div className="user-details-child-item-value">
                                {item.amount}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Scheduled
                              </div>
                              <div className="user-details-child-item-value">
                                {item.scheduled}
                              </div>
                            </div>
                          </div> */}
                        </div>
                      </div>
                      <div className="child-card-footer">
                        <div className="action action-home">
                        {
                          index%2==0
                          ? <i className="material-icons">home</i>
                          : <i className="material-icons">phonelink_ring</i>
                        }
                        </div>
                        <div
                          className="action action-start-estimate"
                          onClick={e => this.startScheduledEstimate(e, index)}
                        >
                          Start estimate
                        </div>
                      </div>
                    </Card>
                  ))}
                </div>
              </Card>
            </Col>
            <Col
              span={6}
              xs={24}
              sm={6}
              md={6}
              lg={6}
              xl={6}
              xxl={6}
              className="gx-p-0"
            >
              <Card className="gx-card gx-card-estimates gx-card-estimates-under-review">
                <div className="card-header">
                  <div className="card-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="card-header-mark card-header-mark-under-review"></span>
                    <div>
                      <div className="card-title">Under review</div>
                      <div className="card-total-value">
                        <span>{this.state.under_review.length} estimate</span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getUnderReviewTotal()} value</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  {this.state.under_review.map((item, index) => (
                    <Card
                      key={index}
                      className="gx-card gx-child-card-estimates gx-child-card-estimates-under-review"
                    >
                      <div className="child-card-body">
                        <div className="user-details">
                          <div className="user-details-top">
                            <div className="user-avatar">
                              <i className="material-icons">account_circle</i>
                              <div className="gx-flex-column">
                                <Link to="#">
                                  <span className="user-avatar-name">
                                    {item.name}
                                  </span>
                                </Link>
                                <div className="user-estimate-details">
                                  <span>{"$"+item.amount}</span>
                                  <span className="dot">
                                    <i className="material-icons">lens</i>
                                  </span>
                                  <span>{item.sent_on}</span>
                                </div>
                              </div>
                            </div>
                            <Popover
                              overlayClassName="gx-estimate-popover"
                              placement="bottom"
                              content={
                                <div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">edit</i>
                                    <span>Edit estimate</span>
                                  </div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">pan_tool</i>
                                    <span>Refuse estimate</span>
                                  </div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">delete</i>
                                    <span>Delete estiamte</span>
                                  </div>
                                </div>
                              }
                              trigger="click"
                            >
                              <Button className="btn-user-more">
                                <i className="material-icons">more_vert</i>
                              </Button>
                            </Popover>
                          </div>
                          {/* <div className="user-details-body">
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Estimate
                              </div>
                              <div
                                className="user-details-child-item-value"
                                style={{ color: "#257cde" }}
                              >
                                {item.estimate}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Amount
                              </div>
                              <div className="user-details-child-item-value">
                                {item.amount}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Sent on
                              </div>
                              <div className="user-details-child-item-value">
                                {item.sent_on}
                              </div>
                            </div>
                          </div> */}
                        </div>
                      </div>
                      <div className="child-card-footer">
                        <div className="action action-eye">
                          <i className="material-icons">visibility_off</i>
                        </div>
                        <div className="action-group gx-custom-flex-row">
                          <Row className="gx-m-0 gx-w-100">
                            <Col span={12} className="gx-p-0 gx-m-0">
                              <div className="action action-accepted">
                                Accepted
                              </div>
                            </Col>
                            <Col span={12} className="gx-p-0 gx-m-0">
                              <div className="action action-refused">
                                Refused
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </div>
                    </Card>
                  ))}
                </div>
              </Card>
            </Col>
            <Col
              span={6}
              xs={24}
              sm={6}
              md={6}
              lg={6}
              xl={6}
              xxl={6}
              className="gx-p-0"
            >
              <Card className="gx-card gx-card-estimates gx-card-estimates-accepted-estimates">
                <div className="card-header">
                  <div className="card-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="card-header-mark card-header-mark-accepted-estimates"></span>
                    <div>
                      <div className="card-title">Accepted estimates</div>
                      <div className="card-total-value">
                        <span>
                          {this.state.accepted_estimates.length} estimate
                        </span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getAcceptedEstimateTotal()} value</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-body">
                  {this.state.accepted_estimates.map((item, index) => (
                    <Card
                      key={index}
                      className="gx-card gx-child-card-estimates gx-child-card-estimates-accepted-estimates"
                    >
                      <div className="child-card-body">
                        <div className="user-details">
                          <div className="user-details-top">
                            <div className="user-avatar">
                              <i className="material-icons">account_circle</i>
                              <div className="gx-flex-column">
                                <Link to="#">
                                  <span className="user-avatar-name">
                                    {item.name}
                                  </span>
                                </Link>
                                <div className="user-estimate-details">
                                  <span>{"$"+item.amount}</span>
                                  <span className="dot">
                                    <i className="material-icons">lens</i>
                                  </span>
                                  <span>{item.accepted}</span>
                                </div>
                              </div>
                            </div>
                            <Popover
                              overlayClassName="gx-estimate-popover"
                              placement="bottom"
                              content={
                                <div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">edit</i>
                                    <span>Edit estimate</span>
                                  </div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">pan_tool</i>
                                    <span>Refuse estimate</span>
                                  </div>
                                  <div className="gx-menuitem">
                                    <i className="material-icons">delete</i>
                                    <span>Delete estiamte</span>
                                  </div>
                                </div>
                              }
                              trigger="click"
                            >
                              <Button className="btn-user-more">
                                <i className="material-icons">more_vert</i>
                              </Button>
                            </Popover>
                          </div>
                          {/* <div className="user-details-body">
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Estimate
                              </div>
                              <div
                                className="user-details-child-item-value"
                                style={{ color: "#257cde" }}
                              >
                                {item.estimate}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Amount
                              </div>
                              <div className="user-details-child-item-value">
                                {item.amount}
                              </div>
                            </div>
                            <div className="user-details-child gx-custom-my-5">
                              <div className="user-details-child-item-title">
                                Accepted
                              </div>
                              <div className="user-details-child-item-value">
                                {item.accepted}
                              </div>
                            </div>
                          </div> */}
                        </div>
                      </div>
                      <div className="child-card-footer">
                        <div className="action action-convert">
                          Convert to job
                        </div>
                      </div>
                    </Card>
                  ))}
                </div>
              </Card>
            </Col>
          </Row>
        </div>
        <div className="gx-panel-estimate-mobile-content gx-flex-row">
          <Collapse
            className="gx-w-100"
            accordion={this.state.width<768? true : false}
            activeKey={this.state.activePanelKey}
            onChange={this.onCollapseChange}
            expandIconPosition="right"
            expandIcon={({ isActive }) => (
              <i className="material-icons">{isActive ? "-" : "+"} </i>
            )}
          >
            <Panel
              showArrow={false}
              header={
                <div className="panel-header">
                  <div className="panel-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="panel-header-mark panel-header-mark-new-estimates"></span>
                    <div>
                      <div className="gx-flex-row gx-align-items-center gx-justify-content-between"></div>
                      <div className="panel-title">New estimates</div>
                      <div className="panel-total-value">
                        <span>{this.state.new_estimates.length} estimate</span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getNewEstimateTotal()} value</span>
                      </div>
                    </div>
                  </div>
                  <i className="ant-collapse-arrow">
                    {(this.state.activePanelKey == 1 ||(typeof this.state.activePanelKey=='object' && this.state.activePanelKey.includes("1")))? "-" : "+"}
                  </i>
                </div>
              }
              key="1"
              className="gx-estimate gx-new-estimates"
            >
              {this.state.new_estimates.map((item, index) => (
                <Card
                  key={index}
                  className="gx-card gx-child-card-estimates gx-child-card-estimates-new-estimates"
                >
                  <div className="child-card-body">
                    <div className="user-details">
                      <div className="user-details-top">
                        <div className="user-avatar">
                          <i className="material-icons">account_circle</i>
                          <div className="gx-flex-column">
                            <Link to="#">
                              <span className="user-avatar-name">
                                {item.name}
                              </span>
                            </Link>
                            <div className="user-estimate-details">
                              <span>{"$"+item.amount}</span>
                              <span className="dot">
                                <i className="material-icons">lens</i>
                              </span>
                              <span>{item.created}</span>
                            </div>
                          </div>
                        </div>
                        <Popover
                          overlayClassName="gx-estimate-popover"
                          placement="bottom"
                          content={
                            <div>
                              <div className="gx-menuitem">
                                <i className="material-icons">edit</i>
                                <span>Edit estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">pan_tool</i>
                                <span>Refuse estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">delete</i>
                                <span>Delete estiamte</span>
                              </div>
                            </div>
                          }
                          trigger="click"
                        >
                          <Button className="btn-user-more">
                            <i className="material-icons">more_vert</i>
                          </Button>
                        </Popover>
                      </div>
                      {/* <div className="user-details-body">
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Estimate
                          </div>
                          <div
                            className="user-details-child-item-value"
                            style={{ color: "#257cde" }}
                          >
                            {item.estimate}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Amount
                          </div>
                          <div className="user-details-child-item-value">
                            {item.amount}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Created
                          </div>
                          <div className="user-details-child-item-value">
                            {item.created}
                          </div>
                        </div>
                      </div> */}
                    </div>
                  </div>
                  <div className="child-card-footer">
                    <div
                      className="action action-start-estimate"
                      onClick={e => this.startNewEstimate(e, index)}
                    >
                      Start now
                    </div>
                    <div className="action action-schedule-estimate">
                      Schedule now
                    </div>
                  </div>
                </Card>
              ))}
            </Panel>
            <Panel
              showArrow={false}
              header={
                <div className="panel-header">
                  <div className="panel-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="panel-header-mark panel-header-mark-scheduled"></span>
                    <div>
                      <div className="panel-title">Scheduled</div>
                      <div className="panel-total-value">
                        <span>{this.state.scheduled.length} estimate</span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getScheduledTotal()} value</span>
                      </div>
                    </div>
                  </div>
                  <i className="ant-collapse-arrow">
                    {(this.state.activePanelKey == 2 || (typeof this.state.activePanelKey=='object' && this.state.activePanelKey.includes("2")))? "-" : "+"}
                  </i>
                </div>
              }
              key="2"
              className="gx-estimate gx-scheduled"
            >
              {this.state.scheduled.map((item, index) => (
                <Card
                  key={index}
                  className="gx-card gx-child-card-estimates gx-child-card-estimates-scheduled"
                >
                  <div className="child-card-body">
                    <div className="user-details">
                      <div className="user-details-top">
                        <div className="user-avatar">
                          <i className="material-icons">account_circle</i>
                          <div className="gx-flex-column">
                            <Link to="#">
                              <span className="user-avatar-name">
                                {item.name}
                              </span>
                            </Link>
                            <div className="user-estimate-details">
                              <span>{"$"+item.amount}</span>
                              <span className="dot">
                                <i className="material-icons">lens</i>
                              </span>
                              <span>{item.scheduled}</span>
                            </div>
                          </div>
                        </div>
                        <Popover
                          overlayClassName="gx-estimate-popover"
                          placement="bottom"
                          content={
                            <div>
                              <div className="gx-menuitem">
                                <i className="material-icons">edit</i>
                                <span>Edit estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">pan_tool</i>
                                <span>Refuse estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">delete</i>
                                <span>Delete estiamte</span>
                              </div>
                            </div>
                          }
                          trigger="click"
                        >
                          <Button className="btn-user-more">
                            <i className="material-icons">more_vert</i>
                          </Button>
                        </Popover>
                      </div>
                      {/* <div className="user-details-body">
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Estimate
                          </div>
                          <div
                            className="user-details-child-item-value"
                            style={{ color: "#257cde" }}
                          >
                            {item.estimate}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Amount
                          </div>
                          <div className="user-details-child-item-value">
                            {item.amount}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Scheduled
                          </div>
                          <div className="user-details-child-item-value">
                            {item.scheduled}
                          </div>
                        </div>
                      </div> */}
                    </div>
                  </div>
                  <div className="child-card-footer">
                    <div className="action action-home">
                    {
                      index%2==0
                      ? <i className="material-icons">home</i>
                      : <i className="material-icons">phonelink_ring</i>
                    }
                    </div>
                    <div
                      className="action action-start-estimate"
                      onClick={e => this.startScheduledEstimate(e, index)}
                    >
                      Start now
                    </div>
                  </div>
                </Card>
              ))}
            </Panel>
            <Panel
              showArrow={false}
              header={
                <div className="panel-header">
                  <div className="panel-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="panel-header-mark panel-header-mark-under-review"></span>
                    <div>
                      <div className="panel-title">Under review</div>
                      <div className="panel-total-value">
                        <span>{this.state.under_review.length} estimate</span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getUnderReviewTotal()} value</span>
                      </div>
                    </div>
                  </div>
                  <i className="ant-collapse-arrow">
                    {(this.state.activePanelKey == 3 || (typeof this.state.activePanelKey=='object' && this.state.activePanelKey.includes("3")))? "-" : "+"}
                  </i>
                </div>
              }
              key="3"
              className="gx-estimate gx-under-review"
            >
              {this.state.under_review.map((item, index) => (
                <Card
                  key={index}
                  className="gx-card gx-child-card-estimates gx-child-card-estimates-under-review"
                >
                  <div className="child-card-body">
                    <div className="user-details">
                      <div className="user-details-top">
                        <div className="user-avatar">
                          <i className="material-icons">account_circle</i>
                          <div className="gx-flex-column">
                            <Link to="#">
                              <span className="user-avatar-name">
                                {item.name}
                              </span>
                            </Link>
                            <div className="user-estimate-details">
                              <span>{"$"+item.amount}</span>
                              <span className="dot">
                                <i className="material-icons">lens</i>
                              </span>
                              <span>{item.sent_on}</span>
                            </div>
                          </div>
                        </div>
                        <Popover
                          overlayClassName="gx-estimate-popover"
                          placement="bottom"
                          content={
                            <div>
                              <div className="gx-menuitem">
                                <i className="material-icons">edit</i>
                                <span>Edit estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">pan_tool</i>
                                <span>Refuse estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">delete</i>
                                <span>Delete estiamte</span>
                              </div>
                            </div>
                          }
                          trigger="click"
                        >
                          <Button className="btn-user-more">
                            <i className="material-icons">more_vert</i>
                          </Button>
                        </Popover>
                      </div>
                      {/* <div className="user-details-body">
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Estimate
                          </div>
                          <div
                            className="user-details-child-item-value"
                            style={{ color: "#257cde" }}
                          >
                            {item.estimate}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Amount
                          </div>
                          <div className="user-details-child-item-value">
                            {item.amount}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Sent on
                          </div>
                          <div className="user-details-child-item-value">
                            {item.sent_on}
                          </div>
                        </div>
                      </div> */}
                    </div>
                  </div>
                  <div className="child-card-footer">
                    <div className="action action-eye">
                      <i className="material-icons">visibility_off</i>
                    </div>
                    <div className="action-group gx-custom-flex-row">
                      <Row className="gx-m-0 gx-w-100">
                        <Col span={12} className="gx-p-0 gx-m-0">
                          <div className="action action-accepted">Accepted</div>
                        </Col>
                        <Col span={12} className="gx-p-0 gx-m-0">
                          <div className="action action-refused">Refused</div>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </Card>
              ))}
            </Panel>
            <Panel
              showArrow={false}
              header={
                <div className="panel-header">
                  <div className="panel-header-content gx-w-100 gx-flex-row gx-flex-align-items-center">
                    <span className="panel-header-mark panel-header-mark-accepted-estimates"></span>
                    <div>
                      <div className="panel-title">Accepted estimates</div>
                      <div className="panel-total-value">
                        <span>
                          {this.state.accepted_estimates.length} estimate
                        </span>
                        <span className="dot">
                          <i className="material-icons">lens</i>
                        </span>
                        <span>${this.getAcceptedEstimateTotal()} value</span>
                      </div>
                    </div>
                  </div>
                  <i className="ant-collapse-arrow">
                    {(this.state.activePanelKey == 4 || (typeof this.state.activePanelKey=='object' && this.state.activePanelKey.includes("4")))? "-" : "+"}
                  </i>
                </div>
              }
              key="4"
              className="gx-estimate gx-accepted-estimates"
            >
              {this.state.accepted_estimates.map((item, index) => (
                <Card
                  key={index}
                  className="gx-card gx-child-card-estimates gx-child-card-estimates-accepted-estimates"
                >
                  <div className="child-card-body">
                    <div className="user-details">
                      <div className="user-details-top">
                        <div className="user-avatar">
                          <i className="material-icons">account_circle</i>
                          <div className="gx-flex-column">
                            <Link to="#">
                              <span className="user-avatar-name">
                                {item.name}
                              </span>
                            </Link>
                            <div className="user-estimate-details">
                              <span>{"$"+item.amount}</span>
                              <span className="dot">
                                <i className="material-icons">lens</i>
                              </span>
                              <span>{item.accepted}</span>
                            </div>
                          </div>
                        </div>
                        <Popover
                          overlayClassName="gx-estimate-popover"
                          placement="bottom"
                          content={
                            <div>
                              <div className="gx-menuitem">
                                <i className="material-icons">edit</i>
                                <span>Edit estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">pan_tool</i>
                                <span>Refuse estimate</span>
                              </div>
                              <div className="gx-menuitem">
                                <i className="material-icons">delete</i>
                                <span>Delete estiamte</span>
                              </div>
                            </div>
                          }
                          trigger="click"
                        >
                          <Button className="btn-user-more">
                            <i className="material-icons">more_vert</i>
                          </Button>
                        </Popover>
                      </div>
                      {/* <div className="user-details-body">
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Estimate
                          </div>
                          <div
                            className="user-details-child-item-value"
                            style={{ color: "#257cde" }}
                          >
                            {item.estimate}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Amount
                          </div>
                          <div className="user-details-child-item-value">
                            {item.amount}
                          </div>
                        </div>
                        <div className="user-details-child gx-custom-my-5">
                          <div className="user-details-child-item-title">
                            Accepted
                          </div>
                          <div className="user-details-child-item-value">
                            {item.accepted}
                          </div>
                        </div>
                      </div> */}
                    </div>
                  </div>
                  <div className="child-card-footer">
                    <div className="action action-convert">
                      Convert to job
                    </div>
                  </div>
                </Card>
              ))}
            </Panel>
          </Collapse>
        </div>
      </div>
    );
  }
}
export default injectIntl(ActiveEstimates);
