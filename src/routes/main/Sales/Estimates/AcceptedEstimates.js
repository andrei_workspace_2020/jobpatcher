import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Card, Row, Col, Button, Popover, Checkbox } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import ButtonGroup from "antd/lib/button/button-group";
import Widget from "components/Widget";
class AcceptedEstimates extends Component {
  state = {
    accepted_estimates: [
      {
        name: "Mark Boldzone",
        estimate: "#00537",
        amount: 500.0,
        accepted: "7 Auguest-15:43 PM",
        status: "Scheduled",
        action: "Job #00533",
        checked: false
      },
      {
        name: "David Chua",
        estimate: "#00538",
        amount: 700.0,
        accepted: "11 Auguest-11:43 PM",
        status: "Completed",
        action: "Job #00547",
        checked: false
      },
      {
        name: "Peter Jackson",
        estimate: "#00513",
        amount: 500.0,
        accepted: "2 May-15:43 PM",
        status: "Completed",
        action: "Job #00538",
        checked: false
      }
    ]
  };
  onChangeAllCheck = e => {
    var array = this.state.accepted_estimates;

    array.map(item => {
      item.checked = e.target.checked;
    });
    this.setState({ accepted_estimates: array });
  };
  onChangeEstimateCheck = (e, index) => {
    var array = this.state.accepted_estimates;
    array[index].checked = !array[index].checked;
    this.setState({ accepted_estimates: array });
  };
  render() {
    const { accepted_estimates } = this.state;
    return (
      <div className="gx-panel-accepted-estimate-content gx-h-100 gx-flex-row gx-flex-justify-content-space-between">
        <Widget styleName="gx-card-full gx-customer-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
          <table className="gx-customer-table gx-accepted-estimates-table">
            <thead>
              <tr>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.accepted.convertedjob" />
                </th>
                <th style={{ minWidth: "200px" }}>
                  <IntlMessages id="sales.estimate.accepted.customer" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.accepted.accepted_on" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.accepted.amount" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.accepted.status" />
                </th>
                <th style={{ minWidth: "210px" }}></th>
              </tr>
            </thead>
            <tbody>
              {this.state.accepted_estimates.map((item, index) => (
                <tr key={index}>
                  <td>
                    <Link to="#">{item.estimate}</Link>
                  </td>
                  <td>
                    <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                      <div className="gx-flex-row gx-align-items-center gx-mr-12">
                        <i
                          className="material-icons gx-w-100 gx-text-center"
                          style={{ fontSize: "30px" }}
                        >
                          account_circle
                        </i>
                      </div>
                      <Link
                        to="/customers/profile"
                        className="gx-text-medium gx-fs-13-20 gx-mr-12"
                      >
                        {item.name}
                      </Link>
                    </div>
                  </td>
                  <td>{item.accepted}</td>
                  <td>${item.amount.toFixed(2)}</td>
                  <td>{item.status}</td>
                  <td>
                    <div
                      className="gx-d-none gx-flex-row gx-align-items-center gx-flex-nowrap"
                      style={{ justifyContent: "flex-end" }}
                    >
                      <Button className="gx-customized-button gx-customized-text-button gx-ml-10" style={{width: "120px"}}>
                        <Link to="/customers/profile">
                          <IntlMessages id="sales.estimate.accepted.view_job" />
                        </Link>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
              <tr></tr>
            </tbody>
          </table>
        </Widget>
      </div>
    );
  }
}
export default injectIntl(AcceptedEstimates);
