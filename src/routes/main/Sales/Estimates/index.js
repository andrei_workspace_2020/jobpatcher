import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Button, Select, Modal, Tag, Tabs, Row, Col } from "antd";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import SearchBox from "components/SearchBox";
import SelectableButtonGroup from "components/Button/SelectableButtonGroup";
import ActiveEstimates from "./ActiveEstimates";
import AcceptedEstimates from "./AcceptedEstimates";
import RefusedEstimates from "./RefusedEstimates";

import TopMenu from "../TopMenu";

class Estimates extends Component {
  state = {
    estimateType: "active",
    searchText: "",
    newEstimateDlgShow: false
  };
  onChangeEstimateType = estimateType => {
    this.setState({ estimateType });
  };
  updateSearchEstimate = searchText => this.setState({ searchText });
  addNewEstimate = () => {
    this.props.history.push("/estimates/add");
    // this.setState({ newEstimateDlgShow: true });
  };
  renderActionBar = () => {
    const { estimateType, newEstimateDlgShow } = this.state;
    return (
      <div className="gx-sales-module-content gx-sales-estimates-toolbar-ss">
        <div>
          {/* mobile reponsive... if width > sm-max then visible, else disappear */}
          <div className="gx-dispatch-top-toolbar gx-a-align-around-ss gx-d-md-flex gx-d-none">
            <div className="gx-div-align-center gx-mb-12">
              <SelectableButtonGroup
                className="gx-d-md-block gx-d-none gx-w-100"
                selected={estimateType}
                onChange={this.onChangeEstimateType}
              >
                <Button
                  className="gx-sales-tab-btn gx-sales-tab-active gx-m-0"
                  key="active"
                >
                  <IntlMessages id="sales.estimate.active" />
                </Button>
                <Button
                  className="gx-sales-tab-btn gx-sales-tab-converted gx-m-0"
                  key="converted"
                >
                  <IntlMessages id="sales.estimate.converted" />
                </Button>
                <Button
                  className="gx-sales-tab-btn gx-sales-tab-refused gx-m-0"
                  key="refused"
                >
                  <IntlMessages id="sales.estimate.refused" />
                </Button>
              </SelectableButtonGroup>
              <Button className="gx-dispatch-employees-btn-filter gx-d-none gx-a-md-block">
                <i className="material-icons">tune</i>
                <div>Filter</div>
              </Button>
            </div>
            <div className="gx-div-align-center gx-mb-12">
              <div className="gx-w-100">
                <SearchBox
                  styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-estimate-search-bar"
                  placeholder={"Search estimate"}
                  onChange={evt => this.updateSearchEstimate(evt.target.value)}
                  value={this.state.searchText}
                />
              </div>

              <span className="gx-ml-10">
                <Button
                  className="gx-nav-btn gx-nav-dispatch-new-btn gx-mb-0"
                  type="primary"
                  onClick={this.addNewEstimate}
                  style={{ paddingLeft: "12px" }}
                >
                  <div className="gx-div-align-center gx-ss-fs-13 gx-ss-fw-500">
                    <i
                      className="material-icons gx-fs-xl"
                      style={{ marginRight: "1px" }}
                    >
                      add
                    </i>
                    <IntlMessages id="sales.estimate.createestimate" />
                  </div>
                </Button>
              </span>
            </div>
          </div>
          {/* end responsive */}

          {/* mobile reponsive... if width < sm-max then visible, else disappear */}
          <div className="gx-sales-top-toolbar gx-a-align-around-ss gx-a-md-block gx-d-none gx-a-md-flex-column">
            <div className="gx-div-align-center gx-mb-12 gx-sales-estimate-mobile-toolbar-btngroup gx-w-100 gx-justify-content-between">
              <div className="gx-div-align-center gx-sales-estimate-mobile-toolbar-search">
                <div className="gx-w-100" style={{ paddingRight: "5px" }}>
                  <SearchBox
                    styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-estimate-search-bar gx-w-100"
                    placeholder={"Search"}
                    onChange={evt =>
                      this.updateSearchEstimate(evt.target.value)
                    }
                    value={this.state.searchText}
                  />
                </div>
              </div>
              <div style={{ paddingLeft: "5px" }}>
                <Button
                  className="gx-nav-btn gx-nav-sales-estimate-new-btn gx-mb-0 gx-w-100"
                  type="primary"
                  onClick={this.addNewEstimate}
                >
                  <div className="gx-div-align-center gx-ss-fs-13 gx-ss-fw-500">
                    <i className="material-icons gx-fs-xl gx-mr-2">add</i>
                    <IntlMessages id="sales.estimate.createestimate" />
                  </div>
                </Button>
              </div>
            </div>
            <div className="gx-div-align-center gx-mb-12 gx-sales-estimate-mobile-toolbar-btngroup gx-w-100">
              <SelectableButtonGroup
                className="gx-d-md-block gx-w-100"
                selected={estimateType}
                onChange={this.onChangeEstimateType}
              >
                <Button
                  className="gx-sales-tab-btn gx-sales-tab-active gx-m-0"
                  key="active"
                >
                  <IntlMessages id="sales.estimate.active" />
                </Button>
                <Button
                  className="gx-sales-tab-btn gx-sales-tab-converted gx-m-0"
                  key="converted"
                >
                  <IntlMessages id="sales.estimate.converted" />
                </Button>
                <Button
                  className="gx-sales-tab-btn gx-sales-tab-refused gx-m-0"
                  key="refused"
                >
                  <IntlMessages id="sales.estimate.refused" />
                </Button>
              </SelectableButtonGroup>
            </div>
          </div>
          {/* end responsive */}
        </div>
      </div>
    );
  };
  render() {
    return (
      <div className="gx-main-content">
        <TopMenu currentPage="0" />
        <div className="gx-app-module gx-sales-module">
          <div className="gx-w-100">
            <div className="gx-sales-module-scroll">
              {this.renderActionBar()}
              <div
                className="gx-panel-content gx-pt-0 gx-ss-sales-estimate"
                style={{ backgroundColor: "transparent" }}
              >
                <div className="gx-panel-content-scroll">
                  {this.state.estimateType == "active" ? (
                    <ActiveEstimates></ActiveEstimates>
                  ) : this.state.estimateType == "converted" ? (
                    <AcceptedEstimates></AcceptedEstimates>
                  ) : (
                    <RefusedEstimates></RefusedEstimates>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(Estimates);
