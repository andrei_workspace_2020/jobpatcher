import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Card, Row, Col, Button, Popover, Checkbox } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import ButtonGroup from "antd/lib/button/button-group";
import Widget from "components/Widget";
class RefusedEstimates extends Component {
  state = {
    refused_estimates: [
      {
        name: "Mark Zoddlila",
        estimate: "#00532",
        amount: 500.0,
        refused_on: "6 Auguest-13:42 PM",
        status: "Refused",
        reason: "Price is too high"
      },
      {
        name: "Peter Jonson",
        estimate: "#00530",
        amount: 700.0,
        refused_on: "4 August-12:25 PM",
        status: "Refused",
        reason: "Chosen another provider"
      }
    ]
  };
  onChangeAllCheck = e => {
    var array = this.state.refused_estimates;

    array.map(item => {
      item.checked = e.target.checked;
    });
    this.setState({ refused_estimates: array });
  };
  onChangeEstimateCheck = (e, index) => {
    var array = this.state.refused_estimates;
    array[index].checked = !array[index].checked;
    this.setState({ refused_estimates: array });
  };
  render() {
    const { refused_estimates } = this.state;
    return (
      <div className="gx-panel-refused-estimate-content gx-h-100 gx-flex-row gx-flex-justify-content-space-between">
        <Widget styleName="gx-card-full gx-customer-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
          <table className="gx-customer-table gx-refused-estimates-table">
            <thead>
              <tr>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.refused.estimate" />
                </th>
                <th style={{ minWidth: "200px" }}>
                  <IntlMessages id="sales.estimate.refused.customer" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.refused.refused_on" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.refused.amount" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="sales.estimate.refused.reason" />
                </th>
                <th style={{ minWidth: "210px" }}></th>
              </tr>
            </thead>
            <tbody>
              {this.state.refused_estimates.map((item, index) => (
                <tr key={index}>
                  <td>
                    <Link to="#">{item.estimate}</Link>
                  </td>
                  <td>
                    <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                      <div className="gx-flex-row gx-align-items-center gx-mr-12">
                        <i
                          className="material-icons gx-w-100 gx-text-center"
                          style={{ fontSize: "30px" }}
                        >
                          account_circle
                        </i>
                      </div>
                      <Link
                        to="/customers/profile"
                        className="gx-text-medium gx-fs-13-20 gx-mr-12"
                      >
                        {item.name}
                      </Link>
                    </div>
                  </td>
                  <td>{item.refused_on}</td>
                  <td>${item.amount.toFixed(2)}</td>
                  <td>{item.reason}</td>
                  <td>
                    <div
                      className="gx-d-none gx-flex-row gx-align-items-center gx-flex-nowrap"
                      style={{ justifyContent: "flex-end" }}
                    >
                      <Button className="gx-customized-button gx-customized-text-button gx-ml-10">
                        <Link to="/customers/profile">
                          <IntlMessages id="sales.estimate.refused.view_estimate" />
                        </Link>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
              <tr></tr>
            </tbody>
          </table>
        </Widget>
      </div>
    );
  }
}
export default injectIntl(RefusedEstimates);
