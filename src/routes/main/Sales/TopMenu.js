import React, { Component } from "react";
import { Tabs } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";

const TabPane = Tabs.TabPane;

const MENU_ITEMS = [
  {
    title: "topmenu.menu.sales.estimates",
    link: "/sales/estimates"
  },
  {
    title: "topmenu.menu.sales.services",
    link: "/sales/services"
  },
  {
    title: "topmenu.menu.sales.materials",
    link: "/sales/materials"
  }
];

class TopMenu extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { currentPage } = this.props;
    return (
      <Tabs className="gx-dispatch-topmenu" defaultActiveKey={currentPage}>
        {MENU_ITEMS.map((menuItem, index) => (
          <TabPane
            tab={
              <Link to={menuItem.link}>
                <IntlMessages id={menuItem.title} />
              </Link>
            }
            key={index}
          ></TabPane>
        ))}
      </Tabs>
    );
  }
}

export default TopMenu;
