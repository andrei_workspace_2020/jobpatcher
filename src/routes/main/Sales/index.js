import React from "react";
import { Route, Switch } from "react-router-dom";

import Estimates from "./Estimates";
import Services from "./Services";
import Materials from "./Materials";

import "assets/sales-custom.css";

const Sales = ({ match }) => (
  <div className="gx-main-content-wrapper gx-sales-content-wrapper">
    <Switch>
      <Route path="/sales/estimates" component={Estimates} />
      <Route path="/sales/services" component={Services} />
      <Route path="/sales/materials" component={Materials} />
    </Switch>
  </div>
);

export default Sales;
