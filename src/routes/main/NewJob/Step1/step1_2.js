import React, { Component } from "react";
import { Select } from "antd";
import { Col, Row, Button, Popover } from "antd";
import { injectIntl } from "react-intl";
import SearchBar from "components/AddJob/SearchBar";
import Widget from "components/Widget";
import PrivateNoteWidget from "components/AddJob/PrivateNoteWidget";
import UploadFileWidget from "components/AddJob/UploadFileWidget";
import CustomerCard from "components/Dispatch/CustomerCard";
import AddressCard from "components/Dispatch/AddressCard";

class Step1_2 extends Component {
  state = {
    customer: null
  };
  constructor(props, context) {
    super(props, context);
  }
  searchCustomer = customer => {
    this.props.onSearchCustomer(customer);
  };

  render() {
    return (
      <div className="gx-main-content-container gx-addjob-step1-2 gx-p-20 gx-flex-row gx-align-items-center gx-justify-content-center gx-h-100 gx-m-auto">
        <Row style={{ justifyContent: "center" }}>
          <Col xxl={20} xl={20} lg={24} md={24} sm={24} xs={24}>
            <Row>
              <Col xxl={16} xl={16} lg={16} md={24} sm={24} xs={24}>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <h3 style={{ paddingBottom: 10 }}>Search customer</h3>
                  <a
                    className="gx-nav-btn gx-nav-new-btn gx-mb-0 gx-addjob-new-customer"
                    onClick={this.props.showNewCustomerDlg}
                  >
                    <div className="gx-div-align-center">
                      <i className="material-icons gx-fs-xl">add</i>
                      New customer
                    </div>
                  </a>
                </div>
              </Col>
            </Row>
            <Row className="gx-addjob-step1-2-lastcard">
              <Col xxl={16} xl={16} lg={16} md={24} sm={24} xs={24}>
                <Row>
                  <Col
                    xxl={24}
                    xl={24}
                    lg={24}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                  >
                    <SearchBar onSearchCustomer={this.searchCustomer} />
                  </Col>
                  <Col
                    xxl={12}
                    xl={12}
                    lg={12}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                  >
                    {/* <GoogleMapWrapper style={{padding:0}}/> */}
                    <CustomerCard
                      data={this.props.data}
                      showNewCustomerDlg={this.props.showNewCustomerDlg}
                    />
                  </Col>
                  <Col
                    xxl={12}
                    xl={12}
                    lg={12}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                  >
                    <AddressCard data={this.props.data} />
                  </Col>
                </Row>
              </Col>

              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Row>
                  <Col
                    xxl={24}
                    xl={24}
                    lg={24}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                  >
                    <PrivateNoteWidget style={{ padding: 0 }} />
                  </Col>
                  <Col
                    xxl={24}
                    xl={24}
                    lg={24}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                  >
                    <UploadFileWidget style={{ padding: 0 }} />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default injectIntl(Step1_2);
