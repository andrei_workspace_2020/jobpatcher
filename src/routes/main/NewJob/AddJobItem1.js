import React, { Component } from "react";
import { Col, Row, InputNumber, Input, Checkbox, Button } from "antd";
import { injectIntl } from "react-intl";
import Widget from "components/Widget";
import PrivateNoteWidget from "components/AddJob/PrivateNoteWidget";
import UploadFileWidget from "components/AddJob/UploadFileWidget";
import JobSubPane from "components/AddJob/JobSubPane";

class AddJobItem extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="gx-main-content-container gx-addjob-step2-addjobitem-main-content gx-p-20 gx-flex-row gx-align-items-center gx-h-100 gx-m-auto">
        <Row style={{ justifyContent: "center" }}>
          <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
            <Row>
              <Col
                xxl={16}
                xl={16}
                lg={16}
                md={24}
                sm={24}
                xs={24}
                style={{ paddingLeft: 33 }}
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <h3 style={{ paddingBottom: 20 }}>Add job items</h3>
                </div>
              </Col>
            </Row>
            <Row>
              <Col xxl={16} xl={16} lg={16} md={24} sm={24} xs={24}>
                <Row>
                  <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
                    <Widget styleName="gx-card-full gx-dispatcher-job-panel gx-m-0">
                      <div className="gx-addjob-step2-addjobitem-total-panel">
                        <JobSubPane kind="Service" />
                        <JobSubPane kind="Material" />
                        <Row className="price-box">
                          <Col
                            xxl={12}
                            xl={12}
                            lg={12}
                            md={0}
                            sm={0}
                            xs={0}
                          ></Col>
                          <Col xxl={12} xl={12} lg={12} md={24} sm={24} xs={24}>
                            <Row>
                              <Col
                                className="price-label"
                                xxl={12}
                                xl={12}
                                lg={12}
                                md={12}
                                sm={12}
                                xs={12}
                              >
                                Subtotal
                              </Col>
                              <Col
                                className="price-value"
                                xxl={7}
                                xl={7}
                                lg={7}
                                md={7}
                                sm={7}
                                xs={7}
                              >
                                $600.00
                              </Col>
                            </Row>
                            <Row>
                              <Col
                                className="price-label"
                                xxl={12}
                                xl={12}
                                lg={12}
                                md={12}
                                sm={12}
                                xs={12}
                              >
                                TPS/TVQ QC@14.975%
                              </Col>
                              <Col
                                className="price-value"
                                xxl={7}
                                xl={7}
                                lg={7}
                                md={7}
                                sm={7}
                                xs={7}
                              >
                                $89.85
                              </Col>
                            </Row>
                            <Row>
                              <Col
                                className="price-label price-label-highlight"
                                xxl={12}
                                xl={12}
                                lg={12}
                                md={12}
                                sm={12}
                                xs={12}
                              >
                                Add discount %
                              </Col>
                              <Col
                                className="price-value price-value-disable"
                                xxl={7}
                                xl={7}
                                lg={7}
                                md={7}
                                sm={7}
                                xs={7}
                              >
                                $00.00
                              </Col>
                            </Row>
                            <Row>
                              <Col
                                className="price-label total-price-label"
                                xxl={12}
                                xl={12}
                                lg={12}
                                md={12}
                                sm={12}
                                xs={12}
                              >
                                Total amount
                              </Col>
                              <Col
                                className="price-value total-price-value"
                                xxl={7}
                                xl={7}
                                lg={7}
                                md={7}
                                sm={7}
                                xs={7}
                              >
                                $689.85
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </div>
                    </Widget>
                  </Col>
                </Row>
              </Col>

              <Col xxl={8} xl={8} lg={8} md={24} sm={24} xs={24}>
                <Row>
                  <Col
                    xxl={24}
                    xl={24}
                    lg={24}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                    className="gx-step2-private-note-container"
                  >
                    <PrivateNoteWidget style={{ padding: 0 }} />
                  </Col>
                  <Col
                    xxl={24}
                    xl={24}
                    lg={24}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                    className="gx-step2-upload-container"
                  >
                    <UploadFileWidget style={{ padding: 0 }} />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}
export default AddJobItem;
