import React, { Component } from "react";
import { Button, Select, Avatar, Tag } from "antd";
import { injectIntl } from "react-intl";

import Auxiliary from "util/Auxiliary";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import Widget from "components/Widget";
import SearchBox from "components/SearchBox";
import DispatchJobCard from "components/Dispatch/DispatchJobCard";
import TopMenu from "./TopMenu";
import JobListItem from "components/List/JobListItem";
import TopToolBar from "./TopToolBar";
import EmployeeWorkHours from "./employee-work-hours";
import EmployeeWorkmonth from "./employee-work-month";
import EmployeeWorkweek from "./employee-work-week";
import EmployeeView from "./ScheduleEmployeeView";
import CalendarView from "./ScheduleCalendarView";
import moment from "moment";

import { global_jobs } from "./data";

const Option = Select.Option;

class DispatchSchedule extends Component {
  state = {
    viewMode: "calendar",
    scheduled: "scheduled",
    jobs: [],
    searchText: "",
    date: moment(new Date())
  };

  constructor(props, context) {
    super(props, context);

    this.renderMainContent = this.renderMainContent.bind(this);
    this.changeJobs = this.changeJobs.bind(this);
  }

  componentDidMount() {
    this.changeJobs(this.state.scheduled);
  }

  changeJobs(scheduled) {
    if (scheduled === "scheduled") {
      this.setState({
        jobs: this.getScheduledJobs(global_jobs)
      });
    } else {
      this.setState({
        jobs: this.getUnscheduledJobs(global_jobs)
      });
    }
  }

  getScheduledJobs(data) {
    return data.filter(job => job.status !== "unscheduled");
  }

  getUnscheduledJobs(data) {
    return data.filter(job => job.status === "unscheduled");
  }

  onChangeViewMode = viewMode => {
    this.setState({ viewMode: viewMode });
    console.log(viewMode);
  };

  onChangeScheduled = scheduled => {
    this.setState({ scheduled: scheduled });
    this.changeJobs(scheduled);
  };

  updateSearchJobs = evt => {
    this.setState({
      searchText: evt.target.value
    });
  };
  onPrevMonthSelected = () => {
    let current = new Date(this.state.date);
    let prev = new Date(current.getFullYear(), current.getMonth() - 1, 15);
    this.setState({ date: moment(prev) });
  };
  onNextMonthSelected = () => {
    let current = new Date(this.state.date);
    let next = new Date(current.getFullYear(), current.getMonth() + 1, 15);
    this.setState({ date: moment(next) });
  };
  setCalendarDate = date => {
    this.setState({ date });
  };
  renderMainContent() {
    const { jobs, scheduled, viewMode, date } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="gx-ss-schedule-position-relative">
        <div className="gx-dispatch-module-content">
          <TopToolBar
            date={date}
            viewMode={viewMode}
            scheduled={scheduled}
            onChangeViewMode={this.onChangeViewMode}
            onChangeScheduled={this.onChangeScheduled}
            scheduledCount={3}
            unscheduledCount={5}
            onPrevMonthSelected={this.onPrevMonthSelected}
            onNextMonthSelected={this.onNextMonthSelected}
          />
          {(viewMode === "employee" && <EmployeeView />) || (
            <CalendarView date={date} setCalendarDate={this.setCalendarDate} />
          )}
        </div>
        {scheduled === "unscheduled" && (
          <div className="gx-ss-unscheduled-drawer">
            <div className="gx-customer-tab gx-dispatcher-unscheduled-tab">
              <div className="gx-customer-tab-header gx-justify-content-between">
                <h5 className="gx-text-uppercase">
                  <IntlMessages
                    id="dispatch.dispatch.unscheduledjobs"
                    values={{ value: jobs.length }}
                  />
                </h5>
              </div>
              <div className="gx-dispatcher-unscheduled-tab-scroll">
                <div className="gx-customer-tab-content">
                  {jobs.map((item, index) => (
                    <JobListItem
                      key={index}
                      avatar={item.customer.avatar}
                      name={item.customer.name}
                      status={item.status}
                    >
                      <div className="gx-fs-11 gx-text-grey">{item.job}</div>
                    </JobListItem>
                  ))}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }

  render() {
    return (
      <div className="gx-main-content">
        <TopMenu currentPage="1" />
        <div className="gx-app-module gx-dispatch-module gx-dispatch-schedule-module">
          <div className="gx-w-100">
            <div className="gx-dispatch-module-scroll">
              {this.renderMainContent()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(DispatchSchedule);
