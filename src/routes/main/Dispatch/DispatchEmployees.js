import React, { Component } from "react";
import { Button, Select, Modal, Row, Col } from "antd";
import { injectIntl } from "react-intl";
import { changeDateMonthYearHourMinuteSecondFormatSS } from "util/DateTime";

import { connect } from "react-redux";
import Widget from "components/Widget";
import IntlMessages from "util/IntlMessages";
import SearchBox from "components/SearchBox";
import TopMenu from "./TopMenu";
import SelectableButtonGroup from "components/Button/SelectableButtonGroup";
import EmployeeJobCard from "components/Dispatch/EmployeeJobCard";
import EditEmployeeDlg from "./EditEmployeeDlg";
import {
  getAllEmployees,
  addEmployee,
  updateEmployee,
  deleteEmployee
} from "appRedux/actions/Employees";

const Option = Select.Option;

class DispatchEmployees extends Component {
  state = {
    searchText: "",
    employeeStatus: "0",
    employeeNewDlgVisible: false,
    employeeEditDlgVisible: false,
    employee: {}
  };

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.getAllEmployees();
  }

  onChangeEmpStatus = employeeStatus => {
    this.setState({ employeeStatus });
  };

  showNewEmpDlg() {
    this.setState({
      employeeNewDlgVisible: true,
      employeeEditDlgVisible: false
    });
  }

  showEditEmpDlg(emp) {
    this.setState({
      employee: emp,
      employeeNewDlgVisible: false,
      employeeEditDlgVisible: true
    });
  }

  onCancel() {
    this.setState({
      employeeNewDlgVisible: false,
      employeeEditDlgVisible: false
    });
  }

  onSaveEmployee = emp => {
    console.log(emp);
    if (emp.id === "") {
      emp.id = parseInt(Math.random() * 100000000).toString();
      emp.created_date = changeDateMonthYearHourMinuteSecondFormatSS(
        new Date()
      );
      this.props.addEmployee(emp);
      // this.setState({ employees: [...this.state.employees, emp] });
      this.setState({ employeeNewDlgVisible: false });
    } else {
      emp.updated_date = changeDateMonthYearHourMinuteSecondFormatSS(
        new Date()
      );
      this.props.updateEmployee(emp.id, emp);
      // let temp = this.state.employees;
      // let list = temp.map(item => {
      //   if (item.id == emp.id) item = emp;
      //   return item;
      // });
      // temp = list;
      // this.setState({ employees: temp });
      this.setState({ employeeEditDlgVisible: false });
    }
    this.props.history.push("/dispatch/employees/profile/" + emp.id);
  };
  updateSearchEmp = searchText => this.setState({ searchText });
  onTerminateEmp = emp => {
    emp.deleted = 1;
    this.props.updateEmployee(emp.id, emp);
  };
  renderMainContent = () => {
    const {
      intl: { formatMessage }
    } = this.props;
    const { employeeStatus, employeeNewDlgVisible } = this.state;
    return (
      <div className="gx-dispatch-module-content gx-dispatch-employees-toolbar-ss">
        <div>
          {/* mobile reponsive... if width > sm-max then visible, else disappear */}
          <div className="gx-dispatch-top-toolbar gx-a-align-around-ss gx-d-md-flex gx-d-none">
            <div className="gx-div-align-center gx-mb-12">
              <SelectableButtonGroup
                className="gx-d-md-block gx-d-none"
                selected={employeeStatus}
                onChange={this.onChangeEmpStatus}
              >
                <Button
                  key="0"
                  style={{ paddingRight: "18px", paddingLeft: "18px" }}
                >
                  <IntlMessages id="dispatch.dispatch.employee.active" />
                </Button>
                <Button
                  key="1"
                  style={{ paddingRight: "22px", paddingLeft: "22px" }}
                >
                  <IntlMessages id="dispatch.dispatch.employee.terminated" />
                </Button>
              </SelectableButtonGroup>
              <Button className="gx-dispatch-employees-btn-filter gx-d-none gx-a-md-block">
                <i className="material-icons">tune</i>
                <div>Filter</div>
              </Button>
            </div>
            <div className="gx-div-align-center gx-mb-12">
              <div className="">
                <SearchBox
                  styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-employee-search-bar"
                  placeholder={"Search Employees"}
                  onChange={evt => this.updateSearchEmp(evt.target.value)}
                  value={this.state.searchText}
                />
              </div>
              <span className="gx-ml-10">
                <Button
                  className="gx-nav-btn gx-nav-dispatch-new-btn gx-mb-0"
                  type="primary"
                  onClick={this.showNewEmpDlg.bind(this)}
                  style={{ paddingLeft: "12px" }}
                >
                  <div className="gx-div-align-center gx-ss-fs-13 gx-ss-fw-500">
                    <i
                      className="material-icons gx-fs-xl"
                      style={{ marginRight: "1px" }}
                    >
                      add
                    </i>
                    <IntlMessages id="dispatch.dispatch.employee.addnewemp" />
                  </div>
                </Button>
              </span>
            </div>
          </div>
          {/* end responsive */}

          {/* mobile reponsive... if width < sm-max then visible, else disappear */}
          <div className="gx-dispatch-top-toolbar gx-a-align-around-ss gx-a-md-block gx-d-none gx-a-md-flex-column">
            <div className="gx-div-align-center gx-mb-12 gx-dispatch-employees-mobile-toolbar-btngroup">
              <Button
                className="gx-dispatch-employees-btn-filter gx-d-none gx-a-md-block"
                style={{ color: "#9399a2" }}
              >
                <i className="material-icons">tune</i>
                <div className="gx-px-10 gx-ss-fs-13 gx-ss-fw-500">Filter</div>
              </Button>
              <Button
                className="gx-nav-btn gx-nav-dispatch-new-btn gx-mb-0"
                type="primary"
                onClick={this.showNewEmpDlg.bind(this)}
              >
                <div className="gx-div-align-center gx-ss-fs-13 gx-ss-fw-500">
                  <i className="material-icons gx-fs-xl gx-mr-2">add</i>
                  <IntlMessages id="dispatch.dispatch.employee.addnewemp" />
                </div>
              </Button>
            </div>
            <div className="gx-div-align-center gx-mb-12 gx-dispatch-employees-mobile-toolbar-search">
              <div className="">
                <SearchBox
                  styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-employee-search-bar"
                  placeholder={"Search Employees"}
                  onChange={evt => this.updateSearchEmp(evt.target.value)}
                  value={this.state.searchText}
                />
              </div>
            </div>
          </div>
          {/* end responsive */}
        </div>
      </div>
    );
  };

  render() {
    const {
      width,
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="gx-main-content">
        <TopMenu currentPage="2" />
        <div className="gx-app-module gx-dispatch-module gx-dispatch-employees-module">
          <div className="gx-w-100">
            <div className="gx-dispatch-module-scroll">
              {this.renderMainContent()}
              <div
                className="gx-panel-content gx-pt-0 gx-pb-0 gx-ss-dispatch-employees-a-pb-60"
                style={{ backgroundColor: "transparent" }}
              >
                <div className="gx-panel-content-scroll">
                  <div className="gx-employee-content-panel gx-a-align-center-ss">
                    {this.props.employees.length == 0 ? (
                      <Widget styleName="gx-card-full gx-empty-employees-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
                        <div className="gx-empty-employees-content gx-h-100 gx-flex-row gx-align-items-center gx-justify-content-center">
                          <div className="gx-flex-column gx-align-items-center">
                            <img
                              src={require("assets/images/employee/no-employees.png")}
                            />
                            <span className="gx-fs-xl gx-font-weight-bold gx-my-30">
                              You haven't added any employees yet
                            </span>
                            <Button
                              type="primary"
                              className="gx-font-weight-medium"
                              style={{ width: "200px" }}
                              onClick={this.showNewEmpDlg.bind(this)}
                            >
                              Add employee
                            </Button>
                          </div>
                        </div>
                      </Widget>
                    ) : this.props.employees.filter(
                        emp =>
                          (emp.first_name + " " + emp.last_name).includes(
                            this.state.searchText
                          ) && emp.deleted == this.state.employeeStatus
                      ).length > 0 ? (
                      <Row style={{ width: "-webkit-fill-available" }}>
                        {this.props.employees
                          .filter(
                            emp =>
                              (emp.first_name + " " + emp.last_name).includes(
                                this.state.searchText
                              ) && emp.deleted == this.state.employeeStatus
                          )
                          .map((emp, index) => (
                            <Col
                              xxl={4}
                              xl={6}
                              lg={8}
                              md={12}
                              sm={12}
                              xs={24}
                              key={index}
                            >
                              <EmployeeJobCard
                                emp={emp}
                                showEditEmpDlg={this.showEditEmpDlg.bind(this)}
                                onTerminateEmp={this.onTerminateEmp}
                              />
                            </Col>
                          ))}
                      </Row>
                    ) : (
                      <div />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Modal
          className="gx-ss-dispatch-new-employee-modal"
          title={
            <div className="gx-customized-modal-header">
              <div className="gx-customized-modal-title">Add new employee</div>
              <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                <i
                  className="material-icons gx-customized-modal-close"
                  onClick={this.onCancel.bind(this)}
                >
                  clear
                </i>
              </div>
            </div>
          }
          closable={false}
          wrapClassName="gx-customized-modal vertical-center-modal"
          visible={this.state.employeeNewDlgVisible}
          onCancel={this.onCancel.bind(this)}
          width={width >= 1144 ? 1084 : width - 60}
        >
          <EditEmployeeDlg
            onSave={this.onSaveEmployee.bind(this)}
            onCancel={this.onCancel.bind(this)}
          />
        </Modal>
        <Modal
          className="gx-ss-dispatch-new-employee-modal"
          title={
            <div className="gx-customized-modal-header">
              <div className="gx-customized-modal-title">Add new employee</div>
              <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                <i
                  className="material-icons gx-customized-modal-close"
                  onClick={this.onCancel.bind(this)}
                >
                  clear
                </i>
              </div>
            </div>
          }
          closable={false}
          wrapClassName="gx-customized-modal vertical-center-modal"
          visible={this.state.employeeEditDlgVisible}
          onCancel={this.onCancel.bind(this)}
          width={width >= 1144 ? 1084 : width - 60}
        >
          <EditEmployeeDlg
            data={this.state.employee}
            onSave={this.onSaveEmployee.bind(this)}
            onCancel={this.onCancel.bind(this)}
          />
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { locale, navStyle, navCollapsed, width, currentPage } = state.settings;
  const { employees } = state.employees;
  return { locale, navStyle, navCollapsed, width, currentPage, employees };
};

export default connect(mapStateToProps, {
  getAllEmployees,
  addEmployee,
  updateEmployee,
  deleteEmployee
})(injectIntl(DispatchEmployees));
