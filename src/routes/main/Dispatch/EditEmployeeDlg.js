import React, { Component } from "react";
import {
  Button,
  Row,
  Col,
  Select,
  Input,
  InputNumber,
  Form,
  DatePicker,
  Divider,
  Radio
} from "antd";

import Widget from "components/Widget";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import ButtonGroup from "antd/lib/button/button-group";
import moment from "moment";

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

class EditEmployeeDlg extends Component {
  constructor(props, context) {
    super(props, context);

    if (this.props.data != undefined)
      this.state = {
        employee: this.props.data
      };
    else
      this.state = {
        employee: {
          id: "",
          avatar: "",
          title: "",
          first_name: "",
          last_name: "",
          email: "",
          company_name: "",
          phone: {
            mobile: "",
            primary_phone: "",
            work: ""
          },
          gps_tracking: true,
          last_login: "",
          active: true,
          starred: false,
          employee_info: {
            hiring_date: "12/12/2019 ",
            role: "Dispatcher",
            hourly_rate: 0.0,
            birthday: "",
            app_version: ""
          },
          address: [],
          notes: [],
          jobs: [],
          estimates: [],
          files: [],
          created_date: "",
          updated_date: "",
          deleted: 0,
          preferences: {
            communication_language: "English",
            email_notifications: true,
            sms_notifications: true,
            push_notifications: true
          },
          permissions: {
            see_phonenumber: true,
            see_pricing: true,
            take_payment: true,
            update_company_info: false,
            delete_customers_employees: false,
            chatting_with_fieldworkers: false
          }
        }
      };
  }
  componentDidMount() {}

  componentWillMount() {}

  componentWillReceiveProps(nextProps) {}

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(this.state.employee);
        this.props.onSave(this.state.employee);
      }
    });
  };
  onFirstNameChange = e => {
    let temp = this.state.employee;
    temp.first_name = e.target.value;
    this.setState({ employee: temp });
    this.props.form.setFieldsValue({ first_name: e.target.value });
  };
  onLastNameChange = e => {
    let temp = this.state.employee;
    temp.last_name = e.target.value;
    this.setState({ employee: temp });
    this.props.form.setFieldsValue({ last_name: e.target.value });
  };
  onEmailChange = e => {
    let temp = this.state.employee;
    temp.email = e.target.value;
    this.setState({ employee: temp });
    this.props.form.setFieldsValue({ email: e.target.value });
  };
  onMobileNumberChange = e => {
    let temp = this.state.employee;
    temp.phone.mobile = e.target.value;
    this.setState({ employee: temp });
    this.props.form.setFieldsValue({ mobile: e.target.value });
  };
  onPhoneNumberChange = e => {
    let temp = this.state.employee;
    temp.phone.primary_phone = e.target.value;
    this.setState({ employee: temp });
    this.props.form.setFieldsValue({ phone_number: e.target.value });
  };
  onHiringDateChange = (date, dateString) => {
    console.log(dateString);
    let temp = this.state.employee;
    temp.employee_info.hiring_date = dateString;
    this.setState({ employee: temp });
    this.props.form.setFieldsValue({ hiring_date: dateString });
  };
  onHourlyRateChange = value => {
    let temp = this.state.employee;
    temp.employee_info.hourly_rate = value;
    this.setState({ employee: temp });
    this.props.form.setFieldsValue({ hourly_rate: value });
  };
  onRoleChange = e => {
    let temp = this.state.employee;
    temp.employee_info.role = e.target.value;
    this.setState({ employee: temp });
  };
  onUpdateCompanyInfoChange = flag => {
    let temp = this.state.employee;
    temp.permissions.update_company_info = flag;
    this.setState({ employee: temp });
  };
  onDeleteCustomersEmployeesChange = flag => {
    let temp = this.state.employee;
    temp.permissions.delete_customers_employees = flag;
    this.setState({ employee: temp });
  };
  onChattingWithFieldworkersChange = flag => {
    let temp = this.state.employee;
    temp.permissions.chatting_with_fieldworkers = flag;
    this.setState({ employee: temp });
  };

  render() {
    const {
      intl: { formatMessage }
    } = this.props;

    const { getFieldDecorator } = this.props.form;
    const mobileFormatter = e =>
      e.target.value.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3");

    return (
      <div className="gx-edit-customer-dlg">
        <Form className="gx-form-row0" onSubmit={this.onSubmit}>
          <div
            className="gx-customized-modal-content"
            style={{ paddingBottom: "0" }}
          >
            <div className="gx-customized-modal-content-block gx-m-30">
              <Row>
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  xxl={12}
                  className="gutter-row gx-p-0"
                >
                  <Widget styleName="gx-customer-details gx-card-full gx-h-100 gx-mb-0">
                    <div className="gx-panel-title-bar">
                      <div className="gx-customized-modal-content-title">
                        <i
                          className="material-icons"
                          style={{ fontSize: "28px" }}
                        >
                          assignment_ind
                        </i>
                        &nbsp;
                        <IntlMessages id="employee.employeedlg.content.label.employee_details" />
                      </div>
                    </div>
                    <div className="gx-panel-content">
                      <Row gutter={10}>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("first_name", {
                              initialValue: this.state.employee.first_name,
                              rules: [
                                {
                                  required: true,
                                  message: "Please write first name!"
                                }
                              ]
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "employee.employeedlg.content.placeholder.first_name"
                                })}
                                onChange={this.onFirstNameChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            {getFieldDecorator("last_name", {
                              initialValue: this.state.employee.last_name,
                              rules: [
                                {
                                  required: true,
                                  message: "Please write last name!"
                                }
                              ]
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "employee.employeedlg.content.placeholder.last_name"
                                })}
                                onChange={this.onLastNameChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="employee.employeedlg.content.label.email_address" />
                            </div>
                            {getFieldDecorator("email", {
                              validateTrigger: "onSubmit",
                              initialValue: this.state.employee.email,
                              rules: [
                                {
                                  type: "email",
                                  message: "Please write valid email address!"
                                }
                              ]
                            })(
                              <Input
                                placeholder={formatMessage({
                                  id:
                                    "employee.employeedlg.content.placeholder.enter_email"
                                })}
                                onChange={this.onEmailChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="employee.employeedlg.content.label.mobile_number" />
                            </div>
                            {getFieldDecorator("mobile", {
                              initialValue: this.state.employee.phone.mobile,
                              getValueFromEvent: mobileFormatter
                            })(
                              <Input
                                maxLength={14}
                                placeholder={formatMessage({
                                  id:
                                    "employee.employeedlg.content.placeholder.enter_number"
                                })}
                                onChange={this.onMobileNumberChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="employee.employeedlg.content.label.phone_number" />
                            </div>
                            {getFieldDecorator("phone_number", {
                              initialValue: this.state.employee.phone
                                .primary_phone,
                              getValueFromEvent: mobileFormatter
                            })(
                              <Input
                                maxLength={14}
                                placeholder={formatMessage({
                                  id:
                                    "employee.employeedlg.content.placeholder.enter_number"
                                })}
                                onChange={this.onPhoneNumberChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="employee.employeedlg.content.label.hiring_date" />
                            </div>
                            {getFieldDecorator("hiring_date", {
                              initialValue: moment(
                                this.state.employee.employee_info.hiring_date,
                                "DD/MM/YYYY"
                              )
                            })(
                              <DatePicker
                                className="gx-w-100"
                                format="DD/MM/YYYY"
                                placeholder={formatMessage({
                                  id:
                                    "employee.employeedlg.content.placeholder.dd_mm_yyyy"
                                })}
                                suffixIcon={
                                  <i className="material-icons">date_range</i>
                                }
                                onChange={this.onHiringDateChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                        <Col lg={12} sm={12} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title">
                              <IntlMessages id="employee.employeedlg.content.label.hourly_rate" />
                            </div>
                            {getFieldDecorator("hourly_rate", {
                              initialValue: this.state.employee.employee_info
                                .hourly_rate
                            })(
                              <InputNumber
                                min={0}
                                precision={2}
                                formatter={value =>
                                  (value = `$ ${value}`.replace(
                                    /\B(?=(\d{3})+(?!\d))/g,
                                    ","
                                  ))
                                }
                                parser={value =>
                                  value.replace(/\$\s?|(,*)[A-Za-z]/g, "")
                                }
                                onChange={this.onHourlyRateChange}
                              />
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                    </div>
                  </Widget>
                </Col>
                <Col
                  xs={24}
                  sm={24}
                  md={12}
                  lg={12}
                  xl={12}
                  xxl={12}
                  className="gutter-row gx-p-0"
                >
                  <Widget styleName="gx-role-permissions gx-card-full gx-h-100 gx-mb-0">
                    <div className="gx-panel-title-bar">
                      <div className="gx-customized-modal-content-title">
                        <i
                          className="material-icons"
                          style={{ fontSize: "28px" }}
                        >
                          settings_applications
                        </i>
                        &nbsp;
                        <IntlMessages id="employee.employeedlg.content.label.role_permissions" />
                      </div>
                    </div>
                    <div className="gx-panel-content">
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Form.Item>
                            <div className="gx-customized-modal-content-field-title gx-mb-20">
                              <IntlMessages id="employee.employeedlg.content.label.employee_role" />
                            </div>
                            {getFieldDecorator("role", {
                              initialValue: this.state.employee.employee_info
                                .role
                            })(
                              <Radio.Group onChange={this.onRoleChange}>
                                <Radio value="Dispatcher">Dispatcher</Radio>
                                <Radio value="Field worker">Field worker</Radio>
                              </Radio.Group>
                            )}
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <Divider />
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <div className="gx-customized-modal-content-field-title gx-mb-20">
                            <IntlMessages id="employee.employeedlg.content.label.employee_permissions" />
                          </div>
                          <div className="gx-flex-row gx-align-items gx-mt-10 gx-mb-10">
                            <ButtonGroup className="gx-custom-toggle-buttons">
                              <Button
                                className={`gx-btn-toggle ${
                                  this.state.employee.permissions
                                    .update_company_info
                                    ? "gx-btn-toggle-yes"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onUpdateCompanyInfoChange(true)
                                }
                              >
                                {this.state.employee.permissions
                                  .update_company_info ? (
                                  <span>Yes</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                              <Button
                                className={`gx-btn-toggle ${
                                  !this.state.employee.permissions
                                    .update_company_info
                                    ? "gx-btn-toggle-no"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onUpdateCompanyInfoChange(false)
                                }
                              >
                                {!this.state.employee.permissions
                                  .update_company_info ? (
                                  <span>No</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                            </ButtonGroup>
                            <span
                              className={`gx-mr-30 gx-fs-sm ${
                                this.state.employee.permissions
                                  .update_company_info
                                  ? "gx-font-weight-medium"
                                  : ""
                              }`}
                              style={{
                                color: this.state.employee.permissions
                                  .update_company_info
                                  ? "#4c586d"
                                  : "#9399a2"
                              }}
                            >
                              Update company account info
                            </span>
                          </div>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <div className="gx-flex-row gx-align-items gx-mb-10">
                            <ButtonGroup className="gx-custom-toggle-buttons">
                              <Button
                                className={`gx-btn-toggle ${
                                  this.state.employee.permissions
                                    .delete_customers_employees
                                    ? "gx-btn-toggle-yes"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onDeleteCustomersEmployeesChange(true)
                                }
                              >
                                {this.state.employee.permissions
                                  .delete_customers_employees ? (
                                  <span>Yes</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                              <Button
                                className={`gx-btn-toggle ${
                                  !this.state.employee.permissions
                                    .delete_customers_employees
                                    ? "gx-btn-toggle-no"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onDeleteCustomersEmployeesChange(false)
                                }
                              >
                                {!this.state.employee.permissions
                                  .delete_customers_employees ? (
                                  <span>No</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                            </ButtonGroup>
                            <span
                              className={`gx-mr-30 gx-fs-sm ${
                                this.state.employee.permissions
                                  .delete_customers_employees
                                  ? "gx-font-weight-medium"
                                  : ""
                              }`}
                              style={{
                                color: this.state.employee.permissions
                                  .delete_customers_employees
                                  ? "#4c586d"
                                  : "#9399a2"
                              }}
                            >
                              Delete customers and employees
                            </span>
                          </div>
                        </Col>
                      </Row>
                      <Row gutter={10}>
                        <Col lg={24} sm={24} xs={24} className=" gutter-row">
                          <div className="gx-flex-row gx-align-items gx-mb-10">
                            <ButtonGroup className="gx-custom-toggle-buttons">
                              <Button
                                className={`gx-btn-toggle ${
                                  this.state.employee.permissions
                                    .chatting_with_fieldworkers
                                    ? "gx-btn-toggle-yes"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onChattingWithFieldworkersChange(true)
                                }
                              >
                                {this.state.employee.permissions
                                  .chatting_with_fieldworkers ? (
                                  <span>Yes</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                              <Button
                                className={`gx-btn-toggle ${
                                  !this.state.employee.permissions
                                    .chatting_with_fieldworkers
                                    ? "gx-btn-toggle-no"
                                    : ""
                                }`}
                                size="small"
                                onClick={() =>
                                  this.onChattingWithFieldworkersChange(false)
                                }
                              >
                                {!this.state.employee.permissions
                                  .chatting_with_fieldworkers ? (
                                  <span>No</span>
                                ) : (
                                  <ToggleButton></ToggleButton>
                                )}
                              </Button>
                            </ButtonGroup>
                            <span
                              className={`gx-mr-30 gx-fs-sm ${
                                this.state.employee.permissions
                                  .chatting_with_fieldworkers
                                  ? "gx-font-weight-medium"
                                  : ""
                              }`}
                              style={{
                                color: this.state.employee.permissions
                                  .chatting_with_fieldworkers
                                  ? "#4c586d"
                                  : "#9399a2"
                              }}
                            >
                              Chatting withi field workers
                            </span>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </Widget>
                </Col>
              </Row>
            </div>
            <div className="gx-customized-modal-footer">
              <div className="gx-flex-row gx-flex-nowrap">
                {this.props.data === undefined && (
                  <Button
                    className="gx-edit-customer-dlg-btn"
                    type="primary"
                    htmlType="submit"
                  >
                    <IntlMessages id="create_sendSMS" />
                  </Button>
                )}
                {this.props.data !== undefined && (
                  <Button
                    className="gx-edit-customer-dlg-btn"
                    type="primary"
                    htmlType="submit"
                  >
                    <IntlMessages id="update" />
                  </Button>
                )}
                <Button
                  className="gx-edit-customer-dlg-btn "
                  style={{
                    color: "white",
                    backgroundColor: "#a5abb5",
                    border: "none"
                  }}
                  onClick={this.props.onCancel}
                >
                  <IntlMessages id="cancel" />
                </Button>
              </div>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

EditEmployeeDlg = Form.create()(EditEmployeeDlg);
export default injectIntl(EditEmployeeDlg);
