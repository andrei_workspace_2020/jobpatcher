import React, { Component } from "react";
import { Button, Checkbox, Avatar, Tabs } from "antd";

import Auxiliary from "util/Auxiliary";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import { Link } from "react-router-dom";
import Informations from "./Informations";
import PreferencesPermissions from "./PreferencesPermissions";
import ScheduledJobs from "./ScheduledJobs";
import History from "./History";

const TabPane = Tabs.TabPane;

class CustomerJobPanel extends Component {
  state = {};

  constructor(props, context) {
    super(props, context);
  }
  onSubmitEmployeeInfo = info => {
    this.props.onSubmitEmployeeInfo(info);
  };
  onDeleteNote = id => {
    this.props.onDeleteNote(id);
  };
  onSubmitNote = note => {
    this.props.onSubmitNote(note);
  };
  onSubmitAddress = address => {
    this.props.onSubmitAddress(address);
  };
  onDeleteAddress = id => {
    this.props.onDeleteAddress(id);
  };
  onAddFile = file => {
    this.props.onAddFile(file);
  };
  onDeleteFile = id => {
    this.props.onDeleteFile(id);
  };
  onSubmitPreferences = info => {
    this.props.onSubmitPreferences(info);
  };
  onSubmitPermissions = info => {
    this.props.onSubmitPermissions(info);
  };
  render() {
    var { data } = this.props;
    console.log(data);
    return (
      <Widget styleName="gx-card-full gx-profile-detail-panel gx-customer-profile">
        <Tabs
          className="gx-customer-detail-panel-tab"
          defaultActiveKey="1"
          animated={false}
          prevIcon={<i className="material-icons">close</i>}
        >
          <TabPane
            tab={<IntlMessages id="employee.profile.tab.information" />}
            key="1"
          >
            <Informations
              data={data}
              onSubmitEmployeeInfo={this.onSubmitEmployeeInfo}
              onSubmitAddress={this.onSubmitAddress}
              onDeleteAddress={this.onDeleteAddress}
              onDeleteNote={this.onDeleteNote}
              onSubmitNote={this.onSubmitNote}
              onAddFile={this.onAddFile}
              onDeleteFile={this.onDeleteFile}
            />
          </TabPane>
          <TabPane
            tab={
              <IntlMessages id="employee.profile.tab.preferences_permissions" />
            }
            key="2"
          >
            <PreferencesPermissions
              data={data}
              onSubmitPreferences={this.onSubmitPreferences}
              onSubmitPermissions={this.onSubmitPermissions}
            />
          </TabPane>
          <TabPane
            tab={<IntlMessages id="employee.profile.tab.scheduledtasks" />}
            key="3"
          >
            <ScheduledJobs data={data} />
          </TabPane>
          <TabPane
            tab={<IntlMessages id="employee.profile.tab.history" />}
            key="4"
          >
            <History data={data} />
          </TabPane>
        </Tabs>
      </Widget>
    );
  }
}

export default CustomerJobPanel;
