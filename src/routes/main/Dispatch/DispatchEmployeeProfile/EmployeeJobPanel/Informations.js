import React, { Component } from "react";
import { Select, Row, Col, Input, Button, Popover, DatePicker } from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import { changeDateYearFormat } from "util/DateTime";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import { GoogleMap, Marker, withGoogleMap } from "react-google-maps";
import CustomerNotes from "./CustomerNotes";
import CustomerUpload from "./CustomerUpload";
import moment, { relativeTimeThreshold } from "moment";

const Option = Select.Option;
const { TextArea } = Input;

const coordinates = { lat: 49.2853171, lng: -121.1119202 };
const SimpleMapExampleGoogleMap = withGoogleMap(props => {
  return (
    <GoogleMap defaultZoom={16} center={props.center}>
      {props.markers.map(marker => {
        return (
          <Marker
            key={marker.id}
            position={{
              lat: parseFloat(marker.lat),
              lng: parseFloat(marker.lng)
            }}
          ></Marker>
        );
      })}
    </GoogleMap>
  );
});

class Informations extends Component {
  state = {
    isEmployeeInfoFormChanged: false,
    isAddressFormChanged: false,
    isAddressActionPopoverVisible: [],
    isNoteActionPopoverVisible: [],
    isFileActionPopoverVisible: [],
    isEmployeeInfoEditable: false,
    isAddressFormEditable: false,
    editablePanelKey: -1,
    selected_address: {
      id: "",
      title: "",
      street: "",
      unit: "",
      city: "",
      state: "",
      zip: "",
      alarmcode: "",
      property_key: ""
    },
    hiring_date: this.props.data.employee_info.hiring_date,
    role: this.props.data.employee_info.role,
    hourly_rate: this.props.data.employee_info.hourly_rate,
    birthday: this.props.data.employee_info.birthday,
    app_version: this.props.data.employee_info.app_version
  };

  constructor(props, context) {
    super(props, context);
  }
  componentDidMount() {
    let array = [];
    for (let i = 0; i < this.props.data.address.length; i++) array[i] = false;
    this.setState({ isAddressActionPopoverVisible: [...array] });
    for (let i = 0; i < this.props.data.notes.length; i++) array[i] = false;
    this.setState({ isNoteActionPopoverVisible: [...array] });
    for (let i = 0; i < this.props.data.files.length; i++) array[i] = false;
    this.setState({ isFileActionPopoverVisible: [...array] });
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.editablePanelKey != this.state.editablePanelKey &&
      this.state.editablePanelKey == 2
    ) {
      //if 2nd panel is active
      if (this.state.selected_address.title == "") {
        let temp = this.state.selected_address;
        temp.title = "Address #" + (this.props.data.address.length + 1);
        this.setState({ selected_address: temp });
      }
    }
  }
  onInitiateState = () => {
    this.setState({
      isEmployeeInfoFormChanged: false,
      isAddressFormChanged: false,
      isAddressActionPopoverVisible: [],
      isNoteActionPopoverVisible: [],
      isFileActionPopoverVisible: [],
      editablePanelKey: -1,
      selected_address: {
        id: "",
        title: "",
        street: "",
        unit: "",
        city: "",
        state: "",
        zip: "",
        alarmcode: "",
        property_key: "",
        lat: "",
        lng: ""
      },
      hiring_date: this.props.data.employee_info.hiring_date,
      role: this.props.data.employee_info.role,
      hourly_rate: this.props.data.employee_info.hourly_rate,
      birthday: this.props.data.employee_info.birthday,
      app_version: this.props.data.employee_info.app_version
    });
  };
  onSetEditablePanelKey = key => {
    this.setState({ editablePanelKey: key }, () => {
      if (key == 2) {
        let inputNode = document.getElementById("street");
        var options = {
          types: ["address"],
          componentRestrictions: {
            country: "ca"
          }
        };
        this.autoComplete = new window.google.maps.places.Autocomplete(
          inputNode,
          options
        );
        this.autoComplete.addListener("place_changed", this.onPlaceChange);
      }
    });
  };
  onPlaceChange = () => {
    let place = this.autoComplete.getPlace();
    console.log(place);
    let location = place.geometry.location;
    let temp = this.state.selected_address;
    temp.lat = location.lat();
    temp.lng = location.lng();
    temp.street = "";
    for (var i = 0; i < place.address_components.length; i++) {
      for (var j = 0; j < place.address_components[i].types.length; j++) {
        if (place.address_components[i].types[j] == "street_number") {
          temp.street += place.address_components[i].long_name + " ";
        } else if (place.address_components[i].types[j] == "route") {
          temp.street += place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "locality") {
          // or administrative_area_level_2
          temp.city = place.address_components[i].long_name;
        } else if (
          place.address_components[i].types[j] == "administrative_area_level_1"
        ) {
          temp.state = place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "postal_code") {
          temp.zipcode = place.address_components[i].long_name;
        }
      }
    }
    this.setState({ selected_address: temp });
  };
  onHiringDateChange = (date, dateString) => {
    this.setState({ hiring_date: dateString });
    this.setState({ isEmployeeInfoFormChanged: true });
  };
  onEmployeeRoleChange = value => {
    this.setState({ role: value });
    this.setState({ isEmployeeInfoFormChanged: true });
  };
  onHourlyRateChange = e => {
    this.setState({ hourly_rate: e.target.value });
    this.setState({ isEmployeeInfoFormChanged: true });
  };
  onDateOfBirthChange = (date, dateString) => {
    this.setState({ birthday: dateString });
    this.setState({ isEmployeeInfoFormChanged: true });
  };

  onSubmitEmployeeInfo = () => {
    if (this.state.isEmployeeInfoFormChanged) {
      let hiring_date = this.state.hiring_date;
      let role = this.state.role;
      let hourly_rate = this.state.hourly_rate;
      let birthday = this.state.birthday;
      let app_version = this.state.app_version;
      console.log("Hourly rate: " + hourly_rate);
      this.props.onSubmitEmployeeInfo({
        hiring_date: hiring_date,
        role: role,
        hourly_rate: hourly_rate,
        birthday: birthday,
        app_version: app_version
      });
      this.setState({ isEmployeeInfoFormChanged: false });
    }
    this.onInitiateState();
  };
  onAddressActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isAddressActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isAddressActionPopoverVisible: temp });
  };
  onAddressTitleChange = e => {
    let temp = this.state.selected_address;
    temp.title = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onStreetChange = e => {
    let temp = this.state.selected_address;
    temp.street = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onUnitChange = e => {
    let temp = this.state.selected_address;
    temp.unit = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onCityChange = e => {
    let temp = this.state.selected_address;
    temp.city = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onStateChange = value => {
    let temp = this.state.selected_address;
    temp.state = value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onZipcodeChange = e => {
    let temp = this.state.selected_address;
    temp.zipcode = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onAlarmcodeChange = e => {
    let temp = this.state.selected_address;
    temp.alarmcode = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onPropertyKeyChange = e => {
    let temp = this.state.selected_address;
    temp.property_key = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onAddAddress = () => {
    this.onInitiateState();
    this.onSetEditablePanelKey(2);
  };
  onSubmitAddress = () => {
    if (this.state.isAddressFormChanged) {
      this.props.onSubmitAddress(this.state.selected_address);
      this.setState({ isAddressFormChanged: false });
    }
    this.onInitiateState();
  };
  onNoteActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isNoteActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isNoteActionPopoverVisible: temp });
  };
  onFileActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isFileActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isFileActionPopoverVisible: temp });
  };
  onDeleteNote = id => {
    this.props.onDeleteNote(id);
  };
  onSubmitNote = note => {
    this.props.onSubmitNote(note);
    this.onInitiateState();
  };
  onAddFile = file => {
    this.props.onAddFile(file);
  };
  onDeleteFile = id => {
    this.props.onDeleteFile(id);
  };

  notAvailableWhenBlank(item) {
    if (item === "" || item == " ") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="customer.profile.notavailable" />
        </div>
      );
    }
    return item;
  }
  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { data } = this.props;
    return (
      <div
        className="gx-tab-content gx-tab-informations-content gx-mb-0"
        style={{ padding: "20px 20px 0px" }}
      >
        <Row>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full">
              <div className="gx-panel-title-bar">
                <h5>
                  <IntlMessages id="employee.profile.informations.employee_info" />
                </h5>
                {this.state.editablePanelKey == 1 ? (
                  <Button
                    className={`gx-btn-save ${
                      this.state.isEmployeeInfoFormChanged
                        ? "gx-btn-save-active"
                        : ""
                    }`}
                    onClick={this.onSubmitEmployeeInfo}
                  >
                    Save
                  </Button>
                ) : (
                  <div
                    className="gx-edit-circle"
                    onClick={() => {
                      this.onInitiateState();
                      this.onSetEditablePanelKey(1);
                    }}
                  >
                    <i className="material-icons gx-icon-action">edit</i>
                  </div>
                )}
              </div>
              <div className="gx-panel-content">
                <div className="gx-customized-content-field">
                  <div
                    className={`gx-customized-content-field-title ${
                      this.state.editablePanelKey == 1 ? "editable" : ""
                    }`}
                  >
                    <IntlMessages id="employee.profile.informations.hiring_date" />
                  </div>
                  <div className="gx-customized-content-field-value">
                    {this.state.editablePanelKey == 1 ? (
                      <DatePicker
                        className="gx-w-100"
                        format="DD/MM/YYYY"
                        suffixIcon={
                          <i className="material-icons">date_range</i>
                        }
                        value={moment(this.state.hiring_date, "DD/MM/YYYY")}
                        onChange={this.onHiringDateChange}
                      />
                    ) : (
                      <span>
                        {changeDateYearFormat(data.employee_info.hiring_date)}
                      </span>
                    )}
                  </div>
                </div>
                <div className="gx-customized-content-field">
                  <div
                    className={`gx-customized-content-field-title ${
                      this.state.editablePanelKey == 1 ? "editable" : ""
                    }`}
                  >
                    <IntlMessages id="employee.profile.informations.employee_role" />
                  </div>
                  <div className="gx-customized-content-field-value">
                    {this.state.editablePanelKey == 1 ? (
                      <Select
                        suffixIcon={
                          <i className="material-icons">expand_more</i>
                        }
                        value={this.state.role}
                        onChange={this.onEmployeeRoleChange}
                      >
                        <Option value="Administrator">Administrator</Option>
                        <Option value="Jobpatcher">Jobpatcher</Option>
                      </Select>
                    ) : (
                      <span>
                        {this.notAvailableWhenBlank(data.employee_info.role)}
                      </span>
                    )}
                  </div>
                </div>
                <div className="gx-customized-content-field">
                  <div className="gx-customized-content-field-title">
                    <IntlMessages id="employee.profile.informations.hourly_rate" />
                  </div>
                  <div className="gx-customized-content-field-value">
                    {this.state.editablePanelKey == 1 ? (
                      <Input
                        value={this.state.hourly_rate}
                        onChange={this.onHourlyRateChange}
                      />
                    ) : (
                      <span>
                        {"$" +
                          parseFloat(data.employee_info.hourly_rate).toFixed(2)}
                      </span>
                    )}
                  </div>
                </div>
                <div className="gx-customized-content-field">
                  <div className="gx-customized-content-field-title">
                    <IntlMessages id="employee.profile.informations.date_of_birth" />
                  </div>
                  <div className="gx-customized-content-field-value">
                    {this.state.editablePanelKey == 1 ? (
                      <DatePicker
                        className="gx-w-100"
                        format="DD/MM/YYYY"
                        suffixIcon={
                          <i className="material-icons">date_range</i>
                        }
                        value={moment(this.state.birthday, "DD/MM/YYYY")}
                        onChange={this.onDateOfBirthChange}
                      />
                    ) : (
                      <span>
                        {data.employee_info.birthday != ""
                          ? changeDateYearFormat(data.employee_info.birthday)
                          : ""}
                      </span>
                    )}
                  </div>
                </div>
                <div className="gx-customized-content-field">
                  <div className="gx-customized-content-field-title">
                    <IntlMessages id="employee.profile.informations.app_version" />
                  </div>
                  <div className="gx-customized-content-field-value">
                    <span>
                      {this.notAvailableWhenBlank(
                        data.employee_info.app_version
                      )}
                    </span>
                  </div>
                </div>
              </div>
            </Widget>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full">
              <div className="gx-panel-title-bar ">
                <h5>
                  <IntlMessages id="employee.profile.informations.employee_address" />
                </h5>
                {this.state.editablePanelKey == 2 ? (
                  <Button
                    className={`gx-btn-save ${
                      this.state.isAddressFormChanged
                        ? "gx-btn-save-active"
                        : ""
                    }`}
                    onClick={this.onSubmitAddress}
                  >
                    Save
                  </Button>
                ) : (
                  <div className="gx-edit-circle" onClick={this.onAddAddress}>
                    <i className="material-icons gx-icon-action">add</i>
                  </div>
                )}
              </div>
              <div
                className="gx-panel-content"
                style={{ padding: "60px 0px 0px 0px" }}
              >
                <div className="gx-panel-content-scroll">
                  {this.state.editablePanelKey == 2 ? (
                    <div className="gx-p-20 gx-w-100">
                      <div className="gx-customized-content-field">
                        <div
                          className={`gx-customized-content-field-title ${
                            this.state.editablePanelKey == 2 ? "editable" : ""
                          }`}
                        >
                          Address title
                        </div>
                        <div className="gx-customized-content-field-value">
                          <Input
                            value={this.state.selected_address.title}
                            onChange={this.onAddressTitleChange}
                          />
                        </div>
                      </div>
                      <div className="gx-customized-content-field">
                        <div
                          className={`gx-customized-content-field-title ${
                            this.state.editablePanelKey == 2 ? "editable" : ""
                          }`}
                        >
                          Street
                        </div>
                        <div className="gx-customized-content-field-value">
                          <Input
                            id="street"
                            value={this.state.selected_address.street}
                            onChange={this.onStreetChange}
                          />
                        </div>
                      </div>
                      <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 2 ? "editable" : ""
                            }`}
                          >
                            Unit
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              placeholder="Unit number"
                              value={this.state.selected_address.unit}
                              onChange={this.onUnitChange}
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-w-100">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 2 ? "editable" : ""
                            }`}
                          >
                            City
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              value={this.state.selected_address.city}
                              onChange={this.onCityChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 2 ? "editable" : ""
                            }`}
                          >
                            State
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Select
                              suffixIcon={
                                <i className="material-icons">expand_more</i>
                              }
                              value={this.state.selected_address.state}
                              onChange={this.onStateChange}
                            >
                              <Option value="Ontario">Ontario</Option>
                              <Option value="Quebec">Quebec</Option>
                            </Select>
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-w-100">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 2 ? "editable" : ""
                            }`}
                          >
                            Zipcode
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              value={this.state.selected_address.zipcode}
                              onChange={this.onZipcodeChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="gx-customized-content-field">
                        <div
                          className={`gx-customized-content-field-title ${
                            this.state.editablePanelKey == 2 ? "editable" : ""
                          }`}
                        >
                          Alarm code
                        </div>
                        <div className="gx-customized-content-field-value">
                          <Input
                            value={this.state.selected_address.alarmcode}
                            onChange={this.onAlarmcodeChange}
                          />
                        </div>
                      </div>
                      <div className="gx-customized-content-field">
                        <div
                          className={`gx-customized-content-field-title ${
                            this.state.editablePanelKey == 2 ? "editable" : ""
                          }`}
                        >
                          Key note
                        </div>
                        <div className="gx-customized-content-field-value">
                          <Input
                            value={this.state.selected_address.property_key}
                            onChange={this.onPropertyKeyChange}
                          />
                        </div>
                      </div>
                    </div>
                  ) : data.address.length > 0 ? (
                    <div>
                      <SimpleMapExampleGoogleMap
                        center={{
                          lat: parseFloat(data.address[0].lat),
                          lng: parseFloat(data.address[0].lng)
                        }}
                        markers={data.address}
                        loadingElement={<div style={{ height: `100%` }} />}
                        containerElement={
                          <div
                            className="gx-dispatch-gps-googlemap-container"
                            style={{ height: "150px" }}
                          />
                        }
                        mapElement={<div style={{ height: `100%` }} />}
                      />
                      {data.address.map((add, index) => (
                        <div
                          key={index}
                          className="address-info"
                          style={{ padding: "15px 10px 5px 20px" }}
                        >
                          <div className="gx-customized-content-field">
                            <div className="gx-customized-content-field-title">
                              <span>
                                {add.title != ""
                                  ? add.title
                                  : "Address #" + (index + 1)}
                              </span>
                            </div>
                            <div className="gx-customized-content-field-value">
                              <table className="gx-w-100">
                                <tbody>
                                  <tr>
                                    <td width="50%">
                                      <span>
                                        {add.street +
                                          " " +
                                          add.city +
                                          ", " +
                                          add.state +
                                          " " +
                                          add.zipcode}
                                      </span>
                                    </td>
                                    <td width="50%">
                                      <div className="gx-flex-row gx-align-items-center gx-justify-content-end">
                                        <Popover
                                          overlayClassName="gx-popover-customer-address-action"
                                          placement="bottomRight"
                                          content={
                                            <div>
                                              <div
                                                className="gx-menuitem"
                                                onClick={() => {
                                                  this.setState({
                                                    selected_address: {
                                                      ...data.address[index]
                                                    }
                                                  });
                                                  this.onAddressActionPopoverVisibleChange(
                                                    false,
                                                    index
                                                  );

                                                  this.onSetEditablePanelKey(2);
                                                }}
                                              >
                                                <i className="material-icons">
                                                  edit
                                                </i>
                                                <span>Edit address</span>
                                              </div>
                                              <div
                                                className="gx-menuitem"
                                                onClick={() => {
                                                  this.props.onDeleteAddress(
                                                    add.id
                                                  );
                                                  this.onAddressActionPopoverVisibleChange(
                                                    false,
                                                    index
                                                  );
                                                }}
                                              >
                                                <i className="material-icons">
                                                  delete
                                                </i>
                                                <span>Delete address</span>
                                              </div>
                                            </div>
                                          }
                                          visible={
                                            this.state
                                              .isAddressActionPopoverVisible[
                                              index
                                            ]
                                          }
                                          onVisibleChange={visible =>
                                            this.onAddressActionPopoverVisibleChange(
                                              visible,
                                              index
                                            )
                                          }
                                          trigger="click"
                                        >
                                          <i
                                            className="material-icons gx-icon-action"
                                            style={{ fontSize: "26px" }}
                                          >
                                            more_vert
                                          </i>
                                        </Popover>
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  ) : (
                    <div
                      className="gx-empty-note-container gx-flex-row gx-align-items-center gx-justify-content-center"
                      style={{ height: "120px" }}
                    >
                      <span
                        className="gx-fs-lg gx-font-weight-medium"
                        style={{ color: "#cfd1d5" }}
                      >
                        Nothing available
                      </span>
                    </div>
                  )}
                </div>
              </div>
            </Widget>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full">
              <div className="gx-panel-title-bar">
                <h5>
                  <IntlMessages id="employee.profile.informations.employee_notes" />
                </h5>
              </div>
              <div
                className="gx-panel-content gx-panel-customer-notes gx-pl-0 gx-pr-0 gx-pb-0"
                style={{ paddingTop: "60px" }}
              >
                <CustomerNotes
                  data={data}
                  onDeleteNote={this.onDeleteNote}
                  onSubmitNote={this.onSubmitNote}
                  editablePanelKey={this.state.editablePanelKey}
                  onInitiateState={this.onInitiateState}
                  onSetEditablePanelKey={this.onSetEditablePanelKey}
                  onNoteActionPopoverVisibleChange={
                    this.onNoteActionPopoverVisibleChange
                  }
                  isNoteActionPopoverVisible={
                    this.state.isNoteActionPopoverVisible
                  }
                />
              </div>
            </Widget>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full">
              <div className="gx-panel-title-bar">
                <h5>
                  <IntlMessages id="employee.profile.informations.employee_files" />
                </h5>
              </div>
              <div
                className="gx-panel-content"
                style={{ padding: "60px 0px 0px 0px" }}
              >
                <CustomerUpload
                  data={data}
                  editablePanelKey={this.state.editablePanelKey}
                  onInitiateState={this.onInitiateState}
                  onSetEditablePanelKey={this.onSetEditablePanelKey}
                  onAddFile={this.onAddFile}
                  onDeleteFile={this.onDeleteFile}
                  onFileActionPopoverVisibleChange={
                    this.onFileActionPopoverVisibleChange
                  }
                  isFileActionPopoverVisible={
                    this.state.isFileActionPopoverVisible
                  }
                />
              </div>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

export default injectIntl(Informations);
