import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Card, Row, Col, Button, Popover, Checkbox } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import ButtonGroup from "antd/lib/button/button-group";
import Widget from "components/Widget";
class JobHistoryInvoicesTabPanel extends Component {
  state = {};
  render() {
    const { data } = this.props;
    return (
      <div className="gx-panel-job-history-content gx-h-100 gx-flex-row gx-flex-justify-content-space-between">
        <Widget styleName="gx-card-full gx-job-history-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
          <table className="gx-job-history-table gx-jobs-table gx-w-100">
            <thead>
              <tr>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="employee.profile.job_history.job_id" />
                </th>
                <th style={{ minWidth: "200px" }}>
                  <IntlMessages id="employee.profile.job_history.scheduled" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="employee.profile.job_history.amount" />
                </th>
                <th style={{ minWidth: "150px" }}>
                  <IntlMessages id="employee.profile.job_history.job_status" />
                </th>
                <th style={{ minWidth: "150px" }}></th>
              </tr>
            </thead>
            <tbody>
              {data.jobs.map((item, index) => (
                <tr key={index}>
                  <td>
                    <Link to="#">{item.id}</Link>
                  </td>
                  <td>{item.scheduled}</td>
                  <td>{"$ " + item.amount.toFixed(2)}</td>
                  <td>
                    <div className="gx-flex-row gx-align-items-center">
                      <i
                        className="material-icons"
                        style={{
                          color: "#39bf58",
                          marginRight: "5px"
                        }}
                      >
                        check_circle_outline
                      </i>
                      {item.job_status}
                    </div>
                  </td>
                  <td>
                    <Button
                      className="gx-btn-view-job gx-customized-button gx-customized-text-button gx-ml-10"
                      type="default"
                      style={{ width: "120px" }}
                    >
                      <IntlMessages id="employee.profile.job_history.view" />
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </Widget>
      </div>
    );
  }
}
export default injectIntl(JobHistoryInvoicesTabPanel);
