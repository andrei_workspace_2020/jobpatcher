import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Button, Select, Modal, Tag, Tabs, Row, Col } from "antd";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import SearchBox from "components/SearchBox";
import SelectableButtonGroup from "components/Button/SelectableButtonGroup";
import JobHistoryJobsTabPanel from "./JobHistoryJobsTabPanel";
import JobHistoryEstimatesTabPanel from "./JobHistoryEstimatesTabPanel";

class JobHistoryPanel extends Component {
  state = {
    tab: "jobs",
    searchText: ""
  };
  onTabChange = tab => {
    this.setState({ tab });
  };
  updateSearchJobs = searchText => this.setState({ searchText });
  renderActionBar = () => {
    const { tab } = this.state;
    return (
      <div className="gx-sales-module-content gx-sales-estimates-toolbar-ss">
        <div>
          {/* mobile reponsive... if width > sm-max then visible, else disappear */}
          <div className="gx-dispatch-top-toolbar gx-a-align-around-ss gx-d-md-flex gx-d-none">
            <div className="gx-div-align-center gx-mb-12">
              <SelectableButtonGroup
                className="gx-d-md-block gx-d-none"
                selected={tab}
                onChange={this.onTabChange}
              >
                <Button
                  key="jobs"
                  style={{ paddingRight: "18px", paddingLeft: "18px" }}
                >
                  <IntlMessages id="employee.profile.job_history.jobs" />
                </Button>
                <Button
                  key="estimates"
                  style={{ paddingRight: "22px", paddingLeft: "22px" }}
                >
                  <IntlMessages id="employee.profile.job_history.estimates" />
                </Button>
              </SelectableButtonGroup>
            </div>
            <div className="gx-div-align-center gx-mb-12">
              <div className="">
                <SearchBox
                  styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-estimate-search-bar"
                  placeholder={"Search jobs"}
                  onChange={evt => this.updateSearchJobs(evt.target.value)}
                  value={this.state.searchText}
                />
              </div>
            </div>
          </div>
          {/* end responsive */}

          {/* mobile reponsive... if width < sm-max then visible, else disappear */}
          <div className="gx-sales-top-toolbar gx-a-align-around-ss gx-a-md-block gx-d-none gx-a-md-flex-column">
            <div className="gx-div-align-center gx-mb-12 gx-sales-estimate-mobile-toolbar-btngroup gx-w-100">
              <div className="gx-div-align-center gx-sales-estimate-mobile-toolbar-search">
                <div className="gx-w-100" style={{ paddingRight: "5px" }}>
                  <SearchBox
                    styleName="gx-lt-icon-search-bar-lg gx-dispatch-search gx-estimate-search-bar gx-w-100"
                    placeholder={"Search"}
                    onChange={evt => this.updateSearchJobs(evt.target.value)}
                    value={this.state.searchText}
                  />
                </div>
              </div>
            </div>
            <div className="gx-div-align-center gx-mb-12 gx-sales-estimate-mobile-toolbar-btngroup gx-w-100">
              <SelectableButtonGroup
                className="gx-d-md-block gx-w-100 gx-selectable-buttongroup"
                selected={tab}
                onChange={this.onTabChange}
              >
                <Button
                  key="jobs"
                  style={{
                    paddingRight: "18px",
                    paddingLeft: "18px",
                    margin: "0",
                    width: "30%"
                  }}
                >
                  <IntlMessages id="employee.profile.job_history.jobs" />
                </Button>
                <Button
                  key="estimates"
                  style={{
                    paddingRight: "22px",
                    paddingLeft: "22px",
                    margin: "0",
                    width: "35%"
                  }}
                >
                  <IntlMessages id="employee.profile.job_history.estimates" />
                </Button>
              </SelectableButtonGroup>
            </div>
          </div>
          {/* end responsive */}
        </div>
      </div>
    );
  };
  render() {
    const { data } = this.props;
    return (
      <div className="gx-job-history-content-body">
        {this.renderActionBar()}
        <div
          className="gx-panel-content gx-pt-0 gx-panel-content-jobs"
          style={{ backgroundColor: "transparent" }}
        >
          <div className="gx-panel-content-scroll gx-h-100">
            {this.state.tab == "jobs" ? (
              <JobHistoryJobsTabPanel data={data} />
            ) : this.state.tab == "estimates" ? (
              <JobHistoryEstimatesTabPanel data={data} />
            ) : (
              <div />
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(JobHistoryPanel);
