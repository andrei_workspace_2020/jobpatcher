import React, { Component } from "react";
import { injectIntl } from "react-intl";
import JobHistoryPanel from "./JobHistoryPanel";
import IntlMessages from "util/IntlMessages";

class History extends Component {
  state = {};

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { data } = this.props;
    return (
      <div className="gx-flex-row gx-w-100 gx-tab-content gx-tab-history-content gx-flex-nowrap">
        <div className="gx-job-history-content gx-my-10 gx-mx-10 gx-w-100">
          <JobHistoryPanel data={data}></JobHistoryPanel>
        </div>
      </div>
    );
  }
}

export default injectIntl(History);
