import React, { Component } from "react";
import { Button, Select, Row, Col, Input } from "antd";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";

const Option = Select.Option;
const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

class PreferencesPermissions extends Component {
  state = {
    isPreferencesFormChanged: false,
    isPermissionsFormChanged: false,
    editablePanelKey: -1,
    communication_language: this.props.data.preferences.communication_language,
    email_notifications: this.props.data.preferences.email_notifications,
    sms_notifications: this.props.data.preferences.sms_notifications,
    push_notifications: this.props.data.preferences.push_notifications,
    see_phonenumber: this.props.data.permissions.see_phonenumber,
    see_pricing: this.props.data.permissions.see_pricing,
    take_payment: this.props.data.permissions.take_payment
  };

  constructor(props, context) {
    super(props, context);
  }
  onInitiateState = () => {
    this.setState({
      isPreferencesFormChanged: false,
      isPermissionsFormChanged: false,
      editablePanelKey: -1,
      communication_language: this.props.data.preferences
        .communication_language,
      email_notifications: this.props.data.preferences.email_notifications,
      sms_notifications: this.props.data.preferences.sms_notifications,
      push_notifications: this.props.data.preferences.push_notifications,
      see_phonenumber: this.props.data.permissions.see_phonenumber,
      see_pricing: this.props.data.permissions.see_pricing,
      take_payment: this.props.data.permissions.take_payment
    });
  };
  onSetEditablePanelKey = key => {
    this.setState({ editablePanelKey: key });
  };
  onCommunicationLanguageChange = value => {
    this.setState({ communication_language: value });
    this.setState({ isPreferencesFormChanged: true });
  };
  onEmailNotificationsChange = value => {
    this.setState({ email_notifications: value });
    this.setState({ isPreferencesFormChanged: true });
  };
  onSMSNotificationsChange = value => {
    this.setState({ sms_notifications: value });
    this.setState({ isPreferencesFormChanged: true });
  };
  onPushNotificationsChange = value => {
    this.setState({ push_notifications: value });
    this.setState({ isPreferencesFormChanged: true });
  };
  onSubmitPreferences = () => {
    if (this.state.isPreferencesFormChanged) {
      let info = {
        communication_language: this.state.communication_language,
        email_notifications: this.state.email_notifications,
        sms_notifications: this.state.sms_notifications,
        push_notifications: this.state.push_notifications
      };
      this.props.onSubmitPreferences(info);
      this.setState({ isPreferencesFormChanged: false });
    }
    this.onInitiateState();
  };
  onSeePhoneNumberChange = value => {
    this.setState({ see_phonenumber: value });
    this.setState({ isPermissionsFormChanged: true });
  };
  onSeePricingChange = value => {
    this.setState({ see_pricing: value });
    this.setState({ isPermissionsFormChanged: true });
  };
  onTakePaymentChange = value => {
    this.setState({ take_payment: value });
    this.setState({ isPermissionsFormChanged: true });
  };
  onSubmitPermissions = () => {
    if (this.state.isPermissionsFormChanged) {
      this.props.onSubmitPermissions({
        see_phonenumber: this.state.see_phonenumber,
        see_pricing: this.state.see_pricing,
        take_payment: this.state.take_payment
      });
      this.setState({ isPermissionsFormChanged: false });
    }
    this.onInitiateState();
  };
  notAvailableWhenBlank(item) {
    if (item === "" || item == " ") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="customer.profile.notavailable" />
        </div>
      );
    }
    return item;
  }
  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { data } = this.props;
    return (
      <div
        className="gx-tab-content gx-tab-financial-data-content"
        style={{ padding: "20px 20px 0px 20px" }}
      >
        <Row>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full gx-card-financial-data gx-mb-20">
              <div className="gx-panel-title-bar gx-pr-10">
                <h5 className="gx-text-uppercase">
                  <IntlMessages id="employee.profile.preferencespermissions.preferences" />
                </h5>
                {this.state.editablePanelKey == 1 ? (
                  <Button
                    className={`gx-btn-save ${
                      this.state.isPreferencesFormChanged
                        ? "gx-btn-save-active"
                        : ""
                    }`}
                    onClick={this.onSubmitPreferences}
                  >
                    Save
                  </Button>
                ) : (
                  <div
                    className="gx-edit-circle"
                    onClick={() => {
                      this.onInitiateState();
                      this.onSetEditablePanelKey(1);
                    }}
                  >
                    <i className="material-icons gx-icon-action">edit</i>
                  </div>
                )}
              </div>
              <div
                className="gx-panel-content gx-panel-customer-notes gx-pl-0 gx-pr-0 gx-pb-0"
                style={{ paddingTop: "60px" }}
              >
                <div className="gx-p-20">
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 1 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="employee.profile.preferencespermissions.communication_language" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 1 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.communication_language}
                          onChange={this.onCommunicationLanguageChange}
                        >
                          <Option value="English">English</Option>
                          <Option value="French">French</Option>
                          <Option value="Spanish">Spanish</Option>
                        </Select>
                      ) : (
                        <span>
                          {this.notAvailableWhenBlank(
                            data.preferences.communication_language
                          )}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 1 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="employee.profile.preferencespermissions.email_notifications" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 1 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.email_notifications}
                          onChange={this.onEmailNotificationsChange}
                        >
                          <Option value={true}>Yes</Option>
                          <Option value={false}>No</Option>
                        </Select>
                      ) : (
                        <span>
                          {data.preferences.email_notifications ? "Yes" : "No"}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 1 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="employee.profile.preferencespermissions.sms_notifications" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 1 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.sms_notifications}
                          onChange={this.onSMSNotificationsChange}
                        >
                          <Option value={true}>Yes</Option>
                          <Option value={false}>No</Option>
                        </Select>
                      ) : (
                        <span>
                          {data.preferences.sms_notifications ? "Yes" : "No"}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 1 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="employee.profile.preferencespermissions.push_notifications" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 1 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.push_notifications}
                          onChange={this.onPushNotificationsChange}
                        >
                          <Option value={true}>Yes</Option>
                          <Option value={false}>No</Option>
                        </Select>
                      ) : (
                        <span>
                          {data.preferences.push_notifications ? "Yes" : "No"}
                        </span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </Widget>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full gx-card-financial-data gx-mb-20">
              <div className="gx-panel-title-bar gx-pr-10">
                <h5 className="gx-text-uppercase">
                  <IntlMessages id="employee.profile.preferencespermissions.permissions" />
                </h5>
                {this.state.editablePanelKey == 2 ? (
                  <Button
                    className={`gx-btn-save ${
                      this.state.isPermissionsFormChanged
                        ? "gx-btn-save-active"
                        : ""
                    }`}
                    onClick={this.onSubmitPermissions}
                  >
                    Save
                  </Button>
                ) : (
                  <div
                    className="gx-edit-circle"
                    onClick={() => {
                      this.onInitiateState();
                      this.onSetEditablePanelKey(2);
                    }}
                  >
                    <i className="material-icons gx-icon-action">edit</i>
                  </div>
                )}
              </div>
              <div
                className="gx-panel-content gx-panel-customer-notes gx-pl-0 gx-pr-0 gx-pb-0"
                style={{ paddingTop: "60px" }}
              >
                <div className="gx-p-20">
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="employee.profile.preferencespermissions.see_phonenumber" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.see_phonenumber}
                          onChange={this.onSeePhoneNumberChange}
                        >
                          <Option value={true}>Yes</Option>
                          <Option value={false}>No</Option>
                        </Select>
                      ) : (
                        <span>
                          {data.permissions.see_phonenumber ? "Yes" : "No"}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="employee.profile.preferencespermissions.see_pricing" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.see_pricing}
                          onChange={this.onSeePricingChange}
                        >
                          <Option value={true}>Yes</Option>
                          <Option value={false}>No</Option>
                        </Select>
                      ) : (
                        <span>
                          {data.permissions.see_pricing ? "Yes" : "No"}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="employee.profile.preferencespermissions.take_payment" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.take_payment}
                          onChange={this.onTakePaymentChange}
                        >
                          <Option value={true}>Yes</Option>
                          <Option value={false}>No</Option>
                        </Select>
                      ) : (
                        <span>
                          {data.permissions.take_payment ? "Yes" : "No"}
                        </span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

export default injectIntl(PreferencesPermissions);
