import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Card, Row, Col, Button, Popover, Checkbox } from "antd";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import ButtonGroup from "antd/lib/button/button-group";
import Widget from "components/Widget";
class JobHistoryEstimatesTabPanel extends Component {
  state = {};
  render() {
    const { data } = this.props;
    console.log(data);
    return (
      <div className="gx-panel-job-history-content gx-h-100 gx-flex-row gx-flex-justify-content-space-between">
        <Widget styleName="gx-card-full gx-job-history-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
          <div className="gx-panel-title-bar gx-p-0">
            <Row className="gx-w-100" gutter={10}>
              <Col xl={4} lg={4} md={4} sm={4} xs={4} className="gutter-row">
                <div className="gx-customer-job-th">
                  <IntlMessages id="employee.profile.job_history.estimate_id" />
                </div>
              </Col>
              <Col xl={5} lg={5} md={5} sm={5} xs={5} className="gutter-row">
                <div className="gx-customer-job-th">
                  <IntlMessages id="employee.profile.job_history.date" />
                </div>
              </Col>
              <Col xl={5} lg={5} md={5} sm={5} xs={5} className="gutter-row">
                <div className="gx-customer-job-th">
                  <IntlMessages id="employee.profile.job_history.amount" />
                </div>
              </Col>
              <Col xl={5} lg={5} md={5} sm={5} xs={5} className="gutter-row">
                <div className="gx-customer-job-th">
                  <IntlMessages id="employee.profile.job_history.status" />
                </div>
              </Col>
              <Col xl={5} lg={5} md={5} sm={5} xs={5} className="gutter-row">
                <div className="gx-customer-job-th"></div>
              </Col>
            </Row>
          </div>
          <div
            className="gx-panel-content gx-p-0"
            style={{ marginTop: "60px" }}
          >
            <div className="gx-panel-content-scroll">
              {data.estimates.length > 0 ? (
                data.estimates.map((item, index) => (
                  <div key={index}>
                    <Row className="gx-w-100" gutter={10}>
                      <Col
                        xl={4}
                        lg={4}
                        md={4}
                        sm={4}
                        xs={4}
                        className="gutter-row"
                      >
                        <div className="gx-customer-job-td">
                          <Link to="#">{item.id}</Link>
                        </div>
                      </Col>
                      <Col
                        xl={4}
                        lg={4}
                        md={4}
                        sm={4}
                        xs={4}
                        className="gutter-row"
                      >
                        <div className="gx-customer-job-td">{item.date}</div>
                      </Col>
                      <Col
                        xl={4}
                        lg={4}
                        md={4}
                        sm={4}
                        xs={4}
                        className="gutter-row"
                      >
                        <div className="gx-customer-job-td">
                          {"$ " + item.amount.toFixed(2)}
                        </div>
                      </Col>
                      <Col
                        xl={4}
                        lg={4}
                        md={4}
                        sm={4}
                        xs={4}
                        className="gutter-row"
                      >
                        <div className="gx-customer-job-td">
                          <span
                            style={{
                              color:
                                item.status == "Converted"
                                  ? "#39bf58"
                                  : item.status == "Refused"
                                  ? "#f55555"
                                  : ""
                            }}
                          >
                            {item.status}
                          </span>
                        </div>
                      </Col>
                      <Col
                        xl={4}
                        lg={4}
                        md={4}
                        sm={4}
                        xs={4}
                        className="gutter-row"
                      >
                        <div className="gx-customer-job-td">
                          <Button
                            className="gx-btn-view-job gx-customized-button gx-customized-text-button gx-ml-10"
                            type="default"
                            style={{ width: "120px" }}
                          >
                            <IntlMessages id="employee.profile.job_history.view" />
                          </Button>
                        </div>
                      </Col>
                    </Row>
                  </div>
                ))
              ) : (
                <div
                  className="gx-flex-row gx-justify-content-center gx-align-items-center"
                  style={{ height: "100%", color: "#cfd1d5" }}
                >
                  <div className="gx-text-center">
                    <div className="gx-fs-xl gx-font-weight-medium">
                      <span>Nothing available</span>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </Widget>
      </div>
    );
  }
}
export default injectIntl(JobHistoryEstimatesTabPanel);
