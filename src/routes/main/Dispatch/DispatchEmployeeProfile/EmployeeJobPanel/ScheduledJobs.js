import React, { Component } from "react";
import { Button, Select, Row, Col, Badge, Calendar, Tabs } from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import IntlMessages from "util/IntlMessages";
import { changeDateFormat } from "util/DateTime";
import ButtonGroup from "antd/lib/button/button-group";
import SelectableButtonGroup from "components/Button/SelectableButtonGroup";
import { changeMonthYearFormat } from "util/DateTime";
import SearchBox from "components/SearchBox";
import moment from "moment";

const Option = Select.Option;
const TabPane = Tabs.TabPane;
function getListData(value) {
  let listData;
  switch (value.date()) {
    case 1:
      listData = [
        {
          time: "2:30 PM",
          name: "Kevin Walker",
          job: "Residential carpet cleaning",
          status: "completed"
        }
      ];
      break;
    case 3:
      listData = [
        {
          time: "2:30 PM",
          name: "Mark Landers",
          job: "Residential deep kitchen cleaning",
          status: "scheduled"
        }
      ];
      break;
    case 6:
      listData = [
        {
          time: "3:20 PM",
          name: "Peter Jackson",
          job: "Residential windows cleaning",
          status: "scheduled"
        }
      ];
      break;
    case 8:
      listData = [
        {
          time: "3:30 PM",
          name: "Maria Rosebella",
          job: "Residential windows cleaning",
          status: "scheduled"
        }
      ];
      break;
    default:
  }
  return listData || [];
}
class ScheduledJobs extends Component {
  state = {
    searchText: "",
    tab: "all",
    date: moment(new Date())
  };

  constructor(props, context) {
    super(props, context);
  }
  updateSearchText = text => {
    this.setState({ searchText: text });
  };
  onTabChange = tab => {
    this.setState({ tab });
  };
  dateCellRender = value => {
    const listData = getListData(value);
    return listData.length > 0 ? (
      <ul className="events gx-w-100">
        {listData.map(item => (
          <li key={item.content}>
            <div
              className={`gx-flex-column ${
                item.status == "completed"
                  ? "gx-schedule-completed-item"
                  : item.status == "scheduled"
                  ? "gx-schedule-scheduled-item"
                  : ""
              }`}
            >
              <div
                className="gx-flex-row gx-align-items-center"
                style={{ height: "18px", overflow: "hidden" }}
              >
                <span className="gx-fs-sm gx-font-weight-bold">
                  {item.time + " - "}
                </span>
                <Link to="/customers/profile">
                  <span className="gx-fs-sm gx-font-weight-bold">
                    {item.name}
                  </span>
                </Link>
              </div>
              <span
                className="gx-fs-sm gx-font-weight-medium"
                style={{ height: "18px", overflow: "hidden" }}
              >
                {item.job}
              </span>
            </div>
          </li>
        ))}
      </ul>
    ) : (
      <div />
    );
  };
  onPrevMonthSelected = () => {
    let current = new Date(this.state.date);
    let prev = new Date(current.getFullYear(), current.getMonth() - 1, 15);
    this.setState({ date: moment(prev) });
  };
  onNextMonthSelected = () => {
    let current = new Date(this.state.date);
    let next = new Date(current.getFullYear(), current.getMonth() + 1, 15);
    this.setState({ date: moment(next) });
  };
  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div
        className="gx-tab-content gx-tab-schedule-content"
        style={{ padding: "20px 20px 0px 20px" }}
      >
        <Row>
          <Col span={24} sm={24} xs={24}>
            <div className="gx-tab-schedule-content-header">
              <ButtonGroup className="gx-customer-list-buttongroup">
                <Button onClick={this.onPrevMonthSelected}>
                  <i className="material-icons">chevron_left</i>
                </Button>
                <Button>
                  <span className="gx-text-normal">
                    {changeMonthYearFormat(this.state.date)}
                  </span>
                </Button>
                <Button onClick={this.onNextMonthSelected}>
                  <i className="material-icons">chevron_right</i>
                </Button>
              </ButtonGroup>
              <span>
                <SearchBox
                  styleName="gx-lt-icon-search-bar-lg gx-dispatch-search"
                  placeholder={"Search tasks"}
                  onChange={evt => this.updateSearchText(evt.target.value)}
                  value={this.state.searchText}
                />
              </span>
            </div>
            <div className="gx-tab-schedule-content-header-mobile">
              <span className="gx-w-100">
                <SearchBox
                  styleName="gx-lt-icon-search-bar-lg gx-dispatch-search"
                  placeholder={"Search tasks"}
                  onChange={evt => this.updateSearchText(evt.target.value)}
                  value={this.state.searchText}
                />
              </span>
              <ButtonGroup className="gx-customer-list-buttongroup gx-w-100 gx-flex-row gx-flex-nowrap gx-align-items-center">
                <Button onClick={this.onPrevMonthSelected}>
                  <i className="material-icons">chevron_left</i>
                </Button>
                <Button className="gx-w-100">
                  <span className="gx-text-normal">
                    {changeMonthYearFormat(this.state.date)}
                  </span>
                </Button>
                <Button onClick={this.onNextMonthSelected}>
                  <i className="material-icons">chevron_right</i>
                </Button>
              </ButtonGroup>
            </div>
          </Col>
        </Row>
        <Row>
          <Col span={24} sm={24} xs={24}>
            <Calendar
              value={this.state.date}
              dateCellRender={this.dateCellRender}
              onSelect={value => this.setState({ date: value })}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default injectIntl(ScheduledJobs);
