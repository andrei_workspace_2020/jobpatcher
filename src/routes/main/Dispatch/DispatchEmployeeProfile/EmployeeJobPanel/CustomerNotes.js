import React, { Component } from "react";
import { Button, Checkbox, Avatar, Tag, Select, Popover, Input } from "antd";
import { injectIntl } from "react-intl";

import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import { changeDateFormat } from "util/DateTime";

const { TextArea } = Input;

class CustomerNotes extends Component {
  state = {
    note: "",
    selected_note: {
      id: "",
      name: "",
      note: "",
      datetime: "",
      avatar: ""
    }
  };
  onNoteChange = e => {
    this.props.onInitiateState();
    this.props.onSetEditablePanelKey(3);
    this.setState({ note: e.target.value });
  };
  onEditNote = id => {
    this.props.onInitiateState();
    this.props.onSetEditablePanelKey(3);
    let notes = this.props.data.notes;
    let index;
    notes.map((item, i) => {
      if (item.id == id) {
        index = i;
        this.setState({ note: item.note });
        this.setState({ selected_note: item });
      }
    });
    this.props.onNoteActionPopoverVisibleChange(false, index);
  };
  onSubmitNote = () => {
    let temp = this.state.selected_note;
    console.log(temp);
    if (temp.id == "") {
      temp.first_name = this.props.data.first_name;
      temp.last_name = this.props.data.last_name;
      temp.note = this.state.note;
      temp.datetime = new Date().toLocaleString("en-US");
      temp.avatar = this.props.data.avatar;
    } else {
      temp.note = this.state.note;
    }
    this.setState({ note: "" });
    this.setState({
      selected_note: {
        id: "",
        name: "",
        note: "",
        datetime: "",
        avatar: ""
      }
    });
    this.props.onSubmitNote(temp);
    this.props.onInitiateState();
  };
  render() {
    const {
      intl: { formatMessage },
      data
    } = this.props;
    return (
      <div className="gx-customer-tab gx-customer-activities-tab gx-p-20">
        <div className="gx-ss-newjob-note-input">
          <TextArea
            className="gx-customer-tab-input gx-border-0 gx-w-100"
            placeholder={formatMessage({
              id: "customer.profile.notes.typenotes"
            })}
            value={this.props.editablePanelKey == 3 ? this.state.note : ""}
            onChange={this.onNoteChange}
            rows="3"
          />
          <div
            className={`${
              this.state.note == "" || this.props.editablePanelKey != 3
                ? "gx-d-none"
                : "gx-flex-row gx-align-items-center gx-mt-10"
            }`}
          >
            <Button
              className="gx-btn-save gx-btn-save-active gx-mr-10"
              onClick={this.onSubmitNote}
            >
              Save
            </Button>
            <Button
              className="gx-btn-cancel"
              onClick={() => {
                this.setState({ note: "" });
              }}
            >
              Discord
            </Button>
          </div>
        </div>
        <div className="gx-customer-top-tab-scroll gx-pt-0">
          <div
            className="gx-customer-tab-content gx-ss-newjob-note-content gx-pt-20 gx-px-0"
            style={{ paddingBottom: "0px" }}
          >
            {data.notes.length === 0 ? (
              <div
                className="gx-flex-row gx-justify-content-center gx-align-items-center"
                style={{ height: "50px", color: "#cfd1d5" }}
              >
                <div className="gx-text-center">
                  <div className="gx-fs-lg gx-font-weight-medium">
                    <IntlMessages id="customer.profile.notes.nonotes" />
                  </div>
                </div>
              </div>
            ) : (
              <div>
                {data.notes.map((item, index) => (
                  <div key={index} className="gx-activity-list">
                    {item.avatar != "" ? (
                      <img
                        alt="avatar"
                        src={item.avatar}
                        className="gx-avatar-img gx-size-30 gx-border-0 gx-mr-10"
                        style={{ minWidth: "30px" }}
                      />
                    ) : (
                      <div
                        className="gx-avatar-img gx-size-30 gx-border-0 gx-mr-10"
                        style={{ minWidth: "30px" }}
                      >
                        {item.first_name.charAt(0)}
                      </div>
                    )}
                    <div className="gx-activity-list-description">
                      <div className="gx-flex-row">
                        <div className="gx-activity-list-name  gx-link">
                          {item.first_name + " " + item.last_name}
                        </div>
                        <div className="gx-activity-list-datetime">
                          {changeDateFormat(item.datetime)}
                        </div>
                      </div>
                      <div className="gx-flex-row gx-flex-nowrap gx-justify-content-between gx-align-items-center">
                        <div className="gx-activity-list-note gx-mr-10">
                          {item.note}
                        </div>
                        <Popover
                          overlayClassName="gx-popover-note-action"
                          placement="bottomRight"
                          trigger="click"
                          visible={this.props.isNoteActionPopoverVisible[index]}
                          onVisibleChange={visible =>
                            this.props.onNoteActionPopoverVisibleChange(
                              visible,
                              index
                            )
                          }
                          content={
                            <div>
                              <div
                                className="gx-menuitem"
                                onClick={() => this.onEditNote(item.id)}
                              >
                                <i className="material-icons">edit</i>
                                <span>Edit note</span>
                              </div>
                              <div
                                className="gx-menuitem"
                                onClick={() => this.props.onDeleteNote(item.id)}
                              >
                                <i className="material-icons">delete</i>
                                <span>Delete note</span>
                              </div>
                            </div>
                          }
                        >
                          <i
                            className="material-icons gx-icon-action"
                            style={{ fontSize: "26px" }}
                          >
                            more_vert
                          </i>
                        </Popover>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(CustomerNotes);
