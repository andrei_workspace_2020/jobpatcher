import React, { Component } from "react";
import { Button, Tag, Popover, Select, Input } from "antd";

import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import { Link } from "react-router-dom";
import ButtonGroup from "antd/lib/button/button-group";

const ToggleButton = () => {
  return (
    <div className="gx-btn-toggle-normal gx-flex-row">
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};
const { Option } = Select;

class ProfileCard extends Component {
  state = {
    isProfileEditable: false,
    isContactEditable: false,
    isCommunicationEditable: false,
    isFormChanged: false,
    customer_name: this.props.data.first_name + " " + this.props.data.last_name,
    email: this.props.data.email,
    primary_phone: this.props.data.phone.primary_phone,
    mobile: this.props.data.phone.mobile
  };

  constructor(props, context) {
    super(props, context);
  }
  onCustomerNameChange = e => {
    this.setState({ customer_name: e.target.value });
    this.setState({ isEditContactFormChanged: true });
  };
  onCustomerEmailChange = e => {
    this.setState({ email: e.target.value });
    this.setState({ isEditContactFormChanged: true });
  };
  onMobileNumberChange = e => {
    this.setState({ mobile: e.target.value });
    this.setState({ isEditContactFormChanged: true });
  };
  onPhoneNumberChange = e => {
    this.setState({ primary_phone: e.target.value });
    this.setState({ isEditContactFormChanged: true });
  };
  onContactFormVisibleChange = visible => {
    this.setState({ isContactEditable: visible });
  };
  onSubmitEmployeeContact = () => {
    if (this.state.isEditContactFormChanged) {
      this.props.onSubmitEmployeeContact({
        email: this.state.email,
        mobile: this.state.mobile,
        primary_phone: this.state.primary_phone
      });
      this.setState({ isEditContactFormChanged: false });
    }
    this.setState({ isContactEditable: false });
  };
  notAvailableWhenBlank(item) {
    if (item === "") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="employee.profile.notavailable" />
        </div>
      );
    }
    return item;
  }
  onGPSTrackingEnabledChange = flag => {
    this.props.onGPSTrackingEnabledChange(flag);
  };
  renderAvatar(icon) {
    return (
      <div className="gx-main-avatar gx-size-50 gx-mr-12">
        <i
          className="material-icons gx-w-100 gx-text-center"
          style={{ color: "#fbfbfd", fontSize: "36px" }}
        >
          {icon}
        </i>
      </div>
    );
  }

  render() {
    var { data } = this.props;

    return (
      <div className="gx-w-100" style={{ zIndex: "10" }}>
        <div className="gx-customer-profile-container">
          <div className="gx-customer-profile-card">
            <div className="gx-flex-row gx-flex-nowrap gx-h-100">
              <div className="gx-d-xl-block gx-mr-20">
                <div
                  className="gx-flex-row gx-flex-nowrap gx-align-items-center"
                  style={{ height: "30px" }}
                >
                  <Link to="/dispatch/employees">
                    <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                      <i className="material-icons gx-mr-2">arrow_back</i>
                      <span
                        className="gx-flex-row gx-flex-nowrap gx-align-items-center"
                        style={{ fontSize: "16px", fontWeight: "500" }}
                      >
                        <IntlMessages id="back" />
                      </span>
                    </div>
                  </Link>
                </div>
              </div>
              <div className="gx-w-100 gx-flex-column gx-justify-content-between">
                <div className="gx-flex-row">
                  <div className="gx-customer-profile-card-title gx-h-auto gx-flex-column gx-w-100">
                    <div className="gx-flex-row gx-align-items-center gx-justify-content-between ">
                      <div className="gx-flex-row">
                        <span
                          className="gx-mr-2"
                          style={{ fontSize: "16px", fontWeight: "500" }}
                        >
                          {data.first_name + " " + data.last_name}
                        </span>
                      </div>
                      {!this.state.isProfileEditable ? (
                        <div className="gx-edit-circle">
                          <i
                            className="material-icons gx-icon-action"
                            onClick={() =>
                              this.setState({ isProfileEditable: true })
                            }
                          >
                            edit
                          </i>
                        </div>
                      ) : (
                        <Button
                          type="default"
                          className={`gx-mb-0 gx-flex-row gx-align-items-center gx-btn-save ${
                            this.state.isFormChanged ? "gx-btn-save-active" : ""
                          }`}
                          onClick={() =>
                            this.setState({ isProfileEditable: false })
                          }
                        >
                          Save
                        </Button>
                      )}
                    </div>
                    <div className="gx-flex-row">
                      <Tag className="gx-tag-custom">
                        {data.employee_info.role}
                      </Tag>
                    </div>
                  </div>
                </div>
                <div className="gx-flex-row">
                  <div className="gx-d-lg-block">
                    <ButtonGroup className="gx-customer-list-buttongroup">
                      <Popover
                        overlayClassName="gx-popover-background-blue gx-customer-profile-popover gx-customer-table-estimate-popover"
                        placement="bottom"
                        trigger="hover"
                        content={
                          <div className="content">
                            Chat with {data.first_name + " " + data.last_name}
                          </div>
                        }
                      >
                        <Button>
                          <i className="material-icons">chat</i>
                        </Button>
                      </Popover>
                      <Popover
                        overlayClassName="gx-popover-background-blue gx-customer-profile-popover gx-customer-table-estimate-popover"
                        placement="bottom"
                        trigger="hover"
                        content={
                          <div className="content">
                            Terminate {data.first_name + " " + data.last_name}
                          </div>
                        }
                      >
                        <Button>
                          <i className="material-icons">cancel</i>
                        </Button>
                      </Popover>
                      <Popover
                        overlayClassName="gx-popover-customer-profile-action"
                        trigger="click"
                        placement="bottomLeft"
                        content={
                          <div>
                            <div className="gx-menuitem">
                              <i className="material-icons">edit</i>
                              <span>Edit employee</span>
                            </div>
                            <div className="gx-menuitem">
                              <i className="material-icons">delete</i>
                              <span>Delete employee</span>
                            </div>
                          </div>
                        }
                      >
                        <Button>
                          <i className="material-icons">more_horiz</i>
                        </Button>
                      </Popover>
                    </ButtonGroup>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="gx-customer-profile-card">
            <div className="gx-customer-profile-card-title gx-flex-row gx-align-items-center gx-justify-content-between">
              <div className="gx-font-weight-bold">
                <IntlMessages id="employee.profile.contact_details" />
              </div>
              <div className="gx-edit-circle">
                <Popover
                  overlayClassName="gx-popover-edit-customer-contact"
                  placement="bottomRight"
                  trigger="click"
                  visible={this.state.isContactEditable}
                  onVisibleChange={this.onContactFormVisibleChange}
                  content={
                    <div className="gx-popover-content">
                      <div className="gx-popover-header">
                        <h5>EDIT CONTACT</h5>
                        <Button
                          className={`gx-btn-save ${
                            this.state.isEditContactFormChanged
                              ? "gx-btn-save-active"
                              : ""
                          }`}
                          onClick={this.onSubmitEmployeeContact}
                        >
                          Save
                        </Button>
                      </div>
                      <div
                        className="gx-popover-body"
                        style={{ marginTop: "60px" }}
                      >
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            <IntlMessages id="employee.profile.email" />
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              placeholder="Enter an email"
                              value={this.state.email}
                              onChange={this.onCustomerEmailChange}
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            <IntlMessages id="employee.profile.mobile" />
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              placeholder="Enter a mobile number"
                              value={this.state.mobile}
                              onChange={this.onMobileNumberChange}
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            <IntlMessages id="employee.profile.phone" />
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              placeholder="Enter a phone number"
                              value={this.state.primary_phone}
                              onChange={this.onPhoneNumberChange}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  }
                >
                  <i className="material-icons gx-icon-action">edit</i>
                </Popover>
              </div>
            </div>
            <table className="gx-customer-profile-card-table">
              <tbody>
                <tr>
                  <td>
                    <IntlMessages id="employee.profile.email" />
                  </td>
                  <td>{data.email}</td>
                </tr>
                <tr>
                  <td>
                    <IntlMessages id="employee.profile.mobile" />
                  </td>
                  <td>{this.notAvailableWhenBlank(data.phone.mobile)}</td>
                </tr>
                <tr>
                  <td>
                    <IntlMessages id="employee.profile.phone" />
                  </td>
                  <td>
                    {this.notAvailableWhenBlank(data.phone.primary_phone)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="gx-d-md-block gx-d-none gx-customer-profile-card">
            <div
              className="gx-customer-profile-card-title gx-flex-row gx-justify-content-between"
              style={{ lineHeight: "30px" }}
            >
              <div className="gx-font-weight-bold">
                <IntlMessages id="employee.profile.gps_tracking" />
              </div>
            </div>
            <table className="gx-customer-profile-card-table">
              <tbody>
                <tr>
                  <td
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "flex-start"
                    }}
                  >
                    <ButtonGroup className="gx-custom-toggle-buttons">
                      <Button
                        className={`gx-btn-toggle ${
                          data.gps_tracking ? "gx-btn-toggle-yes" : ""
                        }`}
                        size="small"
                        onClick={() => this.onGPSTrackingEnabledChange(true)}
                      >
                        {data.gps_tracking ? (
                          <span>Yes</span>
                        ) : (
                          <ToggleButton></ToggleButton>
                        )}
                      </Button>
                      <Button
                        className={`gx-btn-toggle ${
                          !data.gps_tracking ? "gx-btn-toggle-no" : ""
                        }`}
                        size="small"
                        onClick={() => this.onGPSTrackingEnabledChange(false)}
                      >
                        {!data.gps_tracking ? (
                          <span>No</span>
                        ) : (
                          <ToggleButton></ToggleButton>
                        )}
                      </Button>
                    </ButtonGroup>
                    <div
                      className="gx-mr-30"
                      style={{
                        fontWeight: "500",
                        color: data.gps_tracking ? "#4c586d" : "#9399a2"
                      }}
                    >
                      {"Tracking " +
                        (data.gps_tracking ? "enabled" : "disabled")}
                    </div>
                  </td>
                </tr>
                <tr style={{ color: "#9399a2" }}>
                  <td>
                    <div
                      className="gx-flex-row gx-align-items-center"
                      style={{ marginTop: "5px" }}
                    >
                      <i className="material-icons gx-mr-10">info</i>
                      When login on mobile app
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default ProfileCard;
