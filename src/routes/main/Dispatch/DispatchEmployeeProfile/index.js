import React, { Component } from "react";
import ProfileCard from "./ProfileCard";
import EmployeeJobPanel from "./EmployeeJobPanel";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import { getEmployeeById, updateEmployee } from "appRedux/actions/Employees";

class DispatchEmployeeProfile extends Component {
  state = {
    data: {
      id: 1,
      avatar: "",
      title: "Mr.",
      first_name: "Robert",
      last_name: "Brannon",
      email: "robert@website.com",
      company_name: "Robert's company",
      phone: {
        mobile: "+1(705)-255-8504",
        primary_phone: "(613)-966-0626",
        work: ""
      },
      gps_tracking: true,
      active: true,
      starred: false,
      employee_info: {
        hiring_date: "07/25/2019",
        role: "Dispatcher",
        hourly_rate: 15.5,
        birthday: "04/03/1974",
        app_version: "V.2.0.1"
      },
      address: [
        {
          id: 0,
          title: "Address 1",
          street: "496 Hochelaga",
          unit: "",
          city: "Laval",
          state: "Quebec",
          zipcode: "H7P3H6",
          alarmcode: "",
          property_key:
            "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
          lat: 45.5841211,
          lng: -73.5285817
        }
      ],
      notes: [],
      jobs: [
        {
          id: "#3624",
          date: "29/12/2019",
          amount: 250,
          status: "Completed"
        },
        {
          id: "#3622",
          date: "29/12/2019",
          amount: 330,
          status: "Canceled"
        },
        {
          id: "#3468",
          date: "29/12/2019",
          amount: 320,
          status: "Completed"
        }
      ],
      estimates: [
        {
          id: "#3624",
          date: "29/12/2019",
          amount: 250,
          status: "Converted"
        },
        {
          id: "#3622",
          date: "29/12/2019",
          amount: 330,
          status: "Refused"
        },
        {
          id: "#3468",
          date: "29/12/2019",
          amount: 320,
          status: "Converted"
        }
      ],
      files: [],
      preferences: {
        communication_language: "English",
        email_notifications: true,
        sms_notifications: true,
        push_notifications: true
      },
      permissions: {
        see_phonenumber: true,
        see_pricing: true,
        take_payment: true
      }
    }
  };

  constructor(props, context) {
    super(props, context);
  }
  componentDidMount() {
    let id = window.location.href.split("/")[
      window.location.href.split("/").length - 1
    ];
    console.log("Selected employee id: " + id);
    this.props.getEmployeeById(id);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.employees !== this.props.employees) {
      let id = window.location.href.split("/")[
        window.location.href.split("/").length - 1
      ];
      console.log("Selected employee id: " + id);
      this.props.getEmployeeById(id);
    }
  }
  onChangeEmployeeStar = () => {
    let temp = this.props.selected_employee;
    temp.starred = !temp.starred;
    this.props.updateEmployee(temp.id, temp);
  };
  onSubmitEmployeeInfo = info => {
    let temp = this.props.selected_employee;
    temp.employee_info = info;
    this.props.updateEmployee(temp.id, temp);
  };
  onSubmitAddress = address => {
    let temp = this.props.selected_employee;
    let list = temp.address;
    if (address.id === "") {
      address.id = list.length;
      list.push(address);
      temp.address = list;
    } else {
      let newList = list.map(item => {
        if (item.id === address.id) return address;
        else return item;
      });
      temp.address = newList;
    }
    this.props.updateEmployee(temp.id, temp);
  };
  onDeleteAddress = id => {
    let temp = this.props.selected_employee;
    let list = temp.address;
    let index = 0;
    list.map((item, i) => {
      if (item.id === id) {
        index = i;
        return;
      }
    });
    list.splice(index, 1);
    temp.address = list;
    this.props.updateEmployee(temp.id, temp);
  };
  onDeleteNote = id => {
    let temp = this.props.selected_employee;
    let notes = temp.notes;
    let index = 0;
    notes.map((item, i) => {
      if (item.id === id) {
        index = i;
        return;
      }
    });
    notes.splice(index, 1);
    temp.notes = notes;
    this.props.updateEmployee(temp.id, temp);
  };
  onSubmitNote = note => {
    let temp = this.props.selected_employee;
    let list = temp.notes;
    if (note.id === "") {
      console.log("insert");
      note.id = list.length;
      list.unshift(note);
    } else {
      console.log("update");
      list.map(item => {
        if (item.id === note.id) {
          item = note;
        }
        return item;
      });
    }
    temp.notes = list;
    this.props.updateEmployee(temp.id, temp);
  };
  onAddFile = file => {
    let temp = this.props.selected_employee;
    temp.files.push(file);
    this.props.updateEmployee(temp.id, temp);
  };
  onDeleteFile = id => {
    let temp = this.props.selected_employee;
    let files = temp.files;
    let index;
    files.map((item, i) => {
      if (item.id === id) {
        index = i;
        return;
      }
    });
    files.splice(index, 1);
    temp.files = files;
    this.props.updateEmployee(temp.id, temp);
  };
  onSubmitEmployeeContact = contact => {
    console.log(contact);
    let temp = this.props.selected_employee;
    temp.email = contact.email;
    temp.phone.mobile = contact.mobile;
    temp.phone.primary_phone = contact.primary_phone;
    this.props.updateEmployee(temp.id, temp);
  };
  onGPSTrackingEnabledChange = flag => {
    let temp = this.props.selected_employee;
    temp.gps_tracking = flag;
    this.props.updateEmployee(temp.id, temp);
  };
  onSubmitPreferences = info => {
    let temp = this.props.selected_employee;
    temp.preferences = info;
    this.props.updateEmployee(temp.id, temp);
  };
  onSubmitPermissions = info => {
    let temp = this.props.selected_employee;
    temp.permissions = info;
    this.props.updateEmployee(temp.id, temp);
  };
  renderMainContent(emp) {
    return emp !== null ? (
      <div className="gx-main-content-container gx-customer-profile-module-content gx-p-0">
        <ProfileCard
          data={this.props.selected_employee}
          onSubmitEmployeeContact={this.onSubmitEmployeeContact}
          onChangeEmployeeStar={this.onChangeEmployeeStar}
          onGPSTrackingEnabledChange={this.onGPSTrackingEnabledChange}
        />
        <EmployeeJobPanel
          data={this.props.selected_employee}
          onSubmitEmployeeInfo={this.onSubmitEmployeeInfo}
          onSubmitAddress={this.onSubmitAddress}
          onDeleteAddress={this.onDeleteAddress}
          onDeleteNote={this.onDeleteNote}
          onSubmitNote={this.onSubmitNote}
          onAddFile={this.onAddFile}
          onDeleteFile={this.onDeleteFile}
          onSubmitPreferences={this.onSubmitPreferences}
          onSubmitPermissions={this.onSubmitPermissions}
        />
      </div>
    ) : (
      <div />
    );
  }

  render() {
    return (
      <div className="gx-main-content">
        <div className="gx-app-module gx-customer-profile-module">
          <div className="gx-w-100">
            <div className="gx-customer-profile-module-scroll">
              {this.renderMainContent(this.props.selected_employee)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { employees, selected_employee } = state.employees;
  return { employees, selected_employee };
};

export default connect(mapStateToProps, {
  getEmployeeById,
  updateEmployee
})(injectIntl(DispatchEmployeeProfile));
