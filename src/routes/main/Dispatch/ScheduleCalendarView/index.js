import React, { Component } from "react";
import { Calendar } from "antd";
import { Link } from "react-router-dom";

function getListData(value) {
  let listData;
  switch (value.date()) {
    case 1:
      listData = [
        {
          time: "2:30 PM",
          name: "Roger Aguirre",
          job: "Corporate windows cleaning",
          status: "completed"
        }
      ];
      break;
    case 3:
      listData = [
        {
          time: "2:30 PM",
          name: "Mark Rodriguez",
          job: "Residential deep kitchen cleaning",
          status: "scheduled"
        },
        {
          time: "2:40 PM",
          name: "Mark Rodriguez",
          job: "Residential deep kitchen cleaning",
          status: "scheduled"
        },
        {
          time: "3:10 PM",
          name: "Alexander Miller",
          job: "Residential deep kitchen cleaning",
          status: "scheduled"
        },
        {
          time: "3:30 PM",
          name: "Tom Wattson",
          job: "Residential deep kitchen cleaning",
          status: "scheduled"
        },
        {
          time: "4:30 PM",
          name: "Steven Borne",
          job: "Corporate cleaning",
          status: "scheduled"
        }
      ];
      break;
    case 6:
      listData = [
        {
          time: "3:20 PM",
          name: "Burton Kincaid",
          job: "Residential windows cleaning",
          status: "scheduled"
        }
      ];
      break;
    default:
  }
  return listData || [];
}

class ScheduleCalendarView extends Component {
  constructor(props) {
    super(props);
  }
  dateCellRender = value => {
    const listData = getListData(value);
    return listData.length > 0 ? (
      <ul className="events gx-w-100">
        {listData.map((item, index) => (
          <li key={index}>
            <div
              className={`gx-flex-column ${
                item.status == "completed"
                  ? "gx-schedule-completed-item"
                  : item.status == "scheduled"
                  ? "gx-schedule-scheduled-item"
                  : ""
              }`}
            >
              <div
                className="gx-flex-row gx-align-items-center"
                style={{ height: "18px", overflow: "hidden" }}
              >
                <span className="gx-fs-sm gx-font-weight-bold">
                  {item.time + " - "}
                </span>
                <Link to="/customers/profile">
                  <span className="gx-fs-sm gx-font-weight-bold">
                    {item.name}
                  </span>
                </Link>
              </div>
              <span
                className="gx-fs-sm gx-font-weight-medium"
                style={{ height: "18px", overflow: "hidden" }}
              >
                {item.job}
              </span>
            </div>
          </li>
        ))}
      </ul>
    ) : (
      <div />
    );
  };
  render() {
    const { date } = this.props;
    return (
      <div className="gx-employee-work-month-area">
        <div className="gx-mx-30">
          <Calendar
            value={date}
            dateCellRender={this.dateCellRender}
            onSelect={value => this.props.setCalendarDate(value)}
          />
        </div>
      </div>
    );
  }
}
export default ScheduleCalendarView;
