import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import {
  Button,
  Select,
  Modal,
  Input,
  Tag,
  Tabs,
  Row,
  Col,
  Card,
  Menu,
  Popover,
  Switch,
  Upload,
  Badge,
  Avatar,
  Skeleton
} from "antd";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import ButtonGroup from "antd/lib/button/button-group";
import IntlMessages from "util/IntlMessages";
import IntlHtmlMessages from "util/IntlHtmlMessages";
import SearchBox from "components/SearchBox";
import Widget from "components/Widget";
import NewJobUpload from "./NewJobUpload";
import NewJobNotes from "./NewJobNotes";

import { Picker } from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";

import "assets/chat-custom.css";

const { Option } = Select;
const TabPane = Tabs.TabPane;
const { TextArea } = Input;
const Dragger = Upload.Dragger;
let previews = [];

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

class Chat extends Component {
  state = {
    current_employee: undefined,
    employees: [
      {
        name: "Peter Jonson",
        status: "active",
        badge: 2,
        role: "Home cleaning expert",
        started_job: "#000376",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_peterjonson.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          {
            time: "7:24 AM",
            status: "sent",
            type: "text",
            msg:
              "Multiply fly tree the firmament night sixth first given to a seas."
          },
          {
            time: "8:47 AM",
            status: "received",
            type: "text",
            msg: "Very herb likness were may two grass seed bearing."
          }
        ]
      },
      {
        name: "Robert Branon",
        status: "active",
        badge: 1,
        role: "Home cleaning expert",
        started_job: "#000377",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_robertbranon.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          { time: "11:14 AM", type: "sent", msg: "I am available now." }
        ]
      },
      {
        name: "Robb Bernidoz",
        status: "away",
        badge: 3,
        role: "Home cleaning expert",
        started_job: "#000378",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_robbbernidoz.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          {
            time: "3:24 PM",
            type: "sent",
            msg: "How many hours should I stay here?"
          },
          { time: "5:07 PM", type: "received", msg: "As much as possible..." }
        ]
      },
      {
        name: "John Tembark",
        status: "offline",
        badge: 0,
        role: "Home cleaning expert",
        started_job: "#000379",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_johntembark.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          {
            time: "7:24 AM",
            type: "sent",
            msg:
              "Multiply fly tree the firmament night sixth first given to a seas."
          }
        ]
      },
      {
        name: "Liana Perizoa",
        status: "offline",
        badge: 0,
        role: "Home cleaning expert",
        started_job: "#000380",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_lianaperizoa.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          { time: "1:24 PM", type: "sent", msg: "Hi." },
          { time: "8:47 AM", type: "received", msg: "I am here" }
        ]
      },
      {
        name: "Peter Jackson",
        status: "active",
        badge: 0,
        role: "Home cleaning expert",
        started_job: "#000381",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_peterjackson.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          {
            time: "7:24 AM",
            type: "sent",
            msg:
              "Multiply fly tree the firmament night sixth first given to a seas."
          },
          {
            time: "8:47 AM",
            type: "received",
            msg: "Very herb likness were may two grass seed bearing."
          }
        ]
      },
      {
        name: "Leana Mortaza",
        status: "away",
        badge: 0,
        role: "Home cleaning expert",
        started_job: "#000382",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_leanamortaza.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          { time: "7:24 AM", type: "sent", msg: "I will go there soon" }
        ]
      },
      {
        name: "Kevin Aderins",
        status: "away",
        badge: 0,
        role: "Home cleaning expert",
        started_job: "#000383",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_kevinanderins.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          {
            time: "10:24 AM",
            type: "sent",
            msg: "Please tell me asap where I need to go."
          }
        ]
      },
      {
        name: "Joseff Mormont",
        status: "active",
        badge: 0,
        role: "Home cleaning expert",
        started_job: "#000385",
        started_job_time: "12:45 PM",
        avatar: require("assets/images/avatar/avatar_joseffmormont.png"),
        activities: [
          {
            activity: "#000536",
            status: "Scheduled at 24 July",
            date: "Today",
            time: "3:35 PM"
          },
          {
            activity: "#000532",
            status: "Completed job",
            date: "23 July",
            time: "2:43 PM"
          },
          {
            activity: "#000532",
            status: "Started job",
            date: "22 July",
            time: "2:27 PM"
          }
        ],
        jobs: [
          {
            who: "Max Panck",
            icon: "person",
            status: "scheduled",
            role: "Deep house cleaning",
            time: "3:30 PM"
          },
          {
            who: "Marie Curie",
            icon: "business",
            status: "scheduled",
            role: "Windows cleaning",
            time: "4:30 PM"
          },
          {
            who: "James Clerk",
            icon: "person",
            status: "scheduled",
            role: "Carpet cleaning",
            time: "3:30 PM"
          }
        ],
        messages: [
          {
            time: "7:24 AM",
            type: "sent",
            msg: "I just finsihed ongoing job."
          },
          { time: "9:14 AM", type: "received", msg: "Well done." }
        ]
      }
    ],
    filtered_employees: [],
    searchText: "",
    msg_text: "",
    fileList: [],
    uploading: false,
    previewImages: [],
    showEmojis: false
  };
  componentDidMount() {
    this.setState({ filtered_employees: this.state.employees });
  }
  onSearchTextChange = e => {
    this.setState({ searchText: e.target.value });
    if (e.target.value != "") {
      var array = this.state.employees.filter(emp =>
        emp.name.includes(e.target.value)
      );
      this.setState({ filtered_employees: array });
    } else {
      this.setState({ filtered_employees: this.state.employees });
    }
  };
  addEmoji = e => {
    let emoji = e.native;
    this.setState({
      msg_text: this.state.msg_text + emoji
    });
  };
  showEmojis = e => {
    this.setState(
      {
        showEmojis: true
      },
      () => document.addEventListener("click", this.closeMenu)
    );
  };
  closeMenu = e => {
    if (this.emojiPicker !== null && !this.emojiPicker.contains(e.target)) {
      this.setState(
        {
          showEmojis: false
        },
        () => document.removeEventListener("click", this.closeMenu)
      );
    }
  };
  onMessageTextChange = e => {
    this.setState({ msg_text: e.target.value });
  };
  sendMsg = () => {
    if (this.state.current_employee != undefined && this.state.msg_text != "") {
      var temp = this.state.current_employee;
      var now = new Date();
      // datetime = now.toLocaleString("en-us");
      // var datetime =
      //   now.getFullYear() +
      //   "-" +
      //   ("0" + (now.getMonth() + 1)).slice(-2) +
      //   "-" +
      //   ("0" + now.getDate()).slice(-2) +
      //   " " +
      //   ("0" + now.getHours()).slice(-2) +
      //   ":" +
      //   ("0" + now.getMinutes()).slice(-2) +
      //   ":" +
      //   ("0" + now.getSeconds()).slice(-2);
      var hours = now.getHours();
      var minutes = now.getMinutes();
      var ampm = hours >= 12 ? "PM" : "AM";
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? "0" + minutes : minutes;
      var datetime = hours + ":" + minutes + " " + ampm;
      temp.messages.push({
        time: datetime,
        type: "received",
        msg: this.state.msg_text
      });
      this.setState({ current_employee: temp });
      this.setState({ msg_text: "" });
    }
  };
  onEnterMsg = async e => {
    if (e.key === "Enter") {
      if (
        this.state.current_employee != undefined &&
        this.state.fileList.length > 0
      ) {
        var files = this.state.fileList;
        // const formData = new FormData();
        // fileList.forEach(file => {
        //   formData.append('files[]', file);
        // });
        // reqwest({
        //   url: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        //   method: 'post',
        //   processData: false,
        //   data: formData,
        //   success: () => {
        //     this.setState({
        //       fileList: [],
        //       uploading: false,
        //     });
        //     message.success('upload successfully.');
        //   },
        //   error: () => {
        //     this.setState({
        //       uploading: false,
        //     });
        //     message.error('upload failed.');
        //   },
        // });
        /* files.forEach(file => {
          this.handlePreview(file);
        }); */
        /* for (const file of files) {
          await this.handlePreview(file);
        }*/

        await Promise.all(
          files.map(async file => {
            await this.handlePreview(file);
          })
        );

        this.setState({ previewImages: previews }, () => {
          var temp = this.state.current_employee;
          var now = new Date();
          var hours = now.getHours();
          var minutes = now.getMinutes();
          var ampm = hours >= 12 ? "PM" : "AM";
          hours = hours % 12;
          hours = hours ? hours : 12; // the hour '0' should be '12'
          minutes = minutes < 10 ? "0" + minutes : minutes;
          var datetime = hours + ":" + minutes + " " + ampm;
          this.state.previewImages.forEach(preview => {
            temp.messages.push({
              time: datetime,
              type: "image",
              status: "received",
              msg: <img src={preview} style={{ width: "300px " }} />
            });
          });
          this.setState({ current_employee: temp });
          this.setState({ fileList: [] });
          previews = [];
        });
      }
      if (
        this.state.current_employee != undefined &&
        this.state.msg_text != ""
      ) {
        var temp = this.state.current_employee;
        var now = new Date();
        var hours = now.getHours();
        var minutes = now.getMinutes();
        var ampm = hours >= 12 ? "PM" : "AM";
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? "0" + minutes : minutes;
        var datetime = hours + ":" + minutes + " " + ampm;
        temp.messages.push({
          time: datetime,
          type: "text",
          status: "received",
          msg: this.state.msg_text
        });
        this.setState({ current_employee: temp });
        this.setState({ msg_text: "" });
      }
    }
  };
  async handlePreview(file) {
    var preview;
    if (file.type.search("image/") > -1) {
      preview = await getBase64(file);
    } else {
      // this.setState({extension:""})
      preview = "";
    }
    previews.push(preview);
  }

  getFileSize(size) {
    if (size < 1024) {
      return size + " Bytes";
    } else if (size < 1024 * 1024) {
      return (size / 1024).toFixed(2) + " KB";
    } else if (size < 1024 * 1024 * 1024) {
      return (size / (1024 * 1024)).toFixed(2) + " MB";
    } else {
      return (size / (1024 * 1024 * 1024)).toFixed(2) + " GB";
    }
  }
  onAttachFileItemRemove = (e, index) => {
    let temp = this.state.fileList;
    temp.splice(index, 1);
    this.setState({ fileList: temp });
  };
  render() {
    const { current_employee } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;
    const upload_props = {
      multiple: true,
      onRemove: file => {
        this.setState(state => {
          const index = state.fileList.indexOf(file);
          const newFileList = state.fileList.slice();
          newFileList.splice(index, 1);
          return {
            fileList: newFileList
          };
        });
      },
      beforeUpload: file => {
        if (this.state.current_employee != undefined) {
          this.setState(state => ({
            fileList: [...state.fileList, file]
          }));
          return false;
        }
      },
      fileList: this.state.fileList
    };
    return (
      <div className="gx-main-content">
        <div className="gx-app-module gx-chat-module gx-pt-0">
          <div className="gx-w-100">
            <div className="gx-chat-module-scroll">
              <div
                className="gx-panel-content gx-chat-panel-content"
                style={{ backgroundColor: "transparent" }}
              >
                <div className="gx-panel-content-scroll">
                  <Widget styleName="gx-card-full gx-chat-widget gx-mb-0 gx-no-bottom-radius gx-w-100">
                    <div className="gx-chat-content">
                      <div className="gx-chat-sidebar">
                        <div className="gx-chat-sidebar-header gx-px-20 gx-flex-row gx-align-items-center gx-custom-flex-justify-content-space-between">
                          <SearchBox
                            styleName="gx-lt-icon-search-bar-lg gx-employee-search gx-chat-search-bar"
                            placeholder="Search employee"
                            onChange={this.onSearchTextChange}
                            value={this.state.searchText}
                          />
                        </div>
                        <div className="gx-chat-sidebar-body">
                          <Menu mode="inline" theme="light">
                            {this.state.filtered_employees.map(
                              (employee, index) => (
                                <Menu.Item
                                  key={index}
                                  onClick={() => {
                                    if (this.state.current_employee != employee)
                                      this.setState({ fileList: [] });
                                    this.setState({
                                      current_employee: employee
                                    });
                                  }}
                                >
                                  <div key={index} className="gx-sidebar-item">
                                    <div className="gx-user-thumb gx-mr-3 gx-flex-row gx-size-30">
                                      <Avatar
                                        className={`gx-size-30 ${
                                          employee.avatar == ""
                                            ? "gx-avatar-default"
                                            : ""
                                        }`}
                                        alt={employee.avatar}
                                        src={employee.avatar}
                                      >
                                        {employee.name.charAt(0)}
                                      </Avatar>
                                      <span
                                        className={`gx-employee-status ${
                                          employee.status == "active"
                                            ? "gx-employee-status-active"
                                            : employee.status == "away"
                                            ? "gx-employee-status-away"
                                            : employee.status == "offline"
                                            ? "gx-employee-status-offline"
                                            : ""
                                        }`}
                                      ></span>
                                    </div>
                                    <span
                                      className={`gx-employee-name ${
                                        employee.status == "active"
                                          ? "gx-employee-name-active"
                                          : employee.status == "away"
                                          ? "gx-employee-name-away"
                                          : employee.status == "offline"
                                          ? "gx-employee-name-offline"
                                          : ""
                                      }`}
                                    >
                                      {employee.name}
                                    </span>
                                    {employee.badge > 0 ? (
                                      <Badge
                                        className="gx-ml-10"
                                        count={employee.badge}
                                      />
                                    ) : (
                                      <div />
                                    )}
                                  </div>
                                </Menu.Item>
                              )
                            )}
                          </Menu>
                        </div>
                      </div>
                      <div className="gx-chat-body gx-w-100">
                        <div className="gx-chat-body-content-header">
                          <div className="gx-chat-body-content-header-desktop">
                            {current_employee != undefined ? (
                              <div className="gx-flex-row gx-align-items">
                                <div
                                  className="gx-flex-row gx-align-items-center"
                                  style={{ marginRight: "24px" }}
                                >
                                  <Avatar
                                    className={`gx-size-30 gx-mr-10 ${
                                      current_employee.avatar == ""
                                        ? "gx-avatar-default"
                                        : ""
                                    }`}
                                    alt={current_employee.avatar}
                                    src={current_employee.avatar}
                                  >
                                    {current_employee.name.charAt(0)}
                                  </Avatar>
                                  <span className="gx-employee-name">
                                    {current_employee.name}
                                  </span>
                                </div>
                                <div
                                  className="gx-flex-row gx-align-items-center"
                                  style={{ marginRight: "24px" }}
                                >
                                  <span
                                    className={`gx-size-10 gx-mr-10 gx-employee-status-${current_employee.status}`}
                                  ></span>
                                  <span>
                                    {current_employee.status == "active"
                                      ? "Active now"
                                      : current_employee.status == "away"
                                      ? "Away now"
                                      : current_employee.status == "offline"
                                      ? "Offline now"
                                      : ""}
                                  </span>
                                </div>
                                <div
                                  className="gx-flex-row gx-align-items-center"
                                  style={{
                                    fontSize: "12px",
                                    color: "#9399a2"
                                  }}
                                >
                                  <i
                                    className="material-icons"
                                    style={{
                                      fontSize: "20px",
                                      color: "#9399a2",
                                      marginRight: "7px"
                                    }}
                                  >
                                    access_time
                                  </i>
                                  <span>
                                    Started job{" "}
                                    <Link to="#">
                                      {current_employee.started_job}
                                    </Link>{" "}
                                    at {current_employee.started_job_time}
                                  </span>
                                </div>
                              </div>
                            ) : (
                              <div className="gx-flex-row gx-align-items-center">
                                <Avatar
                                  className="gx-size-30 gx-mr-10"
                                  style={{ backgroundColor: "#f5f5f5" }}
                                ></Avatar>
                                <div
                                  className="gx-flex-row default-blank"
                                  style={{ marginRight: "15px" }}
                                ></div>
                                <div
                                  className="gx-flex-row default-blank"
                                  style={{
                                    marginRight: "15px"
                                  }}
                                ></div>
                                <div
                                  className="gx-flex-row default-blank"
                                  style={{
                                    marginRight: "15px"
                                  }}
                                ></div>
                              </div>
                            )}
                          </div>
                        </div>
                        <div className="gx-chat-body-content-body">
                          <div className="gx-chat-body-content-main">
                            {current_employee != undefined ? (
                              <div
                                className="gx-msg-body-content"
                                style={{ padding: "30px" }}
                              >
                                {current_employee.messages.length > 0 ? (
                                  current_employee.messages.map(
                                    (message, index) => (
                                      <div
                                        key={index}
                                        className="gx-msg-content"
                                      >
                                        {message.status == "sent" ? (
                                          <div className="gx-flex-row gx-msg-sent">
                                            <Avatar
                                              className={`gx-size-30 gx-mr-10 ${
                                                current_employee.avatar == ""
                                                  ? "gx-avatar-default"
                                                  : ""
                                              }`}
                                              alt={current_employee.avatar}
                                              src={current_employee.avatar}
                                            ></Avatar>
                                            <div className="gx-flex-column">
                                              <span className="gx-msg-time">
                                                {message.time}
                                              </span>
                                              <div className="gx-msg-content">
                                                {message.msg}
                                              </div>
                                            </div>
                                          </div>
                                        ) : message.status == "received" ? (
                                          <div className="gx-flex-row gx-msg-received">
                                            <div className="gx-flex-column">
                                              <span className="gx-msg-time">
                                                {message.time}
                                              </span>
                                              <div className="gx-msg-content">
                                                {message.msg}
                                              </div>
                                            </div>
                                          </div>
                                        ) : (
                                          <div />
                                        )}
                                      </div>
                                    )
                                  )
                                ) : (
                                  <div />
                                )}
                              </div>
                            ) : (
                              <div className="default-body-content gx-msg-body-content gx-flex-column gx-align-items-center gx-justify-content-center">
                                <div
                                  className="gx-flex-column"
                                  style={{ width: "380px" }}
                                >
                                  <div className="gx-flex-row gx-align-items-center gx-justify-content-center">
                                    <img
                                      src={require("assets/images/chat/default-chat-content.png")}
                                    />
                                  </div>
                                  <span
                                    className="gx-fs-lg gx-font-weight-medium"
                                    style={{
                                      textAlign: "center",
                                      marginTop: "25px"
                                    }}
                                  >
                                    Please select a chat or employee
                                  </span>
                                  <span
                                    className="gx-fs-13-20"
                                    style={{
                                      textAlign: "center",
                                      marginTop: "15px",
                                      color: "#9399a2",
                                      fontWeight: "500"
                                    }}
                                  >
                                    Wherein deep creepeth stars very. Hath third
                                    tree great, This were won't the air to also
                                    it.
                                  </span>
                                </div>
                              </div>
                            )}
                            <div className="gx-attached-files">
                              {this.state.fileList.length > 0 ? (
                                this.state.fileList.map((file, index) => (
                                  <div key={index} className="gx-file-item">
                                    <span className="gx-file-name">
                                      {file.name}
                                    </span>
                                    <i
                                      className="material-icons gx-icon-action"
                                      onClick={e =>
                                        this.onAttachFileItemRemove(e, index)
                                      }
                                    >
                                      close
                                    </i>
                                  </div>
                                ))
                              ) : (
                                <div />
                              )}
                            </div>
                            <div className="gx-message-input">
                              <div className="gx-message-actions">
                                {this.state.showEmojis ? (
                                  <span
                                    className="gx-emojiPicker"
                                    ref={el => (this.emojiPicker = el)}
                                  >
                                    <Picker
                                      onSelect={this.addEmoji}
                                      emojiTooltip={true}
                                      title="Emoticons"
                                    ></Picker>
                                  </span>
                                ) : (
                                  <div />
                                )}
                                <i
                                  className={`material-icons gx-mr-10 ${
                                    this.state.showEmojis
                                      ? "showEmojiPicker"
                                      : ""
                                  }`}
                                  onClick={this.showEmojis}
                                >
                                  insert_emoticon
                                </i>
                                <Upload {...upload_props}>
                                  <i className="material-icons">
                                    add_circle_outline
                                  </i>
                                </Upload>
                              </div>
                              <Input
                                className="gx-msg-text"
                                value={this.state.msg_text}
                                onKeyDown={this.onEnterMsg}
                                onChange={this.onMessageTextChange}
                                placeholder={formatMessage({
                                  id:
                                    "chat.message.placeholder.type_message_here"
                                })}
                              ></Input>
                              <Button
                                type="default"
                                className="gx-btn-msg-send gx-mb-0"
                                onClick={this.sendMsg}
                              >
                                <IntlMessages id="chat.message.send" />
                                <i className="material-icons gx-ml-10">send</i>
                              </Button>
                            </div>
                          </div>
                          <div className="gx-chat-body-content-right-sidebar">
                            <div className="gx-chat-body-content-right-sidebar-header">
                              {current_employee != undefined ? (
                                <div className="gx-flex-row gx-align-items-center">
                                  <Avatar
                                    className={`gx-size-80 gx-mr-20 ${
                                      current_employee.avatar == ""
                                        ? "gx-avatar-default"
                                        : ""
                                    }`}
                                    alt={current_employee.avatar}
                                    src={current_employee.avatar}
                                  >
                                    {current_employee.name.charAt(0)}
                                  </Avatar>
                                  <div className="gx-flex-column">
                                    <span className="gx-profile-name">
                                      {current_employee.name}
                                    </span>
                                    <span className="gx-profile-role">
                                      {current_employee.role}
                                    </span>
                                    <ButtonGroup className="gx-mt-10">
                                      <Button>
                                        <i className="material-icons">phone</i>
                                      </Button>
                                      <Button>
                                        <i className="material-icons">email</i>
                                      </Button>
                                      <Popover
                                        placement="bottom"
                                        content={
                                          <div>
                                            <div className="gx-menuitem">
                                              <span>Link 01</span>
                                            </div>
                                            <div className="gx-menuitem">
                                              <span>Link 02</span>
                                            </div>
                                            <div className="gx-menuitem">
                                              <span>Link 03</span>
                                            </div>
                                          </div>
                                        }
                                        trigger="click"
                                      >
                                        <Button>
                                          <i className="material-icons">
                                            more_horiz
                                          </i>
                                        </Button>
                                      </Popover>
                                    </ButtonGroup>
                                  </div>
                                </div>
                              ) : (
                                <div className="gx-flex-row gx-align-items-center">
                                  <Avatar
                                    className="gx-size-80 gx-mr-20"
                                    style={{
                                      backgroundColor: "#f5f5f5"
                                    }}
                                  />
                                  <div className="gx-flex-column">
                                    <div className="gx-profile-name default-blank"></div>
                                    <div className="gx-profile-role default-blank"></div>
                                    <div className="btn-group default-blank gx-mt-10"></div>
                                  </div>
                                </div>
                              )}
                            </div>
                            <div className="gx-chat-body-content-right-sidebar-body">
                              {current_employee != undefined ? (
                                <Tabs
                                  className="gx-tabs-sidebar"
                                  defaultActiveKey="1"
                                >
                                  <TabPane
                                    tab={
                                      <span className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                                        <i className="material-icons gx-mr-2">
                                          whatshot
                                        </i>
                                        <IntlMessages id="chat.profile.sidebar.activities" />
                                      </span>
                                    }
                                    key="1"
                                  >
                                    <div className="gx-tab-content gx-py-10 gx-px-20">
                                      {current_employee != undefined ? (
                                        current_employee.activities.map(
                                          (activity, index) => (
                                            <div
                                              key={index}
                                              className="gx-employee-activity gx-py-10"
                                            >
                                              <div className="gx-flex-row gx-align-items-center">
                                                <div className="gx-flex-row gx-align-items-center">
                                                  <Avatar
                                                    className={`gx-size-30 gx-mr-10 ${
                                                      current_employee.avatar ==
                                                      ""
                                                        ? "gx-avatar-default"
                                                        : ""
                                                    }`}
                                                    alt={
                                                      current_employee.avatar
                                                    }
                                                    src={
                                                      current_employee.avatar
                                                    }
                                                  >
                                                    {current_employee.name.charAt(
                                                      0
                                                    )}
                                                  </Avatar>
                                                </div>
                                                <div className="gx-flex-column">
                                                  <div className="gx-flex-row gx-align-items-center">
                                                    <span className="gx-employee-name">
                                                      {current_employee.name}
                                                    </span>
                                                  </div>
                                                  <span className="gx-activity-details">
                                                    <Link to="#">
                                                      {activity.activity}
                                                    </Link>
                                                    -{activity.status}
                                                  </span>
                                                </div>
                                              </div>
                                              <span className="gx-activity-datetime">
                                                {activity.date}
                                                {/* <span className="dot">
                                                  <i className="material-icons">
                                                    lens
                                                  </i>
                                                </span>
                                                {activity.time} */}
                                              </span>
                                            </div>
                                          )
                                        )
                                      ) : (
                                        <div />
                                      )}
                                    </div>
                                  </TabPane>
                                  <TabPane
                                    tab={
                                      <span className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                                        <i className="material-icons gx-mr-2">
                                          build
                                        </i>
                                        <IntlMessages id="chat.profile.sidebar.jobs" />
                                      </span>
                                    }
                                    key="2"
                                  >
                                    <div className="gx-tab-content gx-p-20">
                                      {current_employee != undefined ? (
                                        current_employee.jobs.map(
                                          (job, index) => (
                                            <div
                                              key={index}
                                              className="gx-employee-job gx-mb-10"
                                            >
                                              <Link to="#">
                                                <Widget className="gx-card-full gx-mb-10">
                                                  <div className="gx-job-details gx-flex-row gx-align-items-center gx-flex-nowrap">
                                                    <div className="gx-size-30 gx-mr-10 gx-avatar-default gx-flex-0">
                                                      <i className="material-icons gx-job-icon gx-text-center gx-w-100">
                                                        {job.icon}
                                                      </i>
                                                    </div>
                                                    <div className="gx-job-details-content gx-w-100">
                                                      <div className="gx-flex-row gx-align-items-center">
                                                        <span className="gx-job-who">
                                                          {job.who}
                                                        </span>
                                                        <div className="gx-custom-tag gx-bg-blue">
                                                          {job.status.toUpperCase()}
                                                        </div>
                                                      </div>
                                                      <div className="gx-flex-row gx-align-items-center gx-justify-content-between">
                                                        <div className="gx-flex-row gx-align-items-center">
                                                          <span className="gx-job-role">
                                                            {job.role}
                                                          </span>
                                                        </div>
                                                        <div className="gx-flex-row gx-align-items-center">
                                                          <span className="gx-job-datetime">
                                                            {job.time}
                                                          </span>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </Widget>
                                              </Link>
                                            </div>
                                          )
                                        )
                                      ) : (
                                        <div />
                                      )}
                                    </div>
                                  </TabPane>
                                  <TabPane
                                    tab={
                                      <span className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                                        <i className="material-icons gx-mr-2">
                                          description
                                        </i>
                                        <IntlMessages id="chat.profile.sidebar.notes" />
                                      </span>
                                    }
                                    key="3"
                                  >
                                    <div className="gx-tab-content gx-p-20">
                                      <NewJobNotes />
                                    </div>
                                  </TabPane>
                                  <TabPane
                                    tab={
                                      <span className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                                        <i className="material-icons gx-mr-2">
                                          folder
                                        </i>
                                        <IntlMessages id="chat.profile.sidebar.files" />
                                      </span>
                                    }
                                    key="4"
                                  >
                                    <div className="gx-tab-content">
                                      <NewJobUpload />
                                    </div>
                                  </TabPane>
                                </Tabs>
                              ) : (
                                <div className="gx-tabs-blank">
                                  <div className="gx-tabs-header-blank gx-flex-row gx-align-items-center gx-justify-content-between">
                                    <div className="gx-tab-blank" />
                                    <div className="gx-tab-blank" />
                                    <div className="gx-tab-blank" />
                                    <div className="gx-tab-blank" />
                                  </div>
                                  <div className="gx-tabs-body-blank gx-p-20">
                                    <div className="gx-tabs-tab-content-blank gx-flex-row gx-justify-content-between gx-mb-20">
                                      <div className="gx-flex-row gx-align-items-center">
                                        <Avatar
                                          className="gx-size-30 gx-mr-20"
                                          style={{ backgroundColor: "#f5f5f5" }}
                                        ></Avatar>
                                        <div className="gx-flex-column">
                                          <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-name"></div>
                                          <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-role"></div>
                                        </div>
                                      </div>
                                      <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-datetime"></div>
                                    </div>
                                    <div className="gx-tabs-tab-content-blank gx-flex-row gx-justify-content-between gx-mb-20">
                                      <div className="gx-flex-row gx-align-items-center">
                                        <Avatar
                                          className="gx-size-30 gx-mr-20"
                                          style={{ backgroundColor: "#f5f5f5" }}
                                        ></Avatar>
                                        <div className="gx-flex-column">
                                          <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-name"></div>
                                          <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-role"></div>
                                        </div>
                                      </div>
                                      <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-datetime"></div>
                                    </div>
                                    <div className="gx-tabs-tab-content-blank gx-flex-row gx-justify-content-between gx-mb-20">
                                      <div className="gx-flex-row gx-align-items-center">
                                        <Avatar
                                          className="gx-size-30 gx-mr-20"
                                          style={{ backgroundColor: "#f5f5f5" }}
                                        ></Avatar>
                                        <div className="gx-flex-column">
                                          <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-name"></div>
                                          <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-role"></div>
                                        </div>
                                      </div>
                                      <div className="gx-tabs-tab-detail-blank gx-tabs-tab-detail-datetime"></div>
                                    </div>
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Widget>
                  {current_employee == undefined ? (
                    <Widget styleName="gx-card-full gx-chat-widget-mobile gx-chat-employees-widget-mobile">
                      <div className="gx-card-employees-header gx-flex-row gx-align-items-center gx-justify-content-between gx-px-20">
                        <span>
                          <IntlMessages id="chat.employees.employee_chat" />
                        </span>
                        <i className="material-icons gx-employee-search">
                          search
                        </i>
                      </div>
                      <div className="gx-card-employees-body">
                        <Menu mode="inline" theme="light">
                          {this.state.filtered_employees.map(
                            (employee, index) => (
                              <Menu.Item
                                key={index}
                                onClick={() =>
                                  this.setState({
                                    current_employee: employee
                                  })
                                }
                              >
                                <div className="gx-sidebar-item">
                                  <div className="gx-user-thumb gx-mr-10 gx-flex-row gx-size-30">
                                    <Avatar
                                      className={`gx-size-30 ${
                                        employee.avatar == ""
                                          ? "gx-avatar-default"
                                          : ""
                                      }`}
                                      alt={employee.avatar}
                                      src={employee.avatar}
                                    >
                                      {employee.name.charAt(0)}
                                    </Avatar>
                                    <span
                                      className={`gx-employee-status ${
                                        employee.status == "active"
                                          ? "gx-employee-status-active"
                                          : employee.status == "away"
                                          ? "gx-employee-status-away"
                                          : employee.status == "offline"
                                          ? "gx-employee-status-offline"
                                          : ""
                                      }`}
                                    ></span>
                                  </div>
                                  <div className="gx-flex-column gx-w-100">
                                    <div className="gx-flex-row gx-align-items-center gx-justify-content-between gx-w-100">
                                      <div className="gx-flex-row gx-align-items-center">
                                        <span
                                          className={`gx-employee-name ${
                                            employee.status == "active"
                                              ? "gx-employee-name-active"
                                              : employee.status == "away"
                                              ? "gx-employee-name-away"
                                              : employee.status == "offline"
                                              ? "gx-employee-name-offline"
                                              : ""
                                          }`}
                                        >
                                          {employee.name}
                                        </span>
                                        {employee.badge > 0 ? (
                                          <Badge
                                            className="gx-ml-10"
                                            count={employee.badge}
                                          />
                                        ) : (
                                          <div />
                                        )}
                                      </div>
                                      <span className="gx-employee-time">
                                        {employee.messages.length > 0 ? (
                                          employee.messages[
                                            employee.messages.length - 1
                                          ].time
                                        ) : (
                                          <div />
                                        )}
                                      </span>
                                    </div>
                                    <span className="gx-employee-message">
                                      {employee.messages[
                                        employee.messages.length - 1
                                      ].msg.length < 35
                                        ? employee.messages[
                                            employee.messages.length - 1
                                          ].msg
                                        : employee.messages[
                                            employee.messages.length - 1
                                          ].msg.substring(0, 35) + "..."}
                                    </span>
                                  </div>
                                </div>
                              </Menu.Item>
                            )
                          )}
                        </Menu>
                      </div>
                    </Widget>
                  ) : (
                    <Widget styleName="gx-card-full gx-chat-widget-mobile gx-chat-content-widget-mobile">
                      <div className="gx-card-content-header gx-flex-row gx-align-items-center gx-px-20">
                        <Link
                          to="#"
                          className="gx-flex-row gx-align-items-center gx-btn-back"
                          onClick={() =>
                            this.setState({ current_employee: undefined })
                          }
                        >
                          <i className="material-icons gx-mr-10">arrow_back</i>
                          <IntlMessages id="chat.messages.back" />
                        </Link>
                        <div className="gx-flex-row gx-align-items-center gx-ml-20">
                          <Avatar
                            className={`gx-size-30 gx-mr-10 ${
                              current_employee.avatar == ""
                                ? "gx-avatar-default"
                                : ""
                            }`}
                            alt={current_employee.avatar}
                            src={current_employee.avatar}
                          >
                            {current_employee.name.charAt(0)}
                          </Avatar>
                          <span className="gx-employee-name">
                            {current_employee.name}
                          </span>
                          <div
                            className={`gx-ml-10 gx-employee-status gx-employee-status-${current_employee.status}`}
                          ></div>
                        </div>
                      </div>
                      <div className="gx-card-content-body gx-flex-column">
                        <div className="gx-messages-content">
                          <div className="gx-messages gx-p-20">
                            {current_employee.messages.length > 0 ? (
                              current_employee.messages.map(
                                (message, index) => (
                                  <div key={index} className="gx-msg-content">
                                    {message.status == "sent" ? (
                                      <div className="gx-msg-sent">
                                        <Avatar
                                          className={`gx-size-30 gx-mr-10 ${
                                            current_employee.avatar == ""
                                              ? "gx-avatar-default"
                                              : ""
                                          }`}
                                          alt={current_employee.avatar}
                                          src={current_employee.avatar}
                                        ></Avatar>
                                        <div className="gx-flex-column">
                                          <span className="gx-msg-time">
                                            {message.time}
                                          </span>
                                          <div className="gx-msg">
                                            {message.msg}
                                          </div>
                                        </div>
                                      </div>
                                    ) : message.status == "received" ? (
                                      <div className="gx-msg-received">
                                        <div className="gx-flex-column">
                                          <span className="gx-msg-time">
                                            {message.time}
                                          </span>
                                          <div className="gx-msg">
                                            {message.msg}
                                          </div>
                                        </div>
                                      </div>
                                    ) : (
                                      <div />
                                    )}
                                  </div>
                                )
                              )
                            ) : (
                              <div />
                            )}
                          </div>
                        </div>
                        <div className="gx-message-input gx-px-20">
                          <div className="gx-message-actions">
                            {this.state.showEmojis ? (
                              <span
                                className="gx-emojiPicker"
                                ref={el => (this.emojiPicker = el)}
                              >
                                <Picker
                                  onSelect={this.addEmoji}
                                  emojiTooltip={true}
                                  title="Emoticons"
                                ></Picker>
                              </span>
                            ) : (
                              <div />
                            )}
                            <i
                              className={`material-icons ${
                                this.state.showEmojis ? "showEmojiPicker" : ""
                              }`}
                              onClick={this.showEmojis}
                              style={{ marginRight: "5px" }}
                            >
                              insert_emoticon
                            </i>
                            <Dragger {...this.state.upload_props}>
                              <i className="material-icons">
                                add_circle_outline
                              </i>
                            </Dragger>
                          </div>
                          <Input
                            className="gx-msg-text"
                            value={this.state.msg_text}
                            onChange={this.onMessageTextChange}
                            onKeyDown={this.onEnterMsg}
                            placeholder={formatMessage({
                              id: "chat.message.placeholder.type_message_here"
                            })}
                          ></Input>
                          <i
                            className="material-icons gx-btn-msg-send"
                            onClick={this.sendMsg}
                          >
                            send
                          </i>
                        </div>
                      </div>
                    </Widget>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(Chat);
