import React, { Component } from "react";
import { Button, Checkbox, Avatar, Tag, Input } from "antd";

import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import { changeDateFormat } from "util/DateTime";
import CustomScrollbars from "util/CustomScrollbars";
import { Link } from "react-router-dom";

//import TextArea from "antd/lib/input/TextArea";

const { TextArea } = Input;
//const data = [];
const data = [
  {
    name: "Robert Brannon",
    note: "Lesser land heaven which above first.",
    datetime: "4/24/2019 15:26",
    avatar: ""
  },
  {
    name: "Leana Rosebell",
    note:
      "Lesser land heaven which above first. Lesser land heaven which above first.",
    datetime: "4/22/2019 14:26",
    avatar: require("assets/images/avatar/avatar03.png")
  }
];

class NewJobNotes extends Component {
  state = {
    note: ""
  };

  constructor(props, context) {
    super(props, context);
  }
  onNoteChange = e => {
    this.setState({ note: e.target.value });
  };
  onSubmitNote = () => {
    data.push({
      name: "Leana Rosebell",
      note: this.state.note,
      datetime: new Date().toLocaleString("en-US"),
      avatar: require("assets/images/avatar/avatar03.png")
    });
    this.setState({ note: "" });
  };
  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div className="gx-customer-tab gx-customer-activities-tab">
        <div className="gx-ss-newjob-note-input">
          <TextArea
            className="gx-customer-tab-input gx-border-0 gx-w-100"
            placeholder={formatMessage({
              id: "customer.profile.notes.typenotes"
            })}
            value={this.state.note}
            onChange={this.onNoteChange}
          />
          <Link
            to="#"
            className={`gx-btn-note-submit ${
              this.state.note != "" ? "gx-btn-note-submit-active" : ""
            }`}
            onClick={this.onSubmitNote}
          >
            <span>SUBMIT</span>
            <i className="material-icons">arrow_forward</i>
          </Link>
        </div>
        <div className="gx-customer-top-tab-scroll">
          <div className="gx-customer-tab-content gx-ss-newjob-note-content">
            {data.length === 0 ? (
              <div className="gx-flex-row gx-justify-content-center gx-align-items-center gx-text-grey">
                <div className="gx-text-center gx-mt-20">
                  <i className="material-icons gx-fs-xlxl gx-font-weight-medium gx-mb-2">
                    block
                  </i>
                  <div className="gx-fs-md gx-font-weight-medium">
                    <IntlMessages id="customer.profile.notes.nonotes" />
                  </div>
                </div>
              </div>
            ) : (
              <div>
                {data.map((item, index) => (
                  <div key={index} className="gx-activity-list">
                    {item.avatar != "" ? (
                      <img
                        alt="avatar"
                        src={item.avatar}
                        className="gx-avatar-img gx-size-30 gx-border-0 gx-mr-10"
                      />
                    ) : (
                      <div className="gx-avatar-img gx-size-30 gx-border-0 gx-mr-10">
                        {item.name.charAt(0)}
                      </div>
                    )}
                    <div className="gx-activity-list-description">
                      <div className="gx-flex-row">
                        <div className="gx-activity-list-name">{item.name}</div>
                        <div className="gx-activity-list-datetime">
                          {changeDateFormat(item.datetime)}
                        </div>
                      </div>
                      <div className="gx-activity-list-note">{item.note}</div>
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(NewJobNotes);
