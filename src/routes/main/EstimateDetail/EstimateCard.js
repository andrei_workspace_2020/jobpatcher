import React, { Component } from "react";
import { Button, Checkbox, Avatar, Tag } from "antd";
import ButtonGroup from "antd/lib/button/button-group";

import Auxiliary from "util/Auxiliary";
import { Link } from "react-router-dom";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import EstimateStatStep from "components/Steps/EstimateStatStep";

class EstimateCard extends Component {
  state = {};

  constructor(props, context) {
    super(props, context);
  }

  renderAvatar(icon) {
    return (
      <div className="gx-main-avatar gx-size-32">
        <i
          className="material-icons gx-w-100 gx-text-center"
          style={{ color: "#fbfbfd" }}
        >
          {icon}
        </i>
      </div>
    );
  }
  onChangeStep = step => {
    this.props.onChangeStep(step);
  };
  render() {
    let { data, step, steps_data } = this.props;
    return (
      <div className="gx-w-100" style={{ zIndex: "100" }}>
        <div className="gx-job-customer-profile-container">
          <div className="gx-customer-profile-card gx-job-customer-profile-card gx-p-20">
            <div className="gx-flex-row gx-w-100 gx-h-100">
              <div className="gx-sub-title gx-h-100">
                <div className="gx-flex-row gx-h-100">
                  <Link to="/dispatch">
                    <div className="gx-flex-row  gx-align-items-center">
                      <i className="material-icons gx-mr-2">arrow_back</i>
                      <IntlMessages id="back" />
                    </div>
                  </Link>
                  <div className="gx-h-100 gx-flex-row">
                    <div className="gx-ml-5 gx-h-100 gx-flex-column gx-justify-content-between">
                      <div
                        className="gx-flex-row gx-align-items-center gx-mb-20"
                        style={{ height: "24px" }}
                      >
                        <IntlMessages id="estimate" />
                        <span className="gx-ml-1 gx-mr-2">{data.id}</span>
                      </div>
                      <div className="gx-flex-row">
                        <div className="gx-d-lg-block">
                          <ButtonGroup className="gx-customer-list-buttongroup">
                            <Button>
                              <i className="material-icons">assignment_ind</i>
                            </Button>
                            <Button>
                              <i className="material-icons">history</i>
                            </Button>
                            <Button>
                              <i className="material-icons">more_horiz</i>
                            </Button>
                          </ButtonGroup>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="gx-customer-profile-card gx-job-customer-profile-card gx-job-customer-status">
            <EstimateStatStep
              times={data.datetimes}
              current={step}
              steps_data={steps_data}
              onChangeStep={this.onChangeStep}
              history={this.props.history}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default EstimateCard;
