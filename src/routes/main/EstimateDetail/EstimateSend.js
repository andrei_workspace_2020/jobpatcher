import React, { Component } from "react";
import { Button, Row, Col, Divider, Tabs } from "antd";

import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import { injectIntl } from "react-intl";
import Auxiliary from "util/Auxiliary";
import ButtonGroup from "antd/lib/button/button-group";

const { TabPane } = Tabs;
const ToggleButton = () => {
    return (
      <div className="gx-btn-toggle-normal gx-flex-row">
        <div></div>
        <div></div>
        <div></div>
      </div>
    );
};
  

class EstimateSend extends Component {
    state = { current: 1, via_email: false, via_sms: false };

    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() { }

    onNextStep = () => {
        this.setState({ current: this.state.current + 1 });
    };
    onSend = () => {
        
    }
    onSendViaEmailToggleChange = value => {
        this.setState({ via_email: value });
    }
    onSendViaSMSToggleChange = value => {
        this.setState({ via_sms: value });
    }
    
    Step1 = () => {
        return (
            <Auxiliary>
                <div className="gx-flex-row gx-align-items-center gx-justify-content-end gx-mb-10">
                    <i className="material-icons gx-icon-action gx-mr-10">
                        get_app
                        </i>
                    <i className="material-icons gx-icon-action gx-mr-10">
                        local_printshop
                        </i>
                    <i className="material-icons gx-icon-action gx-mr-0">
                        settings_applications
                        </i>
                </div>
                <div className="gx-addjob-step2-addjobitem-total-panel">
                    <Widget styleName="gx-card-full gx-addjob-step2-subpane gx-card-estimate-send">
                        <div className="gx-flex-row gx-align-items-center gx-justify-content-center gx-m-30">
                            <Row className="gx-w-100 gx-mb-30">
                                <Col span={8}>
                                    <img
                                        src={require("assets/images/estimate/send_logo.png")}
                                    />
                                </Col>
                                <Col span={16}>
                                    <div className="gx-flex-column gx-w-100">
                                        <span className="gx-fs-lg gx-font-weight-bold gx-text-uppercase gx-mb-20">
                                            Probird environmental services
                                </span>
                                        <div className="gx-flex-row gx-w-100">
                                            <div className="gx-flex-row gx-w-50">
                                                <i className="material-icons gx-fs-xl gx-text-grey gx-mr-10">
                                                    location_on
                                    </i>
                                                <span className="gx-fs-13-20 gx-font-weight-medium">
                                                    Lock and Damn Rd Barling, AR 72923
                                    </span>
                                            </div>
                                            <div className="gx-flex-column gx-w-50">
                                                <div className="gx-flex-row gx-align-items-center">
                                                    <i className="material-icons gx-fs-xl gx-text-grey gx-mr-10">
                                                        perm_phone_msg
                                      </i>
                                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                                        +1(418)455-8990
                                      </span>
                                                </div>
                                                <div className="gx-flex-row gx-align-items-center">
                                                    <i className="material-icons gx-fs-xl gx-text-grey gx-mr-10">
                                                        email
                                      </i>
                                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                                        email@address.com
                                      </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <Row className="gx-w-100" gutter={30}>
                                <Col span={12} className="gutter-row gx-pl-0">
                                    <div className="gx-info-wrapper gx-estimate-info gx-h-100">
                                        <span className="gx-fs-lg gx-font-weight-semi-bold gx-text-uppercase gx-text-header gx-mb-10">
                                            Peter Jonson
                                </span>
                                        <div className="gx-flex-column">
                                            <span className="gx-fs-13-20 gx-font-weight-medium gx-mb-10">
                                                7015 Harrison
                                    <br />
                                                Ave Kankakee, IL 60901
                                  </span>
                                            <div className="gx-flex-row gx-align-items-center">
                                                <i className="material-icons gx-text-grey gx-fs-xl gx-mr-10">
                                                    perm_phone_msg
                                    </i>
                                                <span className="gx-fs-13-20 gx-font-weight-medium">
                                                    +1(418)238-0452
                                    </span>
                                            </div>
                                            <div className="gx-flex-row gx-align-items-center">
                                                <i className="material-icons gx-text-grey gx-fs-xl gx-mr-10">
                                                    email
                                    </i>
                                                <span className="gx-fs-13-20 gx-font-weight-medium">
                                                    email@address.com
                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col span={12} className="gutter-row gx-pr-0">
                                    <div className="gx-info-wrapper gx-estimate-info gx-h-100">
                                        <span className="gx-fs-lg gx-font-weight-semi-bold gx-text-uppercase gx-text-header gx-mb-10">
                                            Estimate
                                </span>
                                        <table
                                            width="100%"
                                            className="gx-fs-13-20 gx-font-weight-medium"
                                        >
                                            <tbody>
                                                <tr>
                                                    <td>Estimate ID:</td>
                                                    <td>#55046</td>
                                                </tr>
                                                <tr>
                                                    <td>Scheduled:</td>
                                                    <td>Dec 6, 2019 - 3:30 PM</td>
                                                </tr>
                                                <tr>
                                                    <td>Total items:</td>
                                                    <td>03</td>
                                                </tr>
                                                <tr>
                                                    <td>Total amount:</td>
                                                    <td>$175.00</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Widget>
                    <Widget styleName="gx-card-full gx-dispatcher-job-panel gx-addjob-step2-subpane  gx-addjob-step2-subpane-Services">
                        <div className="gx-panel-title-bar">
                            <div className="gx-w-100 gx-flex-row gx-justify-content-between">
                                <Row className="gx-p-0">
                                    <Col
                                        span={16}
                                        className="gx-flex-row gx-justify-content-center"
                                    >
                                        <Row className="gx-p-0" gutter={20}>
                                            <Col span={24} className="gutter-row gx-pl-0">
                                                <div className="gx-addjob-step2-subpane-header-item">
                                                    <h5>Services</h5>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col
                                        span={8}
                                        className="gx-flex-row gx-justify-content-center"
                                    >
                                        <Row className="gx-p-0" gutter={20}>
                                            <Col span={8} className="gutter-row">
                                                <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                                                    <h5>QTY</h5>
                                                </div>
                                            </Col>
                                            <Col span={8} className="gutter-row">
                                                <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                                                    <h5>Unit price</h5>
                                                </div>
                                            </Col>
                                            <Col span={8} className="gutter-row">
                                                <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                                                    <h5>Amount</h5>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        <div className="gx-panel-content">
                            <div className="gx-panel-item gx-flex-row">
                                <div className="gx-flex-column gx-w-100">
                                    <div className="gx-flex-row">
                                        <Row className="gx-p-0">
                                            <Col
                                                span={16}
                                                className="gx-flex-row gx-justify-content-center"
                                            >
                                                <Row gutter={20} className="gx-p-0">
                                                    <Col span={24} className="gutter-row gx-pl-0">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                Flat-Rate Clean - 1BD/1BA Home Clean
                                          </span>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col
                                                span={8}
                                                className="gx-flex-row gx-justify-content-center"
                                            >
                                                <Row className="gx-p-0" gutter={20}>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                2
                                          </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                $50.00
                                          </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                $100.00
                                          </span>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="gx-flex-row gx-justify-content-between gx-mt-10">
                                        <Row className="gx-p-0">
                                            <Col span={16}>
                                                <div className="gx-addjob-step2-subpane-body-description gx-pr-10">
                                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                                        Has it been a while since you've had a clean
                                                        home? Schedule with us and we'll clean it
                                                        from top to bottom! We bring all of the
                                                        cleaning supplies, and we'll even work with
                                                        you to make a custom-fit cleaning list to
                                                        ensure that your sepcific home's needs are
                                                        met.
                                      </span>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                                <Divider className="gx-mt-20 gx-mb-20" />
                                <div className="gx-flex-column gx-w-100">
                                    <div className="gx-flex-row">
                                        <Row className="gx-p-0">
                                            <Col
                                                span={16}
                                                className="gx-flex-row gx-justify-content-center"
                                            >
                                                <Row gutter={20} className="gx-p-0">
                                                    <Col span={24} className="gutter-row gx-pl-0">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                Flat-Rate Clean - 1BD/1BA Home Clean
                                          </span>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col
                                                span={8}
                                                className="gx-flex-row gx-justify-content-center"
                                            >
                                                <Row className="gx-p-0" gutter={20}>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                1
                                          </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                $50.00
                                          </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                $50.00
                                          </span>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="gx-flex-row gx-justify-content-between gx-mt-10">
                                        <Row className="gx-p-0">
                                            <Col span={16}>
                                                <div className="gx-addjob-step2-subpane-body-description gx-pr-10">
                                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                                        Has it been a while since you've had a clean
                                                        home? Schedule with us and we'll clean it
                                                        from top to bottom! We bring all of the
                                                        cleaning supplies.
                                      </span>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Widget>
                    <Widget styleName="gx-card-full gx-dispatcher-job-panel gx-addjob-step2-subpane  gx-addjob-step2-subpane-Materials">
                        <div className="gx-panel-title-bar">
                            <div className="gx-w-100 gx-flex-row gx-justify-content-between">
                                <Row className="gx-p-0">
                                    <Col
                                        span={16}
                                        className="gx-flex-row gx-justify-content-center"
                                    >
                                        <Row className="gx-p-0" gutter={20}>
                                            <Col span={24} className="gutter-row gx-pl-0">
                                                <div className="gx-addjob-step2-subpane-header-item">
                                                    <h5>Materials</h5>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col
                                        span={8}
                                        className="gx-flex-row gx-justify-content-center"
                                    >
                                        <Row className="gx-p-0" gutter={20}>
                                            <Col span={8} className="gutter-row">
                                                <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                                                    <h5>QTY</h5>
                                                </div>
                                            </Col>
                                            <Col span={8} className="gutter-row">
                                                <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                                                    <h5>Unit price</h5>
                                                </div>
                                            </Col>
                                            <Col span={8} className="gutter-row">
                                                <div className="gx-addjob-step2-subpane-header-item gx-justify-content-center">
                                                    <h5>Amount</h5>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        <div className="gx-panel-content">
                            <div className="gx-panel-item gx-flex-row">
                                <div className="gx-flex-column gx-w-100">
                                    <div className="gx-flex-row">
                                        <Row className="gx-p-0">
                                            <Col
                                                span={16}
                                                className="gx-flex-row gx-justify-content-center"
                                            >
                                                <Row gutter={20} className="gx-p-0">
                                                    <Col span={24} className="gutter-row gx-pl-0">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                Home Cleaning Kits
                                          </span>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col
                                                span={8}
                                                className="gx-flex-row gx-justify-content-center"
                                            >
                                                <Row className="gx-p-0" gutter={20}>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                1
                                          </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                $50.00
                                          </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={8} className="gutter-row">
                                                        <div className="gx-addjob-step2-subpane-body-detail-item gx-justify-content-center">
                                                            <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-header">
                                                                $50.00
                                          </span>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="gx-flex-row gx-justify-content-between gx-mt-10">
                                        <Row className="gx-p-0">
                                            <Col span={16}>
                                                <div className="gx-addjob-step2-subpane-body-description gx-pr-10">
                                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                                        We bring all of the cleaning supplies, and
                                                        we'll even work with you to make a
                                                        custom-fit cleaning list to ensure that your
                                                        specific home's needs.
                                      </span>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Widget>
                    <div className="gx-price-box-widget gx-addjob-step2-subpane gx-addjob-step2-subpane-pricebox">
                        <div className="price-box">
                            <div className="">
                                <div className="price-detail">
                                    <div className="price-label">Subtotal</div>
                                    <div className="price-value">$175.00</div>
                                </div>
                                <div className="price-detail">
                                    <div className="price-label gx-text-header">
                                        Total
                              </div>
                                    <div className="price-value gx-text-header">
                                        $175.00
                              </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Auxiliary>
        );
    }
    Step2 = () => {
        return (
            <Auxiliary>
                <Widget styleName="gx-card-full gx-card-estimate-send gx-mt-50">
                    <Tabs className="gx-card-tabs" defaultActiveKey="1">
                        <TabPane tab="Email" key="1">
                            <div className="gx-flex-column">
                                <div className="gx-flex-row gx-align-items-center" style={{padding: "15px 20px 15px 20px"}}>
                                    <ButtonGroup className="gx-custom-toggle-buttons">
                                        <Button
                                        className={`gx-btn-toggle ${
                                            this.state.via_email ? "gx-btn-toggle-yes" : ""
                                        }`}
                                        size="small"
                                        onClick={() => this.onSendViaEmailToggleChange(true)}
                                        >
                                        {this.state.via_email ? (
                                            <span>Yes</span>
                                        ) : (
                                            <ToggleButton></ToggleButton>
                                        )}
                                        </Button>
                                        <Button
                                        className={`gx-btn-toggle ${
                                            !this.state.via_email ? "gx-btn-toggle-no" : ""
                                        }`}
                                        size="small"
                                        onClick={() => this.onSendViaEmailToggleChange(false)}
                                        >
                                        {!this.state.via_email ? (
                                            <span>No</span>
                                        ) : (
                                            <ToggleButton></ToggleButton>
                                        )}
                                        </Button>
                                    </ButtonGroup>
                                    <span className={`gx-fs-13-20 gx-font-weight-medium ${!this.state.via_email?"gx-text-grey":""}`}>Send via email</span>
                                </div>
                                <div className="gx-flex-row">
                                    <table className="gx-table-send-estimate-info gx-w-100 gx-fs-13-20 gx-font-weight-medium">
                                        <tbody>
                                            <tr>
                                                <td className="gx-text-grey">Email address</td>
                                                <td>email@address.com</td>
                                            </tr>
                                            <tr>
                                                <td className="gx-text-grey">Subject</td>
                                                <td>Estimate from Probird environmental Services</td>
                                            </tr>
                                            <tr>
                                                <td className="gx-text-grey">Attachment</td>
                                                <td>esteeimate-#55046.pdf</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="gx-flex-row gx-p-20">
                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                        Hi Jonson,<br /><br />
                                        Thanks for your interest on us to clean your home and kitchen.<br />
                                        To view the esteimate online please <a href="#">click here</a><br />
                                        or download th PDF version from attachment below.<br /><br />
                                        Thank you.
                                    </span>
                                </div>
                            </div>
                        </TabPane>
                        <TabPane tab="SMS" key="2">
                            <div className="gx-flex-column">
                                <div className="gx-flex-row gx-align-items-center" style={{padding: "15px 20px 15px 20px"}}>
                                    <ButtonGroup className="gx-custom-toggle-buttons">
                                        <Button
                                        className={`gx-btn-toggle ${
                                            this.state.via_sms ? "gx-btn-toggle-yes" : ""
                                        }`}
                                        size="small"
                                        onClick={() => this.onSendViaEmailToggleChange(true)}
                                        >
                                        {this.state.via_sms ? (
                                            <span>Yes</span>
                                        ) : (
                                            <ToggleButton></ToggleButton>
                                        )}
                                        </Button>
                                        <Button
                                        className={`gx-btn-toggle ${
                                            !this.state.via_sms ? "gx-btn-toggle-no" : ""
                                        }`}
                                        size="small"
                                        onClick={() => this.onSendViaEmailToggleChange(false)}
                                        >
                                        {!this.state.via_sms ? (
                                            <span>No</span>
                                        ) : (
                                            <ToggleButton></ToggleButton>
                                        )}
                                        </Button>
                                    </ButtonGroup>
                                    <span className={`gx-fs-13-20 gx-font-weight-medium ${!this.state.via_sms?"gx-text-grey":""}`}>Send via SMS</span>
                                </div>
                                <div className="gx-flex-row">
                                    <table className="gx-table-send-estimate-info gx-w-100 gx-fs-13-20 gx-font-weight-medium">
                                        <tbody>
                                            <tr>
                                                <td className="gx-text-grey">Phone number</td>
                                                <td>+1(418)238-0452</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="gx-flex-row gx-p-20">
                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                        Hi Jonson,<br /><br />
                                        <a href="#">Click here</a> to view your estimate from Probird Environmental Services.<br /><br />
                                        Thank you.
                                    </span>
                                </div>
                            </div>
                        </TabPane>
                    </Tabs>
                </Widget>
            </Auxiliary>
        );
    }

  render() {
    return (
      <div className="gx-main-content-wrapper">
        <div className="gx-main-content">
          <div className="gx-dispatch-topmenu gx-addjob-topbar">
            <div className="gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-between">
              <a
                href="/estimates/detail"
                className="gx-flex-row gx-align-items-center gx-fs-lg gx-font-weight-bold"
              >
                <i className="material-icons gx-mr-10">arrow_back</i>
                <span className="">Send Estimate</span>
            </a>
            {
                this.state.current == 1 ? (
                    <Button
                        className="gx-nav-btn gx-nav-new-btn gx-mb-0"
                        style={{
                        width: "100px",
                        backgroundColor: "#6ab4ff",
                        border: "0"
                        }}
                        onClick={this.onNextStep}
                    >
                        Next
                    </Button>
                ) : this.state.current == 2 ? (
                    <Button
                        className="gx-nav-btn gx-nav-new-btn gx-mb-0"
                        style={{
                        width: "100px",
                        backgroundColor: "#6ab4ff",
                        border: "0"
                        }}
                        onClick={this.onSend}
                    >
                        Send
                    </Button>
                ) : <div/>
            }
            </div>
          </div>
          <div className="steps-content-wrapper gx-flex-column gx-align-items-center gx-py-20">
            <Row className="gx-w-100 gx-flex-row gx-justify-content-center">
                <Col span={12}>
                {
                    this.state.current == 1 ? 
                        this.Step1()
                    : this.state.current == 2 ? 
                        this.Step2()
                    : <div />
                }
                </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(EstimateSend);
