import React, { Component } from "react";
import {
  Row,
  Col,
  Popover,
  Button,
  Select,
  Input,
  Avatar,
  DatePicker
} from "antd";
import { injectIntl } from "react-intl";
import { changeDateTimeDashFormat } from "util/DateTime";
import IntlMessages from "util/IntlMessages";
import CustomerCard from "components/Dispatch/CustomerCard";
import AddressCard from "components/Dispatch/AddressCard";
import JobSubPane from "components/AddJob/JobSubPane";
import Widget from "components/Widget";
import EstimateNotes from "./EstimateNotes";
import EstimateUpload from "./EstimateUpload";

const { Option } = Select;

class JobPanel extends Component {
  state = {
    services: [
      {
        id: 1,
        category: "Steel",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 30.0,
        quantity: 0
      },
      {
        id: 2,
        category: "Steel",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 3,
        category: "Copper",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 4,
        category: "Copper",
        name: "Valve",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      },
      {
        id: 5,
        category: "Plastic",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 6,
        category: "Plastic",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 7,
        category: "Plastic",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      }
    ],
    materials: [
      {
        id: 1,
        category: "Steel",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 30.0,
        quantity: 0
      },
      {
        id: 2,
        category: "Steel",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 3,
        category: "Copper",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 4,
        category: "Copper",
        name: "Valve",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      },
      {
        id: 5,
        category: "Plastic",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 6,
        category: "Plastic",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 7,
        category: "Plastic",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      }
    ],
    service_categories: ["All services", "Steel", "Copper", "Plastic"],
    material_categories: ["All materials", "Steel", "Copper", "Plastic"],
    isAddressActionPopoverVisible: false,
    isNoteActionPopoverVisible: [],
    isFileActionPopoverVisible: [],
    isAddressDeletePopoverVisible: false,
    isPaymentHistoryActionPopoverVisible: false,
    isCustomerInfoFormChanged: false,
    isAddressFormChanged: false,
    editablePanelKey: "",
    isEditableInvoiceMessage: false,
    invoiceMessageText: ""
  };

  constructor(props, context) {
    super(props, context);
  }
  componentDidMount() {
    let array = [];
    for (let i = 0; i < this.props.data.notes.length; i++) array[i] = false;
    this.setState({ isNoteActionPopoverVisible: [...array] });
    for (let i = 0; i < this.props.data.files.length; i++) array[i] = false;
    this.setState({ isFileActionPopoverVisible: [...array] });
  }
  onInitiateState = () => {
    this.setState({
      isCustomerInfoFormChanged: false,
      isAddressFormChanged: false,
      isAddressActionPopoverVisible: false,
      isNoteActionPopoverVisible: [],
      isFileActionPopoverVisible: [],
      isAddressDeletePopoverVisible: false,
      editablePanelKey: ""
    });
  };
  onSetEditablePanelKey = key => {
    this.setState({ editablePanelKey: key }, () => {
      if (key == "address") {
        let inputNode = document.getElementById("street");
        var options = {
          types: ["address"],
          componentRestrictions: {
            country: "ca"
          }
        };
        this.autoComplete = new window.google.maps.places.Autocomplete(
          inputNode,
          options
        );
        this.autoComplete.addListener("place_changed", this.onPlaceChange);
      }
    });
  };
  onPlaceChange = () => {
    let place = this.autoComplete.getPlace();
    console.log(place);
    let location = place.geometry.location;
    let temp = { ...this.state.selected_address };
    temp.lat = location.lat();
    temp.lng = location.lng();
    temp.street = "";
    for (var i = 0; i < place.address_components.length; i++) {
      for (var j = 0; j < place.address_components[i].types.length; j++) {
        if (place.address_components[i].types[j] == "street_number") {
          temp.street += place.address_components[i].long_name + " ";
        } else if (place.address_components[i].types[j] == "route") {
          temp.street += place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "locality") {
          // or administrative_area_level_2
          temp.city = place.address_components[i].long_name;
        } else if (
          place.address_components[i].types[j] == "administrative_area_level_1"
        ) {
          temp.state = place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "postal_code") {
          temp.zipcode = place.address_components[i].long_name;
        }
      }
    }
    this.setState({ selected_address: temp });
  };
  addItems = (kind, items) => {
    if (kind == "Service") this.setState({ services: items });
    else if (kind == "Material") this.setState({ materials: items });
  };
  onDeleteNote = id => {
    this.props.onDeleteNote(id);
  };
  onSubmitNote = note => {
    this.props.onSubmitNote(note);
    this.onInitiateState();
  };
  onAddFile = file => {
    this.props.onAddFile(file);
  };
  onDeleteFile = id => {
    this.props.onDeleteFile(id);
  };
  onNoteActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isNoteActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isNoteActionPopoverVisible: temp });
  };
  onFileActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isFileActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isFileActionPopoverVisible: temp });
  };
  onPaymentHistoryActionPopoverVisibleChange = visible => {
    this.setState({ isPaymentHistoryActionPopoverVisible: visible });
  };
  onSetAddPaymentModalVisible = visible => {
    this.props.onSetAddPaymentModalVisible(visible);
  };
  onSubmitAddress = address => {
    this.props.onSubmitAddress(address);
  };
  onSetEditableInvoiceMessage = value => {
    this.setState({ isEditableInvoiceMessage: value });
  };
  onInvoiceMessageChange = e => {
    this.setState({ invoiceMessageText: e.target.value });
  };
  onSaveInvoiceMessage = () => {
    console.log("kkkkk");
    this.props.onSaveInvoiceMessage(this.state.invoiceMessageText);
    this.setState({ isEditableInvoiceMessage: false });
  };
  onDiscardInvoiceMessage = () => {
    this.setState({ isEditableInvoiceMessage: false });
  };
  render() {
    let {
      data,
      customer,
      step,
      intl: { formatMessage }
    } = this.props;

    return (
      <Widget styleName="gx-card-full gx-job-panel gx-profile-detail-panel gx-mb-0">
        <div className="gx-job-detail-content">
          <div className="gx-job-detail-content-scroll">
            <div className="gx-customer-profile-tab-panel-content">
              <Row>
                <Col xs={24} sm={24} md={24} lg={8} xl={12} xxl={12}>
                  <Row>
                    <Col
                      xs={24}
                      sm={12}
                      md={12}
                      lg={24}
                      xl={12}
                      xxl={12}
                      className="gx-pb-20"
                    >
                      <CustomerCard
                        data={customer}
                        showEditCustomerDlg={this.props.showEditCustomerDlg}
                        editablePanelKey={this.state.editablePanelKey}
                        onSetEditablePanelKey={this.onSetEditablePanelKey}
                      />
                    </Col>
                    <Col
                      xs={24}
                      sm={12}
                      md={12}
                      lg={24}
                      xl={12}
                      xxl={12}
                      className="gx-pb-20"
                    >
                      <AddressCard
                        data={data}
                        editablePanelKey={this.state.editablePanelKey}
                        onSetEditablePanelKey={this.onSetEditablePanelKey}
                        onSubmitAddress={this.onSubmitAddress}
                      />
                    </Col>
                    <Col
                      xs={24}
                      sm={12}
                      md={12}
                      lg={24}
                      xl={12}
                      xxl={12}
                      className="gx-pb-20"
                    >
                      <Widget styleName="gx-card-full gx-card-notes gx-h-100 gx-mb-0">
                        <div className="gx-panel-title-bar">
                          <h5>
                            <IntlMessages id="estimate.detail.estimate_notes" />
                          </h5>
                        </div>
                        <div
                          className="gx-panel-content gx-panel-customer-notes gx-pl-0 gx-pr-0 gx-pb-0"
                          style={{ paddingTop: "60px" }}
                        >
                          <EstimateNotes
                            data={data}
                            editablePanelKey={this.state.editablePanelKey}
                            onInitiateState={this.onInitiateState}
                            onSetEditablePanelKey={this.onSetEditablePanelKey}
                            onDeleteNote={this.onDeleteNote}
                            onSubmitNote={this.onSubmitNote}
                            onNoteActionPopoverVisibleChange={
                              this.onNoteActionPopoverVisibleChange
                            }
                            isNoteActionPopoverVisible={
                              this.state.isNoteActionPopoverVisible
                            }
                          />
                        </div>
                      </Widget>
                    </Col>
                    <Col
                      xs={24}
                      sm={12}
                      md={12}
                      lg={24}
                      xl={12}
                      xxl={12}
                      className="gx-pb-20"
                    >
                      <Widget styleName="gx-card-full gx-card-upload gx-h-100 gx-mb-0">
                        <div className="gx-panel-title-bar">
                          <h5>
                            <IntlMessages id="estimate.detail.estimate_files" />
                          </h5>
                        </div>
                        <div
                          className="gx-panel-content"
                          style={{ padding: "60px 0px 0px 0px" }}
                        >
                          <EstimateUpload
                            data={data}
                            editablePanelKey={this.state.editablePanelKey}
                            onInitiateState={this.onInitiateState}
                            onSetEditablePanelKey={this.onSetEditablePanelKey}
                            onAddFile={this.onAddFile}
                            onDeleteFile={this.onDeleteFile}
                            onFileActionPopoverVisibleChange={
                              this.onFileActionPopoverVisibleChange
                            }
                            isFileActionPopoverVisible={
                              this.state.isFileActionPopoverVisible
                            }
                          />
                        </div>
                      </Widget>
                    </Col>
                  </Row>
                </Col>
                <Col xs={24} sm={24} md={24} lg={16} xl={12} xxl={12}>
                  <div className="gx-addjob-step2-addjobitem-total-panel">
                    <Widget styleName="gx-card-full gx-invoice-widget gx-addjob-step2-subpane">
                      <div className="gx-panel-title-bar ">
                        <h5 className="text-uppercase">ESTIMATE #70458</h5>
                        <div className="gx-panel-title-buttons gx-flex-row gx-align-items-center">
                          <div className="gx-edit-circle gx-mr-2">
                            <i className="material-icons gx-icon-action">
                              send
                            </i>
                          </div>
                          <div className="gx-edit-circle">
                            <i className="material-icons gx-icon-action">
                              print
                            </i>
                          </div>
                        </div>
                      </div>
                      <div className="gx-panel-content">
                        <div className="gx-job-invoice-wrapper">
                          {/* <Button className="gx-customized-round-button">
                            <span>
                              Due:{" "}
                              <span className="gx-text-primary">
                                Upon receipt
                              </span>
                            </span>
                            <i className="material-icons">expand_more</i>
                          </Button> */}
                          <div className="gx-customized-user-avatar-name-round gx-flex-row gx-align-items-center">
                            <Avatar className="gx-size-30 gx-avatar-default gx-mr-2">
                              P
                            </Avatar>
                            <span className="gx-fs-13-20 gx-font-weight-medium">
                              Peter Jonson
                            </span>
                          </div>
                        </div>
                      </div>
                    </Widget>
                    <JobSubPane
                      kind="Service"
                      items={[...this.state.services]}
                      categories={this.state.service_categories}
                      addItems={this.addItems}
                    />
                    <JobSubPane
                      kind="Material"
                      items={[...this.state.materials]}
                      categories={this.state.material_categories}
                      addItems={this.addItems}
                    />
                    <div className="gx-price-box-widget gx-addjob-step2-subpane gx-addjob-step2-subpane-pricebox">
                      <div className="price-box">
                        <Row gutter={20}>
                          <Col span={16} className="gutter-row">
                            <div className="gx-flex-row">
                              {!this.state.isEditableInvoiceMessage &&
                              data.invoiceMessage === "" ? (
                                <div className="gx-flex-column gx-justify-content-end">
                                  <div
                                    className="gx-link gx-flex-row gx-align-items-center"
                                    onClick={() =>
                                      this.onSetEditableInvoiceMessage(true)
                                    }
                                  >
                                    <i className="material-icons">add</i>
                                    <span className="gx-fs-13-20 gx-font-weight-medium">
                                      Message on invoice
                                    </span>
                                  </div>
                                </div>
                              ) : (
                                <div className="gx-flex-column gx-justify-content-between gx-w-100">
                                  <span className="gx-fs-13-20 gx-font-weight-semi-bold gx-text-left gx-mb-10">
                                    Message on invoice
                                  </span>
                                  {data.invoiceMessage === "" ? (
                                    <div className="gx-flex-column">
                                      <Input
                                        value={this.state.invoiceMessageText}
                                        onChange={this.onInvoiceMessageChange}
                                        className="gx-customized-input"
                                        placeholder={formatMessage({
                                          id:
                                            "estimate.detail.placeholder.type_here"
                                        })}
                                      />
                                      <div className="gx-flex-row gx-mt-10">
                                        <Button
                                          className="gx-btn-save gx-bg-primary gx-mr-10"
                                          onClick={this.onSaveInvoiceMessage}
                                        >
                                          Save
                                        </Button>
                                        <Button
                                          className="gx-btn-cancel"
                                          onClick={() =>
                                            this.onSetEditableInvoiceMessage(
                                              false
                                            )
                                          }
                                          onClick={this.onDiscardInvoiceMessage}
                                        >
                                          Discard
                                        </Button>
                                      </div>
                                    </div>
                                  ) : (
                                    <span className="gx-fs-sm gx-font-weight-medium gx-text-left">
                                      {data.invoiceMessage}
                                    </span>
                                  )}
                                </div>
                              )}
                            </div>
                          </Col>
                          <Col span={8} className="gutter-row">
                            <div className="">
                              <div className="price-detail">
                                <div className="price-label">Subtotal</div>
                                <div className="price-value">$00.00</div>
                              </div>
                              <div className="price-detail">
                                <div className="price-label">Total</div>
                                <div className="price-value">$00.00</div>
                              </div>
                              <div className="price-detail">
                                <Popover
                                  overlayClassName="gx-popover-add-discount gx-estimate-popover"
                                  placement="top"
                                  trigger="hover"
                                  content={
                                    <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                                      <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                        <img
                                          className="gx-mr-10"
                                          src={require("assets/images/discount.png")}
                                        />
                                        <div className="gx-flex-column">
                                          <span className="gx-fs-lg gx-font-weight-bold">
                                            Add discount
                                          </span>
                                          <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                                            Please select an option
                                          </span>
                                        </div>
                                      </div>
                                      <div className="gx-flex-row gx-align-items-center gx-flex-nowrap gx-mt-10">
                                        <div className="gx-w-100">
                                          <Select
                                            suffixIcon={
                                              <i className="material-icons">
                                                expand_more
                                              </i>
                                            }
                                            value="Percent"
                                          >
                                            <Option value="Percent">
                                              Percent
                                            </Option>
                                          </Select>
                                        </div>
                                        <div className="gx-w-100">
                                          <Input
                                            defaultValue="5"
                                            className="gx-text-center"
                                          ></Input>
                                        </div>
                                      </div>
                                      <div
                                        className="gx-popover-action"
                                        style={{ marginTop: "15px" }}
                                      >
                                        <Button
                                          type="default"
                                          className="gx-btn-cancel gx-mr-10"
                                        >
                                          Cancel
                                        </Button>
                                        <Button
                                          type="primary"
                                          className="gx-btn-confirm"
                                        >
                                          Confirm
                                        </Button>
                                      </div>
                                    </div>
                                  }
                                >
                                  <div className="price-label price-label-highlight">
                                    Add discount %
                                  </div>
                                </Popover>
                                <div className="price-value price-value-disable">
                                  $00.00
                                </div>
                              </div>
                              <div className="price-detail">
                                <Popover
                                  overlayClassName="gx-popover-add-deposit gx-estimate-popover"
                                  placement="top"
                                  trigger="hover"
                                  content={
                                    <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                                      <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                        <img
                                          className="gx-mr-10"
                                          src={require("assets/images/deposit.png")}
                                        />
                                        <div className="gx-flex-column">
                                          <span className="gx-fs-lg gx-font-weight-bold">
                                            Add deposit
                                          </span>
                                          <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                                            Please fill the options
                                          </span>
                                        </div>
                                      </div>
                                      <div className="gx-flex-row gx-align-items-center gx-flex-nowrap gx-mt-10">
                                        <div className="gx-w-100">
                                          <Select
                                            suffixIcon={
                                              <i className="material-icons">
                                                expand_more
                                              </i>
                                            }
                                            value="Percent"
                                          >
                                            <Option value="amount">
                                              Amount
                                            </Option>
                                          </Select>
                                        </div>
                                        <div className="gx-w-100">
                                          <Input
                                            defaultValue="20"
                                            className="gx-text-center"
                                          ></Input>
                                        </div>
                                      </div>
                                      <div className="gx-customized-content-field gx-mt-10">
                                        <div className="gx-customized-content-field-title">
                                          Due date
                                        </div>
                                        <div className="gx-customized-content-field-value">
                                          <DatePicker
                                            className="gx-w-100"
                                            format="DD/MM/YYYY"
                                            placeholder={formatMessage({
                                              id:
                                                "estimate.detail.add_deposit.dd_mm_yyyy"
                                            })}
                                            suffixIcon={
                                              <i className="material-icons">
                                                date_range
                                              </i>
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div
                                        className="gx-popover-action"
                                        style={{ marginTop: "15px" }}
                                      >
                                        <Button
                                          type="default"
                                          className="gx-btn-cancel gx-mr-10"
                                        >
                                          Cancel
                                        </Button>
                                        <Button
                                          type="primary"
                                          className="gx-btn-confirm"
                                        >
                                          Add deposit
                                        </Button>
                                      </div>
                                    </div>
                                  }
                                >
                                  <div className="price-label price-label-highlight">
                                    Add deposit
                                  </div>
                                </Popover>
                                <div className="price-value price-value-disable">
                                  $00.00
                                </div>
                              </div>
                            </div>
                          </Col>
                        </Row>
                      </div>
                    </div>
                    {data.payment_history.length > 0 ? (
                      <Widget
                        styleName={`gx-card-full gx-card-job-payment-history gx-price-box-widget gx-addjob-step2-subpane`}
                      >
                        <div className="gx-panel-title-bar">
                          <h5>
                            <IntlMessages id="estimate.detail.payment_history" />
                          </h5>
                          <Popover
                            overlayClassName="gx-popover-payment-history-action"
                            placement="bottomRight"
                            trigger="click"
                            visible={
                              this.state.isPaymentHistoryActionPopoverVisible
                            }
                            onVisibleChange={visible =>
                              this.onPaymentHistoryActionPopoverVisibleChange(
                                visible
                              )
                            }
                            content={
                              <div>
                                <div className="gx-menuitem">
                                  <i className="material-icons">attach_money</i>
                                  <span>Create an invoice</span>
                                </div>
                                <div
                                  className="gx-menuitem"
                                  onClick={() => {
                                    this.onPaymentHistoryActionPopoverVisibleChange(
                                      false
                                    );
                                    this.onSetAddPaymentModalVisible(true);
                                  }}
                                >
                                  <i className="material-icons">add</i>
                                  <span>Add a payment</span>
                                </div>
                              </div>
                            }
                          >
                            <Button className="gx-customized-button gx-flex-row gx-align-items-center">
                              <span>New</span>
                              <i className="material-icons">expand_more</i>
                            </Button>
                          </Popover>
                        </div>
                        <div className="gx-panel-content">
                          <div className="gx-panel-content-scroll gx-px-20 gx-py-0">
                            {data.payment_history.map((item, index) => (
                              <div
                                key={index}
                                className="gx-job-payment-history-item gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-between gx-px-20 gx-py-10"
                              >
                                <div className="gx-job-payment-history-item-detail gx-flex-column">
                                  <div className="gx-flex-row gx-align-items-center">
                                    <span className="gx-fs-sm gx-link">
                                      #{item.id}
                                    </span>
                                    <span className="dot gx-text-grey">
                                      <i className="material-icons">lens</i>
                                    </span>
                                    <span className="gx-fs-sm gx-text-grey">
                                      {changeDateTimeDashFormat(item.datetime)}
                                    </span>
                                  </div>
                                  <span className="gx-fs-sm gx-font-weight-medium">
                                    ${item.amount} has been paid with{" "}
                                    {item.method}
                                  </span>
                                </div>
                                <div className="gx-job-payment-history-item-action gx-flex-row gx-align-items-center">
                                  <i className="material-icons gx-icon-action gx-mr-10">
                                    edit
                                  </i>
                                  <i className="material-icons gx-icon-action">
                                    more_vert
                                  </i>
                                </div>
                              </div>
                            ))}
                          </div>
                        </div>
                      </Widget>
                    ) : (
                      <div />
                    )}
                  </div>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </Widget>
    );
  }
}

export default injectIntl(JobPanel);
