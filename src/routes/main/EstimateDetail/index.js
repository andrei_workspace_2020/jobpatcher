import React, { Component } from "react";
import { Modal } from "antd";
import { connect } from "react-redux";

import IntlMessages from "util/IntlMessages";
import { injectIntl } from "react-intl";
import { changeDateMonthYearHourMinuteSecondFormatSS } from "util/DateTime";
import EstimateCard from "./EstimateCard";
import EstimatePanel from "./EstimatePanel";
import AddPaymentDlg from "./AddPaymentDialog/AddPaymentDlg";
import EditCustomerDlg from "./CustomerDialog/EditCustomerDlg";

class EstimateDetail extends Component {
  state = {
    step: 1,
    steps_data: [
      {
        title: "schedule"
      },
      {
        title: "on the way",
        date: null,
        time: null,
        notify: true
      },
      {
        title: "finish",
        date: null,
        time: null,
        notify: false
      },
      {
        title: "send"
      },
      {
        title: "approval"
      },
      {
        title: "convert"
      }
    ],
    job: {
      id: "#00546",
      title: "Corporate windows cleaning",
      status: "on the way",
      datetimes: ["05/04/2019 14:30", "05/04/2019 15:30"],
      customer_id: 12345678,
      notes: [],
      files: [],
      address: [
        {
          title: "Home addressXXX",
          street: "2655 Speers Road",
          unit: "",
          city: "Oakville",
          state: "Ontario",
          zipcode: "L6J3X4",
          alarmcode: "88590",
          property_key: "",
          lat: 43.4030085,
          lng: -79.7314425
        }
      ],
      payment_history: [
        {
          id: "70458",
          amount: 350.0,
          method: "Cash",
          datetime: "05/04/2019 14:30"
        },
        {
          id: "70459",
          amount: 250.0,
          method: "Cash",
          datetime: "05/04/2019 14:30"
        }
      ],
      invoiceMessage: ""
    },
    customer: {
      id: 12345678,
      avatar: "",
      title: "Mr.",
      first_name: "Robert",
      last_name: "Brannon",
      email: "robert@website.com",
      phone: {
        mobile: "+1(705)-255-1111",
        primary_phone: "(613)-966-1111",
        work: ""
      },
      birthday: "01/01/1974",
      balance: 100.0,
      status: "Active",
      starred: false,
      customer_info: {
        customer_type: "Commercial",
        lead_source: "Business card",
        creation_date: "01/01/2018",
        created_by: "Peter Jonson" //later it should be user id
      },
      address: [
        {
          id: 12345678,
          title: "Home address",
          street: "2655 Speers Road",
          unit: "",
          city: "Oakville",
          state: "Ontario",
          zipcode: "L6J3X4",
          alarmcode: "88590",
          property_key: "",
          lat: 43.4030085,
          lng: -79.7314425
        },
        {
          id: 1,
          title: "Office address",
          street: "496 Hochelaga",
          unit: "",
          city: "Laval",
          state: "Quebec",
          zipcode: "H7P3H6",
          alarmcode: "",
          property_key:
            "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
          lat: 45.5841211,
          lng: -73.5285817
        }
      ],
      communication: {
        name_format: ["Mr.", "last_name"],
        email_communication: true,
        sms_communication: true,
        preferred_language: "English - US"
      },
      notes: [
        {
          id: 0,
          name: "Robert Brannon",
          note: "Lesser land heaven which above first.",
          datetime: "4/24/2019 15:26",
          avatar: ""
        },
        {
          id: 1,
          name: "Leana Rosebell",
          note:
            "Lesser land heaven which above first. Lesser land heaven which above first.",
          datetime: "4/22/2019 14:26",
          avatar: require("assets/images/avatar/avatar03.png")
        }
      ],
      jobs: [
        {
          id: "#3624",
          date: "29/12/2019",
          amount: 250,
          status: "Completed"
        },
        {
          id: "#3622",
          date: "29/12/2019",
          amount: 330,
          status: "Canceled"
        },
        {
          id: "#3468",
          date: "29/12/2019",
          amount: 320,
          status: "Completed"
        }
      ],
      estimates: [
        {
          id: "#3624",
          date: "29/12/2019",
          amount: 250,
          status: "Converted"
        },
        {
          id: "#3622",
          date: "29/12/2019",
          amount: 330,
          status: "Refused"
        },
        {
          id: "#3468",
          date: "29/12/2019",
          amount: 320,
          status: "Converted"
        }
      ],
      invoices: [
        {
          id: "#3624",
          date: "22/12/2019",
          due_date: "29/12/2019",
          amount: 250,
          status: "Overdue"
        },
        {
          id: "#3622",
          date: "22/12/2019",
          due_date: "29/12/2019",
          amount: 330,
          status: "Due"
        },
        {
          id: "#3468",
          date: "22/12/2019",
          due_date: "29/12/2019",
          amount: 320,
          status: "Paid"
        }
      ],
      files: [],
      financial_activities: {
        overdue_invoices: [
          {
            job: "#3452",
            invoice: 520.01,
            overdue: "6 days"
          },
          {
            job: "#3454",
            invoice: 520.01,
            overdue: "6 days"
          }
        ],
        non_invoiced_jobs: [
          {
            job: "#3457",
            description: "Invoice will be auto-generated after 4 jobs"
          }
        ],
        not_due_yet: [
          {
            job: "#3448",
            due_days: "3 days"
          }
        ]
      },
      financial_info: {
        automatic_invoice: "Weekly",
        payment_term: "Due after 7 days",
        bill_to: "",
        taxable: true,
        discount_rate: "5%"
      },
      billing_info: {
        billing_name: "Kevin Jonson",
        street: "496 Hochelaga",
        unit: "",
        city: "Laval",
        state: "Quebec",
        zipcode: "H7P3H6",
        invoice_to: "kevin@website.com",
        lat: 45.5841211,
        lng: -73.5285817
      },
      payment_history: [
        {
          job: "#5470",
          date: "28 October, 2019",
          invoice: 350.0,
          payment_method: "Stripe"
        },
        {
          job: "#5456",
          date: "23 October, 2019",
          invoice: 200.0,
          payment_method: "Cheque"
        },
        {
          job: "#5448",
          date: "20 September, 2019",
          invoice: 450.0,
          payment_method: "Cash"
        }
      ]
    },
    isAddPaymentVisible: false,
    isCustomerEditDlgVisible: false
  };

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {}
  showEditCustomerDlg = customer => {
    this.setState({ customer: customer, isCustomerEditDlgVisible: true });
  };
  onCancelAddCustomer = () => {
    this.setState({ isCustomerEditDlgVisible: false });
  };
  onChangeStep = step => {
    this.setState({ step });
  };
  onCancelAddPayment = () => {
    this.setState({ isAddPaymentVisible: false });
  };
  onSaveCustomer = customer => {
    this.setState({ customer });
    this.setState({ isCustomerEditDlgVisible: false });
  };
  onSubmitAddress = address => {
    let temp = { ...this.state.job };
    temp.address[0] = { ...address };
    this.setState({ job: { ...temp } });
  };
  onSubmitNote = note => {
    console.log(note);
    let temp = this.state.job;
    let list = temp.notes;
    if (note.id === "") {
      note.id = list.length;
      list.unshift(note);
    } else {
      list.map(item => {
        if (item.id === note.id) {
          item = note;
        }
      });
    }
    temp.notes = list;
    this.setState({ job: { ...temp } });
  };
  onDeleteNote = id => {
    let temp = this.state.job;
    let notes = temp.notes;
    let index = 0;
    notes.map((item, i) => {
      if (item.id === id) index = i;
    });
    notes.splice(index, 1);
    temp.notes = notes;
    this.setState({ job: { ...temp } });
  };
  onAddFile = file => {
    let temp = this.state.job;
    temp.files.push(file);
    this.setState({ job: { ...temp } });
  };
  onDeleteFile = id => {
    let temp = this.state.job;
    let files = temp.files;
    let index;
    files.map((item, i) => {
      if (item.id === id) index = i;
    });
    files.splice(index, 1);
    temp.files = files;
    this.setState({ job: { ...temp } });
  };
  onSetAddPaymentModalVisible = visible => {
    this.setState({ isAddPaymentVisible: visible });
  };
  onSaveInvoiceMessage = msg => {
    console.log("hehehe");
    let temp = this.state.job;
    temp.invoiceMessage = msg;
    this.setState({ job: { ...temp } });
  };
  render() {
    const { job, customer, steps_data, step } = this.state;
    const { width } = this.props;
    return (
      <div className="gx-main-content-wrapper">
        <div className="gx-main-content">
          <div className="gx-app-module gx-customer-profile-module">
            <div className="gx-w-100">
              <div className="gx-customer-profile-module-scroll">
                <div className="gx-main-content-container gx-customer-profile-module-content gx-p-0">
                  <EstimateCard
                    data={job}
                    step={step}
                    steps_data={steps_data}
                    onChangeStep={this.onChangeStep}
                    history={this.props.history}
                  />
                  <EstimatePanel
                    data={job}
                    customer={customer}
                    step={step}
                    showEditCustomerDlg={this.showEditCustomerDlg}
                    onSetAddPaymentModalVisible={
                      this.onSetAddPaymentModalVisible
                    }
                    onSubmitAddress={this.onSubmitAddress}
                    onSubmitNote={this.onSubmitNote}
                    onDeleteNote={this.onDeleteNote}
                    onSaveInvoiceMessage={this.onSaveInvoiceMessage}
                  />

                  <Modal
                    className="gx-modal-customer-add-payment gx-ss-customers-new-modal"
                    title={
                      <div className="gx-customized-modal-header">
                        <div className="gx-customized-modal-title">
                          <IntlMessages id="customer.addpaymentdlg.content.label.add_new_payment" />
                        </div>
                        <div>
                          <i
                            className="material-icons gx-customized-modal-close"
                            onClick={this.onCancelAddPayment}
                          >
                            clear
                          </i>
                        </div>
                      </div>
                    }
                    closable={false}
                    wrapClassName="gx-customized-modal vertical-center-modal"
                    visible={this.state.isAddPaymentVisible}
                    onCancel={this.onCancelAddPayment}
                  >
                    <AddPaymentDlg
                      onCancel={this.onCancelAddPayment}
                      onSave={this.onAddPayment}
                      data={job}
                    />
                  </Modal>
                  <Modal
                    className="gx-ss-customers-new-modal"
                    title={
                      <div className="gx-customized-modal-header">
                        <div className="gx-customized-modal-title">
                          <IntlMessages id="customer.customerdlg.editcustomer" />
                        </div>
                        <div>
                          <i
                            className="material-icons gx-customized-modal-close"
                            onClick={this.onCancelAddCustomer}
                          >
                            clear
                          </i>
                        </div>
                      </div>
                    }
                    closable={false}
                    wrapClassName="gx-customized-modal vertical-center-modal"
                    visible={this.state.isCustomerEditDlgVisible}
                    onCancel={this.onCancelAddCustomer}
                    width={
                      (width >= 1144 && 1084) ||
                      (width >= 500 && width - 60) ||
                      width - 30
                    }
                  >
                    <EditCustomerDlg
                      onCancel={this.onCancelAddCustomer}
                      onSave={this.onSaveCustomer}
                      data={this.state.customer}
                    />
                  </Modal>
                </div>
              </div>
            </div>
            {/* <div className="gx-customer-sidenav">
              <EstimateDrawer />
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { locale, navStyle, navCollapsed, width, currentPage } = state.settings;
  return {
    locale,
    navStyle,
    navCollapsed,
    width,
    currentPage
  };
};

export default connect(mapStateToProps)(injectIntl(EstimateDetail));
