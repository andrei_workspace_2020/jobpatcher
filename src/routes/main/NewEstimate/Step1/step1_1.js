import React, { Component } from "react";
import { Select } from "antd";
import { Col, Row, Button, Modal } from "antd";
import { injectIntl } from "react-intl";
import SearchBar from "components/AddEstimate/SearchBar";
import PrivateNoteWidget from "components/AddEstimate/PrivateNoteWidget";
import UploadFileWidget from "components/AddEstimate/UploadFileWidget";
import { Link } from "react-router-dom";
import IntlMessages from "util/IntlMessages";
import EditCustomerDlg from "../../Customers/CustomerDialog/EditCustomerDlg";
import ImportCustomerDlg from "../../Customers/CustomerDialog/ImportCustomerDlg";
import Step1_2 from "./step1_2";
import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_MINI_SIDEBAR,
  TAB_SIZE,
  MOBILE_SIZE
} from "constants/ThemeSetting";

class Step1_1 extends Component {
  state = {
    customer: null,
    width: 0,
    customerNewDlgVisible: false,
    customerImportDlgVisible: false,
    isNewPopoverVisible: false
  };
  constructor(props, context) {
    super(props, context);
    this.searchCustomer = this.searchCustomer.bind(this);
  }
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.updateDimensions);
  }
  updateDimensions = () => {
    this.setState({
      width: window.innerWidth
    });
  };
  searchCustomer(customer) {
    console.log(customer);
    this.setState({ customer });
  }
  showNewCustomerDlg = () => {
    this.setState({
      customerNewDlgVisible: true,
      customerImportDlgVisible: false
    });
    this.setState({ isNewPopoverVisible: false });
  };
  onCancel = () => {
    this.setState({
      customerNewDlgVisible: false,
      customerImportDlgVisible: false
    });
  };
  onSaveCustomer = () => {
    this.setState({ customerNewDlgVisible: false });
  };
  showImportCustomerDlg = () => {
    this.setState({
      customerNewDlgVisible: false,
      customerImportDlgVisible: true
    });
  };
  render() {
    const { customer } = this.state;
    return (
      <div className="gx-h-100">
        {customer != null ? (
          <Step1_2
            showNewCustomerDlg={this.showNewCustomerDlg}
            data={customer}
            onSearchCustomer={this.searchCustomer}
          />
        ) : (
          <div className="gx-main-content-container gx-addjob-step1-1-main-content gx-p-20 gx-flex-row gx-align-items-center gx-justify-content-center gx-h-100 gx-m-auto">
            <Row>
              <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
                <Row>
                  <Col
                    xxl={24}
                    xl={24}
                    lg={24}
                    md={24}
                    sm={24}
                    xs={24}
                    style={{ paddingBottom: 20 }}
                  >
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center"
                      }}
                    >
                      <h3 style={{ paddingBottom: 20 }}>Search customer</h3>
                      <a
                        className="gx-nav-btn gx-nav-new-btn gx-mb-0 gx-addjob-new-customer"
                        onClick={this.showNewCustomerDlg}
                      >
                        <div className="gx-div-align-center">
                          <i className="material-icons gx-fs-xl">add</i>
                          New customer
                        </div>
                      </a>
                    </div>
                    <SearchBar
                      customer={this.state.customer}
                      onSearchCustomer={this.searchCustomer}
                    />
                  </Col>
                  <Col
                    xxl={12}
                    xl={12}
                    lg={12}
                    md={12}
                    sm={24}
                    xs={24}
                    style={{ paddingTop: 10 }}
                    className="gx-step1-1-upload-container"
                  >
                    <UploadFileWidget style={{ padding: 0 }} />
                  </Col>

                  <Col
                    xxl={12}
                    xl={12}
                    lg={12}
                    md={12}
                    sm={24}
                    xs={24}
                    style={{ paddingTop: 10 }}
                  >
                    <PrivateNoteWidget style={{ padding: 0 }} />
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        )}
        <Modal
          className="gx-ss-customers-new-modal"
          title={
            <div className="gx-customized-modal-header">
              <div className="gx-customized-modal-title">Add new customer</div>
              <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                <Button
                  className="gx-customized-button gx-d-none gx-d-md-block notDisplayOnMobile"
                  onClick={() => {
                    this.showImportCustomerDlg();
                  }}
                >
                  <div className="gx-div-align-center">
                    <i
                      className="material-icons gx-fs-xl gx-mr-2"
                      style={{ marginLeft: "-6px" }}
                    >
                      publish
                    </i>
                    <IntlMessages id="customer.customerdlg.importcontacts" />
                  </div>
                </Button>
                <Button
                  className="gx-customized-button gx-d-md-none notDisplayOnMobile"
                  onClick={() => {
                    this.showImportCustomerDlg();
                  }}
                >
                  <div className="gx-div-align-center">
                    <i
                      className="material-icons gx-fs-xl gx-mr-2"
                      style={{ marginLeft: "-6px" }}
                    >
                      publish
                    </i>
                    <IntlMessages id="customer.customerdlg.import" />
                  </div>
                </Button>
                <i
                  className="material-icons gx-customized-modal-close"
                  onClick={this.onCancel.bind(this)}
                >
                  clear
                </i>
              </div>
            </div>
          }
          closable={false}
          wrapClassName="gx-customized-modal vertical-center-modal"
          visible={this.state.customerNewDlgVisible}
          onCancel={this.onCancel.bind(this)}
          // width={ width >= 1144 ? 1084 : width - 60 }
          width={
            (this.state.width >= 1144 && 1084) ||
            (this.state.width >= 500 && this.state.width - 60) ||
            this.state.width - 30
          }
        >
          <EditCustomerDlg
            onCancel={this.onCancel.bind(this)}
            onSave={this.onSaveCustomer.bind(this)}
          />
        </Modal>
        <Modal
          title={
            <div className="gx-customized-modal-header">
              <div className="gx-customized-modal-title">
                <IntlMessages id="customer.customerdlg.importcustomers" />
              </div>
              <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                <Button
                  className="gx-customized-button gx-d-none gx-d-md-block"
                  onClick={() => {
                    this.showNewCustomerDlg();
                  }}
                >
                  <div className="gx-div-align-center">
                    <i
                      className="material-icons gx-fs-xl gx-mr-2"
                      style={{ marginLeft: "-6px" }}
                    >
                      publish
                    </i>
                    <IntlMessages id="customer.customerdlg.addmanually" />
                  </div>
                </Button>
                <i
                  className="material-icons gx-customized-modal-close"
                  onClick={this.onCancel.bind(this)}
                >
                  clear
                </i>
              </div>
            </div>
          }
          closable={false}
          wrapClassName="gx-customized-modal vertical-center-modal"
          visible={this.state.customerImportDlgVisible}
          onCancel={this.onCancel.bind(this)}
          width={
            this.state.width >= TAB_SIZE
              ? 830
              : this.state.width >= MOBILE_SIZE
              ? 710
              : this.state.width - 40
          }
        >
          <ImportCustomerDlg
            onCancel={this.onCancel.bind(this)}
            onSave={this.onSaveCustomer.bind(this)}
          />
        </Modal>
      </div>
    );
  }
}

export default injectIntl(Step1_1);
