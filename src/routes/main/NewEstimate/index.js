import React, { Component } from "react";
import { Button, Select, Avatar, Tag, Col, Row, Steps } from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import Step1_1 from "./Step1/step1_1";
import Step1_2 from "./Step1/step1_2";
import AddEstimateItem from "./AddEstimateItem";
import { global_jobs } from "../Dispatch/data";
import ScheduleStep from "./ScheduleStep";
import {
  switchLanguage,
  updateWindowWidth,
  setCurrentPage
} from "../../../appRedux/actions/Setting";
import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_MINI_SIDEBAR,
  TAB_SIZE,
  MOBILE_SIZE
} from "../../../constants/ThemeSetting";
import { connect } from "react-redux";

const { Step } = Steps;

const steps = [
  {
    step: 1,
    title: "Choose customer",
    content: <Step1_1 />
  },
  {
    step: 2,
    title: "Line items",
    content: <AddEstimateItem />
  },
  {
    step: 3,
    title: "Schedule",
    content: <ScheduleStep />
  }
];

class NewEstimate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0
    };
  }

  next() {
    const current = this.state.current + 1;
    console.log(current);
    if (current === 3) {
      this.props.history.push("/estimates/detail");
    }
    this.setState({ current });
  }
  onChange = current => {
    console.log("onChange:", current);
    if (current <= this.state.current) this.setState({ current });
  };

  componentDidMount() {
    this.props.updateWindowWidth(window.innerWidth);
    window.addEventListener("resize", () => {
      this.props.updateWindowWidth(window.innerWidth);
    });
  }

  render() {
    const {
      locale,
      width,
      navCollapsed,
      navStyle,
      pathname,
      currentPage,
      setCurrentPage,
      intl: { formatMessage }
    } = this.props;
    const { current } = this.state;
    const SCREEN_MD_MAX = 768;
    return (
      <div className="gx-main-content-wrapper gx-flex-column gx-h-100">
        <div className="gx-dispatch-topmenu gx-addjob-topbar">
          <h3 className="gx-addjob-top-title">Add new estimate</h3>
          <div className="gx-addjob-steps">
            <Steps current={current} onChange={this.onChange}>
              {steps.map((item, index) => (
                <Step key={index} title={item.title} />
              ))}
            </Steps>
          </div>
          {width > SCREEN_MD_MAX && (
            <div className="gx-addjob-top-btngroup">
              <Button className="gx-nav-btn gx-nav-customer-btn gx-mb-0 gx-addjob-save-btn">
                Save without scheduling
              </Button>
              <Button
                className="gx-nav-btn gx-nav-customer-btn gx-mb-0 gx-addjob-next-btn"
                onClick={() => this.next()}
              >
                {this.state.current < 2 ? "Next" : "Save"}
              </Button>
            </div>
          )}
        </div>
        {width <= SCREEN_MD_MAX && (
          <div className="gx-addjob-top-btngroup-mobile">
            <Button className="gx-nav-btn gx-nav-customer-btn gx-mb-0 gx-addjob-save-btn">
              Save without scheduling
            </Button>
            <Button
              className="gx-nav-btn gx-nav-customer-btn gx-mb-0 gx-addjob-next-btn"
              onClick={() => this.next()}
            >
              {this.state.current < 2 ? "Next" : "Save"}
            </Button>
          </div>
        )}
        <div className="steps-content-wrapper gx-h-100">
          <div className="steps-content gx-profile-detail-panel gx-h-100 gx-flex-row gx-align-items-center gx-justify-content-center">
            {steps[current].content}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ settings }) => {
  const {
    locale,
    navStyle,
    navCollapsed,
    width,
    pathname,
    currentPage
  } = settings;
  return { locale, navStyle, navCollapsed, width, pathname, currentPage };
};

export default connect(mapStateToProps, {
  switchLanguage,
  setCurrentPage,
  updateWindowWidth
})(injectIntl(NewEstimate));
