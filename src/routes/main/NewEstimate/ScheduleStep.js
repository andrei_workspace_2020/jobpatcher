import React, { Component } from "react";
import { injectIntl } from "react-intl";
import { Button, Select, Avatar, Tag, Row, Col } from "antd";
import TopToolBar from "./Step3/TopToolBar";
import EmployeeWorkHours from "./Step3/employee-work-hours";

import { global_jobs } from "../Dispatch/data";

const Option = Select.Option;

class ScheduleStep extends Component {
  state = {
    duration: "week",
    scheduled: "scheduled",
    jobs: [],
    searchText: "",
    screenWidth: 0
  };

  constructor(props, context) {
    super(props, context);

    this.renderMainContent = this.renderMainContent.bind(this);
    this.changeJobs = this.changeJobs.bind(this);
  }

  componentDidMount() {
    this.changeJobs(this.state.scheduled);
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }
  componentWillUnmount() {
    window.addEventListener("resize", this.updateDimensions);
  }
  updateDimensions = () => {
    this.setState({
      screenWidth: window.innerWidth
    });
  };
  changeJobs(scheduled) {
    if (scheduled === "scheduled") {
      this.setState({
        jobs: this.getScheduledJobs(global_jobs)
      });
    } else {
      this.setState({
        jobs: this.getUnscheduledJobs(global_jobs)
      });
    }
  }

  getScheduledJobs(data) {
    return data.filter(job => job.status !== "unscheduled");
  }

  getUnscheduledJobs(data) {
    return data.filter(job => job.status === "unscheduled");
  }

  onChangeDuration(duration) {
    this.setState({ duration: duration });
  }

  onChangeScheduled(scheduled) {
    this.setState({ scheduled: scheduled });
    this.changeJobs(scheduled);
  }

  updateSearchJobs = evt => {
    this.setState({
      searchText: evt.target.value
    });
  };

  renderMainContent() {
    const { jobs, scheduled, duration } = this.state;
    const {
      intl: { formatMessage }
    } = this.props;
    return (
      <div>
        <TopToolBar
          duration={duration}
          scheduled={scheduled}
          onChangeDuration={this.onChangeDuration.bind(this)}
          onChangeScheduled={this.onChangeScheduled.bind(this)}
        />
        <EmployeeWorkHours />
      </div>
    );
  }

  render() {
    return (
      <div className="gx-main-content-container gx-addjob-step3-schedule gx-p-20 gx-flex-row gx-align-items-center gx-h-100 gx-m-auto">
        <Row
          className="gx-app-module gx-dispatch-module gx-pt-0"
          style={{ width: this.state.screenWidth }}
        >
          <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
            {this.renderMainContent()}
          </Col>
        </Row>
      </div>
    );
  }
}
export default injectIntl(ScheduleStep);
