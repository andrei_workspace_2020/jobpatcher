import React, { Component } from "react";
import { Popover, Button, Select, Input } from "antd";
import Widget from "components/Widget";
import PrivateNoteWidget from "components/AddEstimate/PrivateNoteWidget";
import UploadFileWidget from "components/AddEstimate/UploadFileWidget";
import JobSubPane from "components/AddEstimate/JobSubPane";

const { Option } = Select;

class AddEstimateItem extends Component {
  state = {
    services: [
      {
        id: 1,
        category: "Steel",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 30.0,
        quantity: 0
      },
      {
        id: 2,
        category: "Steel",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 3,
        category: "Copper",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 4,
        category: "Copper",
        name: "Valve",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      },
      {
        id: 5,
        category: "Plastic",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 6,
        category: "Plastic",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 7,
        category: "Plastic",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      }
    ],
    materials: [
      {
        id: 1,
        category: "Steel",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 30.0,
        quantity: 0
      },
      {
        id: 2,
        category: "Steel",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 3,
        category: "Copper",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 4,
        category: "Copper",
        name: "Valve",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      },
      {
        id: 5,
        category: "Plastic",
        name: "Adaptor",
        image: "material_adaptor.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 50.0,
        quantity: 0
      },
      {
        id: 6,
        category: "Plastic",
        name: "Plug and cap",
        image: "",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 80.0,
        quantity: 0
      },
      {
        id: 7,
        category: "Plastic",
        name: "Barb",
        image: "material_barb.png",
        desc:
          "Creature every herb open abundantly had this, It creepeth moving is. You their days there",
        tags: ["EL 423836 XV5", "Taxable"],
        price: 120.0,
        quantity: 0
      }
    ],
    service_categories: ["All services", "Steel", "Copper", "Plastic"],
    material_categories: ["All materials", "Steel", "Copper", "Plastic"]
  };
  constructor(props, context) {
    super(props, context);
  }

  componentDidUpdate() {
    console.log("parent update", this.state.services);
  }
  addItems = (kind, items) => {
    if (kind == "Service") this.setState({ services: items });
    else if (kind == "Material") this.setState({ materials: items });
  };

  render() {
    return (
      <div className="gx-main-content-container gx-addjob-step2-addjobitem-main-content gx-p-20 gx-flex-row gx-align-items-center gx-m-auto gx-h-100">
        <div className="gx-m-auto">
          <div className="gx-addjob-step2-panel-content-header">
            <div style={{ paddingLeft: "15px" }}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <h3 style={{ paddingBottom: 20 }}>Add line items</h3>
              </div>
            </div>
          </div>
          <div className="gx-addjob-step2-panel-content-body">
            <div className="gx-addjob-step2-left-panel">
              <div className="gx-addjob-step2-addjobitem-total-panel">
                <JobSubPane
                  kind="Service"
                  items={[...this.state.services]}
                  categories={this.state.service_categories}
                  addItems={this.addItems}
                />
                <JobSubPane
                  kind="Material"
                  items={[...this.state.materials]}
                  categories={this.state.material_categories}
                  addItems={this.addItems}
                />
                <Widget
                  styleName={`gx-card-full gx-price-box-widget gx-addjob-step2-subpane gx-addjob-step2-subpane-pricebox`}
                >
                  <div className="price-box">
                    <div className="price-detail">
                      <div className="price-label">Subtotal</div>
                      <div className="price-value">$600.00</div>
                    </div>
                    <div className="price-detail">
                      <div className="price-label">TPS/TVQ QC@14.975%</div>
                      <div className="price-value">$89.85</div>
                    </div>
                    <div className="price-detail">
                      <Popover
                        overlayClassName="gx-popover-add-discount gx-estimate-popover"
                        placement="top"
                        trigger="hover"
                        content={
                          <div className="gx-popover-estimate-content gx-popover-new-estimate-delete-content">
                            <div className="gx-popover-title gx-flex-row gx-align-items-center">
                              <img
                                className="gx-mr-10"
                                src={require("assets/images/discount.png")}
                              />
                              <div className="gx-flex-column">
                                <span className="gx-fs-lg gx-font-weight-bold">
                                  Add discount
                                </span>
                                <span className="gx-fs-13-20 gx-font-weight-medium gx-text-grey">
                                  Please select an option
                                </span>
                              </div>
                            </div>
                            <div className="gx-flex-row gx-align-items-center gx-flex-nowrap gx-mt-10">
                              <div className="gx-w-100 gx-mr-10">
                                <Select
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  value="Percent"
                                >
                                  <Option value="Percent">Percent</Option>
                                </Select>
                              </div>
                              <div className="gx-w-100">
                                <Input defaultValue="5"></Input>
                              </div>
                            </div>
                            <div
                              className="gx-popover-action"
                              style={{ marginTop: "15px" }}
                            >
                              <Button
                                type="default"
                                className="gx-btn-cancel gx-mr-10"
                              >
                                Cancel
                              </Button>
                              <Button type="primary" className="gx-btn-confirm">
                                Confirm
                              </Button>
                            </div>
                          </div>
                        }
                      >
                        <div className="price-label price-label-highlight">
                          Add discount %
                        </div>
                      </Popover>
                      <div className="price-value price-value-disable">
                        $00.00
                      </div>
                    </div>
                    <div className="price-detail">
                      <div className="price-label total-price-label">
                        Total amount
                      </div>
                      <div className="price-value total-price-value">
                        $689.85
                      </div>
                    </div>
                  </div>
                </Widget>
              </div>
            </div>
            <div className="gx-addjob-step2-right-panel">
              <div
                style={{ paddingBottom: 20 }}
                className="gx-step2-private-note-container"
              >
                <PrivateNoteWidget style={{ padding: 0 }} />
              </div>
              <div
                style={{ paddingBottom: 20 }}
                className="gx-step2-upload-container"
              >
                <UploadFileWidget style={{ padding: 0 }} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default AddEstimateItem;
