import React, { Component } from "react";
import { Button, Checkbox, Select, Tag, Popover, Modal, Row, Col } from "antd";
import { injectIntl } from "react-intl";
import SearchBox from "components/SearchBox";
import { changeDateMonthYearHourMinuteSecondFormatSS } from "util/DateTime";
import FlatfileImporter from "flatfile-csv-importer";

import Auxiliary from "util/Auxiliary";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import { Link } from "react-router-dom";
import { customId } from "custom-id";
import ButtonGroup from "antd/lib/button/button-group";
import EditCustomerDlg from "./CustomerDialog/EditCustomerDlg";
import ImportCustomerDlg from "./CustomerDialog/ImportCustomerDlg";

import { Notification } from "components/Notification";

import { TAB_SIZE, MOBILE_SIZE } from "../../../constants/ThemeSetting";
import { connect } from "react-redux";
import {
  getAllCustomers,
  addCustomer,
  updateCustomer,
  deleteCustomer
} from "appRedux/actions/Customers";

const { Option } = Select;
const FLATFILE_LICENSE_KEY = "88658037-b0b0-4db1-9b77-dd7b0f0adb3b";

const default_customer = {
  id: "",
  avatar: "",
  title: "",
  name: "",
  first_name: "",
  last_name: "",
  email: "",
  phone: {
    mobile: "",
    primary_phone: "",
    work: ""
  },
  birthday: "01/01/1974",
  balance: 0.0,
  status: "Active",
  starred: false,
  customer_info: {
    customer_type: "",
    lead_source: "",
    creation_date: "",
    created_by: "" //later it should be user id
  },
  address: [],
  communication: {
    name_format: [],
    email_communication: true,
    sms_communication: false,
    preferred_language: "English - US"
  },
  notes: [],
  jobs: [],
  estimates: [],
  invoices: [],
  files: [],
  financial_activities: {
    overdue_invoices: [],
    non_invoiced_jobs: [],
    not_due_yet: []
  },
  financial_info: {
    automatic_invoice: "",
    payment_term: "",
    bill_to: "",
    taxable: false,
    discount_rate: ""
  },
  billing_info: {
    billing_name: "",
    street: "",
    unit: "",
    city: "",
    state: "",
    zipcode: "",
    invoice_to: ""
  },
  payment_history: []
};

class Customers extends Component {
  state = {
    searchText: "",
    filterValue: "",
    checkedList: [],
    customerNewDlgVisible: false,
    customerEditDlgVisible: false,
    customerImportDlgVisible: false,
    customer: {},
    isEditCustomerDlgVisible: [],
    isEditCustomerDlgMobileVisible: [],
    isImportExportPopoverVisible: false
  };

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.getAllCustomers();
    let array = [];
    for (let i = 0; i < this.props.customers.length; i++) array[i] = false;
    this.setState({ isEditCustomerDlgVisible: [...array] });
    this.setState({ isEditCustomerDlgMobileVisible: [...array] });
  }
  componentDidUpdate(prevProps) {}
  onChangeAllCheck = checked => {
    if (checked) {
      let status = [];
      this.props.customers.map(customer => status.push(customer.id));
      this.setState({ checkedList: status });
    } else {
      let status = [];
      this.setState({ checkedList: [...status] });
    }
  };

  showNewCustomerDlg = () => {
    this.setState({
      customerNewDlgVisible: true,
      customerImportDlgVisible: false
    });
  };

  showEditCustomerDlg = customer => {
    this.setState({ customer: customer, customerEditDlgVisible: true });
  };

  showImportCustomerDlg = () => {
    this.setState({
      customerNewDlgVisible: false,
      customerImportDlgVisible: true
    });
  };
  isCustomerExist = customer => {
    let temp = this.props.customers.filter(
      cus => customer.email !== "" && cus.email === customer.email
    );
    if (temp.length > 0) return true;
    else return false;
  };
  onSaveCustomer = customer => {
    if (customer.id === "") {
      let exist = this.isCustomerExist(customer);
      if (!exist) {
        customer.id = parseInt(Math.random() * 100000000).toString();
        customer.customer_info.creation_date = changeDateMonthYearHourMinuteSecondFormatSS(
          new Date()
        );
        if (customer.title != "") {
          customer.communication.name_format[0] = customer.title;
          customer.communication.name_format[1] = "last_name";
        }
        let owner_info = JSON.parse(localStorage.getItem("user_info"));
        customer.customer_info.created_by =
          owner_info.first_name + " " + owner_info.last_name;
        // this.setState({ customers: [...this.state.customers, customer] });
        this.setState({ customerNewDlgVisible: false });
        this.props.addCustomer(customer);
        this.props.history.push("/customers/profile/" + customer.id);
      } else alert("Customer is already exist. Please try again!");
    } else {
      let exist = this.isCustomerExist(customer.email);
      if (!exist) {
        customer.customer_info.updated_date = changeDateMonthYearHourMinuteSecondFormatSS(
          new Date()
        );
        this.props.updateCustomer(customer.id, customer);
        this.setState({ customerEditDlgVisible: false });
        this.props.history.push("/customers/profile/" + customer.id);
      } else alert("Customer is already exist. Please try again!");
    }
  };

  onUpdateCustomer = () => {
    this.setState({ customerEditDlgVisible: false });
  };

  onCancel = () => {
    this.setState({
      customerNewDlgVisible: false,
      customerEditDlgVisible: false,
      customerImportDlgVisible: false
    });
  };
  onChangeCustomerCheck = customer_id => {
    if (this.state.checkedList.includes(customer_id)) {
      let temp = this.state.checkedList;
      let index = temp.indexOf(customer_id);
      temp.splice(index, 1);
      this.setState({ checkedList: temp });
    } else {
      let temp = this.state.checkedList;
      temp.push(customer_id);
      this.setState({ checkedList: temp });
    }
  };
  notAvailableWhenBlank(item) {
    if (item === "" || item == " ") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="customer.profile.notavailable" />
        </div>
      );
    }
    return item;
  }
  onChangeCustomerStar = customer_id => {
    let temp = null;
    this.props.customers.map(customer => {
      if (customer.id == customer_id) temp = customer;
    });
    temp.starred = !temp.starred;
    temp.customer_info.updated_date = changeDateMonthYearHourMinuteSecondFormatSS(
      new Date()
    );
    this.props.updateCustomer(customer_id, temp);
  };

  updateSearchCustomers = evt => {
    this.setState({
      searchText: evt.target.value
    });
  };
  onFilterChange = value => {
    this.setState({ filterValue: value });
  };
  onDeleteCustomers = () => {
    this.state.checkedList.map(id => this.props.deleteCustomer(id));
    this.setState({ checkedList: [] });
  };
  onDeleteCustomer = id => {
    this.props.deleteCustomer(id);
    this.setState({ checkedList: [] });
  };
  onFavouriteCustomers = () => {
    let temp = this.props.customers.filter(customer =>
      this.state.checkedList.includes(customer.id)
    );
    temp.map(customer => {
      customer.starred = !customer.starred;
      customer.customer_info.updated_date = changeDateMonthYearHourMinuteSecondFormatSS(
        new Date()
      );
      this.props.updateCustomer(customer.id, customer);
    });
    this.onChangeAllCheck(false);
  };
  onEditCustomerDlgVisibleChange = (index, visible) => {
    let temp = this.state.isEditCustomerDlgVisible;
    temp[index] = visible;
    this.setState({ isEditCustomerDlgVisible: temp });
  };
  onEditCustomerDlgMobileVisibleChange = (index, visible) => {
    let temp = this.state.isEditCustomerDlgMobileVisible;
    temp[index] = visible;
    this.setState({ isEditCustomerDlgMobileVisible: temp });
  };
  onImportExportPopoverVisibleChange = visible => {
    this.setState({ isImportExportPopoverVisible: visible });
  };
  onImportCustomers = async () => {
    try {
      const options = {
        fields: [
          {
            label: "First name",
            key: "first_name",
            isRequired: true,
            validators: [
              {
                validate: "required_with",
                fields: ["last_name"]
              }
            ]
          },
          {
            label: "Last name",
            key: "last_name",
            isRequired: true,
            validators: [
              {
                validate: "required_with",
                fields: ["first_name"]
              }
            ]
          },
          {
            label: "Business name",
            key: "company_name",
            isRequired: true
          },
          {
            label: "Phone number",
            key: "phone_number"
            // validators: [
            //   {
            //     validate: "regex_matches",
            //     regex: /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/,
            //     error: "This is invalid phone number"
            //   }
            // ]
          },
          {
            label: "Email",
            key: "email"
            // validators: [
            //   {
            //     validate: "regex_matches",
            //     regex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            //     error: "This is invalid email address"
            //   }
            // ]
          }
        ],
        type: "customer",
        allowCustom: true
      };
      const importer = new FlatfileImporter(FLATFILE_LICENSE_KEY, options);
      const results = await importer.requestDataFromUser();
      importer.displayLoader();
      await this.importCustomersData(results.data);
      importer.displaySuccess();
      this.onImportExportPopoverVisibleChange(false);
    } catch (e) {
      console.log(e);
    }
  };
  importCustomersData = data => {
    let customer = {};
    if (data.length > 0) {
      for (let i = 0; i < data.length; i++) {
        let exist = this.isCustomerExist(data[i]);
        if (!exist) {
          customer = { ...default_customer };
          customer.first_name = data[i].first_name;
          customer.last_name = data[i].last_name;
          customer.email = data[i].email;
          //customer.company_name = data[i].company_name;
          let temp = customer.phone;
          temp.primary_phone = data[i].phone_number;
          customer.phone = { ...temp };
          customer.id = parseInt(Math.random() * 100000000).toString();
          customer.customer_info.creation_date = changeDateMonthYearHourMinuteSecondFormatSS(
            new Date()
          );
          let owner_info = JSON.parse(localStorage.getItem("user_info"));
          customer.customer_info.created_by =
            owner_info.first_name + " " + owner_info.last_name;
          this.props.addCustomer(customer);
        }
      }
    }
  };
  render() {
    const {
      width,
      intl: { formatMessage }
    } = this.props;
    return (
      <Auxiliary>
        <div className="gx-main-content-container">
          <div className="gx-sub-title-container gx-flex-row gx-flex-nowrap gx-w-100 gx-justify-content-between gx-align-items-center gx-ss-customers-top-toolbar">
            <div className="gx-sub-title">
              {this.props.customers.length > 0 ? (
                this.state.checkedList.length == 0 ? (
                  <h3>
                    {
                      this.props.customers
                        .filter(
                          customer =>
                            (customer.first_name + " " + customer.last_name)
                              .toLowerCase()
                              .includes(this.state.searchText.toLowerCase()) ||
                            customer.company_name
                              .toLowerCase()
                              .includes(this.state.searchText.toLowerCase()) ||
                            customer.email
                              .toLowerCase()
                              .includes(this.state.searchText.toLowerCase()) ||
                            customer.phone.primary_phone
                              .toLowerCase()
                              .includes(this.state.searchText.toLowerCase())
                        )
                        .filter(
                          customer =>
                            customer.status == this.state.filterValue ||
                            this.state.filterValue == ""
                        ).length
                    }{" "}
                    <IntlMessages id="customers" />
                  </h3>
                ) : this.state.checkedList.length == 1 ? (
                  <h3>{this.state.checkedList.length} customer selected</h3>
                ) : (
                  <h3>{this.state.checkedList.length} customers selected</h3>
                )
              ) : (
                <h3>Customers</h3>
              )}
            </div>
            {this.state.checkedList.length == 0 ? (
              <div className="gx-flex-row gx-flex-nowrap gx-ss-tool">
                {/* <span>
                  <SearchBox
                    styleName="gx-lt-icon-search-bar-lg gx-customer-search"
                    placeholder={formatMessage({
                      id: "customer.search.placeholder"
                    })}
                    onChange={this.updateSearchCustomers.bind(this)}
                    value={this.state.searchText}
                  />
                </span> */}
                {/* <span className="gx-d-none gx-d-md-block gx-ml-10"> */}
                <span className="gx-d-md-block gx-mr-10">
                  <Button
                    className="gx-nav-btn gx-nav-customer-btn gx-mb-0 gx-w-100"
                    type="primary"
                    onClick={this.showNewCustomerDlg}
                  >
                    <div className="gx-div-align-center gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-center">
                      <i className="material-icons gx-fs-xl gx-mr-2">add</i>
                      <IntlMessages id="customers.new_customer" />
                    </div>
                  </Button>
                </span>
                <span className="gx-d-md-block">
                  <Popover
                    overlayClassName="gx-popover-customers-more-action"
                    placement="bottomLeft"
                    visible={this.state.isImportExportPopoverVisible}
                    onVisibleChange={visible =>
                      this.onImportExportPopoverVisibleChange(visible)
                    }
                    trigger="click"
                    content={
                      <div>
                        <div
                          className="gx-menuitem"
                          onClick={this.onImportCustomers}
                        >
                          <i className="material-icons">system_update_alt</i>
                          <IntlMessages id="customer.list.pop.importcustomers" />
                        </div>
                        <div
                          className="gx-menuitem"
                          onClick={this.onExportCustomers}
                        >
                          <i className="material-icons">launch</i>
                          <IntlMessages id="customer.list.pop.exportcustomers" />
                        </div>
                      </div>
                    }
                  >
                    <Button
                      className="gx-customized-button gx-mb-0 gx-w-100"
                      onClick={() =>
                        this.onImportExportPopoverVisibleChange(true)
                      }
                    >
                      <div className="gx-div-align-center gx-w-100 gx-flex-row gx-align-items-center gx-justify-content-center">
                        <i className="material-icons gx-fs-xl gx-mr-2">
                          more_horiz
                        </i>
                        <IntlMessages id="customers.more_actions" />
                      </div>
                    </Button>
                  </Popover>
                </span>
              </div>
            ) : (
              <div className="gx-flex-row gx-align-items-center">
                <Button
                  className="gx-btn-customers-action gx-customized-button gx-flex-row gx-align-items-center"
                  onClick={this.onDeleteCustomers}
                >
                  <i className="material-icons gx-mr-10 gx-icon-action">
                    delete
                  </i>
                  <span>Delete</span>
                </Button>
                <Button
                  className="gx-btn-customers-action gx-customized-button gx-flex-row gx-align-items-center"
                  onClick={this.onFavouriteCustomers}
                >
                  <i className="material-icons gx-mr-10 gx-icon-action">star</i>
                  <span>Important</span>
                </Button>
              </div>
            )}
          </div>

          <Widget styleName="gx-card-full gx-customers-widget gx-mb-0 gx-no-bottom-radius">
            {this.props.customers.length > 0 ? (
              <div>
                <div className="gx-panel-title-bar gx-justify-content-center gx-p-0">
                  <Row className="gx-w-100" gutter={10}>
                    <Col
                      xl={2}
                      lg={2}
                      md={4}
                      sm={0}
                      xs={0}
                      className="gutter-row"
                    >
                      <div className="gx-customer-th gx-customer-check-main">
                        <Checkbox
                          checked={
                            this.state.checkedList.length ==
                              this.props.customers.length &&
                            this.props.customers.length != 0
                              ? true
                              : false
                          }
                          onChange={e =>
                            this.onChangeAllCheck(e.target.checked)
                          }
                        />
                      </div>
                    </Col>
                    <Col
                      xl={7}
                      lg={10}
                      md={12}
                      sm={24}
                      xs={24}
                      className="gutter-row"
                    >
                      <div className="gx-customer-th gx-th-name gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <span className="gx-mr-10">
                          <SearchBox
                            focus={true}
                            styleName="gx-lt-icon-search-bar-lg gx-customer-search"
                            placeholder={formatMessage({
                              id: "customers.search.placeholder"
                            })}
                            onChange={this.updateSearchCustomers.bind(this)}
                            value={this.state.searchText}
                          />
                        </span>
                        <span style={{ width: "150px" }}>
                          <Select
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            value={this.state.filterValue}
                            onChange={this.onFilterChange}
                          >
                            <Option value="">All customers</Option>
                            <Option value="Active">Active</Option>
                            <Option value="Idle">Idle</Option>
                            <Option value="Prospect">Prospect</Option>
                            <Option value="Recurring">Recurring</Option>
                          </Select>
                        </span>
                      </div>
                    </Col>
                    <Col
                      xl={3}
                      lg={6}
                      md={0}
                      sm={0}
                      xs={0}
                      className="gutter-row"
                    >
                      <div className="gx-customer-th gx-th-phone">
                        <IntlMessages id="customers.label.phone" />
                      </div>
                    </Col>
                    <Col
                      xl={3}
                      lg={0}
                      md={0}
                      sm={0}
                      xs={0}
                      className="gutter-row"
                    >
                      <div className="gx-customer-th gx-th-email">
                        <IntlMessages id="customers.label.email" />
                      </div>
                    </Col>
                    <Col
                      xl={3}
                      lg={0}
                      md={0}
                      sm={0}
                      xs={0}
                      className="gutter-row"
                    >
                      <div className="gx-customer-th gx-th-balance">
                        <IntlMessages id="customers.label.balance" />
                      </div>
                    </Col>
                    <Col
                      xl={6}
                      lg={6}
                      md={8}
                      sm={0}
                      xs={0}
                      className="gutter-row"
                    >
                      <div className="gx-customer-th gx-th-action"></div>
                    </Col>
                  </Row>
                </div>
                <div
                  className="gx-panel-content gx-p-0"
                  style={{ marginTop: "60px" }}
                >
                  <div className="gx-panel-content-scroll">
                    {this.props.customers
                      .filter(
                        customer =>
                          (customer.first_name + " " + customer.last_name)
                            .toLowerCase()
                            .includes(this.state.searchText.toLowerCase()) ||
                          customer.company_name
                            .toLowerCase()
                            .includes(this.state.searchText.toLowerCase()) ||
                          customer.email
                            .toLowerCase()
                            .includes(this.state.searchText.toLowerCase()) ||
                          customer.phone.primary_phone
                            .toLowerCase()
                            .includes(this.state.searchText.toLowerCase())
                      )
                      .filter(
                        customer =>
                          customer.status == this.state.filterValue ||
                          this.state.filterValue == ""
                      )
                      .map((customer, index) => (
                        <div
                          key={index}
                          className={`gx-customer-row ${
                            this.state.checkedList.length > 0 &&
                            this.state.checkedList.includes(customer.id)
                              ? "gx-customer-selected"
                              : ""
                          }`}
                        >
                          <Row className="gx-w-100" gutter={10}>
                            <Col
                              xl={2}
                              lg={2}
                              md={4}
                              sm={0}
                              xs={0}
                              className="gutter-row"
                            >
                              <div className="gx-customer-td">
                                <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                                  <Checkbox
                                    checked={
                                      this.state.checkedList.length > 0 &&
                                      this.state.checkedList.includes(
                                        customer.id
                                      )
                                    }
                                    onClick={() => {
                                      this.onChangeCustomerCheck(customer.id);
                                    }}
                                  />
                                  <i
                                    className="material-icons gx-ml-10"
                                    style={{
                                      cursor: "pointer",
                                      color: customer.starred
                                        ? "#f7c43d"
                                        : "#dcdde5"
                                    }}
                                    onClick={() => {
                                      this.onChangeCustomerStar(customer.id);
                                    }}
                                  >
                                    {customer.starred ? "star" : "star_border"}
                                  </i>
                                </div>
                              </div>
                            </Col>
                            <Col
                              xl={7}
                              lg={10}
                              md={12}
                              sm={24}
                              xs={24}
                              className="gutter-row"
                            >
                              <div className="gx-customer-td gx-td-name gx-justify-content-between">
                                <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                                  <i
                                    className="material-icons gx-mr-20"
                                    style={{
                                      cursor: "pointer",
                                      color: customer.starred
                                        ? "#f7c43d"
                                        : "#dcdde5"
                                    }}
                                    onClick={() => {
                                      this.onChangeCustomerStar(customer.id);
                                    }}
                                  >
                                    {customer.starred ? "star" : "star_border"}
                                  </i>
                                  <div className="gx-flex-0 gx-main-avatar gx-size-32 gx-mr-12">
                                    <i
                                      className="material-icons gx-w-100 gx-text-center"
                                      style={{
                                        color: "#fbfbfd",
                                        fontSize: "20px"
                                      }}
                                    >
                                      {customer.avatar == ""
                                        ? "person"
                                        : customer.avatar}
                                    </i>
                                  </div>
                                  <Link
                                    to={`/customers/profile/${customer.id}`}
                                    className="gx-text-medium gx-fs-13-20 gx-mr-12"
                                  >
                                    {customer.company_name !== ""
                                      ? this.notAvailableWhenBlank(
                                          customer.company_name
                                        )
                                      : this.notAvailableWhenBlank(
                                          customer.first_name +
                                            " " +
                                            customer.last_name
                                        )}
                                  </Link>
                                  <Tag className="gx-customer-list-tag">
                                    {customer.status}
                                  </Tag>
                                  {customer.address.length > 0 ? (
                                    <Tag className="gx-customer-list-tag">
                                      {customer.address[0].city}
                                    </Tag>
                                  ) : (
                                    <span />
                                  )}
                                </div>
                                <Popover
                                  overlayClassName="gx-customer-table-more-popover"
                                  content={
                                    <div>
                                      <div
                                        className="gx-menuitem"
                                        onClick={() => {
                                          this.showEditCustomerDlg(customer);
                                          this.onEditCustomerDlgMobileVisibleChange(
                                            index,
                                            false
                                          );
                                        }}
                                      >
                                        <i className="material-icons gx-fs-xl gx-mr-10">
                                          edit
                                        </i>
                                        <div className="gx-fs-13-20 gx-font-weight-medium">
                                          <IntlMessages id="customer.list.pop.editcustomer" />
                                        </div>
                                      </div>
                                      <div
                                        className="gx-menuitem"
                                        onClick={() => {
                                          this.onDeleteCustomer(customer.id);
                                          this.onEditCustomerDlgMobileVisibleChange(
                                            index,
                                            false
                                          );
                                        }}
                                      >
                                        <i className="material-icons gx-fs-xl gx-mr-10">
                                          delete
                                        </i>
                                        <div className="gx-fs-13-20 gx-font-weight-medium">
                                          <IntlMessages id="customer.list.pop.deletecustomer" />
                                        </div>
                                      </div>
                                    </div>
                                  }
                                  visible={
                                    this.state.isEditCustomerDlgMobileVisible[
                                      index
                                    ]
                                  }
                                  onVisibleChange={visible =>
                                    this.onEditCustomerDlgMobileVisibleChange(
                                      index,
                                      visible
                                    )
                                  }
                                  placement="bottomRight"
                                  trigger="click"
                                >
                                  <i
                                    className="material-icons gx-icon-action"
                                    onClick={() =>
                                      this.onEditCustomerDlgMobileVisibleChange(
                                        index,
                                        true
                                      )
                                    }
                                  >
                                    more_vert
                                  </i>
                                </Popover>
                              </div>
                            </Col>
                            <Col
                              xl={3}
                              lg={6}
                              md={0}
                              sm={0}
                              xs={0}
                              className="gutter-row"
                            >
                              <div className="gx-customer-td gx-th-phone">
                                {this.notAvailableWhenBlank(
                                  customer.phone.primary_phone
                                )}
                              </div>
                            </Col>
                            <Col
                              xl={3}
                              lg={0}
                              md={0}
                              sm={0}
                              xs={0}
                              className="gutter-row"
                            >
                              <div className="gx-customer-td gx-th-email">
                                {this.notAvailableWhenBlank(customer.email)}
                              </div>
                            </Col>
                            <Col
                              xl={3}
                              lg={0}
                              md={0}
                              sm={0}
                              xs={0}
                              className="gutter-row"
                            >
                              <div className="gx-customer-td gx-th-balance">
                                {"$" + parseFloat(customer.balance).toFixed(2)}
                              </div>
                            </Col>
                            <Col
                              xl={6}
                              lg={6}
                              md={8}
                              sm={0}
                              xs={0}
                              className="gutter-row"
                            >
                              <div className="gx-customer-td gx-customer-action">
                                <div className="gx-customer-action-desktop">
                                  <ButtonGroup className="gx-customer-list-buttongroup gx-flex-row">
                                    {/* <Button onClick={() => { this.showNotification() }}><i className="material-icons">sms</i></Button> */}
                                    <Popover
                                      overlayClassName="gx-popover-background-blue gx-customer-profile-popover gx-customer-table-estimate-popover"
                                      placement="bottom"
                                      trigger="hover"
                                      content={
                                        <div className="content">
                                          Create an estimate for{" "}
                                          {customer.first_name +
                                            " " +
                                            customer.last_name}
                                        </div>
                                      }
                                    >
                                      <Button
                                        onClick={() => {
                                          this.showEditCustomerDlg(customer);
                                        }}
                                      >
                                        <i className="material-icons">build</i>
                                      </Button>
                                    </Popover>
                                    <Popover
                                      overlayClassName="gx-popover-background-blue gx-customer-table-estimate-popover"
                                      content={
                                        <div className="content">
                                          Create an estiamte for{" "}
                                          {customer.first_name +
                                            " " +
                                            customer.last_name}
                                        </div>
                                      }
                                      placement="bottom"
                                      trigger="hover"
                                    >
                                      <Button>
                                        <i className="material-icons">
                                          assignment_turned_in
                                        </i>
                                      </Button>
                                    </Popover>
                                    <Popover
                                      overlayClassName="gx-customer-table-more-popover"
                                      content={
                                        <div>
                                          <div
                                            className="gx-menuitem"
                                            onClick={() => {
                                              this.showEditCustomerDlg(
                                                customer
                                              );
                                              this.onEditCustomerDlgVisibleChange(
                                                index,
                                                false
                                              );
                                            }}
                                          >
                                            <i className="material-icons gx-fs-xl gx-mr-10">
                                              edit
                                            </i>
                                            <div className="gx-fs-13-20 gx-font-weight-medium">
                                              <IntlMessages id="customer.list.pop.editcustomer" />
                                            </div>
                                          </div>
                                          <div
                                            className="gx-menuitem"
                                            onClick={() => {
                                              this.onDeleteCustomer(
                                                customer.id
                                              );
                                              this.onEditCustomerDlgVisibleChange(
                                                index,
                                                false
                                              );
                                            }}
                                          >
                                            <i className="material-icons gx-fs-xl gx-mr-10">
                                              delete
                                            </i>
                                            <div className="gx-fs-13-20 gx-font-weight-medium">
                                              <IntlMessages id="customer.list.pop.deletecustomer" />
                                            </div>
                                          </div>
                                        </div>
                                      }
                                      visible={
                                        this.state.isEditCustomerDlgVisible[
                                          index
                                        ]
                                      }
                                      onVisibleChange={visible =>
                                        this.onEditCustomerDlgVisibleChange(
                                          index,
                                          visible
                                        )
                                      }
                                      placement="bottom"
                                      trigger="click"
                                    >
                                      <Button
                                        onClick={() =>
                                          this.onEditCustomerDlgVisibleChange(
                                            index,
                                            true
                                          )
                                        }
                                      >
                                        <i className="material-icons">
                                          more_horiz
                                        </i>
                                      </Button>
                                    </Popover>
                                  </ButtonGroup>
                                  <Button className="gx-customized-button gx-customized-text-button gx-ml-10">
                                    <Link
                                      to={`/customers/profile/${customer.id}`}
                                    >
                                      <IntlMessages id="customers.action.view" />
                                    </Link>
                                  </Button>
                                </div>
                                <div className="gx-customer-action-mobile">
                                  <i className="material-icons gx-pointer gx-text-grey">
                                    more_vert
                                  </i>
                                </div>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            ) : (
              <div className="gx-empty-customers-content">
                <div className="gx-flex-column gx-align-items-center gx-justify-content-center">
                  <div className="gx-flex-column gx-justify-content-center gx-align-items-center gx-mb-30">
                    <span
                      className="gx-mb-20 gx-text-center gx-font-weight-bold"
                      style={{ fontSize: "20px", color: "#1a2f52" }}
                    >
                      No customers are available, Let's add some customers!
                    </span>
                    <span className="gx-custom-span">
                      Wherein deep creepeth stars very. Hath third tree great,
                    </span>
                    <span className="gx-custom-span">
                      This were won't their air to also it.
                    </span>
                  </div>
                  <div className="gx-flex-row">
                    <Widget styleName="gx-full-card gx-card-customer gx-card-add">
                      <div className="gx-card-content gx-flex-column gx-justify-content-center gx-align-items-center">
                        <div className="gx-size-60 gx-circle gx-flex-row gx-justify-content-center gx-align-items-center gx-mb-20">
                          <img
                            src={require("assets/images/customer/hand_img.png")}
                          />
                        </div>
                        <h5 className="gx-text-center gx-mb-10">
                          ADD CUSTOMER MANUALLY
                        </h5>
                        <span className="gx-custom-span gx-fs-13-20 gx-mb-10 gx-text-center">
                          Manually enter client and their informations here.
                        </span>
                        <Button
                          type="primary"
                          onClick={this.showNewCustomerDlg}
                        >
                          Add a client
                        </Button>
                      </div>
                    </Widget>
                    <Widget styleName="gx-full-card gx-card-customer gx-card-sync">
                      <div className="gx-card-content gx-flex-column gx-justify-content-center gx-align-items-center">
                        <div className="gx-size-60 gx-circle gx-flex-row gx-justify-content-center gx-align-items-center gx-mb-20">
                          <img
                            src={require("assets/images/customer/sync_img.png")}
                          />
                        </div>
                        <h5 className="gx-text-center gx-mb-10">
                          QUICKBOOKS SYNC
                        </h5>
                        <span className="gx-custom-span gx-fs-13-20 gx-mb-10 gx-text-center">
                          Have a quickbooks online account? Sync all your
                          customers here.
                        </span>
                        <Button type="primary">Sync now</Button>
                      </div>
                    </Widget>
                    <Widget styleName="gx-full-card gx-card-customer gx-card-import">
                      <div className="gx-card-content gx-flex-column gx-justify-content-center gx-align-items-center">
                        <div className="gx-size-60 gx-circle gx-flex-row gx-justify-content-center gx-align-items-center gx-mb-20">
                          <img
                            src={require("assets/images/customer/import_img.png")}
                          />
                        </div>
                        <h5 className="gx-text-center gx-mb-10">
                          IMPORT CUSTOMERS
                        </h5>
                        <span className="gx-custom-span gx-fs-13-20 gx-mb-10 gx-text-center">
                          Are all of your customers in a Excel spreadsheet?
                          Easily import them here.
                        </span>
                        <Button type="primary" onClick={this.onImportCustomers}>
                          Import now
                        </Button>
                      </div>
                    </Widget>
                  </div>
                </div>
              </div>
            )}
          </Widget>

          <Modal
            className="gx-ss-customers-new-modal"
            title={
              <div className="gx-customized-modal-header">
                <div className="gx-customized-modal-title">
                  Add new customer
                </div>
                <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                  <Button
                    className="gx-customized-button gx-d-none gx-d-md-block notDisplayOnMobile"
                    onClick={() => {
                      this.showImportCustomerDlg();
                    }}
                  >
                    <div className="gx-div-align-center">
                      <i
                        className="material-icons gx-fs-xl gx-mr-2"
                        style={{ marginLeft: "-6px" }}
                      >
                        publish
                      </i>
                      <IntlMessages id="customer.customerdlg.importcontacts" />
                    </div>
                  </Button>
                  <Button
                    className="gx-customized-button gx-d-md-none notDisplayOnMobile"
                    onClick={() => {
                      this.showImportCustomerDlg();
                    }}
                  >
                    <div className="gx-div-align-center">
                      <i
                        className="material-icons gx-fs-xl gx-mr-2"
                        style={{ marginLeft: "-6px" }}
                      >
                        publish
                      </i>
                      <IntlMessages id="customer.customerdlg.import" />
                    </div>
                  </Button>
                  <i
                    className="material-icons gx-customized-modal-close"
                    onClick={this.onCancel.bind(this)}
                  >
                    clear
                  </i>
                </div>
              </div>
            }
            closable={false}
            wrapClassName="gx-customized-modal vertical-center-modal"
            visible={this.state.customerNewDlgVisible}
            onCancel={this.onCancel.bind(this)}
            // width={ width >= 1144 ? 1084 : width - 60 }
            width={
              (width >= 1144 && 1084) ||
              (width >= 500 && width - 60) ||
              width - 30
            }
          >
            <EditCustomerDlg
              isModalVisible={this.state.customerNewDlgVisible}
              onCancel={this.onCancel.bind(this)}
              onSave={this.onSaveCustomer.bind(this)}
            />
          </Modal>

          <Modal
            className="gx-ss-customers-new-modal"
            title={
              <div className="gx-customized-modal-header">
                <div className="gx-customized-modal-title">
                  <IntlMessages id="customer.customerdlg.editcustomer" />
                </div>
                <div>
                  <i
                    className="material-icons gx-customized-modal-close"
                    onClick={this.onCancel.bind(this)}
                  >
                    clear
                  </i>
                </div>
              </div>
            }
            closable={false}
            wrapClassName="gx-customized-modal vertical-center-modal"
            visible={this.state.customerEditDlgVisible}
            onCancel={this.onCancel.bind(this)}
            width={width >= 1144 ? 1084 : width - 60}
          >
            <EditCustomerDlg
              isModalVisible={this.state.customerEditDlgVisible}
              onCancel={this.onCancel.bind(this)}
              onSave={this.onSaveCustomer.bind(this)}
              data={this.state.customer}
            />
          </Modal>
          <Modal
            title={
              <div className="gx-customized-modal-header">
                <div className="gx-customized-modal-title">
                  <IntlMessages id="customer.customerdlg.importcustomers" />
                </div>
                <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                  <Button
                    className="gx-customized-button gx-d-none gx-d-md-block"
                    onClick={() => {
                      this.showNewCustomerDlg();
                    }}
                  >
                    <div className="gx-div-align-center">
                      <i
                        className="material-icons gx-fs-xl gx-mr-2"
                        style={{ marginLeft: "-6px" }}
                      >
                        publish
                      </i>
                      <IntlMessages id="customer.customerdlg.addmanually" />
                    </div>
                  </Button>
                  <i
                    className="material-icons gx-customized-modal-close"
                    onClick={this.onCancel.bind(this)}
                  >
                    clear
                  </i>
                </div>
              </div>
            }
            closable={false}
            wrapClassName="gx-customized-modal vertical-center-modal"
            visible={this.state.customerImportDlgVisible}
            onCancel={this.onCancel.bind(this)}
            width={
              width >= TAB_SIZE ? 830 : width >= MOBILE_SIZE ? 710 : width - 40
            }
          >
            <ImportCustomerDlg
              onCancel={this.onCancel.bind(this)}
              onSave={this.onSaveCustomer.bind(this)}
            />
          </Modal>
        </div>
      </Auxiliary>
    );
  }
}

const mapStateToProps = state => {
  const { locale, navStyle, navCollapsed, width, currentPage } = state.settings;
  const { customers, selected_customer } = state.customers;
  return {
    locale,
    navStyle,
    navCollapsed,
    width,
    currentPage,
    customers,
    selected_customer
  };
};

export default connect(mapStateToProps, {
  getAllCustomers,
  addCustomer,
  updateCustomer,
  deleteCustomer
})(injectIntl(Customers));
