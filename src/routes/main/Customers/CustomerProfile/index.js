import React, { Component } from "react";
import ProfileCard from "./ProfileCard";
import { Modal } from "antd";
import IntlMessages from "util/IntlMessages";
import CustomerJobPanel from "./CustomerJobPanel";
import AddPaymentDlg from "./AddPaymentDialog/AddPaymentDlg";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import { getCustomerById, updateCustomer } from "appRedux/actions/Customers";

class CustomerProfile extends Component {
  state = {
    // data: this.props.location.state.customer
    data: {
      id: 0,
      avatar: "",
      title: "Mr.",
      first_name: "Robert",
      last_name: "Brannon",
      email: "robert@website.com",
      phone: {
        mobile: "+1(705)-255-1111",
        primary_phone: "(613)-966-1111",
        work: ""
      },
      birthday: "01/01/1974",
      balance: 100.0,
      status: "Active",
      starred: false,
      customer_info: {
        customer_type: "Commercial",
        lead_source: "Business card",
        creation_date: "01/01/2018",
        created_by: "Peter Jonson" //later it should be user id
      },
      address: [
        {
          id: 0,
          title: "Home address",
          street: "2655 Speers Road",
          unit: "",
          city: "Oakville",
          state: "Ontario",
          zipcode: "L6J3X4",
          alarmcode: "88590",
          property_key: "",
          lat: 43.4030085,
          lng: -79.7314425
        },
        {
          id: 1,
          title: "Office address",
          street: "496 Hochelaga",
          unit: "",
          city: "Laval",
          state: "Quebec",
          zipcode: "H7P3H6",
          alarmcode: "",
          property_key:
            "Creeping fruit saying goo fourth good. Deep bring face from meat called in.",
          lat: 45.5841211,
          lng: -73.5285817
        }
      ],
      communication: {
        name_format: ["Mr.", "last_name"],
        email_communication: true,
        sms_communication: true,
        preferred_language: "English - US"
      },
      notes: [
        {
          id: 0,
          name: "Robert Brannon",
          note: "Lesser land heaven which above first.",
          datetime: "4/24/2019 15:26",
          avatar: ""
        },
        {
          id: 1,
          name: "Leana Rosebell",
          note:
            "Lesser land heaven which above first. Lesser land heaven which above first.",
          datetime: "4/22/2019 14:26",
          avatar: require("assets/images/avatar/avatar03.png")
        }
      ],
      jobs: [
        {
          id: "#3624",
          date: "29/12/2019",
          amount: 250,
          status: "Completed"
        },
        {
          id: "#3622",
          date: "29/12/2019",
          amount: 330,
          status: "Canceled"
        },
        {
          id: "#3468",
          date: "29/12/2019",
          amount: 320,
          status: "Completed"
        }
      ],
      estimates: [
        {
          id: "#3624",
          date: "29/12/2019",
          amount: 250,
          status: "Converted"
        },
        {
          id: "#3622",
          date: "29/12/2019",
          amount: 330,
          status: "Refused"
        },
        {
          id: "#3468",
          date: "29/12/2019",
          amount: 320,
          status: "Converted"
        }
      ],
      invoices: [
        {
          id: "#3624",
          date: "22/12/2019",
          due_date: "29/12/2019",
          amount: 250,
          status: "Overdue"
        },
        {
          id: "#3622",
          date: "22/12/2019",
          due_date: "29/12/2019",
          amount: 330,
          status: "Due"
        },
        {
          id: "#3468",
          date: "22/12/2019",
          due_date: "29/12/2019",
          amount: 320,
          status: "Paid"
        }
      ],
      files: [],
      financial_activities: {
        overdue_invoices: [
          {
            job: "#3452",
            invoice: 520.01,
            overdue: "6 days"
          },
          {
            job: "#3454",
            invoice: 520.01,
            overdue: "6 days"
          }
        ],
        non_invoiced_jobs: [
          {
            job: "#3457",
            description: "Invoice will be auto-generated after 4 jobs"
          }
        ],
        not_due_yet: [
          {
            job: "#3448",
            due_days: "3 days"
          }
        ]
      },
      financial_info: {
        automatic_invoice: "Weekly",
        payment_term: "Due after 7 days",
        bill_to: "",
        taxable: true,
        discount_rate: "5%"
      },
      billing_info: {
        billing_name: "Kevin Jonson",
        street: "496 Hochelaga",
        unit: "",
        city: "Laval",
        state: "Quebec",
        zipcode: "H7P3H6",
        invoice_to: "kevin@website.com",
        lat: 45.5841211,
        lng: -73.5285817
      },
      payment_history: [
        {
          job: "#5470",
          date: "28 October, 2019",
          invoice: 350.0,
          payment_method: "Stripe"
        },
        {
          job: "#5456",
          date: "23 October, 2019",
          invoice: 200.0,
          payment_method: "Cheque"
        },
        {
          job: "#5448",
          date: "20 September, 2019",
          invoice: 450.0,
          payment_method: "Cash"
        }
      ]
    },
    isCustomerAddPaymentVisible: false
  };

  constructor(props, context) {
    super(props, context);
  }
  componentDidMount() {
    let id = window.location.href.split("/")[
      window.location.href.split("/").length - 1
    ];
    localStorage.setItem("customer_ID", id);
    this.props.getCustomerById(id);
  }
  componentDidUpdate(prevProps) {
    if (localStorage.getItem("customer_ID") == undefined)
      this.props.history.push("/customers");
  }
  onChangeCustomerStar = () => {
    let temp = this.props.selected_customer;
    temp.starred = !temp.starred;
    this.props.updateCustomer(temp.id, temp);
  };
  onSubmitCustomerInfo = info => {
    let temp = this.props.selected_customer;
    temp.customer_info = info;
    this.props.updateCustomer(temp.id, temp);
  };
  onSubmitCommunication = communication => {
    let temp = this.props.selected_customer;
    temp.communication = communication;
    this.props.updateCustomer(temp.id, temp);
  };
  onSubmitAddress = async address => {
    let temp = this.props.selected_customer;
    let list = temp.address;

    if (address.id === "") {
      if (list.length > 0) address.id = list[list.length - 1].id + 1;
      else address.id = 0;
      list.push(address);
      temp.address = list;
    } else {
      let newList = list.map(item => {
        if (item.id === address.id) return address;
        else return item;
      });
      temp.address = newList;
    }
    this.props.updateCustomer(temp.id, temp);
  };
  onDeleteAddress = id => {
    let temp = this.props.selected_customer;
    let list = temp.address;
    let index = 0;
    list.map((item, i) => {
      if (item.id === id) index = i;
    });
    list.splice(index, 1);
    temp.address = list;
    this.props.updateCustomer(temp.id, temp);
  };
  onDeleteNote = id => {
    let temp = this.props.selected_customer;
    let notes = temp.notes;
    let index = 0;
    notes.map((item, i) => {
      if (item.id === id) index = i;
    });
    notes.splice(index, 1);
    temp.notes = notes;
    this.props.updateCustomer(temp.id, temp);
  };
  onSubmitNote = note => {
    let temp = this.props.selected_customer;
    let list = temp.notes;
    if (note.id === "") {
      note.id = list.length;
      list.unshift(note);
    } else {
      list.map(item => {
        if (item.id === note.id) {
          item = note;
        }
      });
    }
    temp.notes = list;
    this.props.updateCustomer(temp.id, temp);
  };
  onAddFile = file => {
    let temp = this.props.selected_customer;
    temp.files.push(file);
    this.props.updateCustomer(temp.id, temp);
  };
  onDeleteFile = id => {
    let temp = this.props.selected_customer;
    let files = temp.files;
    let index;
    files.map((item, i) => {
      if (item.id === id) index = i;
    });
    files.splice(index, 1);
    temp.files = files;
    this.props.updateCustomer(temp.id, temp);
  };
  onSubmitCustomerContact = contact => {
    let temp = this.props.selected_customer;
    temp.first_name = contact.customer_name.split(" ")[0]
      ? contact.customer_name.split(" ")[0]
      : "";
    temp.last_name = contact.customer_name.split(" ")[1]
      ? contact.customer_name.split(" ")[1]
      : "";
    temp.email = contact.email;
    temp.phone.primary_phone = contact.primary_phone;
    this.props.updateCustomer(temp.id, temp);
  };
  onSubmitFinancialInformation = info => {
    let temp = this.props.selected_customer;
    temp.financial_info = info;
    this.props.updateCustomer(temp.id, temp);
  };
  onSubmitBillingInformation = info => {
    let temp = this.props.selected_customer;
    temp.billing_info = info;
    this.props.updateCustomer(temp.id, temp);
  };
  onCancelCustomerAddPayment = () => {
    this.setState({ isCustomerAddPaymentVisible: false });
  };
  onSetCustomerAddPaymentModalVisible = visible => {
    this.setState({ isCustomerAddPaymentVisible: visible });
  };
  renderMainContent(customer) {
    return customer != null ? (
      <div className="gx-main-content-container gx-customer-profile-module-content gx-p-0">
        <ProfileCard
          data={customer}
          history={this.props.history}
          onSubmitCustomerContact={this.onSubmitCustomerContact}
          onChangeCustomerStar={this.onChangeCustomerStar}
          onSetCustomerAddPaymentModalVisible={
            this.onSetCustomerAddPaymentModalVisible
          }
        />
        <CustomerJobPanel
          data={customer}
          onSubmitCustomerInfo={this.onSubmitCustomerInfo}
          onSubmitCommunication={this.onSubmitCommunication}
          onSubmitAddress={this.onSubmitAddress}
          onDeleteAddress={this.onDeleteAddress}
          onDeleteNote={this.onDeleteNote}
          onSubmitNote={this.onSubmitNote}
          onAddFile={this.onAddFile}
          onDeleteFile={this.onDeleteFile}
          onSubmitFinancialInformation={this.onSubmitFinancialInformation}
          onSubmitBillingInformation={this.onSubmitBillingInformation}
          onSetCustomerAddPaymentModalVisible={
            this.onSetCustomerAddPaymentModalVisible
          }
        />
        <Modal
          className="gx-modal-customer-add-payment gx-ss-customers-new-modal"
          title={
            <div className="gx-customized-modal-header">
              <div className="gx-customized-modal-title">
                <IntlMessages id="customer.addpaymentdlg.content.label.add_new_payment" />
              </div>
              <div>
                <i
                  className="material-icons gx-customized-modal-close"
                  onClick={this.onCancelCustomerAddPayment}
                >
                  clear
                </i>
              </div>
            </div>
          }
          closable={false}
          wrapClassName="gx-customized-modal vertical-center-modal"
          visible={this.state.isCustomerAddPaymentVisible}
          onCancel={this.onCancelCustomerAddPayment}
        >
          <AddPaymentDlg
            onCancel={this.onCancelCustomerAddPayment}
            onSave={this.onAddPayment}
            data={customer}
          />
        </Modal>
      </div>
    ) : (
      <div />
    );
  }

  render() {
    return (
      <div className="gx-main-content">
        <div className="gx-app-module gx-customer-profile-module">
          <div className="gx-w-100">
            <div className="gx-customer-profile-module-scroll">
              {this.renderMainContent(this.props.selected_customer)}
              {/* {this.renderMainContent(customer)} */}
            </div>
          </div>
          {/* <div className="gx-customer-sidenav">
            <ProfileDrawer />
          </div> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { customers, selected_customer } = state.customers;
  return { customers, selected_customer };
};

export default connect(mapStateToProps, {
  getCustomerById,
  updateCustomer
})(injectIntl(CustomerProfile));
