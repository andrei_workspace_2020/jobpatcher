import React, { Component } from "react";
import { Button, Tag, Popover, Input, Modal } from "antd";

import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import { Link } from "react-router-dom";
import ButtonGroup from "antd/lib/button/button-group";
import { connect } from "react-redux";
import EditCustomerDlg from "../CustomerDialog/EditCustomerDlg";
import { changeDateMonthYearHourMinuteSecondFormatSS } from "util/DateTime";
import { updateCustomer, deleteCustomer } from "appRedux/actions/Customers";

class ProfileCard extends Component {
  state = {
    isProfileEditable: false,
    isContactEditable: false,
    isCommunicationEditable: false,
    isFormChanged: false,
    customer_name: this.props.data.first_name + " " + this.props.data.last_name,
    email: this.props.data.email,
    primary_phone: this.props.data.phone.primary_phone,
    isCustomerBalanceActionPopoverVisible: false,
    isCustomerProfileActionPopoverVisible: false
  };

  constructor(props, context) {
    super(props, context);
  }
  componentDidMonunt() {}
  componentDidUpdate(prevProps) {}
  onCustomerNameChange = e => {
    this.setState({ customer_name: e.target.value });
    this.setState({ isEditContactFormChanged: true });
  };
  onCustomerEmailChange = e => {
    this.setState({ email: e.target.value });
    this.setState({ isEditContactFormChanged: true });
  };
  onPhoneNumberChange = e => {
    this.setState({ primary_phone: e.target.value });
    this.setState({ isEditContactFormChanged: true });
  };
  onContactFormVisibleChange = visible => {
    this.setState({ isContactEditable: visible });
  };
  onSubmitCustomerContact = () => {
    if (this.state.isEditContactFormChanged) {
      this.props.onSubmitCustomerContact({
        customer_name: this.state.customer_name,
        email: this.state.email,
        primary_phone: this.state.primary_phone
      });
      this.setState({ isEditContactFormChanged: false });
    }
    this.setState({ isContactEditable: false });
  };
  onChangeCustomerStar = () => {
    this.props.onChangeCustomerStar();
  };
  notAvailableWhenBlank(item) {
    if (item === "" || item == " ") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="customer.profile.notavailable" />
        </div>
      );
    }
    return item;
  }

  renderAvatar(icon) {
    return (
      <div className="gx-main-avatar gx-size-50 gx-mr-12">
        <i
          className="material-icons gx-w-100 gx-text-center"
          style={{ color: "#fbfbfd", fontSize: "36px" }}
        >
          {icon}
        </i>
      </div>
    );
  }

  onSaveCustomer(customer) {
    customer.customer_info.updated_date = changeDateMonthYearHourMinuteSecondFormatSS(
      new Date()
    );
    this.props.updateCustomer(customer.id, customer);
    this.setState({ customerEditDlgVisible: false });
    this.setState({ isProfileEditable: false });
  }
  onCancel() {
    this.setState({ isProfileEditable: false });
  }
  onCustomerBalanceActionPopoverVisibleChange = visible => {
    this.setState({ isCustomerBalanceActionPopoverVisible: visible });
  };
  onCustomerProfileActionPopoverVisibleChange = visible => {
    this.setState({ isCustomerProfileActionPopoverVisible: visible });
  };
  render() {
    var { data, width } = this.props;

    return (
      <div className="gx-w-100" style={{ zIndex: "10" }}>
        <div className="gx-customer-profile-container">
          <div className="gx-customer-profile-card">
            <div className="gx-flex-row gx-flex-nowrap gx-h-100">
              <div className="gx-d-xl-block gx-mr-20">
                <div
                  className="gx-flex-row gx-flex-nowrap gx-align-items-center"
                  style={{ height: "30px" }}
                >
                  <Link to="/customers">
                    <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                      <i className="material-icons gx-mr-2">arrow_back</i>
                      <span
                        className="gx-flex-row gx-flex-nowrap gx-align-items-center"
                        style={{ fontSize: "16px", fontWeight: "500" }}
                      >
                        <IntlMessages id="back" />
                      </span>
                    </div>
                  </Link>
                </div>
              </div>
              <div className="gx-w-100 gx-flex-column gx-justify-content-between">
                <div className="gx-flex-row">
                  <div className="gx-customer-profile-card-title gx-h-auto gx-flex-column gx-w-100">
                    <div className="gx-flex-row gx-align-items-center gx-justify-content-between ">
                      <div className="gx-flex-row gx-align-items-center">
                        <span
                          className="gx-mr-2"
                          style={{ fontSize: "16px", fontWeight: "500" }}
                        >
                          {data.company_name !== ""
                            ? data.company_name
                            : data.first_name + " " + data.last_name}
                        </span>
                        <i
                          className="material-icons"
                          style={{
                            cursor: "pointer",
                            color: data.starred ? "#f7c43d" : "#dcdde5"
                          }}
                          onClick={this.props.onChangeCustomerStar}
                        >
                          {data.starred ? "star" : "star_border"}
                        </i>
                      </div>
                      <div
                        className="gx-edit-circle"
                        onClick={() =>
                          this.setState({ isProfileEditable: true })
                        }
                      >
                        <i className="material-icons gx-icon-action">edit</i>
                      </div>
                      <Modal
                        className="gx-ss-customers-new-modal"
                        title={
                          <div className="gx-customized-modal-header">
                            <div className="gx-customized-modal-title">
                              <IntlMessages id="customer.customerdlg.editcustomer" />
                            </div>
                            <div>
                              <i
                                className="material-icons gx-customized-modal-close"
                                onClick={this.onCancel.bind(this)}
                              >
                                clear
                              </i>
                            </div>
                          </div>
                        }
                        closable={false}
                        wrapClassName="gx-customized-modal vertical-center-modal"
                        visible={this.state.isProfileEditable}
                        onCancel={this.onCancel.bind(this)}
                        width={width >= 1144 ? 1084 : width - 60}
                      >
                        <EditCustomerDlg
                          onCancel={this.onCancel.bind(this)}
                          onSave={this.onSaveCustomer.bind(this)}
                          data={this.props.data}
                        />
                      </Modal>
                    </div>
                    <div className="gx-flex-row">
                      <Tag className="gx-tag-custom">{data.status}</Tag>
                    </div>
                  </div>
                </div>
                <div className="gx-flex-row">
                  <div className="gx-d-lg-block">
                    <ButtonGroup className="gx-customer-list-buttongroup">
                      <Popover
                        overlayClassName="gx-popover-background-blue gx-customer-profile-popover gx-customer-table-estimate-popover"
                        placement="bottom"
                        trigger="hover"
                        content={
                          <div className="content">
                            Create an estimate for{" "}
                            {data.company_name !== ""
                              ? data.company_name
                              : data.first_name + " " + data.last_name}
                          </div>
                        }
                      >
                        <Button>
                          <i className="material-icons">assignment_turned_in</i>
                        </Button>
                      </Popover>
                      <Popover
                        overlayClassName="gx-popover-background-blue gx-customer-profile-popover gx-customer-table-estimate-popover"
                        placement="bottom"
                        trigger="hover"
                        content={
                          <div className="content">
                            Create a job for{" "}
                            {data.company_name !== ""
                              ? data.company_name
                              : data.first_name + " " + data.last_name}
                          </div>
                        }
                      >
                        <Button>
                          <i className="material-icons">build</i>
                        </Button>
                      </Popover>
                      <Popover
                        overlayClassName="gx-popover-customer-profile-action"
                        trigger="click"
                        placement="bottomLeft"
                        visible={
                          this.state.isCustomerProfileActionPopoverVisible
                        }
                        onVisibleChange={visible =>
                          this.onCustomerProfileActionPopoverVisibleChange(
                            visible
                          )
                        }
                        content={
                          <div>
                            <div
                              className="gx-menuitem"
                              onClick={() => {
                                this.onCustomerProfileActionPopoverVisibleChange(
                                  false
                                );
                                this.setState({ isProfileEditable: true });
                              }}
                            >
                              <i className="material-icons">edit</i>
                              <span>Edit customer</span>
                            </div>
                            <div
                              className="gx-menuitem"
                              onClick={() => {
                                this.onCustomerProfileActionPopoverVisibleChange(
                                  false
                                );
                                this.props.deleteCustomer(data.id);
                              }}
                            >
                              <i className="material-icons">delete</i>
                              <span>Delete customer</span>
                            </div>
                          </div>
                        }
                      >
                        <Button
                          onClick={() =>
                            this.onCustomerProfileActionPopoverVisibleChange(
                              true
                            )
                          }
                        >
                          <i className="material-icons">more_horiz</i>
                        </Button>
                      </Popover>
                    </ButtonGroup>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="gx-customer-profile-card">
            <div className="gx-customer-profile-card-title gx-flex-row gx-align-items-center gx-justify-content-between">
              <div className="gx-font-weight-bold">
                <IntlMessages id="customer.profile.primary_contact" />
              </div>
              <div className="gx-edit-circle">
                <Popover
                  overlayClassName="gx-popover-edit-customer-contact"
                  placement="bottomRight"
                  trigger="click"
                  visible={this.state.isContactEditable}
                  onVisibleChange={this.onContactFormVisibleChange}
                  content={
                    <div className="gx-popover-content">
                      <div className="gx-popover-header">
                        <h5>EDIT CONTACT</h5>
                        <Button
                          className={`gx-btn-save ${
                            this.state.isEditContactFormChanged
                              ? "gx-btn-save-active"
                              : ""
                          }`}
                          onClick={this.onSubmitCustomerContact}
                        >
                          Save
                        </Button>
                      </div>
                      <div
                        className="gx-popover-body"
                        style={{ marginTop: "60px" }}
                      >
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            <span>Customer name</span>
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              placeholder="Enter a name"
                              value={this.state.customer_name}
                              onChange={this.onCustomerNameChange}
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            <span>Primary email</span>
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              placeholder="Enter an email"
                              value={this.state.email}
                              onChange={this.onCustomerEmailChange}
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            Primary phone
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              maxLength={10}
                              placeholder="Enter a phone number"
                              value={this.state.primary_phone}
                              onChange={this.onPhoneNumberChange}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  }
                >
                  <i className="material-icons gx-icon-action">edit</i>
                </Popover>
              </div>
            </div>
            <table className="gx-customer-profile-card-table">
              <tbody>
                <tr>
                  <td>
                    <IntlMessages id="customer.profile.name" />
                  </td>
                  <td>
                    {this.notAvailableWhenBlank(
                      data.first_name + " " + data.last_name
                    )}
                  </td>
                </tr>
                <tr>
                  <td>
                    <IntlMessages id="customer.profile.email" />
                  </td>
                  <td>{this.notAvailableWhenBlank(data.email)}</td>
                </tr>
                <tr>
                  <td>
                    <IntlMessages id="customer.profile.phone" />
                  </td>
                  <td>
                    {this.notAvailableWhenBlank(data.phone.primary_phone)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="gx-d-md-block gx-d-none gx-customer-profile-card">
            <div
              className="gx-customer-profile-card-title gx-flex-row gx-justify-content-between"
              style={{ lineHeight: "30px" }}
            >
              <div className="gx-font-weight-bold">
                <IntlMessages id="customer.profile.balance_due" />
              </div>
            </div>
            <div className="gx-flex-row">
              <Tag
                className="gx-tag-custom gx-flex-row gx-align-items-center"
                style={{ padding: "5px" }}
              >
                <h2 style={{ margin: "0px 10px", color: "#4c586d" }}>
                  {"$" + data.balance.toFixed(2)}
                </h2>
                <Popover
                  overlayClassName="gx-popover-customer-balance-action"
                  trigger="click"
                  placement="bottomRight"
                  visible={this.state.isCustomerBalanceActionPopoverVisible}
                  onVisibleChange={visible =>
                    this.onCustomerBalanceActionPopoverVisibleChange(visible)
                  }
                  content={
                    <div>
                      <div
                        className="gx-menuitem"
                        onClick={() => {
                          this.onCustomerBalanceActionPopoverVisibleChange(
                            false
                          );
                          this.props.onSetCustomerAddPaymentModalVisible(true);
                        }}
                      >
                        <i className="material-icons">attach_money</i>
                        <span>Create an invoice</span>
                      </div>
                      <div className="gx-menuitem">
                        <i className="material-icons">add</i>
                        <span>Add a payment</span>
                      </div>
                    </div>
                  }
                >
                  <Button className="gx-customized-button gx-flex-row gx-align-items-center">
                    <span>New</span>
                    <i
                      className="material-icons"
                      style={{ marginRight: "-5px" }}
                    >
                      expand_more
                    </i>
                  </Button>
                </Popover>
              </Tag>
            </div>

            {/* <table className="gx-customer-profile-card-table">
              <tbody>
                <tr>
                  <td>
                    <IntlMessages id="customer.profile.email_communication" />
                  </td>
                  <td>
                    {this.state.isCommunicationEditable ? (
                      <ButtonGroup className="gx-custom-toggle-buttons">
                        <Button
                          className={`gx-btn-toggle ${
                            data.email_communication ? "gx-btn-toggle-yes" : ""
                          }`}
                          size="small"
                          onClick={() => this.onEmailCommunicationChange(true)}
                        >
                          {data.email_communication ? (
                            <span>Yes</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                        <Button
                          className={`gx-btn-toggle ${
                            !data.email_communication ? "gx-btn-toggle-no" : ""
                          }`}
                          size="small"
                          onClick={() => this.onEmailCommunicationChange(false)}
                        >
                          {!data.email_communication ? (
                            <span>No</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                      </ButtonGroup>
                    ) : (
                      <span>{data.email_communication ? "Yes" : "No"}</span>
                    )}
                  </td>
                </tr>
                <tr>
                  <td>
                    <IntlMessages id="customer.profile.sms_communication" />
                  </td>
                  <td>
                    {this.state.isCommunicationEditable ? (
                      <ButtonGroup className="gx-custom-toggle-buttons">
                        <Button
                          className={`gx-btn-toggle ${
                            data.sms_communication ? "gx-btn-toggle-yes" : ""
                          }`}
                          size="small"
                          onClick={() => this.onSMSCommunicationChange(true)}
                        >
                          {data.sms_communication ? (
                            <span>Yes</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                        <Button
                          className={`gx-btn-toggle ${
                            !data.sms_communication ? "gx-btn-toggle-no" : ""
                          }`}
                          size="small"
                          onClick={() => this.onSMSCommunicationChange(false)}
                        >
                          {!data.sms_communication ? (
                            <span>No</span>
                          ) : (
                            <ToggleButton></ToggleButton>
                          )}
                        </Button>
                      </ButtonGroup>
                    ) : (
                      <span>{data.sms_communication ? "Yes" : "No"}</span>
                    )}
                  </td>
                </tr>
                <tr>
                  <td>
                    <IntlMessages id="customer.profile.communication_name" />
                  </td>
                  <td>
                    {!this.state.isCommunicationEditable ? (
                      <span>
                        {data.communication_name.length > 0
                          ? data.communication_name.map((item, index) =>
                              item == "title"
                                ? data.title
                                : item == "first_name"
                                ? data.first_name
                                : item == "last_name"
                                ? data.last_name
                                : ""
                            )
                          : ""}
                      </span>
                    ) : (
                      <div className="gx-communication-name gx-flex-row">
                        {data.communication_name.length > 0
                          ? data.communication_name.map((item, index) => (
                              <div className="gx-customized-badge">
                                {item == "title"
                                  ? "Title"
                                  : item == "first_name"
                                  ? "First name"
                                  : item == "last_name"
                                  ? "Last name"
                                  : ""}
                              </div>
                            ))
                          : ""}
                      </div>
                    )}
                  </td>
                </tr>
                <tr>
                  <td>
                    <IntlMessages id="customer.profile.preferred_language" />
                  </td>
                  <td>
                    {!this.state.isCommunicationEditable ? (
                      <span>{data.preferred_language}</span>
                    ) : (
                      <div className="gx-customized-badge">
                        {data.preferred_language}
                      </div>
                    )}
                  </td>
                </tr>
              </tbody>
            </table> */}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { locale, navStyle, navCollapsed, width, currentPage } = state.settings;
  return {
    locale,
    navStyle,
    navCollapsed,
    width,
    currentPage
  };
};

export default connect(mapStateToProps, {
  // getCustomers,
  // addCustomer,
  updateCustomer,
  deleteCustomer
})(injectIntl(ProfileCard));
