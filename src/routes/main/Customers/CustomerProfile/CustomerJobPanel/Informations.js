/*global google*/
import React, { Component } from "react";
import { Select, Row, Col, Input, Button, Popover } from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import { changeDateYearFormat } from "util/DateTime";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import { GoogleMap, withGoogleMap, Marker } from "react-google-maps";
import "react-google-places-autocomplete/dist/assets/index.css";
import CustomerNotes from "./CustomerNotes";
import CustomerUpload from "./CustomerUpload";
import moment from "moment";

const Option = Select.Option;
const { TextArea } = Input;

const SimpleMapExampleGoogleMap = withGoogleMap(props => {
  return (
    <GoogleMap defaultZoom={16} center={props.center}>
      {props.markers.map((marker, index) => {
        return (
          <Marker
            key={index}
            position={{
              lat: parseFloat(marker.lat),
              lng: parseFloat(marker.lng)
            }}
          ></Marker>
        );
      })}
    </GoogleMap>
  );
});

class Informations extends Component {
  state = {
    isCustomerInfoFormChanged: false,
    isAddressFormChanged: false,
    isCommunicationFormChanged: false,
    isAddressActionPopoverVisible: [],
    isNoteActionPopoverVisible: [],
    isFileActionPopoverVisible: [],
    isAddressDeletePopoverVisible: false,
    editablePanelKey: "",
    selected_address: {
      id: "",
      title: "",
      street: "",
      unit: "",
      city: "",
      state: "",
      zip: "",
      alarmcode: "",
      property_key: ""
    },
    customer_type: this.props.data.customer_info.customer_type,
    lead_source: this.props.data.customer_info.lead_source,
    creation_date: this.props.data.customer_info.creation_date,
    created_by: this.props.data.customer_info.created_by,
    name_format: this.props.data.communication.name_format,
    email_communication: this.props.data.communication.email_communication,
    sms_communication: this.props.data.communication.sms_communication,
    preferred_language: this.props.data.communication.preferred_language
  };

  constructor(props, context) {
    super(props, context);
    this.autoComplete = null;
  }
  componentDidMount() {
    let array = [];
    for (let i = 0; i < this.props.data.address.length; i++) array[i] = false;
    this.setState({ isAddressActionPopoverVisible: [...array] });
    for (let i = 0; i < this.props.data.notes.length; i++) array[i] = false;
    this.setState({ isNoteActionPopoverVisible: [...array] });
    for (let i = 0; i < this.props.data.files.length; i++) array[i] = false;
    this.setState({ isFileActionPopoverVisible: [...array] });
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.editablePanelKey != this.state.editablePanelKey &&
      this.state.editablePanelKey == "address"
    ) {
      //if 3rd panel is active
      // if (this.state.selected_address.title == "") {
      //   let temp = this.state.selected_address;
      //   temp.title = "Address #" + (this.props.data.address.length + 1);
      //   this.setState({ selected_address: temp });
      // }
    }
  }
  onInitiateState = () => {
    this.setState({
      isCustomerInfoFormChanged: false,
      isAddressFormChanged: false,
      isCommunicationFormChanged: false,
      isAddressActionPopoverVisible: [],
      isNoteActionPopoverVisible: [],
      isFileActionPopoverVisible: [],
      isAddressDeletePopoverVisible: false,
      editablePanelKey: "",
      selected_address: {
        id: "",
        title: "",
        street: "",
        unit: "",
        city: "",
        state: "",
        zipcode: "",
        alarmcode: "",
        property_key: "",
        lat: 0,
        lng: 0
      },
      customer_type: this.props.data.customer_info.customer_type,
      lead_source: this.props.data.customer_info.lead_source,
      creation_date: this.props.data.customer_info.creation_date,
      created_by: this.props.data.customer_info.created_by,
      name_format: this.props.data.communication.name_format,
      email_communication: this.props.data.communication.email_communication,
      sms_communication: this.props.data.communication.sms_communication,
      preferred_language: this.props.data.communication.preferred_language
    });
  };
  onSetEditablePanelKey = key => {
    this.setState({ editablePanelKey: key }, () => {
      if (key == "address") {
        let inputNode = document.getElementById("street");
        var options = {
          types: ["address"],
          componentRestrictions: {
            country: "ca"
          }
        };
        this.autoComplete = new window.google.maps.places.Autocomplete(
          inputNode,
          options
        );
        this.autoComplete.addListener("place_changed", this.onPlaceChange);
      }
    });
  };
  onPlaceChange = () => {
    let place = this.autoComplete.getPlace();
    console.log(place);
    let location = place.geometry.location;
    let temp = { ...this.state.selected_address };
    temp.lat = location.lat();
    temp.lng = location.lng();
    temp.street = "";
    for (var i = 0; i < place.address_components.length; i++) {
      for (var j = 0; j < place.address_components[i].types.length; j++) {
        if (place.address_components[i].types[j] == "street_number") {
          temp.street += place.address_components[i].long_name + " ";
        } else if (place.address_components[i].types[j] == "route") {
          temp.street += place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "locality") {
          // or administrative_area_level_2
          temp.city = place.address_components[i].long_name;
        } else if (
          place.address_components[i].types[j] == "administrative_area_level_1"
        ) {
          temp.state = place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "postal_code") {
          temp.zipcode = place.address_components[i].long_name;
        }
      }
    }
    this.setState({ selected_address: temp });
  };

  onCustomerTypeChange = value => {
    this.setState({ customer_type: value });
    this.setState({ isCustomerInfoFormChanged: true });
  };
  onLeadSourceChange = e => {
    this.setState({ lead_source: e.target.value });
    this.setState({ isCustomerInfoFormChanged: true });
  };
  onSubmitCustomerInfo = () => {
    if (this.state.isCustomerInfoFormChanged) {
      let customer_type = this.state.customer_type;
      let lead_source = this.state.lead_source;
      let creation_date = this.state.creation_date;
      let created_by = this.state.created_by;
      this.props.onSubmitCustomerInfo({
        customer_type: customer_type,
        lead_source: lead_source,
        creation_date: creation_date,
        created_by: created_by
      });
      this.setState({ isCustomerInfoFormChanged: false });
    }
    this.onInitiateState();
  };
  onAddressActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isAddressActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isAddressActionPopoverVisible: temp });
  };
  onAddressTitleChange = e => {
    let temp = this.state.selected_address;
    temp.title = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onStreetChange = e => {
    let temp = this.state.selected_address;
    temp.street = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onUnitChange = e => {
    let temp = this.state.selected_address;
    temp.unit = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onCityChange = e => {
    let temp = this.state.selected_address;
    temp.city = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onStateChange = value => {
    let temp = this.state.selected_address;
    temp.state = value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onZipcodeChange = e => {
    let temp = this.state.selected_address;
    temp.zipcode = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onAlarmcodeChange = e => {
    let temp = this.state.selected_address;
    temp.alarmcode = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onPropertyKeyChange = e => {
    let temp = this.state.selected_address;
    temp.property_key = e.target.value;
    this.setState({ selected_address: temp });
    this.setState({ isAddressFormChanged: true });
  };
  onAddAddress = () => {
    this.onInitiateState();
    this.onSetEditablePanelKey("address");
  };
  onSubmitAddress = () => {
    if (this.state.isAddressFormChanged) {
      this.props.onSubmitAddress(this.state.selected_address);
    }
    this.onInitiateState();
  };
  onNoteActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isNoteActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isNoteActionPopoverVisible: temp });
  };
  onFileActionPopoverVisibleChange = (visible, index) => {
    let temp = this.state.isFileActionPopoverVisible;
    temp[index] = visible;
    this.setState({ isFileActionPopoverVisible: temp });
  };
  onAddressDeletePopoverVisibleChange = visible => {
    console.log("delete popover");
    this.setState({ isAddressDeletePopoverVisible: visible });
  };
  onDeleteNote = id => {
    this.props.onDeleteNote(id);
  };
  onSubmitNote = note => {
    this.props.onSubmitNote(note);
    this.onInitiateState();
  };
  onAddFile = file => {
    this.props.onAddFile(file);
  };
  onDeleteFile = id => {
    this.props.onDeleteFile(id);
  };
  onNameFormat1Change = value => {
    let temp = this.state.name_format;
    temp[0] = value;
    this.setState({ name_format: temp });
    this.setState({ isCommunicationFormChanged: true });
  };
  onNameFormat2Change = value => {
    let temp = this.state.name_format;
    temp[1] = value;
    this.setState({ name_format: temp });
    this.setState({ isCommunicationFormChanged: true });
  };
  onEmailCommunicationChange = value => {
    this.setState({ email_communication: value });
    this.setState({ isCommunicationFormChanged: true });
  };
  onSMSCommunicationChange = value => {
    this.setState({ sms_communication: value });
    this.setState({ isCommunicationFormChanged: true });
  };
  onPreferredLanguageChange = value => {
    this.setState({ preferred_language: value });
    this.setState({ isCommunicationFormChanged: true });
  };
  notAvailableWhenBlank(item) {
    if (item === "" || item == " ") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="customer.profile.notavailable" />
        </div>
      );
    }
    return item;
  }
  onSubmitCommunication = () => {
    if (this.state.isCommunicationFormChanged) {
      let name_format = this.state.name_format;
      let email_communication = this.state.email_communication;
      let sms_communication = this.state.sms_communication;
      let preferred_language = this.state.preferred_language;
      this.props.onSubmitCommunication({
        name_format: name_format,
        email_communication: email_communication,
        sms_communication: sms_communication,
        preferred_language: preferred_language
      });
      this.setState({ isCommunicationFormChanged: false });
    }
    this.onInitiateState();
  };
  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { data } = this.props;
    return (
      <div
        className="gx-tab-content gx-tab-informations-content"
        style={{ padding: "20px 20px 0px 20px" }}
      >
        <Row>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Row>
              <Col span={24}>
                <Widget styleName="gx-card-full">
                  <div className="gx-panel-title-bar">
                    <h5>
                      <IntlMessages id="customer.profile.informations.customer_info" />
                    </h5>
                    {this.state.editablePanelKey == "customer" ? (
                      <Button
                        className={`gx-btn-save ${
                          this.state.isCustomerInfoFormChanged
                            ? "gx-btn-save-active"
                            : ""
                        }`}
                        onClick={this.onSubmitCustomerInfo}
                      >
                        Save
                      </Button>
                    ) : (
                      <div
                        className="gx-edit-circle"
                        onClick={() => {
                          this.onInitiateState();
                          this.onSetEditablePanelKey("customer");
                        }}
                      >
                        <i className="material-icons gx-icon-action">edit</i>
                      </div>
                    )}
                  </div>
                  <div className="gx-panel-content">
                    <div className="gx-customized-content-field">
                      <div
                        className={`gx-customized-content-field-title ${
                          this.state.editablePanelKey == "customer"
                            ? "editable"
                            : ""
                        }`}
                      >
                        <IntlMessages id="customer.profile.informations.customer_type" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        {this.state.editablePanelKey == "customer" ? (
                          <Select
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            value={this.state.customer_type}
                            onChange={this.onCustomerTypeChange}
                          >
                            <Option value="Commercial">Commercial</Option>
                            <Option value="Commercial1">Commercial1</Option>
                          </Select>
                        ) : (
                          <span>
                            {this.notAvailableWhenBlank(
                              data.customer_info.customer_type
                            )}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="gx-customized-content-field">
                      <div
                        className={`gx-customized-content-field-title ${
                          this.state.editablePanelKey == "customer"
                            ? "editable"
                            : ""
                        }`}
                      >
                        <IntlMessages id="customer.profile.informations.lead_source" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        {this.state.editablePanelKey == "customjer" ? (
                          <Input
                            value={this.state.lead_source}
                            onChange={this.onLeadSourceChange}
                          />
                        ) : (
                          <span>
                            {this.notAvailableWhenBlank(
                              data.customer_info.lead_source
                            )}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="gx-customized-content-field">
                      <div className="gx-customized-content-field-title">
                        <IntlMessages id="customer.profile.informations.creation_date" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        <span>
                          {changeDateYearFormat(
                            data.customer_info.creation_date
                          )}
                        </span>
                      </div>
                    </div>
                    <div className="gx-customized-content-field">
                      <div className="gx-customized-content-field-title">
                        <IntlMessages id="customer.profile.informations.created_by" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        <span>{data.customer_info.created_by}</span>
                      </div>
                    </div>
                  </div>
                </Widget>
              </Col>
              <Col span={24}>
                <Widget styleName="gx-card-full">
                  <div className="gx-panel-title-bar">
                    <h5>
                      <IntlMessages id="customer.profile.informations.communication" />
                    </h5>
                    {this.state.editablePanelKey == "communication" ? (
                      <Button
                        className={`gx-btn-save ${
                          this.state.isCommunicationFormChanged
                            ? "gx-btn-save-active"
                            : ""
                        }`}
                        onClick={this.onSubmitCommunication}
                      >
                        Save
                      </Button>
                    ) : (
                      <div
                        className="gx-edit-circle"
                        onClick={() => {
                          this.onInitiateState();
                          this.onSetEditablePanelKey("communication");
                        }}
                      >
                        <i className="material-icons gx-icon-action">edit</i>
                      </div>
                    )}
                  </div>
                  <div className="gx-panel-content">
                    <div className="gx-customized-content-field">
                      <div className="gx-customized-content-field-title">
                        <IntlMessages id="customer.profile.informations.name_format" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        {this.state.editablePanelKey == "communication" ? (
                          <div className="gx-flex-row gx-align-items-center gx-flex-nowrap">
                            <Select
                              className="gx-w-100 gx-mr-10"
                              suffixIcon={
                                <i className="material-icons">expand_more</i>
                              }
                              value={this.state.name_format[0]}
                              onChange={this.onNameFormat1Change}
                            >
                              <Option value="Mr.">Mr.</Option>
                              <Option value="Ms.">Ms.</Option>
                              <Option value="Mrs.">Mrs.</Option>
                              <Option value="Miss.">Miss.</Option>
                              <Option value="Dr.">Dr.</Option>
                            </Select>
                            <Select
                              className="gx-w-100"
                              suffixIcon={
                                <i className="material-icons">expand_more</i>
                              }
                              value={this.state.name_format[1]}
                              onChange={this.onNameFormat2Change}
                            >
                              <Option value="first_name">First name</Option>
                              <Option value="last_name">Last name</Option>
                            </Select>
                          </div>
                        ) : (
                          <span>
                            {this.notAvailableWhenBlank(
                              data.communication.name_format[0] +
                                " " +
                                (data.communication.name_format[1] ===
                                "first_name"
                                  ? data.first_name
                                  : data.communication.name_format[1] ===
                                    "last_name"
                                  ? data.last_name
                                  : "")
                            )}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="gx-customized-content-field">
                      <div className="gx-customized-content-field-title">
                        <IntlMessages id="customer.profile.informations.email_communication" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        {this.state.editablePanelKey == "communication" ? (
                          <Select
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            value={this.state.email_communication}
                            onChange={this.onEmailCommunicationChange}
                          >
                            <Option value={true}>Yes</Option>
                            <Option value={false}>No</Option>
                          </Select>
                        ) : (
                          <span>
                            {data.communication.email_communication
                              ? "Yes"
                              : "No"}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="gx-customized-content-field">
                      <div className="gx-customized-content-field-title">
                        <IntlMessages id="customer.profile.informations.sms_communication" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        {this.state.editablePanelKey == "communication" ? (
                          <Select
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            value={this.state.sms_communication}
                            onChange={this.onSMSCommunicationChange}
                          >
                            <Option value={true}>Yes</Option>
                            <Option value={false}>No</Option>
                          </Select>
                        ) : (
                          <span>
                            {data.communication.sms_communication
                              ? "Yes"
                              : "No"}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="gx-customized-content-field">
                      <div className="gx-customized-content-field-title">
                        <IntlMessages id="customer.profile.informations.preferred_language" />
                      </div>
                      <div className="gx-customized-content-field-value">
                        {this.state.editablePanelKey == "communication" ? (
                          <Select
                            suffixIcon={
                              <i className="material-icons">expand_more</i>
                            }
                            value={this.state.preferred_language}
                            onChange={this.onPreferredLanguageChange}
                          >
                            <Option value="English-US">English-US</Option>
                            <Option value="English-UK">English-UK</Option>
                          </Select>
                        ) : (
                          <span>
                            {this.notAvailableWhenBlank(
                              data.communication.preferred_language
                            )}
                          </span>
                        )}
                      </div>
                    </div>
                  </div>
                </Widget>
              </Col>
            </Row>
          </Col>
          <Col xs={24} sm={12} md={12} lg={16} xl={18} xxl={18}>
            <Row>
              <Col xs={24} sm={24} md={24} lg={12} xl={8} xxl={8}>
                <Widget styleName="gx-card-full">
                  <div className="gx-panel-title-bar ">
                    <h5>
                      <IntlMessages id="customer.profile.informations.address" />
                    </h5>
                    {this.state.editablePanelKey == "address" ? (
                      <Button
                        className={`gx-btn-save ${
                          this.state.isAddressFormChanged
                            ? "gx-btn-save-active"
                            : ""
                        }`}
                        onClick={this.onSubmitAddress}
                      >
                        Save
                      </Button>
                    ) : (
                      <div
                        className="gx-edit-circle"
                        onClick={this.onAddAddress}
                      >
                        <i className="material-icons gx-icon-action">add</i>
                      </div>
                    )}
                  </div>
                  <div
                    className="gx-panel-content"
                    style={{ padding: "60px 0px 0px 0px" }}
                  >
                    <div className="gx-panel-content-scroll">
                      {this.state.editablePanelKey == "address" ? (
                        <div className="gx-p-20 gx-w-100">
                          <div className="gx-customized-content-field">
                            <div
                              className={`gx-customized-content-field-title ${
                                this.state.editablePanelKey == "address"
                                  ? "editable"
                                  : ""
                              }`}
                            >
                              Address title
                            </div>
                            <div className="gx-customized-content-field-value">
                              <Input
                                value={this.state.selected_address.title}
                                onChange={this.onAddressTitleChange}
                              />
                            </div>
                          </div>
                          <div className="gx-customized-content-field">
                            <div
                              className={`gx-customized-content-field-title ${
                                this.state.editablePanelKey == "address"
                                  ? "editable"
                                  : ""
                              }`}
                            >
                              Street
                            </div>
                            <div className="gx-customized-content-field-value">
                              <Input
                                id="street"
                                value={this.state.selected_address.street}
                                onChange={this.onStreetChange}
                              />
                            </div>
                          </div>
                          <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                            <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                              <div
                                className={`gx-customized-content-field-title ${
                                  this.state.editablePanelKey == "address"
                                    ? "editable"
                                    : ""
                                }`}
                              >
                                Unit
                              </div>
                              <div className="gx-customized-content-field-value">
                                <Input
                                  placeholder="Unit number"
                                  value={this.state.selected_address.unit}
                                  onChange={this.onUnitChange}
                                />
                              </div>
                            </div>
                            <div className="gx-customized-content-field gx-w-100">
                              <div
                                className={`gx-customized-content-field-title ${
                                  this.state.editablePanelKey == "address"
                                    ? "editable"
                                    : ""
                                }`}
                              >
                                City
                              </div>
                              <div className="gx-customized-content-field-value">
                                <Input
                                  value={this.state.selected_address.city}
                                  onChange={this.onCityChange}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                            <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                              <div
                                className={`gx-customized-content-field-title ${
                                  this.state.editablePanelKey == "address"
                                    ? "editable"
                                    : ""
                                }`}
                              >
                                State
                              </div>
                              <div className="gx-customized-content-field-value">
                                <Select
                                  suffixIcon={
                                    <i className="material-icons">
                                      expand_more
                                    </i>
                                  }
                                  value={this.state.selected_address.state}
                                  onChange={this.onStateChange}
                                >
                                  <Option value="Ontario">Ontario</Option>
                                  <Option value="Quebec">Quebec</Option>
                                </Select>
                              </div>
                            </div>
                            <div className="gx-customized-content-field gx-w-100">
                              <div
                                className={`gx-customized-content-field-title ${
                                  this.state.editablePanelKey == "address"
                                    ? "editable"
                                    : ""
                                }`}
                              >
                                Zipcode
                              </div>
                              <div className="gx-customized-content-field-value">
                                <Input
                                  value={this.state.selected_address.zipcode}
                                  onChange={this.onZipcodeChange}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="gx-customized-content-field">
                            <div
                              className={`gx-customized-content-field-title ${
                                this.state.editablePanelKey == "address"
                                  ? "editable"
                                  : ""
                              }`}
                            >
                              Alarm code
                            </div>
                            <div className="gx-customized-content-field-value">
                              <Input
                                value={this.state.selected_address.alarmcode}
                                onChange={this.onAlarmcodeChange}
                              />
                            </div>
                          </div>
                          <div className="gx-customized-content-field">
                            <div
                              className={`gx-customized-content-field-title ${
                                this.state.editablePanelKey == "address"
                                  ? "editable"
                                  : ""
                              }`}
                            >
                              Key note
                            </div>
                            <div className="gx-customized-content-field-value">
                              <Input
                                value={this.state.selected_address.property_key}
                                onChange={this.onPropertyKeyChange}
                              />
                            </div>
                          </div>
                        </div>
                      ) : this.props.data.address.length > 0 ? (
                        <div>
                          <SimpleMapExampleGoogleMap
                            markers={this.props.data.address}
                            center={{
                              lat: parseFloat(this.props.data.address[0].lat),
                              lng: parseFloat(this.props.data.address[0].lng)
                            }}
                            loadingElement={<div style={{ height: `100%` }} />}
                            containerElement={
                              <div
                                className="gx-dispatch-gps-googlemap-container"
                                style={{ height: "150px" }}
                              />
                            }
                            mapElement={<div style={{ height: `100%` }} />}
                          />
                          {this.props.data.address.map((add, index) => (
                            <div
                              key={index}
                              className="address-info"
                              style={{ padding: "15px 10px 5px 20px" }}
                            >
                              <div className="gx-customized-content-field">
                                <div className="gx-customized-content-field-title">
                                  <span>
                                    {add.title !== ""
                                      ? add.title
                                      : "Address #" + (index + 1)}
                                  </span>
                                </div>
                                <div className="gx-customized-content-field-value">
                                  <table className="gx-w-100">
                                    <tbody>
                                      <tr>
                                        <td width="50%">
                                          <span>
                                            {add.street +
                                              " " +
                                              add.city +
                                              ", " +
                                              add.state +
                                              " " +
                                              add.zipcode}
                                          </span>
                                        </td>
                                        <td width="50%">
                                          <div className="gx-flex-row gx-align-items-center gx-justify-content-end">
                                            <div
                                              className={`gx-circle gx-size-30 gx-mr-10 ${
                                                add.alarmcode != ""
                                                  ? "active"
                                                  : ""
                                              }`}
                                            >
                                              <Popover
                                                overlayClassName="gx-popover-customer-alarmcode"
                                                placement="top"
                                                trigger="hover"
                                                content={
                                                  <div
                                                    className="gx-address-alarmcode-content"
                                                    style={{
                                                      padding: "10px 20px"
                                                    }}
                                                  >
                                                    {add.alarmcode != "" ? (
                                                      <div className="gx-flex-column gx-justify-content-center gx-align-items-center">
                                                        <span
                                                          style={{
                                                            color: "#9399a2",
                                                            fontSize: "13px",
                                                            fontWeight: "500",
                                                            marginBottom: "10px"
                                                          }}
                                                        >
                                                          Alarm code
                                                        </span>
                                                        <span
                                                          className="gx-custom-popover-content-value"
                                                          style={{
                                                            color: "#4c586d",
                                                            fontSize: "18px",
                                                            fontWeight: "bold"
                                                          }}
                                                        >
                                                          {add.alarmcode}
                                                        </span>
                                                      </div>
                                                    ) : (
                                                      <span
                                                        style={{
                                                          color: "#9399a2",
                                                          fontSize: "13px",
                                                          fontWeight: "500",
                                                          marginBottom: "10px"
                                                        }}
                                                      >
                                                        Not available
                                                      </span>
                                                    )}
                                                  </div>
                                                }
                                              >
                                                <i className="material-icons">
                                                  notification_important
                                                </i>
                                              </Popover>
                                            </div>
                                            <div
                                              className={`gx-circle gx-size-30 gx-mr-10 ${
                                                add.property_key != ""
                                                  ? "active"
                                                  : ""
                                              }`}
                                            >
                                              <Popover
                                                overlayClassName="gx-popover-customer-propertykey"
                                                placement="top"
                                                trigger="hover"
                                                content={
                                                  <div
                                                    className="gx-address-propertykey-content"
                                                    style={{
                                                      padding: "10px"
                                                    }}
                                                  >
                                                    {add.property_key != "" ? (
                                                      <div className="gx-flex-column gx-justify-content-center gx-align-items-center">
                                                        <span
                                                          style={{
                                                            color: "#9399a2",
                                                            fontSize: "13px",
                                                            fontWeight: "500",
                                                            marginBottom: "10px"
                                                          }}
                                                        >
                                                          Property key note
                                                        </span>
                                                        <span
                                                          className="gx-custom-popover-content-value gx-text-center"
                                                          style={{
                                                            color: "#4c586d",
                                                            fontSize: "12px",
                                                            fontWeight: "500",
                                                            maxWidth: "250px",
                                                            lineHeight: "20px"
                                                          }}
                                                        >
                                                          {add.property_key}
                                                        </span>
                                                      </div>
                                                    ) : (
                                                      <span
                                                        style={{
                                                          color: "#9399a2",
                                                          fontSize: "13px",
                                                          fontWeight: "500",
                                                          marginBottom: "10px"
                                                        }}
                                                      >
                                                        Not available
                                                      </span>
                                                    )}
                                                  </div>
                                                }
                                              >
                                                <i className="material-icons">
                                                  vpn_key
                                                </i>
                                              </Popover>
                                            </div>
                                            <Popover
                                              overlayClassName="gx-popover-customer-address-action"
                                              placement="bottomRight"
                                              content={
                                                !this.state
                                                  .isAddressDeletePopoverVisible ? (
                                                  <div>
                                                    <div
                                                      className="gx-menuitem"
                                                      onClick={() => {
                                                        this.setState({
                                                          selected_address: {
                                                            ...this.props.data
                                                              .address[index]
                                                          }
                                                        });
                                                        this.onAddressActionPopoverVisibleChange(
                                                          false,
                                                          index
                                                        );
                                                        this.onSetEditablePanelKey(
                                                          "address"
                                                        );
                                                      }}
                                                    >
                                                      <i className="material-icons">
                                                        edit
                                                      </i>
                                                      <span>Edit address</span>
                                                    </div>
                                                    <div
                                                      className="gx-menuitem"
                                                      onClick={() => {
                                                        this.setState({
                                                          selected_address: {
                                                            ...this.props.data
                                                              .address[index]
                                                          }
                                                        });
                                                        // this.props.onDeleteAddress(
                                                        //   add.id
                                                        // );
                                                        // this.onAddressActionPopoverVisibleChange(
                                                        //   false,
                                                        //   index
                                                        // );
                                                        this.onAddressDeletePopoverVisibleChange(
                                                          true
                                                        );
                                                      }}
                                                    >
                                                      <i className="material-icons">
                                                        delete
                                                      </i>
                                                      <span>
                                                        Delete address
                                                      </span>
                                                    </div>
                                                  </div>
                                                ) : (
                                                  <div className="gx-popover-delete-address-content">
                                                    <div className="gx-popover-title gx-flex-row gx-align-items-center">
                                                      <i
                                                        className="gx-size-30 gx-mr-10 material-icons"
                                                        style={{
                                                          color: "#fc6262",
                                                          fontSize: "30px"
                                                        }}
                                                      >
                                                        warning
                                                      </i>
                                                      <div className="gx-flex-column">
                                                        <span
                                                          style={{
                                                            fontSize: "15px",
                                                            color: "#4c586d",
                                                            fontWeight: "bold"
                                                          }}
                                                        >
                                                          Delete this address
                                                        </span>
                                                        <span
                                                          style={{
                                                            fontSize: "13px",
                                                            color: "#9399a2",
                                                            fontWeight: "500"
                                                          }}
                                                        >
                                                          This action can't be
                                                          undone
                                                        </span>
                                                      </div>
                                                    </div>
                                                    <div
                                                      className="gx-popover-action"
                                                      style={{
                                                        marginTop: "15px"
                                                      }}
                                                    >
                                                      <Button
                                                        type="default"
                                                        className="gx-btn-cancel"
                                                        onClick={() =>
                                                          this.onAddressDeletePopoverVisibleChange(
                                                            index,
                                                            false
                                                          )
                                                        }
                                                      >
                                                        Cancel
                                                      </Button>
                                                      <Button
                                                        type="primary"
                                                        className="gx-btn-confirm"
                                                        onClick={() => {
                                                          this.onAddressDeletePopoverVisibleChange(
                                                            index,
                                                            false
                                                          );
                                                          this.onAddressActionPopoverVisibleChange(
                                                            index,
                                                            false
                                                          );
                                                          this.props.onDeleteAddress(
                                                            this.state
                                                              .selected_address
                                                              .id
                                                          );
                                                        }}
                                                      >
                                                        Confirm
                                                      </Button>
                                                    </div>
                                                  </div>
                                                )
                                              }
                                              visible={
                                                this.state
                                                  .isAddressActionPopoverVisible[
                                                  index
                                                ]
                                              }
                                              onVisibleChange={visible =>
                                                this.onAddressActionPopoverVisibleChange(
                                                  visible,
                                                  index
                                                )
                                              }
                                              trigger="click"
                                            >
                                              <i
                                                className="material-icons gx-icon-action"
                                                style={{ fontSize: "26px" }}
                                              >
                                                more_vert
                                              </i>
                                            </Popover>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>
                      ) : (
                        <div
                          className="gx-empty-note-container gx-flex-row gx-align-items-center gx-justify-content-center"
                          style={{ height: "120px" }}
                        >
                          <span
                            className="gx-fs-lg gx-font-weight-medium"
                            style={{ color: "#cfd1d5" }}
                          >
                            Nothing available
                          </span>
                        </div>
                      )}
                    </div>
                  </div>
                </Widget>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={8} xxl={8}>
                <Widget styleName="gx-card-full">
                  <div className="gx-panel-title-bar">
                    <h5>
                      <IntlMessages id="customer.profile.informations.customer_notes" />
                    </h5>
                  </div>
                  <div
                    className="gx-panel-content gx-panel-customer-notes gx-pl-0 gx-pr-0 gx-pb-0"
                    style={{ paddingTop: "60px" }}
                  >
                    <CustomerNotes
                      data={data}
                      editablePanelKey={this.state.editablePanelKey}
                      onInitiateState={this.onInitiateState}
                      onSetEditablePanelKey={this.onSetEditablePanelKey}
                      onDeleteNote={this.onDeleteNote}
                      onSubmitNote={this.onSubmitNote}
                      onNoteActionPopoverVisibleChange={
                        this.onNoteActionPopoverVisibleChange
                      }
                      isNoteActionPopoverVisible={
                        this.state.isNoteActionPopoverVisible
                      }
                    />
                  </div>
                </Widget>
              </Col>
              <Col xs={24} sm={24} md={24} lg={12} xl={8} xxl={8}>
                <Widget styleName="gx-card-full">
                  <div className="gx-panel-title-bar">
                    <h5>
                      <IntlMessages id="customer.profile.informations.customer_files" />
                    </h5>
                  </div>
                  <div
                    className="gx-panel-content"
                    style={{ padding: "60px 0px 0px 0px" }}
                  >
                    <CustomerUpload
                      data={data}
                      editablePanelKey={this.state.editablePanelKey}
                      onInitiateState={this.onInitiateState}
                      onSetEditablePanelKey={this.onSetEditablePanelKey}
                      onAddFile={this.onAddFile}
                      onDeleteFile={this.onDeleteFile}
                      onFileActionPopoverVisibleChange={
                        this.onFileActionPopoverVisibleChange
                      }
                      isFileActionPopoverVisible={
                        this.state.isFileActionPopoverVisible
                      }
                    />
                  </div>
                </Widget>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default injectIntl(Informations);
