import React, { Component } from "react";
import { Button, Checkbox, Avatar, Tabs } from "antd";

import Auxiliary from "util/Auxiliary";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import { Link } from "react-router-dom";
import Informations from "./Informations";
import FinancialData from "./FinancialData";
import ScheduledJobs from "./ScheduledJobs";
import History from "./History";

const TabPane = Tabs.TabPane;

class CustomerJobPanel extends Component {
  state = {};

  constructor(props, context) {
    super(props, context);
  }
  onSubmitCustomerInfo = info => {
    this.props.onSubmitCustomerInfo(info);
  };
  onSubmitCommunication = info => {
    this.props.onSubmitCommunication(info);
  };
  onDeleteNote = id => {
    this.props.onDeleteNote(id);
  };
  onSubmitNote = note => {
    this.props.onSubmitNote(note);
  };
  onSubmitAddress = address => {
    this.props.onSubmitAddress(address);
  };
  onDeleteAddress = id => {
    this.props.onDeleteAddress(id);
  };
  onDefaultPaymentMethodChange = param => {
    this.props.onDefaultPaymentMethodChange(param);
  };
  onPaymentTermsChange = param => {
    this.props.onPaymentTermsChange(param);
  };
  onParentCustomerChange = param => {
    this.props.onParentCustomerChange(param);
  };
  onInvoiceToChange = param => {
    this.props.onInvoiceToChange(param);
  };
  onInvoiceByChange = param => {
    this.props.onInvoiceByChange(param);
  };
  onTaxableChange = active => {
    this.props.onTaxableChange(active);
  };
  onAddBillingAddress = param => {
    this.props.onAddBillingAddress(param);
  };
  onAddCreditCard = param => {
    this.props.onAddCreditCard(param);
  };
  onAddFile = file => {
    this.props.onAddFile(file);
  };
  onDeleteFile = id => {
    this.props.onDeleteFile(id);
  };
  onSubmitFinancialInformation = info => {
    this.props.onSubmitFinancialInformation(info);
  };
  onSubmitBillingInformation = info => {
    this.props.onSubmitBillingInformation(info);
  };
  onSetCustomerAddPaymentModalVisible = visible => {
    this.props.onSetCustomerAddPaymentModalVisible(visible);
  };
  render() {
    var { data } = this.props;
    return (
      <Widget styleName="gx-card-full gx-profile-detail-panel gx-customer-profile">
        <Tabs
          className="gx-customer-detail-panel-tab"
          defaultActiveKey="1"
          animated={false}
          prevIcon={<i className="material-icons">close</i>}
        >
          <TabPane
            tab={<IntlMessages id="customer.profile.tab.information" />}
            key="1"
          >
            <Informations
              data={data}
              onSubmitCustomerInfo={this.onSubmitCustomerInfo}
              onSubmitCommunication={this.onSubmitCommunication}
              onSubmitAddress={this.onSubmitAddress}
              onDeleteAddress={this.onDeleteAddress}
              onDeleteNote={this.onDeleteNote}
              onSubmitNote={this.onSubmitNote}
              onAddFile={this.onAddFile}
              onDeleteFile={this.onDeleteFile}
            />
          </TabPane>
          <TabPane
            tab={<IntlMessages id="customer.profile.tab.financialdata" />}
            key="2"
          >
            <FinancialData
              data={data}
              onSubmitFinancialInformation={this.onSubmitFinancialInformation}
              onSubmitBillingInformation={this.onSubmitBillingInformation}
              onSetCustomerAddPaymentModalVisible={
                this.onSetCustomerAddPaymentModalVisible
              }
            />
          </TabPane>
          <TabPane
            tab={<IntlMessages id="customer.profile.tab.scheduledtasks" />}
            key="3"
          >
            <ScheduledJobs data={data} />
          </TabPane>
          <TabPane
            tab={<IntlMessages id="customer.profile.tab.history" />}
            key="4"
          >
            <History data={data} />
          </TabPane>
        </Tabs>
      </Widget>
    );
  }
}

export default CustomerJobPanel;
