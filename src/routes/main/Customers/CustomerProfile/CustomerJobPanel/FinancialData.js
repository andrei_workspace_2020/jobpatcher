/*global google*/
import React, { Component } from "react";
import { Button, Select, Row, Col, Input, Collapse, Popover } from "antd";
import { injectIntl } from "react-intl";
import IntlMessages from "util/IntlMessages";
import Widget from "components/Widget";
import SearchBox from "components/SearchBox";
import { GoogleMap, Marker, withGoogleMap } from "react-google-maps";

const Option = Select.Option;
const { Panel } = Collapse;
const SimpleMapExampleGoogleMap = withGoogleMap(props => (
  <GoogleMap defaultZoom={16} center={props.center}>
    <Marker
      position={{
        lat: parseFloat(props.marker.lat),
        lng: parseFloat(props.marker.lng)
      }}
    ></Marker>
  </GoogleMap>
));

const customers = [
  {
    id: 0,
    first_name: "Peter",
    last_name: "Jonson"
  },
  {
    id: 1,
    first_name: "Peter",
    last_name: "Pan"
  },
  {
    id: 2,
    first_name: "Peter",
    last_name: "Jackson"
  },
  {
    id: 3,
    first_name: "Tony",
    last_name: "William"
  },
  {
    id: 4,
    first_name: "Maria",
    last_name: "Carie"
  },
  {
    id: 5,
    first_name: "Nathan",
    last_name: "Young"
  }
];
class FinancialData extends Component {
  state = {
    filtered_customers: [],
    searchText: "",
    isSearchListPopoverVisible: false,
    isFinancialInformationFormChanged: false,
    isBillingInformationFormChanged: false,
    editablePanelKey: -1,
    automatic_invoice: this.props.data.financial_info.automatic_invoice,
    payment_term: this.props.data.financial_info.payment_term,
    bill_to: this.props.data.financial_info.bill_to,
    taxable: this.props.data.financial_info.taxable,
    discount_rate: this.props.data.financial_info.discount_rate,
    billing_name: this.props.data.billing_info.billing_name,
    street: this.props.data.billing_info.street,
    unit: this.props.data.billing_info.unit,
    city: this.props.data.billing_info.city,
    state: this.props.data.billing_info.state,
    zipcode: this.props.data.billing_info.zipcode,
    lat: this.props.data.billing_info.lat,
    lng: this.props.data.billing_info.lng,
    invoice_to: this.props.data.billing_info.invoice_to,
    activePanelKey: 1,
    isFinancialActivitiesActionPopoverVisible: false,
    isPaymentHistoryActionPopoverVisible: false
  };

  constructor(props, context) {
    super(props, context);
    this.searchField = React.createRef();
    this.searchSuggestions = React.createRef();
  }
  componentDidMount() {}
  componentDidUpdate(prevProps) {}
  onInitiateState = () => {
    this.setState({
      filtered_customers: [],
      searchText: "",
      isSearchListPopoverVisible: false,
      isFinancialInformationFormChanged: false,
      isBillingInformationFormChanged: false,
      editablePanelKey: -1,
      automatic_invoice: this.props.data.financial_info.automatic_invoice,
      payment_term: this.props.data.financial_info.payment_term,
      bill_to: this.props.data.financial_info.bill_to,
      taxable: this.props.data.financial_info.taxable,
      discount_rate: this.props.data.financial_info.discount_rate,
      billing_name: this.props.data.billing_info.billing_name,
      street: this.props.data.billing_info.street,
      unit: this.props.data.billing_info.unit,
      city: this.props.data.billing_info.city,
      state: this.props.data.billing_info.state,
      zipcode: this.props.data.billing_info.zipcode,
      lat: this.props.data.billing_info.lat,
      lng: this.props.data.billing_info.lng,
      invoice_to: this.props.data.billing_info.invoice_to,
      activePanelKey: 1,
      isFinancialActivitiesActionPopoverVisible: false,
      isPaymentHistoryActionPopoverVisible: false
    });
  };
  onSetEditablePanelKey = key => {
    this.setState({ editablePanelKey: key }, () => {
      if (key == 3) {
        console.log("You can change billing info now!");
        let inputNode = document.getElementById("street");
        console.log(inputNode);
        var options = {
          types: ["address"],
          componentRestrictions: {
            country: "ca"
          }
        };
        this.autoComplete = new window.google.maps.places.Autocomplete(
          inputNode,
          options
        );
        this.autoComplete.addListener("place_changed", this.onPlaceChange);
      }
    });
  };
  onPlaceChange = () => {
    let place = this.autoComplete.getPlace();
    console.log(place);
    let location = place.geometry.location;
    let temp = {};
    temp.lat = location.lat();
    temp.lng = location.lng();
    temp.street = "";
    for (var i = 0; i < place.address_components.length; i++) {
      for (var j = 0; j < place.address_components[i].types.length; j++) {
        if (place.address_components[i].types[j] == "street_number") {
          temp.street += place.address_components[i].long_name + " ";
        } else if (place.address_components[i].types[j] == "route") {
          temp.street += place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "locality") {
          // or administrative_area_level_2
          temp.city = place.address_components[i].long_name;
        } else if (
          place.address_components[i].types[j] == "administrative_area_level_1"
        ) {
          temp.state = place.address_components[i].long_name;
        } else if (place.address_components[i].types[j] == "postal_code") {
          temp.zipcode = place.address_components[i].long_name;
        }
      }
    }
    this.setState({ street: temp.street });
    this.setState({ city: temp.city });
    this.setState({ state: temp.state });
    this.setState({ zipcode: temp.zipcode });
    this.setState({ lat: temp.lat });
    this.setState({ lng: temp.lng });
  };
  updateSearchText = text => {
    this.setState({ isFinancialInformationFormChanged: true });
    if (text != "") this.setState({ isSearchListPopoverVisible: true });
    else this.setState({ isSearchListPopoverVisible: false });
    this.setState({ searchText: text });
    let temp = customers.filter(item =>
      (item.first_name + " " + item.last_name).includes(text)
    );
    if (temp.length == 0) this.setState({ isSearchListPopoverVisible: false });

    this.setState({ filtered_customers: temp });
  };
  onSearchListPopoverVisibleChange = visible => {
    this.setState({ isSearchListPopoverVisible: visible });
  };
  onCollapseChange = key => {
    this.setState({ activePanelKey: key });
  };
  onAutomaticInvoiceChange = value => {
    this.setState({ automatic_invoice: value });
    this.setState({ isFinancialInformationFormChanged: true });
  };
  onPaymentTermChange = value => {
    this.setState({ payment_term: value });
    this.setState({ isFinancialInformationFormChanged: true });
  };
  onBillToChange = value => {
    this.setState({ bill_to: value });
    this.setState({ isFinancialInformationFormChanged: true });
  };
  onTaxableChange = value => {
    this.setState({ taxable: value });
    this.setState({ isFinancialInformationFormChanged: true });
  };
  onDiscountRateChange = e => {
    this.setState({ discount_rate: e.target.value });
    this.setState({ isFinancialInformationFormChanged: true });
  };
  notAvailableWhenBlank(item) {
    if (item === "" || item == " ") {
      return (
        <div className="gx-text-grey">
          <IntlMessages id="customer.profile.notavailable" />
        </div>
      );
    }
    return item;
  }
  onSubmitFinancialInformation = () => {
    if (this.state.isFinancialInformationFormChanged) {
      let info = {
        automatic_invoice: this.state.automatic_invoice,
        payment_term: this.state.payment_term,
        bill_to: this.state.bill_to,
        taxable: this.state.taxable,
        discount_rate: this.state.discount_rate
      };
      console.log(info);
      this.props.onSubmitFinancialInformation(info);
      this.setState({ isFinancialInformationFormChanged: false });
    }
    this.onInitiateState();
  };
  onBillingNameChange = e => {
    this.setState({ billing_name: e.target.value });
    this.setState({ isBillingInformationFormChanged: true });
  };
  onStreetChange = e => {
    this.setState({ street: e.target.value });
    this.setState({ isBillingInformationFormChanged: true });
  };
  onUnitChange = e => {
    this.setState({ unit: e.target.value });
    this.setState({ isBillingInformationFormChanged: true });
  };
  onCityChange = e => {
    this.setState({ city: e.target.value });
    this.setState({ isBillingInformationFormChanged: true });
  };
  onStateChange = value => {
    this.setState({ state: value });
    this.setState({ isBillingInformationFormChanged: true });
  };
  onZipcodeChange = e => {
    this.setState({ zipcode: e.target.value });
    this.setState({ isBillingInformationFormChanged: true });
  };
  onInvoiceToChange = e => {
    this.setState({ invoice_to: e.target.value });
    this.setState({ isBillingInformationFormChanged: true });
  };
  onSubmitBillingInformation = () => {
    if (this.state.isBillingInformationFormChanged) {
      this.props.onSubmitBillingInformation({
        billing_name: this.state.billing_name,
        street: this.state.street,
        unit: this.state.unit,
        city: this.state.city,
        state: this.state.state,
        zipcode: this.state.zipcode,
        invoice_to: this.state.invoice_to,
        lat: this.state.lat,
        lng: this.state.lng
      });
      this.setState({ isBillingInformationFormChanged: false });
    }
    this.onInitiateState();
  };
  onFinancialActivitiesActionPopoverVisibleChange = visible => {
    this.setState({ isFinancialActivitiesActionPopoverVisible: visible });
  };
  onPaymentHistoryActionPopoverVisibleChange = visible => {
    this.setState({ isPaymentHistoryActionPopoverVisible: visible });
  };
  onSetCustomerAddPaymentModalVisible = visible => {
    this.props.onSetCustomerAddPaymentModalVisible(visible);
  };
  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { data } = this.props;
    return (
      <div
        className="gx-tab-content gx-tab-financial-data-content"
        style={{ padding: "20px 20px 0px 20px" }}
      >
        <Row>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full gx-card-financial-data gx-mb-20">
              <div className="gx-panel-title-bar">
                <h5 className="gx-text-uppercase">
                  <IntlMessages id="customer.profile.financial_data.financial_activities" />
                </h5>
                <Popover
                  overlayClassName="gx-popover-financial-activities-action"
                  placement="bottomRight"
                  trigger="click"
                  visible={this.state.isFinancialActivitiesActionPopoverVisible}
                  onVisibleChange={visible =>
                    this.onFinancialActivitiesActionPopoverVisibleChange(
                      visible
                    )
                  }
                  content={
                    <div>
                      <div className="gx-menuitem">
                        <i className="material-icons">attach_money</i>
                        <span>Create an invoice</span>
                      </div>
                      <div
                        className="gx-menuitem"
                        onClick={() => {
                          this.onFinancialActivitiesActionPopoverVisibleChange(
                            false
                          );
                          this.onSetCustomerAddPaymentModalVisible(true);
                        }}
                      >
                        <i className="material-icons">add</i>
                        <span>Add a payment</span>
                      </div>
                    </div>
                  }
                >
                  <Button className="gx-customized-button gx-flex-row gx-align-items-center gx-flex-nowrap">
                    <span>New</span>
                    <i className="material-icons">expand_more</i>
                  </Button>
                </Popover>
              </div>
              <div
                className="gx-panel-content gx-pl-0 gx-pr-0 gx-pb-0"
                style={{ paddingTop: "60px" }}
              >
                <div className="gx-p-20">
                  <Collapse
                    className="gx-w-100 gx-financial-activities-collapse"
                    accordion
                    activeKey={this.state.activePanelKey}
                    onChange={this.onCollapseChange}
                    expandIconPosition="right"
                    expandIcon={({ isActive }) => (
                      <i className="material-icons">{isActive ? "-" : "+"} </i>
                    )}
                  >
                    <Panel
                      showArrow={false}
                      header={
                        <div className="panel-header">
                          <div className="gx-flex-row gx-align-items-center">
                            <span className="gx-fs-13-20 gx-font-weight-medium gx-mr-10">
                              Overdue invoices
                            </span>
                            <span className="gx-badge gx-badge-danger gx-text-white gx-mb-0">
                              {
                                this.props.data.financial_activities
                                  .overdue_invoices.length
                              }
                            </span>
                          </div>
                          <i className="ant-collapse-arrow">
                            {this.state.activePanelKey == 1 ? "-" : "+"}
                          </i>
                        </div>
                      }
                      key="1"
                      className="gx-financial-activity gx-overdue-invoices"
                    >
                      {this.props.data.financial_activities.overdue_invoices
                        .length > 0 ? (
                        this.props.data.financial_activities.overdue_invoices.map(
                          (invoice, index) => (
                            <div
                              key={index}
                              className="gx-financial-activity-item gx-financial-overdue-invoice gx-flex-row gx-flex-nowrap gx-align-items-center"
                            >
                              <i
                                className="material-icons gx-financial-activity-icon gx-mr-10"
                                style={{
                                  color: "#f55555"
                                }}
                              >
                                error
                              </i>
                              <span className="gx-fs-sm">
                                <span className="gx-link">{invoice.job}</span>
                                {" - " +
                                  "(" +
                                  "$" +
                                  invoice.invoice +
                                  ")" +
                                  " Late since " +
                                  invoice.overdue}
                              </span>
                            </div>
                          )
                        )
                      ) : (
                        <div />
                      )}
                    </Panel>
                    <Panel
                      showArrow={false}
                      header={
                        <div className="panel-header">
                          <div className="gx-flex-row gx-align-items-center">
                            <span className="gx-fs-13-20 gx-font-weight-medium gx-mr-10">
                              Non-invoices jobs
                            </span>
                            <span className="gx-badge gx-badge-warning gx-text-white gx-mb-0">
                              {
                                this.props.data.financial_activities
                                  .non_invoiced_jobs.length
                              }
                            </span>
                          </div>
                          <i className="ant-collapse-arrow">
                            {this.state.activePanelKey == 2 ? "-" : "+"}
                          </i>
                        </div>
                      }
                      key="2"
                      className="gx-financial-activity gx-non-invoices-jobs"
                    >
                      {this.props.data.financial_activities.non_invoiced_jobs
                        .length > 0 ? (
                        this.props.data.financial_activities.non_invoiced_jobs.map(
                          (job, index) => (
                            <div
                              key={index}
                              className="gx-financial-activity-item gx-financial-non-invoiced-jobs gx-flex-row gx-flex-nowrap gx-align-items-center"
                            >
                              <i
                                className="material-icons gx-financial-activity-icon gx-mr-10"
                                style={{
                                  color: "#efb51c"
                                }}
                              >
                                error
                              </i>
                              <span className="gx-fs-sm">
                                <span className="gx-link">{job.job}</span>
                                {" - " + job.description}
                              </span>
                            </div>
                          )
                        )
                      ) : (
                        <div />
                      )}
                    </Panel>
                    <Panel
                      showArrow={false}
                      header={
                        <div className="panel-header">
                          <div className="gx-flex-row gx-align-items-center">
                            <span className="gx-fs-13-20 gx-font-weight-medium gx-mr-10">
                              Not due yet
                            </span>
                            <span className="gx-badge gx-badge-warning gx-text-white gx-mb-0">
                              {
                                this.props.data.financial_activities.not_due_yet
                                  .length
                              }
                            </span>
                          </div>
                          <i className="ant-collapse-arrow">
                            {this.state.activePanelKey == 3 ? "-" : "+"}
                          </i>
                        </div>
                      }
                      key="3"
                      className="gx-financial-activity gx-not-due-yet"
                    >
                      {this.props.data.financial_activities.not_due_yet.length >
                      0 ? (
                        this.props.data.financial_activities.not_due_yet.map(
                          (job, index) => (
                            <div
                              key={index}
                              className="gx-financial-activity-item gx-financial-not-due-yet gx-flex-row gx-flex-nowrap"
                            >
                              <i
                                className="material-icons gx-financial-activity-icon gx-mr-10"
                                style={{
                                  color: "#efb51c"
                                }}
                              >
                                error
                              </i>
                              <span className="gx-fs-sm">
                                <span className="gx-link">{job.job}</span>
                                {" - " +
                                  "Invoice will be due in " +
                                  job.due_days}
                              </span>
                            </div>
                          )
                        )
                      ) : (
                        <div />
                      )}
                    </Panel>
                  </Collapse>
                </div>
              </div>
            </Widget>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full gx-card-financial-data gx-mb-20">
              <div className="gx-panel-title-bar gx-pr-10">
                <h5 className="gx-text-uppercase">
                  <IntlMessages id="customer.profile.financial_data.financial_information" />
                </h5>
                {this.state.editablePanelKey == 2 ? (
                  <Button
                    className={`gx-btn-save ${
                      this.state.isFinancialInformationFormChanged
                        ? "gx-btn-save-active"
                        : ""
                    }`}
                    onClick={this.onSubmitFinancialInformation}
                  >
                    Save
                  </Button>
                ) : (
                  <div
                    className="gx-edit-circle"
                    onClick={() => {
                      this.onInitiateState();
                      this.onSetEditablePanelKey(2);
                    }}
                  >
                    <i className="material-icons gx-icon-action">edit</i>
                  </div>
                )}
              </div>
              <div
                className="gx-panel-content gx-panel-customer-notes gx-pl-0 gx-pr-0 gx-pb-0"
                style={{ paddingTop: "60px" }}
              >
                <div className="gx-p-20">
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="customer.profile.financial_data.automatic_invoice" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.automatic_invoice}
                          onChange={this.onAutomaticInvoiceChange}
                        >
                          <Option value="Daily">Daily</Option>
                          <Option value="Weekly">Weekly</Option>
                          <Option value="Monthly">Monthly</Option>
                        </Select>
                      ) : (
                        <span>
                          {this.notAvailableWhenBlank(
                            data.financial_info.automatic_invoice
                          )}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="customer.profile.financial_data.payment_term" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.payment_term}
                          onChange={this.onPaymentTermChange}
                        >
                          <Option value="Due after 7 days">
                            Due after 7 days
                          </Option>
                          <Option value="Due after 14 days">
                            Due after 14 days
                          </Option>
                          <Option value="Due after 30 days">
                            Due after 30 days
                          </Option>
                        </Select>
                      ) : (
                        <span>
                          {this.notAvailableWhenBlank(
                            data.financial_info.payment_term
                          )}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="customer.profile.financial_data.bill_to" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        this.state.bill_to != "" ? (
                          <div className="gx-flex-row gx-align-items-center gx-justify-content-between">
                            <div className="gx-flex-row gx-align-items-center">
                              <i className="material-icons gx-mr-10">
                                account_circle
                              </i>
                              <span>{this.state.bill_to}</span>
                            </div>
                            <span
                              className="gx-link gx-link-remove"
                              onClick={() => this.setState({ bill_to: "" })}
                            >
                              Remove
                            </span>
                          </div>
                        ) : (
                          <Popover
                            overlayClassName="gx-popover-search-customers-result"
                            placement="bottomLeft"
                            visible={this.state.isSearchListPopoverVisible}
                            onVisibleChange={visible =>
                              this.onSearchListPopoverVisibleChange(visible)
                            }
                            content={
                              this.state.filtered_customers.length > 0 ? (
                                <div
                                  style={{
                                    width: this.searchField.current.nameInput
                                      .clientWidth
                                  }}
                                >
                                  {this.state.filtered_customers.map(
                                    (item, index) => (
                                      <div
                                        key={index}
                                        className="gx-menuitem"
                                        onClick={() =>
                                          this.setState({
                                            bill_to:
                                              item.first_name +
                                              " " +
                                              item.last_name
                                          })
                                        }
                                      >
                                        <i className="material-icons gx-mr-10">
                                          account_circle
                                        </i>
                                        <span>
                                          {item.first_name +
                                            " " +
                                            item.last_name}
                                        </span>
                                      </div>
                                    )
                                  )}
                                </div>
                              ) : (
                                <div />
                              )
                            }
                          >
                            <SearchBox
                              styleName="gx-lt-icon-search-bar-lg gx-dispatch-search"
                              placeholder={"Search customer"}
                              ref={this.searchField}
                              onChange={e =>
                                this.updateSearchText(e.target.value)
                              }
                              value={this.state.searchText}
                            />
                          </Popover>
                        )
                      ) : (
                        <span>
                          {this.notAvailableWhenBlank(
                            data.financial_info.bill_to
                          )}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="customer.profile.financial_data.taxable" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        <Select
                          suffixIcon={
                            <i className="material-icons">expand_more</i>
                          }
                          value={this.state.taxable}
                          onChange={this.onTaxableChange}
                        >
                          <Option value={true}>Yes</Option>
                          <Option value={false}>No</Option>
                        </Select>
                      ) : (
                        <span>
                          {data.financial_info.taxable ? "Yes" : "No"}
                        </span>
                      )}
                    </div>
                  </div>
                  <div className="gx-customized-content-field">
                    <div
                      className={`gx-customized-content-field-title ${
                        this.state.editablePanelKey == 2 ? "editable" : ""
                      }`}
                    >
                      <IntlMessages id="customer.profile.financial_data.discount_rate" />
                    </div>
                    <div className="gx-customized-content-field-value">
                      {this.state.editablePanelKey == 2 ? (
                        <Input
                          value={this.state.discount_rate}
                          onChange={this.onDiscountRateChange}
                        />
                      ) : (
                        <span>
                          {this.notAvailableWhenBlank(
                            data.financial_info.discount_rate
                          )}
                        </span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </Widget>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full gx-card-financial-data gx-mb-20">
              <div className="gx-panel-title-bar gx-pr-10">
                <h5 className="gx-text-uppercase">
                  <IntlMessages id="customer.profile.financial_data.billing_information" />
                </h5>
                {this.state.editablePanelKey == 3 ? (
                  <Button
                    className={`gx-btn-save ${
                      this.state.isBillingInformationFormChanged
                        ? "gx-btn-save-active"
                        : ""
                    }`}
                    onClick={this.onSubmitBillingInformation}
                  >
                    Save
                  </Button>
                ) : (
                  <div
                    className="gx-edit-circle"
                    onClick={() => {
                      this.onSetEditablePanelKey(3);
                    }}
                  >
                    <i className="material-icons gx-icon-action">edit</i>
                  </div>
                )}
              </div>
              <div
                className="gx-panel-content"
                style={{ padding: "60px 0px 0px 0px" }}
              >
                <div className="gx-panel-content-scroll">
                  {this.state.editablePanelKey == 3 ? (
                    <div className="gx-p-20 gx-w-100">
                      <div className="gx-customized-content-field">
                        <div
                          className={`gx-customized-content-field-title ${
                            this.state.editablePanelKey == 3 ? "editable" : ""
                          }`}
                        >
                          Billing name
                        </div>
                        <div className="gx-customized-content-field-value">
                          <Input
                            value={this.state.billing_name}
                            onChange={this.onBillingNameChange}
                          />
                        </div>
                      </div>
                      <div className="gx-customized-content-field">
                        <div
                          className={`gx-customized-content-field-title ${
                            this.state.editablePanelKey == 3 ? "editable" : ""
                          }`}
                        >
                          Street
                        </div>
                        <div className="gx-customized-content-field-value">
                          <Input
                            id="street"
                            value={this.state.street}
                            onChange={this.onStreetChange}
                          />
                        </div>
                      </div>
                      <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 3 ? "editable" : ""
                            }`}
                          >
                            Unit
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              placeholder="Unit number"
                              value={this.state.unit}
                              onChange={this.onUnitChange}
                            />
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-w-100">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 3 ? "editable" : ""
                            }`}
                          >
                            City
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              value={this.state.city}
                              onChange={this.onCityChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="gx-customized-content-field gx-flex-row gx-align-items-center gx-flex-nowrap">
                        <div className="gx-customized-content-field gx-w-100 gx-mr-10">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 3 ? "editable" : ""
                            }`}
                          >
                            State
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Select
                              suffixIcon={
                                <i className="material-icons">expand_more</i>
                              }
                              value={this.state.state}
                              onChange={this.onStateChange}
                            >
                              <Option value="Ontario">Ontario</Option>
                              <Option value="Quebec">Quebec</Option>
                            </Select>
                          </div>
                        </div>
                        <div className="gx-customized-content-field gx-w-100">
                          <div
                            className={`gx-customized-content-field-title ${
                              this.state.editablePanelKey == 3 ? "editable" : ""
                            }`}
                          >
                            Zipcode
                          </div>
                          <div className="gx-customized-content-field-value">
                            <Input
                              value={this.state.zipcode}
                              onChange={this.onZipcodeChange}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="gx-customized-content-field">
                        <div
                          className={`gx-customized-content-field-title ${
                            this.state.editablePanelKey == 3 ? "editable" : ""
                          }`}
                        >
                          Send invoice to
                        </div>
                        <div className="gx-customized-content-field-value">
                          <Input
                            value={this.state.invoice_to}
                            onChange={this.onInvoiceToChange}
                          />
                        </div>
                      </div>
                    </div>
                  ) : this.props.data.billing_info.street != "" ? (
                    <div>
                      <SimpleMapExampleGoogleMap
                        marker={this.props.data.billing_info}
                        center={{
                          lat: parseFloat(this.props.data.billing_info.lat),
                          lng: parseFloat(this.props.data.billing_info.lng)
                        }}
                        loadingElement={<div style={{ height: `100%` }} />}
                        containerElement={
                          <div
                            className="gx-dispatch-gps-googlemap-container"
                            style={{ height: "150px" }}
                          />
                        }
                        mapElement={<div style={{ height: `100%` }} />}
                      />
                      <div className="gx-p-20">
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            Billing name
                          </div>
                          <div className="gx-customized-content-field-value">
                            <span>
                              {this.props.data.billing_info.billing_name}
                            </span>
                          </div>
                        </div>
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            Billing address
                          </div>
                          <div className="gx-customized-content-field-value">
                            <span className="gx-d-block">
                              {this.props.data.billing_info.street}
                            </span>
                            <span className="gx-d-block">
                              {this.props.data.billing_info.street != ""
                                ? this.props.data.billing_info.city +
                                  ", " +
                                  this.props.data.billing_info.state +
                                  " " +
                                  this.props.data.billing_info.zipcode
                                : ""}
                            </span>
                          </div>
                        </div>
                        <div className="gx-customized-content-field">
                          <div className="gx-customized-content-field-title">
                            Send invoice to
                          </div>
                          <div className="gx-customized-content-field-value">
                            <span>
                              {this.props.data.billing_info.invoice_to}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div className="gx-p-20">
                      <div
                        className="gx-flex-row gx-justify-content-center gx-align-items-center"
                        style={{ height: "100px", color: "#cfd1d5" }}
                      >
                        <div className="gx-text-center">
                          <div className="gx-fs-lg gx-font-weight-medium">
                            <span>Nothing available</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </Widget>
          </Col>
          <Col xs={24} sm={12} md={12} lg={8} xl={6} xxl={6}>
            <Widget styleName="gx-card-full gx-card-financial-data gx-mb-20">
              <div className="gx-panel-title-bar gx-pr-10">
                <h5 className="gx-text-uppercase">
                  <IntlMessages id="customer.profile.financial_data.payment_history" />
                </h5>
                <Popover
                  overlayClassName="gx-popover-payment-history-action"
                  placement="bottomRight"
                  trigger="click"
                  visible={this.state.isPaymentHistoryActionPopoverVisible}
                  onVisibleChange={visible =>
                    this.onPaymentHistoryActionPopoverVisibleChange(visible)
                  }
                  content={
                    <div>
                      <div className="gx-menuitem">
                        <i className="material-icons">attach_money</i>
                        <span>Create an invoice</span>
                      </div>
                      <div
                        className="gx-menuitem"
                        onClick={() => {
                          this.onPaymentHistoryActionPopoverVisibleChange(
                            false
                          );
                          this.onSetCustomerAddPaymentModalVisible(true);
                        }}
                      >
                        <i className="material-icons">add</i>
                        <span>Add a payment</span>
                      </div>
                    </div>
                  }
                >
                  <Button className="gx-customized-button gx-flex-row gx-align-items-center gx-flex-nowrap">
                    <span>New</span>
                    <i className="material-icons">expand_more</i>
                  </Button>
                </Popover>
              </div>
              <div
                className="gx-panel-content gx-panel-customer-notes gx-pl-0 gx-pr-0 gx-pb-0"
                style={{ paddingTop: "60px" }}
              >
                <div className="gx-p-20">
                  {this.props.data.payment_history.length > 0 ? (
                    this.props.data.payment_history.map((history, index) => (
                      <Widget
                        key={index}
                        styleName="gx-financial-payment-history gx-card-full gx-p-15"
                      >
                        <div className="gx-flex-row gx-align-items-center">
                          <span className="gx-link">{history.job}</span>
                          <i className="material-icons gx-dot">lens</i>
                          <span className="gx-history-date">
                            {history.date}
                          </span>
                        </div>
                        <span className="gx-history-desc gx-font-weight-medium">
                          {history.invoice +
                            " has been paid with " +
                            history.payment_method}
                        </span>
                      </Widget>
                    ))
                  ) : (
                    <div
                      className="gx-flex-row gx-justify-content-center gx-align-items-center"
                      style={{ height: "100px", color: "#cfd1d5" }}
                    >
                      <div className="gx-text-center">
                        <div className="gx-fs-lg gx-font-weight-medium">
                          <span>Nothing available</span>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </Widget>
          </Col>
        </Row>
      </div>
    );
  }
}

export default injectIntl(FinancialData);
