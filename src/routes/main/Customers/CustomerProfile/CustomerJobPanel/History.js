import React, { Component } from "react";
import { injectIntl } from "react-intl";
import JobHistoryPanel from "./JobHistoryPanel";
import IntlMessages from "util/IntlMessages";

class History extends Component {
  state = {};

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const {
      intl: { formatMessage }
    } = this.props;
    const { data } = this.props;
    return (
      <div className="gx-flex-row gx-w-100 gx-tab-content gx-tab-history-content gx-flex-nowrap">
        <div className="gx-job-analytics-content gx-flex-column">
          <h4 className="gx-mb-20">Customer stats</h4>
          <div className="gx-job-analytics-item gx-job-lifetimeearning-analytics">
            <IntlMessages id="customer.profile.history.lifetime_value" />
            <h1>$800.00</h1>
          </div>
          <div className="gx-job-analytics-item gx-job-lastmonth-analytics">
            <IntlMessages id="customer.profile.history.last_month" />
            <div className="gx-flex-row gx-align-items-center">
              <h1>$480.00</h1>
              <i
                className="material-icons"
                style={{
                  color: "#39bf58",
                  marginLeft: "5px",
                  cursor: "pointer"
                }}
              >
                trending_up
              </i>
            </div>
          </div>
          <div className="gx-job-analytics-item gx-job-totaljobs-analytics">
            <IntlMessages id="customer.profile.history.total_jobs" />
            <h1>03</h1>
          </div>
          <div className="gx-job-analytics-item gx-job-averagejobearning-analytics">
            <IntlMessages id="customer.profile.history.average_job_value" />

            <div className="gx-flex-row gx-align-items-center">
              <h1>$250.00</h1>
              <i
                className="material-icons"
                style={{
                  color: "#f55555",
                  marginLeft: "5px",
                  cursor: "pointer"
                }}
              >
                trending_down
              </i>
            </div>
          </div>
        </div>
        <div
          className="gx-job-history-content gx-w-100"
          style={{ marginTop: "12px" }}
        >
          <JobHistoryPanel data={data}></JobHistoryPanel>
        </div>
      </div>
    );
  }
}

export default injectIntl(History);
