import React, { Component } from "react";
import { Form, Input, Button, Select } from "antd";
import IntlMessages from "util/IntlMessages";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import { hideMessage, showAuthLoader, setUpAccount } from "appRedux/actions/Auth";

const checkOptions = ["Scheduling", "Invocing", "Online payment"];
const { Option } = Select;

class WelcomeModal extends React.Component {
  state = {
      company_name: "",
      phone_number: "",
      service_industry: "",
      role: "",
      company_size: 0,
      exp_years: 0
  };
  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(this.state)
        this.props.setUpAccount(this.state);
        this.updateUserInfo(this.state);
      }
    });
  };
  
  updateUserInfo = info => {
    this.props.updateUserInfo(info);
  };

  onCompanyNameChange = e=>{
      this.props.form.setFieldsValue({company_name: e.target.value});
      this.setState({company_name: e.target.value});
  }
  onPhoneNumberChange = e=>{
      this.props.form.setFieldsValue({phone_number: e.target.value});
      this.setState({phone_number: e.target.value});
  }
  onServiceIndustryChange = value=>{
      this.props.form.setFieldsValue({service_industry: value});
      this.setState({service_industry: value});
  }
  onRoleChange = e=>{
      this.setState({role: e.target.value});
  }
  onCompanySizeChange = value=>{
      this.setState({company_size: value});
  }
  onExpYearChange = value=>{
      this.setState({exp_years: value});
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const mobileFormatter = e =>
      e.target.value.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3");
    return (
      <div>
          <div className="welcome-modal-header">
            <div className="gx-app-auth-logo">
                <img src={require("assets/images/logo/logo.png")} />
            </div>
            <div className="gx-app-auth-language">
                <Select
                    className="language"
                    defaultValue="en"
                    suffixIcon={<i className="material-icons">expand_more</i>}
                >
                    <Option value="en">English</Option>
                    <Option value="ru">Russian</Option>
                    <Option value="fr">French</Option>
                </Select>
            </div>
            </div>
            <div className="welcome-modal-content">
            <div className="welcome-modal-content-left">
                <Form onSubmit={this.onSubmit} className="gx-h-100 gx-form-row0">
                    <div className="welcome-modal-content-left-container">
                        <div className="welcome-modal-content-left-header" style={{marginBottom: "30px"}}>
                            <h1>SETUP YOUR ACCOUNT</h1>
                        </div>
                        <div className="welcome-modal-content-left-body">
                            <Form.Item>
                                <label>
                                    Company name <span style={{color: "#f55555"}}>*</span>
                                </label>
                                <div>
                                    {getFieldDecorator("company_name", {
                                    rules: [
                                        {
                                        required: true,
                                        type: "string",
                                        message: "Please input your company name!"
                                        }
                                    ]
                                    })(
                                    <Input
                                    placeholder="Enter name" onChange={this.onCompanyNameChange}
                                    />)}
                                </div>
                            </Form.Item>
                            <Form.Item>
                                <label>
                                    Phone number <span style={{color: "#f55555"}}>*</span>
                                </label>
                                <div>
                                    {getFieldDecorator("phone_number", {
                                    rules: [
                                        {
                                        required: true,
                                        message: "Please input your phone number!"
                                        }
                                    ],
                                    getValueFromEvent: mobileFormatter
                                    })(
                                    <Input
                                    placeholder="Enter number"
                                    maxLength={14}
                                    onChange={this.onPhoneNumberChange}
                                    />)}
                                </div>
                            </Form.Item>
                            <div className="gx-flex-row gx-flex-nowrap">
                                <div className="gx-w-50">
                                    <Form.Item>
                                        <label>Service industry <span style={{color: "#f55555"}}>*</span></label>
                                        <div>
                                        {getFieldDecorator("service_industry", {
                                            rules: [
                                            {
                                                required: true,
                                                message: "Please choose your industry!"
                                            }
                                            ]
                                        })(
                                        <Select
                                            suffixIcon={
                                            <i className="material-icons">expand_more</i>
                                            }
                                            placeholder="Select a number"
                                            onChange={this.onServiceIndustryChange}
                                        >
                                            <Option value="a">AAAAAAA</Option>
                                            <Option value="b">BBBBBBB</Option>
                                            <Option value="c">CCCCCCC</Option>
                                            <Option value="d">DDDDDDD</Option>
                                        </Select>)}
                                        </div>
                                    </Form.Item>
                                </div>
                                <div className="gx-w-50 gx-ml-10">
                                    <Form.Item>
                                        <label>Your role</label>
                                        <div>
                                            <Input
                                                placeholder="Eg. Manager"
                                                onChange={this.onRoleChange}
                                            />
                                        </div>
                                    </Form.Item>
                                </div>
                            </div>
                            <div className="welcome-form-item gx-flex-row gx-flex-nowrap">
                                <div className="gx-w-50">
                                    <Form.Item>
                                        <label>
                                            Company size
                                        </label>
                                        <div>
                                            <Select
                                                suffixIcon={
                                                    <i className="material-icons">expand_more</i>
                                                }
                                                placeholder="Select a number"
                                                onChange={this.onCompanySizeChange}
                                            >
                                                <Option value="1">1</Option>
                                                <Option value="2">2</Option>
                                                <Option value="3">3</Option>
                                                <Option value="4">4</Option>
                                                <Option value="5">5</Option>
                                                <Option value="6">6</Option>
                                                <Option value="7">7</Option>
                                                <Option value="8">8</Option>
                                                <Option value="9">9</Option>
                                                <Option value="10">10</Option>
                                            </Select>
                                        </div>
                                    </Form.Item>
                                </div>
                                <div className="gx-w-50 gx-ml-10">
                                    <Form.Item>
                                        <label>
                                            Years in business
                                        </label>
                                        <div>
                                            <Select
                                                suffixIcon={
                                                <i className="material-icons">expand_more</i>
                                                }
                                                placeholder="Select a number"
                                                onChange={this.onExpYearChange}
                                            >
                                                <Option value="1">1</Option>
                                                <Option value="2">2</Option>
                                                <Option value="3">3</Option>
                                                <Option value="4">4</Option>
                                                <Option value="5">5</Option>
                                                <Option value="6">6</Option>
                                                <Option value="7">7</Option>
                                                <Option value="8">8</Option>
                                                <Option value="9">9</Option>
                                                <Option value="10">10</Option>
                                            </Select>
                                        </div>
                                    </Form.Item>
                                </div>
                            </div>
                        </div>
                        <div className="welcome-modal-content-left-footer">
                            <Button
                            className="gx-btn-start"
                            htmlType="submit"
                            >
                            SAVE AND FINISH
                            </Button>
                        </div>
                    </div>
                </Form>
            </div>

            <div className="welcome-modal-content-right">
                <div className="welcome-modal-content-right-img">
                    <div className="welcome-modal-content-right-container">
                        <div className="welcome-modal-content-right-body">
                            <h2>Your are almost done!</h2>
                            <h3>Enjoy every features with 14 days free trial</h3>
                            <div>
                                <i className="material-icons">check</i>
                                <p>Real-time dispatching</p>
                            </div>
                            <div>
                                <i className="material-icons">check</i>
                                <p>Drop & drag scheduling</p>
                            </div>
                            <div>
                                <i className="material-icons">check</i>
                                <p>Advanced estimating</p>
                            </div>
                            <div>
                                <i className="material-icons">check</i>
                                <p>Live GPS and Email</p>
                            </div>
                            <div>
                                <i className="material-icons">check</i>
                                <p>Custom SMS and Email</p>
                            </div>
                            <div>
                                <i className="material-icons">check</i>
                                <p>Accept oneline-payment</p>
                            </div>
                            <div>
                                <i className="material-icons">check</i>
                                <p>Live chatting</p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
      </div>
    );
  }
}

const WrappedWelcomeModalForm = Form.create({
  name: "welcome_modal"
})(WelcomeModal);

const mapStateToProps = ({ auth }) => {
    const { loader, alertMessage, showMessage, authUser } = auth;
    return { loader, alertMessage, showMessage, authUser };
};
export default connect(
    mapStateToProps,
    {
      setUpAccount,
      hideMessage,
      showAuthLoader
    }
)(WrappedWelcomeModalForm);
