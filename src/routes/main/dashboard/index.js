import React, { Component } from "react";
import { Col, Row, Modal, Checkbox, Select, Button, Input, Form } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Auxiliary from "util/Auxiliary";

import InvoiceCard from "components/dashboard/cards/InvoiceCard";
import TodayStatCard from "components/dashboard/cards/TodayStatCard";
import EstimateCard from "components/dashboard/cards/EstimateCard";
import ShortcutCard from "components/dashboard/cards/ShortcutCard";
import JobCard from "components/dashboard/cards/JobCard";
import CustomerCard from "components/dashboard/cards/CustomerCard";
import SuggestedActionCard from "components/dashboard/cards/SuggestedActionCard";
import ReferCard from "components/dashboard/cards/ReferCard";
import UpgradeCard from "components/dashboard/cards/UpgradeCard";

import Membership from "components/Membership";
import PurchasePlan from "components/Membership/PurchasePlan";
import OverallCard from "../../../components/dashboard/cards/OverallCard";
import WrappedWelcomeModalForm from "./WelcomeModal";
import { hideWelcomeModal } from "appRedux/actions/Auth";

class Dashboard extends Component {
  state = {
    membershipDlgVisible: false,
    membershipPurchaseVisible: false,
    membershipType: "Team",
    welcomeModalVisible: false,
    auth: JSON.parse(localStorage.getItem("user_info")),
    isShortcutEditPopoverVisible: false,
    shortcutLinks: [
      { id: 1, link: "Add a customer", visible: true },
      { id: 2, link: "Add an employee", visible: true },
      { id: 3, link: "Create a job", visible: true },
      { id: 4, link: "Create an estimate", visible: true },
      { id: 5, link: "My company profile", visible: true },
      { id: 6, link: "Finanace settings", visible: false },
      { id: 7, link: "Communication settings", visible: true },
      { id: 8, link: "Custom link 1", visible: false },
      { id: 9, link: "Custom link 2", visible: false },
      { id: 10, link: "Custom link 3", visible: false },
      { id: 11, link: "Custom link 4", visible: false },
      { id: 12, link: "Custom link 5", visible: false },
      { id: 13, link: "Custom link 6", visible: false }
    ]
  };

  constructor(props, context) {
    super(props, context);
  }

  showMembershipDlg() {
    this.setState({ membershipDlgVisible: true });
  }

  onCloseMembershipDlg() {
    this.setState({ membershipDlgVisible: false });
  }

  showPurchasePlan(type) {
    this.setState({
      membershipType: type,
      membershipDlgVisible: false,
      membershipPurchaseVisible: true
    });
  }

  setVisibleWelcomeModal = visible => {
    this.setState({ welcomeModalVisible: visible });
  };
  onClosePurchasePlan() {
    this.setState({ membershipPurchaseVisible: false });
  }
  componentDidMount() {
    if (this.state.auth.company_name === undefined)
      this.setState({ welcomeModalVisible: true });
    else this.setState({ welcomeModalVisible: false });
  }

  updateUserInfo = info => {
    let temp = this.state.auth;
    temp.company_name = info.company_name;
    temp.phone_number = info.phone_number;
    temp.service_industry = info.service_industry;
    temp.role = info.role;
    temp.company_size = info.company_size;
    temp.exp_years = info.exp_years;
    this.setState({ auth: temp });
  };

  setShortcutCardEditPopoverVisible = visible => {
    console.log(visible);
    this.setState({ isShortcutEditPopoverVisible: visible });
  };
  setVisibleShortcutLinks = links => {
    this.setState({ shortcutLinks: links });
  };
  render() {
    const { width } = this.props;
    const {
      membershipDlgVisible,
      membershipPurchaseVisible,
      membershipType
    } = this.state;
    return (
      <div className="gx-main-content-wrapper">
        <Auxiliary>
          <div className="gx-main-content-container gx-mt-30">
            <Modal
              className="modal-welcome"
              closable={false}
              centered
              visible={this.state.auth.company_name == undefined ? true : false}
              footer={[null, null]}
            >
              <WrappedWelcomeModalForm
                setVisibleWelcomeModal={this.setVisibleWelcomeModal}
                hideWelcomeModal={this.props.hideWelcomeModal}
                updateUserInfo={this.updateUserInfo}
              />
            </Modal>
            <Row>
              <Col xxl={12} xl={16} lg={24} md={24} sm={24} xs={24}>
                <OverallCard
                  company={
                    this.state.auth.company_name !== undefined
                      ? this.state.auth.company_name
                      : ""
                  }
                  auth={this.state.auth}
                />
              </Col>
              <Col xxl={6} xl={8} lg={12} md={12} sm={24} xs={24}>
                <InvoiceCard />
              </Col>
              <Col xxl={6} xl={8} lg={12} md={12} sm={24} xs={24}>
                <EstimateCard />
              </Col>
              <Col xxl={6} xl={8} lg={12} md={12} sm={24} xs={24}>
                <CustomerCard />
              </Col>
              <Col xxl={6} xl={8} lg={12} md={12} sm={24} xs={24}>
                <JobCard />
              </Col>
              <Col xxl={6} xl={8} lg={12} md={12} sm={24} xs={24}>
                <ShortcutCard
                  isShortcutEditPopoverVisible={
                    this.state.isShortcutEditPopoverVisible
                  }
                  shortcutLinks={this.state.shortcutLinks}
                  setShortcutCardEditPopoverVisible={
                    this.setShortcutCardEditPopoverVisible
                  }
                  setVisibleShortcutLinks={this.setVisibleShortcutLinks}
                />
              </Col>
              <Col xxl={6} xl={8} lg={12} md={12} sm={24} xs={24}>
                <ReferCard
                  onMembershipClick={this.showMembershipDlg.bind(this)}
                />
              </Col>
            </Row>
          </div>

          <Membership
            visible={membershipDlgVisible}
            onCancel={this.onCloseMembershipDlg.bind(this)}
            onNext={this.showPurchasePlan.bind(this)}
            width={width > 1250 + 40 ? 1250 + 40 : width}
          />
          <PurchasePlan
            visible={membershipPurchaseVisible}
            onCancel={this.onClosePurchasePlan.bind(this)}
            width={width > 1250 + 40 ? 1250 + 40 : width}
            type={membershipType}
          />
        </Auxiliary>
      </div>
    );
  }
}
const mapStateToProps = state => {
  const { locale, navStyle, navCollapsed, width, currentPage } = state.settings;
  const { authUser } = state.auth;
  return {
    locale,
    navStyle,
    navCollapsed,
    width,
    currentPage,
    authUser
  };
};

export default connect(mapStateToProps, {
  hideWelcomeModal
})(Dashboard);
