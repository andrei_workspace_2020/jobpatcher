import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "./dashboard";
import Customers from "./Customers";
import CustomerProfile from "./Customers/CustomerProfile";
import Dispatch from "./Dispatch";
import DispatchSchedule from "./Dispatch/DispatchSchedule";
import DispatchEmployees from "./Dispatch/DispatchEmployees";
import DispatchEmployeeProfile from "./Dispatch/DispatchEmployeeProfile";
import DispatchGps from "./Dispatch/DispatchGps";
import NewJob from "./NewJob";
import JobDetail from "./JobDetail";
import Sales from "./Sales";
import NewEstimate from "./NewEstimate";
import Settings from "./Settings";
import Chat from "./Chat";
import "assets/custom.css";

const Main = ({ match }) => {
  return (
    <Switch>
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/customers/profile" component={CustomerProfile} />
      <Route path="/customers" component={Customers} />
      <Route path="/dispatch/schedule" component={DispatchSchedule} />
      <Route path="/jobs/detail" component={JobDetail} />
      <Route path="/dispatch/gps" component={DispatchGps} />
      <Route
        path="/dispatch/employees/profile"
        component={DispatchEmployeeProfile}
      />
      <Route path="/dispatch/employees" component={DispatchEmployees} />
      <Route path="/jobs/add" component={NewJob} />
      {/* <Route path="/dispatch" component={Dispatch} /> */}
      <Route path="/sales" component={Sales} />

      <Route path="/estimates/add" component={NewEstimate} />
      <Route path="/settings" component={Settings} />
      <Route path="/chat" component={Chat} />
    </Switch>
  );
};

export default Main;
