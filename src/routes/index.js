import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "./main/dashboard";
import Customers from "./main/Customers";
import CustomerProfile from "./main/Customers/CustomerProfile";
import Dispatch from "./main/Dispatch";
import DispatchSchedule from "./main/Dispatch/DispatchSchedule";
import DispatchEmployees from "./main/Dispatch/DispatchEmployees";
import DispatchEmployeeProfile from "./main/Dispatch/DispatchEmployeeProfile";
import DispatchGps from "./main/Dispatch/DispatchGps";
import NewJob from "./main/NewJob";
import JobDetail from "./main/JobDetail";
import Sales from "./main/Sales";
import NewEstimate from "./main/NewEstimate";
import EstimateDetail from "./main/EstimateDetail";
import EstimateSend from "./main/EstimateDetail/EstimateSend";
import Settings from "./main/Settings";
import Chat from "./main/Chat";
import "assets/custom.css";

const App = ({ match }) => {
  return (
    <Switch>
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/customers/profile" component={CustomerProfile} />
      <Route path="/customers" component={Customers} />
      <Route path="/dispatch/schedule" component={DispatchSchedule} />
      <Route path="/jobs/detail" component={JobDetail} />
      <Route path="/dispatch/gps" component={DispatchGps} />
      <Route
        path="/dispatch/employees/profile"
        component={DispatchEmployeeProfile}
      />
      <Route path="/dispatch/employees" component={DispatchEmployees} />
      <Route path="/jobs/add" component={NewJob} />
      {/* <Route path="/dispatch" component={Dispatch} /> */}
      <Route path="/sales" component={Sales} />
      <Route path="/estimates/add" component={NewEstimate} />
      <Route path="/estimates/detail" component={EstimateDetail} />
      <Route path="/estimates/send" component={EstimateSend} />
      <Route path="/settings" component={Settings} />
      <Route path="/chat" component={Chat} />
    </Switch>
  );
};

export default App;
