import React from "react";
import { Card } from "antd";
import { Link } from "react-router-dom";
import { injectIntl } from "react-intl";

import AuthTopbar from "./AuthTopbar";

class Welcome extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="gx-app-auth-wrap gx-app-welcome-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-welcome-main-content">
          <div className="gx-app-welcome-content">
            <div className="gx-app-welcome-header">
              <div>
                <h1>Hello! Welcome to the Jobpatcher.</h1>
                <p>Sign up for free and say goodbye to paperwork!</p>
              </div>
            </div>
            <div className="gx-app-welcome-body">
              <Link to="/signup">
                <Card className="gx-card auth-card">
                  <div className="gx-card-header">
                    <span>Setup a new business</span>
                    <p>Do you manage or own a business</p>
                  </div>
                  <img
                    src={require("assets/images/auth/welcome_signup_img.png")}
                  />
                </Card>
              </Link>
              <Link to="/signin">
                <Card className="gx-card auth-card">
                  <div className="gx-card-header">
                    <span>Join as existing team</span>
                    <p>Are you an employee? Join here now.</p>
                  </div>
                  <img
                    src={require("assets/images/auth/welcome_signin_img.png")}
                  />
                </Card>
              </Link>
            </div>
            <div className="gx-app-welcome-footer">
              <span>
                Already have an account? <Link to="/signin">Sign in</Link>
              </span>
            </div>
            <div className="message-box">
              <i className="material-icons">textsms</i>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(Welcome);
