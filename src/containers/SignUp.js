import React from "react";
import { Card, Button, Popover, Form, Icon, Input, message, Modal } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import { hideMessage, showAuthLoader, userSignUp } from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class SignUp extends React.Component {
  startPosition = 0;
  endPosition = window.innerWidth * (1657 / 1995);
  refreshIntervalId = 0;
  state = {
    screenHeight: 0,
    screenWidth: 0,
    showPassword: false,
    isLoading: false,
    loadingPosition: this.startPosition,
    first_name: "",
    last_name: "",
    email: "",
    password: ""
  };
  constructor(props, context) {
    super(props, context);
    this.updatePosition = this.updatePosition.bind(this);
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.hideMessage();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //this.props.showAuthLoader();
        this.props.userSignUp(values);
        console.log(this.props.showMessage);
      }
    });
  };
  updateDimensions = () => {
    this.setState({
      screenHeight: window.innerHeight,
      screenWidth: window.innerWidth
    });
  };
  componentDidMount() {
    this.props.hideMessage();
    this.setState({ screenWidth: window.innerWidth });
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }
  componentDidUpdate(prevProps) {
    console.log(this.props.authUser);
    if (this.props.authUser != prevProps.authUser) {
      this.setState({ isLoading: true });
      this.refreshIntervalId = setInterval(this.updatePosition, 10);
    }
  }
  componentWillUnmount() {
    clearInterval(this.refreshIntervalId);
  }
  updatePosition() {
    if (this.state.loadingPosition <= this.endPosition - 182)
      this.setState({ loadingPosition: this.state.loadingPosition + 3 });
    else {
      clearInterval(this.refreshIntervalId);
      let path = "/dashboard";
      this.props.history.push(path);
    }
  }
  onFirstNameChange = e => {
    this.setState({ first_name: e.target.value });
    this.props.form.setFieldsValue({ first_name: e.target.value });
  };
  onLastNameChange = e => {
    this.setState({ last_name: e.target.value });
    this.props.form.setFieldsValue({ last_name: e.target.value });
  };
  onEmailChange = e => {
    this.setState({ email: e.target.value });
    this.props.form.setFieldsValue({ email: e.target.value });
  };
  onPasswordChange = e => {
    this.setState({ password: e.target.value });
    this.props.form.setFieldsValue({ password: e.target.value });
  };
  setVisiblePassword = e => {
    this.setState({ showPassword: !this.state.showPassword });
    this.passwordInput.focus();
  };
  render() {
    const { showMessage, loader, alertMessage } = this.props;
    const { getFieldDecorator } = this.props.form;
    // const mobileFormatter = e =>
    //   e.target.value.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3");
    return (
      <div className="gx-app-auth-wrap gx-app-signupstep-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-signupstep-main-container">
          <div className="gx-app-signupstep-content">
            <div className="gx-app-signupstep-form">
              <Card className="gx-card">
                <div className="gx-flex-row gx-flex-nowrap">
                  <Form
                    onSubmit={this.handleSubmit}
                    className="gx-signupstep-form gx-form-row0"
                  >
                    <FormItem>
                      <h1 className="gx-font-weight-bold">
                        START YOUR FREE TRIAL
                      </h1>
                      <span
                        className="gx-d-block"
                        style={{ lineHeight: "20px" }}
                      >
                        Avail all product features and support for free!
                      </span>
                      <span
                        className="gx-d-block"
                        style={{ lineHeight: "20px" }}
                      >
                        No credit card required.
                      </span>
                    </FormItem>
                    {showMessage ? (
                      <FormItem className="gx-mb-0 gx-text-center">
                        <span className="gx-error-msg">
                          {this.props.alertMessage.toString()}
                        </span>
                      </FormItem>
                    ) : (
                      <div />
                    )}
                    <div className="gx-flex-row gx-flex-nowrap">
                      <div className="gx-w-50 gx-mr-10">
                        <FormItem>
                          <label>
                            First name <span style={{ color: "red" }}>*</span>
                          </label>
                          {getFieldDecorator("first_name", {
                            rules: [
                              {
                                required: true,
                                type: "string",
                                message: "Please write your first name"
                              }
                            ]
                          })(
                            <Input
                              placeholder="Enter name"
                              onChange={this.onFirstNameChange}
                            />
                          )}
                        </FormItem>
                      </div>
                      <div className="gx-w-50">
                        <FormItem>
                          <label>
                            Last name <span style={{ color: "red" }}>*</span>
                          </label>
                          {getFieldDecorator("last_name", {
                            rules: [
                              {
                                required: true,
                                type: "string",
                                message: "Please write your last name"
                              }
                            ]
                          })(
                            <Input
                              placeholder="Enter name"
                              onChange={this.onLastNameChange}
                            />
                          )}
                        </FormItem>
                      </div>
                    </div>
                    <FormItem>
                      <label>
                        Email address <span style={{ color: "red" }}>*</span>
                      </label>
                      {getFieldDecorator("email", {
                        validateTrigger: "onSubmit",
                        rules: [
                          {
                            required: true,
                            message: "The email address is required!"
                          },
                          {
                            type: "email",
                            message: "Please input valid email address!"
                          }
                        ]
                      })(
                        <Input
                          placeholder="Enter email"
                          onChange={this.onEmailChange}
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      <label>
                        Password <span style={{ color: "red" }}>*</span>
                      </label>
                      {getFieldDecorator("password", {
                        rules: [
                          {
                            required: true,
                            message: "You must use a password to continue"
                          },
                          {
                            min: 6,
                            message:
                              "The password should contain at least 6 characters!"
                          }
                        ]
                      })(
                        <Input
                          type={
                            this.state.showPassword == true
                              ? "text"
                              : "password"
                          }
                          ref={d => (this.passwordInput = d)}
                          placeholder="Enter password"
                          onChange={this.onPasswordChange}
                          suffix={
                            <i
                              className={
                                this.state.showPassword
                                  ? "material-icons icon-show-password"
                                  : "material-icons"
                              }
                              onClick={this.setVisiblePassword}
                            >
                              {this.state.showPassword
                                ? "visibility"
                                : "visibility_off"}
                            </i>
                          }
                        />
                      )}
                    </FormItem>
                    {/* <FormItem>
                      <label>
                        Mobile number <span style={{ color: "red" }}>*</span>
                      </label>
                      <Popover
                        trigger="focus"
                        placement={
                          this.state.screenWidth <= 768 ? "top" : "right"
                        }
                        overlayClassName="signup-form-popover"
                        content={
                          <div className="form-popover-content">
                            Please write down your mobile number
                          </div>
                        }
                      >
                        {getFieldDecorator("phone_number", {
                          rules: [
                            {
                              required: true,
                              message: "Please provide a valid phone number"
                            }
                          ],
                          getValueFromEvent: mobileFormatter
                        })(<Input placeholder="Enter number" maxLength={14} />)}
                      </Popover>
                    </FormItem> */}
                    <FormItem>
                      <Button htmlType="submit">START TRIAL</Button>
                    </FormItem>
                    <p>
                      By clicking continue you are agreeing to the
                      <br />
                      <Link to="#">terms of service</Link> and{" "}
                      <Link to="#">privacy policy</Link>
                    </p>
                  </Form>
                  <div className="gx-flex-row gx-align-items-center gx-justify-content-center gx-ml-30">
                    <img src={require("assets/images/auth/signup_img.png")} />
                  </div>
                </div>
              </Card>
              <span>
                Already have an account? <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {/* {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}
          {showMessage ? message.error(alertMessage.toString()) : null} */}
          <Modal
            className="modal-loading"
            closable={false}
            centered
            visible={this.state.isLoading}
            footer={[null, null]}
          >
            <div className="loading-screen">
              <img
                id="img-loading-bg"
                src={require("assets/images/auth/loading_bg_img.png")}
              />
              <img
                id="img-loading-car"
                src={require("assets/images/auth/loading_car.png")}
                style={{ left: this.state.loadingPosition + "px" }}
              />
              <img
                id="img-loading-destination"
                src={require("assets/images/auth/loading_destination.png")}
                style={{ left: this.endPosition - 18 + "px" }}
              />
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

const WrappedNormalSignUpForm = Form.create()(SignUp);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser };
};

export default connect(mapStateToProps, {
  userSignUp,
  hideMessage,
  showAuthLoader
})(WrappedNormalSignUpForm);
