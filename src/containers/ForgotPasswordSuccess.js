import React from "react";
import { Card, Button, Carousel, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import { hideMessage, showAuthLoader, userForgotPassword } from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class ForgotPasswordSuccess extends React.Component {
  onResend = ()=>{
    this.props.userForgotPassword({email: this.props.userInfo.email});
  }
  componentDidMount(){
    this.props.hideMessage();
    if(this.props.userInfo!=null)
      this.props.form.setFieldsValue({email: this.props.userInfo.email});
    else this.props.history.push("/signin");
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { showMessage, loader, alertMessage, userInfo } = this.props;
    return (
      <div className="gx-app-auth-wrap gx-app-forgot-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-forgot-main-container">
          <div className="gx-app-forgot-content">
            <div className="gx-app-forgot-form">
              <Card className="gx-card">
                <Form
                  className="gx-forgot-form gx-form-row0"
                >
                  <div className="forgot_desc">
                    <img
                      src={require("assets/images/auth/forgot_password_success_img.png")}
                    />
                    <h1>Check your email inbox</h1>
                    <p className="gx-mb-30">
                      We've sent an email to{" "}
                      <span style={{ color: "#257cde" }}>{userInfo.email}</span>
                      <br />
                      Click the link in the email to reset your password.
                    </p>
                    <p className="resend_desc" style={{ color: "#9399a2" }}>
                      If you don't see the email check your spam/junk folder.
                      <br />
                    </p>
                    <p>
                      <a className="link-resend" href="#" onClick={this.onResend}>
                        Resend email
                      </a>
                    </p>
                  </div>
                </Form>
              </Card>
              <span>
                Go back to <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}  
          {showMessage ? message.error(alertMessage.toString()) : null}
        </div>
      </div>
    );
  }
}

const WrappedNormalForgotPasswordSuccessForm = Form.create()(ForgotPasswordSuccess);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser, userInfo } = auth;
  return { loader, alertMessage, showMessage, authUser, userInfo };
};

export default connect(
  mapStateToProps,
  {
    userForgotPassword,
    hideMessage,
    showAuthLoader
  }
)(WrappedNormalForgotPasswordSuccessForm);
