import React, { Component } from "react";
import { connect } from "react-redux";
import URLSearchParams from "url-search-params";
import { Redirect, Route, Switch } from "react-router-dom";
import { LocaleProvider } from "antd";
import { IntlProvider } from "react-intl";

import AppLocale from "lngProvider";
import MainApp from "./MainApp";
import SignIn from "../SignIn";
import Welcome from "../Welcome";
import SignUp from "../SignUp";
import EmpSignUp from "../EmpSignUp";
import SignUpStep1 from "../SignUpStep1";
import SignUpStep2 from "../SignUpStep2";
import SignUpStep3 from "../SignUpStep3";
import ForgotPassword from "../ForgotPassword";
import ForgotPasswordSuccess from "../ForgotPasswordSuccess";
import ResetPassword from "../ResetPassword";
import { setInitUrl } from "appRedux/actions/Auth";
import {
  onLayoutTypeChange,
  onNavStyleChange,
  setThemeType
} from "appRedux/actions/Setting";

import {
  LAYOUT_TYPE_BOXED,
  LAYOUT_TYPE_FRAMED,
  LAYOUT_TYPE_FULL,
  NAV_STYLE_ABOVE_HEADER,
  NAV_STYLE_BELOW_HEADER,
  NAV_STYLE_DARK_HORIZONTAL,
  NAV_STYLE_DEFAULT_HORIZONTAL,
  NAV_STYLE_INSIDE_HEADER_HORIZONTAL
} from "../../constants/ThemeSetting";

const RestrictedRoute = ({ component: Component, authUser, ...rest }) => {
  const auth = JSON.parse(localStorage.getItem("user_info"));
  if (authUser) {
    if (
      auth.company_name === undefined &&
      rest.location.pathname != "/dashboard"
    )
      return (
        <Route
          {...rest}
          render={props => (
            <Redirect
              to={{
                pathname: "/dashboard",
                state: { from: props.location }
              }}
            />
          )}
        />
      );
    else return <Route {...rest} render={props => <Component {...props} />} />;
  } else
    return (
      <Route
        {...rest}
        render={props => (
          <Redirect
            to={{
              pathname: "/signin",
              state: { from: props.location }
            }}
          />
        )}
      />
    );
};

class App extends Component {
  setLayoutType = layoutType => {
    if (layoutType === LAYOUT_TYPE_FULL) {
      document.body.classList.remove("boxed-layout");
      document.body.classList.remove("framed-layout");
      document.body.classList.add("full-layout");
    } else if (layoutType === LAYOUT_TYPE_BOXED) {
      document.body.classList.remove("full-layout");
      document.body.classList.remove("framed-layout");
      document.body.classList.add("boxed-layout");
    } else if (layoutType === LAYOUT_TYPE_FRAMED) {
      document.body.classList.remove("boxed-layout");
      document.body.classList.remove("full-layout");
      document.body.classList.add("framed-layout");
    }
  };

  setNavStyle = navStyle => {
    if (
      navStyle === NAV_STYLE_DEFAULT_HORIZONTAL ||
      navStyle === NAV_STYLE_DARK_HORIZONTAL ||
      navStyle === NAV_STYLE_INSIDE_HEADER_HORIZONTAL ||
      navStyle === NAV_STYLE_ABOVE_HEADER ||
      navStyle === NAV_STYLE_BELOW_HEADER
    ) {
      document.body.classList.add("full-scroll");
      document.body.classList.add("horizontal-layout");
    } else {
      document.body.classList.remove("full-scroll");
      document.body.classList.remove("horizontal-layout");
    }
  };

  componentWillMount() {
    if (this.props.initURL === "") {
      this.props.setInitUrl(this.props.history.location.pathname);
    }
    const params = new URLSearchParams(this.props.location.search);

    if (params.has("theme")) {
      this.props.setThemeType(params.get("theme"));
    }
    if (params.has("nav-style")) {
      this.props.onNavStyleChange(params.get("nav-style"));
    }
    if (params.has("layout-type")) {
      this.props.onLayoutTypeChange(params.get("layout-type"));
    }
  }
  componentWillUpdate() {
    if (this.props.location.pathname === "/") {
      if (this.props.authUser === null) {
        this.props.history.push("/signin");
      } else if (
        this.props.initURL === "" ||
        this.props.initURL === "/" ||
        this.props.initURL === "/signin"
      ) {
        this.props.history.push("/dashboard");
      } else {
        this.props.history.push(this.props.initURL);
      }
    }
  }
  render() {
    const {
      match,
      location,
      layoutType,
      navStyle,
      locale,
      authUser,
      initURL
    } = this.props;
    this.setLayoutType(layoutType);

    this.setNavStyle(navStyle);

    const currentAppLocale = AppLocale[locale.locale];
    return (
      <LocaleProvider locale={currentAppLocale.antd}>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={currentAppLocale.messages}
        >
          <Switch>
            <Route exact path="/welcome" component={Welcome} />
            <Route exact path="/signin" component={SignIn} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/emp/signup" component={EmpSignUp} />
            {/* <Route exact path="/signup/step_1" component={SignUpStep1} />
            <Route exact path="/signup/step_2" component={SignUpStep2} />
            <Route exact path="/signup/step_3" component={SignUpStep3} /> */}
            <Route exact path="/forgot_password" component={ForgotPassword} />
            <Route
              exact
              path="/forgot_password_success"
              component={ForgotPasswordSuccess}
            />
            <Route exact path="/reset_password" component={ResetPassword} />
            <RestrictedRoute
              path={`${match.url}`}
              authUser={authUser}
              component={MainApp}
            />
          </Switch>
        </IntlProvider>
      </LocaleProvider>
    );
  }
}

const mapStateToProps = ({ settings, auth }) => {
  const { locale, navStyle, layoutType } = settings;
  const { authUser, initURL } = auth;
  return { locale, navStyle, layoutType, authUser, initURL };
};
export default connect(mapStateToProps, {
  setInitUrl,
  setThemeType,
  onNavStyleChange,
  onLayoutTypeChange
})(App);
