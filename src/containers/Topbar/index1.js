import React, { Component } from "react";
import {
  Icon,
  Layout,
  Popover,
  Button,
  Badge,
  Menu,
  Drawer,
  Tabs,
  Modal
} from "antd";
import { injectIntl } from "react-intl";
import { Link } from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import languageData from "./languageData";
import {
  switchLanguage,
  updateWindowWidth,
  setCurrentPage
} from "../../appRedux/actions/Setting";
import SearchBox from "components/SearchBox";
import UserInfo from "components/UserInfo";
import SearchBar from "components/TopMenu/SearchBar";
import AppNotification from "components/TopMenu/AppNotification";
import MailNotification from "components/MailNotification";
import PopupNew from "components/TopMenu/PopupNew";
import PopupMore from "components/TopMenu/PopupMore";
import Auxiliary from "util/Auxiliary";
import IntlMessages from "util/IntlMessages";

import BottomMenu from "./BottomMenu";
import EditCustomerDlg from "./EditCustomerDlg";
import ImportCustomerDlg from "./ImportCustomerDlg";

import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_MINI_SIDEBAR,
  TAB_SIZE,
  MOBILE_SIZE
} from "../../constants/ThemeSetting";
import { connect } from "react-redux";

const { Header } = Layout;

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const TabPane = Tabs.TabPane;

const MenuItem = ({ link, icon, icon_type, title }) => {
  if (link === null || link === "") link = "#";
  return (
    <Link to={link}>
      <div className="gx-top-menu-item">
        <div>
          <i className="material-icons">{icon}</i>
          <div>
            <IntlMessages id={title} />
          </div>
        </div>
      </div>
    </Link>
  );
};
const SubMenuItem = ({ link, icon, title }) => {
  if (link === null || link === "") link = "#";
  return (
    <Link to={link}>
      <div className="gx-div-align-center">
        <i className="material-icons gx-mr-10">{icon}</i>
        <div>
          <IntlMessages id={title} />
        </div>
      </div>
    </Link>
  );
};

const BOTTOM_MENU_W = 1200;

const DrawerHeader = ({ avatar, name, email }) => {
  return (
    <div className="mobile-menu-drawer-header">
      <img
        src={require("assets/images/placeholder.jpg")}
        className="drawer-profile-avatar"
      ></img>
      <div className="drawer-detail">
        <div className="drawer-profile-name">
          <IntlMessages id={name} />
        </div>
        <div className="drawer-profile-email">
          <IntlMessages id={email} />
        </div>
      </div>
    </div>
  );
};
const DrawerContent = (defaultOpenKeys, selectedKeys) => {
  return (
    <div className="mobile-drawer-content">
      <Menu defaultSelectedKeys={["1"]} mode="inline" theme="light">
        <Menu.Item key="1">
          <Link to="/dashboard">
            <div className="gx-drawer-item">
              <i className="material-icons">donut_small</i>
              <IntlMessages id="drawer.dashboard" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to="/dispatch">
            <div className="gx-drawer-item">
              <i className="material-icons">check_circle</i>
              <IntlMessages id="drawer.dispatch" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="3">
          <Link to="/customers">
            <div className="gx-drawer-item">
              <i className="material-icons">account_circle</i>
              <IntlMessages id="drawer.customers" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="4">
          <Link to="/sales">
            <div className="gx-drawer-item">
              <i className="material-icons">radio_button_checked</i>
              <IntlMessages id="drawer.sales" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="5">
          <Link to="/finance">
            <div className="gx-drawer-item">
              <i className="material-icons">monetization_on</i>
              <IntlMessages id="drawer.finance" />
            </div>
          </Link>
        </Menu.Item>
        <Menu.Item key="6">
          <Link to="/settings/general/profile">
            <div className="gx-drawer-item">
              <span className="img-icon-settings"></span>
              <IntlMessages id="drawer.settings" />
            </div>
          </Link>
        </Menu.Item>
      </Menu>
      <div className="mobile-drawer-bottom">
        <Tabs tabPosition="bottom" defaultActiveKey={"-1"}>
          <TabPane
            tab={
              <Link to="/settings/account">
                <span className="gx-pointer gx-d-block">
                  <Badge count={3}>
                    <i className="material-icons">notifications</i>
                  </Badge>
                </span>
                <IntlMessages id="drawer.chat" />
              </Link>
            }
            key="0"
          ></TabPane>
          <TabPane
            tab={
              <Link to="/settings/account">
                <span className="gx-pointer gx-status-pos gx-d-block">
                  <i className="material-icons">sms</i>
                </span>
                <IntlMessages id="drawer.sms" />
              </Link>
            }
            key="1"
          ></TabPane>
          <TabPane
            tab={
              <Link to="/settings/account">
                <span className="gx-pointer gx-status-pos gx-d-block">
                  <i className="material-icons">offline_bolt</i>
                </span>
                <IntlMessages id="drawer.apps" />
              </Link>
            }
            key="2"
          ></TabPane>
        </Tabs>
      </div>
    </div>
  );
};

class Topbar extends Component {
  state = {
    searchText: "",
    drawerVisible: false,
    customerNewDlgVisible: false,
    customerImportDlgVisible: false,
    isNewPopoverVisible: false
  };
  showNewCustomerDlg = () => {
    this.setState({
      customerNewDlgVisible: true,
      customerImportDlgVisible: false
    });
    this.setState({ isNewPopoverVisible: false });
  };
  onCancel = () => {
    this.setState({
      customerNewDlgVisible: false,
      customerImportDlgVisible: false
    });
  };
  onSaveCustomer = () => {
    this.setState({ customerNewDlgVisible: false });
  };
  showImportCustomerDlg = () => {
    this.setState({
      customerNewDlgVisible: false,
      customerImportDlgVisible: true
    });
  };
  onVisibleNewPopover = visible => {
    this.setState({ isNewPopoverVisible: visible });
  };
  languageMenu = () => (
    <CustomScrollbars className="gx-popover-lang-scroll">
      <ul className="gx-sub-popover">
        {languageData.map(language => (
          <li
            className="gx-media gx-pointer"
            key={JSON.stringify(language)}
            onClick={e => this.props.switchLanguage(language)}
          >
            <i className={`flag flag-24 gx-mr-2 flag-${language.icon}`} />
            <span className="gx-language-text">{language.name}</span>
          </li>
        ))}
      </ul>
    </CustomScrollbars>
  );

  updateSearchChatUser = evt => {
    this.setState({
      searchText: evt.target.value
    });
  };

  componentDidMount() {
    this.props.updateWindowWidth(window.innerWidth);
    window.addEventListener("resize", () => {
      this.props.updateWindowWidth(window.innerWidth);
    });
    window.addEventListener("click", e => {
      // if (this.newPopover && this.newPopover != e.target)
      //   this.setState({ isNewPopoverVisible: false });
    });
  }
  showDrawer = () => {
    this.setState({
      drawerVisible: true
    });
  };
  closeDrawer = () => {
    this.setState({
      drawerVisible: false
    });
  };
  render() {
    const {
      locale,
      width,
      navCollapsed,
      navStyle,
      pathname,
      currentPage,
      setCurrentPage,
      intl: { formatMessage }
    } = this.props;

    var selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split("/")[1];
    switch (pathname) {
      case "/dashboard":
        setCurrentPage("sidebar.dashboard");
        break;
      case "/customers":
        selectedKeys = "customers";
        setCurrentPage("topmenu.menu.customers");
        break;
      case "/dispatch":
        selectedKeys = "dispatch";
        setCurrentPage("topmenu.menu.dispatch");
        break;
      case "/dispatch/schedule":
        selectedKeys = "dispatch";
        setCurrentPage("topmenu.menu.dispatch.schedule");
        break;
      case "/dispatch/employees":
        selectedKeys = "dispatch";
        setCurrentPage("topmenu.menu.dispatch.employees");
        break;
      case "/dispatch/gps":
        selectedKeys = "dispatch";
        setCurrentPage("topmenu.menu.dispatch.gps");
        break;
      case "/jobs/add":
        selectedKeys = "dispatch";
        setCurrentPage("dispatch.dispatch.newjob");
        break;
      case "/dispatch/employeeprofile":
        selectedKeys = "dispatch";
        setCurrentPage("dispatch.dispatch.employee.employeeprofile");
        break;
      case "/settings/general/profile":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.general.profile");
        break;
      case "/settings/general/region_language":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.general.region_language");
        break;
      case "/settings/general/time_currency":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.general.time_currency");
        break;
      case "/settings/general/role_permission":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.general.role_permission");
        break;
      case "/settings/general/manage_users":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.general.manage_users");
        break;
      case "/settings/account/profile":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.account.profile");
        break;
      case "/settings/account/security":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.account.security");
        break;
      case "/settings/account/notifications":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.account.notifications");
        break;
      case "/settings/account/billings":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.account.billings");
        break;
      case "/settings/dispatch":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.dispatch");
        break;
      case "/settings/finance":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.finance");
        break;
      case "/settings/communication/send_receive":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.communication.send_receive");
        break;
      case "/settings/communication/email_templates":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.communication.email_templates");
        break;
      case "/settings/communication/sms_templates":
        selectedKeys = "settings";
        setCurrentPage("sidebar.settings.communication.sms_templates");
        break;
      case "/customers/profile":
        selectedKeys = "customers";
        setCurrentPage("sidebar.customers.profile");
        break;
    }
    return (
      <Auxiliary>
        <Header>
          {width < BOTTOM_MENU_W && (
            <Auxiliary>
              <div className="gx-header-title">
                <i className="material-icons" onClick={this.showDrawer}>
                  dehaze
                </i>
                <Drawer
                  className="mobile-menu-drawer"
                  title={
                    <DrawerHeader
                      avatar="assets/images/avatar.png"
                      name="Peter Jackson"
                      email="peter@email.com"
                    />
                  }
                  width={286}
                  placement="left"
                  closable={false}
                  onClose={this.closeDrawer}
                  visible={this.state.drawerVisible}
                >
                  <DrawerContent
                    defaultOpenKeys={defaultOpenKeys}
                    selectedKeys={selectedKeys}
                  />
                </Drawer>
                <IntlMessages id={currentPage} />
              </div>
            </Auxiliary>
          )}

          {width >= BOTTOM_MENU_W && (
            <Auxiliary>
              <img src={require("assets/images/logo/logo.png")} />

              <div className="gx-header-menu">
                <Menu
                  defaultOpenKeys={[defaultOpenKeys]}
                  selectedKeys={[selectedKeys]}
                  mode="horizontal"
                >
                  <Menu.Item key="dashboard">
                    <MenuItem
                      link="/dashboard"
                      icon="donut_small"
                      title="topmenu.menu.dashboard"
                    />
                  </Menu.Item>

                  <SubMenu
                    key="dispatch"
                    popupOffset={[0, 0]}
                    title={
                      <MenuItem
                        link="/dispatch"
                        icon="check_circle"
                        title="topmenu.menu.dispatch"
                      />
                    }
                  >
                    <Menu.Item key="dispatch">
                      <SubMenuItem
                        link="/dispatch"
                        icon="compass_calibration"
                        title="topmenu.menu.dispatch"
                      />
                    </Menu.Item>
                    <Menu.Item key="dispatch/schedule">
                      <SubMenuItem
                        link="/dispatch/schedule"
                        icon="event"
                        title="topmenu.menu.dispatch.schedule"
                      />
                    </Menu.Item>
                    <Menu.Item key="dispatch/employees">
                      <SubMenuItem
                        link="/dispatch/employees"
                        icon="assignment_ind"
                        title="topmenu.menu.dispatch.employees"
                      />
                    </Menu.Item>
                    <Menu.Item key="dispatch/gps">
                      <SubMenuItem
                        link="/dispatch/gps"
                        icon="near_me"
                        title="topmenu.menu.dispatch.gps"
                      />
                    </Menu.Item>
                  </SubMenu>

                  <Menu.Item key="customers">
                    <MenuItem
                      link="/customers"
                      icon="account_circle"
                      title="topmenu.menu.customers"
                    />
                  </Menu.Item>

                  <Menu.Item key="radio">
                    <MenuItem
                      link="/radio"
                      icon="radio_button_checked"
                      title="topmenu.menu.sales"
                    />
                  </Menu.Item>

                  <Menu.Item key="financial/invoice">
                    <MenuItem
                      link=""
                      icon="monetization_on"
                      title="topmenu.menu.finance"
                    />
                  </Menu.Item>

                  {/* <Menu.Item key="settings">
                    <Link to="/settings/general/profile">
                      <div className="gx-top-menu-item">
                        <div>
                          <img
                            src={require("assets/images/icon-settings-active.png")}
                          />
                          <div>
                            <IntlMessages id="topmenu.menu.settings" />
                          </div>
                        </div>
                      </div>
                    </Link>
                  </Menu.Item> */}
                </Menu>
              </div>
            </Auxiliary>
          )}

          <ul className="gx-header-notifications gx-ml-auto gx-h-100">
            {width >= MOBILE_SIZE ? (
              <Auxiliary>
                <li className="gx-notify gx-notify-search gx-d-none gx-d-md-block">
                  {/* <SearchBar width={width} /> */}
                </li>
                <li className="gx-spin gx-h-100">
                  <Popover
                    overlayClassName="gx-popover-horizantal"
                    placement="bottom"
                    content={
                      <div>
                        <div className="gx-popover-header">
                          <h3 className="gx-mb-0">My apps</h3>
                        </div>
                        <div
                          className="gx-popover-body"
                          style={{ marginTop: "60px" }}
                        >
                          <div className="gx-popover-body-content gx-p-20">
                            <img
                              src={require("assets/images/empty.png")}
                              style={{
                                display: "block",
                                margin: "auto auto 10px auto"
                              }}
                            />
                            <span
                              style={{
                                color: "#9399a2",
                                fontWeight: "500"
                              }}
                            >
                              No app is available right now
                            </span>
                          </div>
                        </div>
                      </div>
                    }
                    trigger="click"
                  >
                    <span
                      className="gx-pointer gx-status-pos gx-d-block"
                      style={{ textAlign: "center" }}
                    >
                      <i className="material-icons">apps</i>
                      <p style={{ fontSize: "12px", marginBottom: "0px" }}>
                        Apps
                      </p>
                    </span>
                  </Popover>
                </li>
                {/* <li className="gx-notify">
                  <Popover
                    overlayClassName="gx-popover-horizantal"
                    placement="bottom"
                    content={<AppNotification />}
                    trigger="click"
                  >
                    <span className="gx-pointer gx-status-pos gx-d-block">
                      <Badge count={3}>
                        <i className="material-icons">notifications</i>
                      </Badge>
                      <p style={{ fontSize: "12px", marginBottom: "0px" }}>
                        Alert
                      </p>
                    </span>
                  </Popover>
                </li> */}

                <li className="gx-msg gx-h-100">
                  <Popover
                    overlayClassName="gx-popover-horizantal"
                    placement="bottom"
                    content={<MailNotification />}
                    trigger="click"
                  >
                    <span className="gx-pointer gx-status-pos gx-d-block">
                      <Badge count={2}>
                        <i className="material-icons">chat</i>
                      </Badge>
                      <p style={{ fontSize: "12px", marginBottom: "0px" }}>
                        Chat
                      </p>
                    </span>
                  </Popover>
                </li>
                <li className="gx-spin gx-h-100">
                  <Link to="/settings/general/profile">
                    <span
                      className="gx-pointer gx-status-pos gx-d-block gx-flex-column gx-align-items-center"
                      style={{
                        height: "39px",
                        justifyContent: "space-between"
                      }}
                    >
                      <img
                        src={require("assets/images/icon-settings-active.png")}
                        style={{ width: "19px" }}
                      />
                      <p style={{ fontSize: "12px", marginBottom: "0px" }}>
                        Settings
                      </p>
                    </span>
                  </Link>
                </li>
                {/* <li className="gx-spin">
                  <Popover
                    overlayClassName="gx-popover-horizantal"
                    placement="bottomRight"
                    content={<div></div>}
                    trigger="click"
                  >
                    <span className="gx-pointer gx-status-pos gx-d-block">
                      <i className="material-icons">sms</i>
                      <p style={{ fontSize: "12px", marginBottom: "0px" }}>
                        SMS
                      </p>
                    </span>
                  </Popover>
                </li> */}
                <li className="gx-new gx-h-100">
                  <Popover
                    overlayClassName="gx-popover-horizantal gx-nav-new-popover"
                    placement="bottomRight"
                    visible={this.state.isNewPopoverVisible}
                    content={
                      <PopupNew showNewCustomerDlg={this.showNewCustomerDlg} />
                    }
                    ref={node => (this.newPopover = node)}
                    trigger="click"
                    onVisibleChange={this.onVisibleNewPopover}
                  >
                    <Button
                      className="gx-nav-btn gx-nav-new-btn gx-mb-0"
                      style={{
                        width: "100px",
                        backgroundColor: "#6ab4ff",
                        border: "0"
                      }}
                    >
                      <div className="gx-div-align-center">
                        <i className="material-icons gx-fs-xl gx-mr-2">add</i>
                        <IntlMessages id="New" />
                      </div>
                    </Button>
                  </Popover>
                </li>
                <li className="gx-user-nav">
                  <UserInfo />
                </li>
              </Auxiliary>
            ) : (
              <Auxiliary>
                <li className="gx-notify">
                  <Popover
                    overlayClassName="gx-popover-horizantal"
                    placement="bottom"
                    content={<AppNotification />}
                    trigger="click"
                  >
                    <span className="gx-pointer gx-d-block">
                      <Badge count={3}>
                        <i className="material-icons">notifications</i>
                      </Badge>
                    </span>
                  </Popover>
                </li>
                <li className="gx-new">
                  <Popover
                    overlayClassName="gx-popover-horizantal"
                    placement="bottomRight"
                    content={<PopupNew />}
                    trigger="click"
                  >
                    <Button
                      className="gx-mb-0 gx-no-box-shadow gx-nav-btn gx-nav-new-btn"
                      shape="circle"
                      size="small"
                      icon="plus"
                    />
                  </Popover>
                </li>
              </Auxiliary>
            )}
          </ul>
        </Header>
        <Modal
          className="gx-ss-customers-new-modal"
          title={
            <div className="gx-flex-row gx-w-100 gx-justify-content-between gx-align-items-center">
              <div className="gx-customized-modal-title">Add new customer</div>
              <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                <Button
                  className="gx-customized-button gx-d-none gx-d-md-block notDisplayOnMobile"
                  onClick={() => {
                    this.showImportCustomerDlg();
                  }}
                >
                  <div className="gx-div-align-center">
                    <i
                      className="material-icons gx-fs-xl gx-mr-2"
                      style={{ marginLeft: "-6px" }}
                    >
                      publish
                    </i>
                    <IntlMessages id="customer.customerdlg.importcontacts" />
                  </div>
                </Button>
                <Button
                  className="gx-customized-button gx-d-md-none notDisplayOnMobile"
                  onClick={() => {
                    this.showImportCustomerDlg();
                  }}
                >
                  <div className="gx-div-align-center">
                    <i
                      className="material-icons gx-fs-xl gx-mr-2"
                      style={{ marginLeft: "-6px" }}
                    >
                      publish
                    </i>
                    <IntlMessages id="customer.customerdlg.import" />
                  </div>
                </Button>
                <i
                  className="material-icons gx-customized-modal-close"
                  onClick={this.onCancel.bind(this)}
                >
                  clear
                </i>
              </div>
            </div>
          }
          closable={false}
          wrapClassName="gx-customized-modal vertical-center-modal"
          visible={this.state.customerNewDlgVisible}
          onCancel={this.onCancel.bind(this)}
          // width={ width >= 1144 ? 1084 : width - 60 }
          width={
            (width >= 1144 && 1084) ||
            (width >= 500 && width - 60) ||
            width - 30
          }
        >
          <EditCustomerDlg
            onCancel={this.onCancel.bind(this)}
            onSave={this.onSaveCustomer.bind(this)}
          />
        </Modal>
        <Modal
          title={
            <div className="gx-flex-row gx-w-100 gx-justify-content-between gx-align-items-center">
              <div className="gx-customized-modal-title">
                <IntlMessages id="customer.customerdlg.importcustomers" />
              </div>
              <div className="gx-flex-row gx-flex-nowrap gx-align-items-center">
                <Button
                  className="gx-customized-button gx-d-none gx-d-md-block"
                  onClick={() => {
                    this.showNewCustomerDlg();
                  }}
                >
                  <div className="gx-div-align-center">
                    <i
                      className="material-icons gx-fs-xl gx-mr-2"
                      style={{ marginLeft: "-6px" }}
                    >
                      publish
                    </i>
                    <IntlMessages id="customer.customerdlg.addmanually" />
                  </div>
                </Button>
                <i
                  className="material-icons gx-customized-modal-close"
                  onClick={this.onCancel.bind(this)}
                >
                  clear
                </i>
              </div>
            </div>
          }
          closable={false}
          wrapClassName="gx-customized-modal vertical-center-modal"
          visible={this.state.customerImportDlgVisible}
          onCancel={this.onCancel.bind(this)}
          width={
            width >= TAB_SIZE ? 830 : width >= MOBILE_SIZE ? 710 : width - 40
          }
        >
          <ImportCustomerDlg
            onCancel={this.onCancel.bind(this)}
            onSave={this.onSaveCustomer.bind(this)}
          />
        </Modal>
        {navStyle === NAV_STYLE_DRAWER ||
        ((navStyle === NAV_STYLE_FIXED ||
          navStyle === NAV_STYLE_MINI_SIDEBAR) &&
          width < BOTTOM_MENU_W) ? (
          <BottomMenu />
        ) : null}
      </Auxiliary>
    );
  }
}

const mapStateToProps = ({ settings }) => {
  const {
    locale,
    navStyle,
    navCollapsed,
    width,
    pathname,
    currentPage
  } = settings;
  return { locale, navStyle, navCollapsed, width, pathname, currentPage };
};

export default connect(mapStateToProps, {
  switchLanguage,
  setCurrentPage,
  updateWindowWidth
})(injectIntl(Topbar));
