import React from "react";
import { Card, Button, Carousel, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import {
  hideMessage,
  showAuthLoader,
  userForgotPassword
} from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class ForgotPassword extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //this.props.showAuthLoader();
        console.log(values);
        this.props.userForgotPassword(values);
        this.props.history.push("/forgot_password_success");
      }
    });
  };
  componentDidMount() {
    this.props.hideMessage();
    if (this.props.userInfo != null)
      this.props.form.setFieldsValue({ email: this.props.userInfo.email });
    else this.props.history.push("/signin");
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { showMessage, loader, alertMessage } = this.props;
    return (
      <div className="gx-app-auth-wrap gx-app-forgot-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-forgot-main-container">
          <div className="gx-app-forgot-content">
            <div className="gx-app-forgot-form">
              <Card className="gx-card">
                <Form
                  onSubmit={this.handleSubmit}
                  className="gx-forgot-form gx-form-row0"
                >
                  <div className="forgot_desc">
                    <img
                      src={require("assets/images/auth/forgot_password_img.png")}
                    />
                    <h1>Forgot your password?</h1>
                    <p>
                      No worries! Enter your email address and we will send you
                      the password reset instruction to your inbox.
                    </p>
                  </div>
                  {showMessage ? (
                    <span className="gx-error-msg gx-text-center gx-d-block">
                      {alertMessage.toString()}
                    </span>
                  ) : (
                    <div />
                  )}
                  <FormItem>
                    <label>
                      Email address <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("email", {
                      initialValue: "peterjackson@gmail.com",
                      rules: [
                        {
                          required: true,
                          type: "email",
                          message: "The input is not valid E-mail!"
                        }
                      ]
                    })(<Input placeholder="Enter email" readOnly={true} />)}
                  </FormItem>
                  <FormItem>
                    <Button
                      type="primary"
                      className="gx-mb-0 gx-w-100"
                      htmlType="submit"
                    >
                      <IntlMessages id="userAuth.reset_password" />
                    </Button>
                  </FormItem>
                </Form>
              </Card>
              <span>
                Go back to <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
        </div>
      </div>
    );
  }
}

const WrappedNormalForgotPasswordForm = Form.create()(ForgotPassword);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser, userInfo } = auth;
  return { loader, alertMessage, showMessage, authUser, userInfo };
};

export default connect(mapStateToProps, {
  userForgotPassword,
  hideMessage,
  showAuthLoader
})(WrappedNormalForgotPasswordForm);
