import React from "react";
import { Card, Button, Carousel, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import { hideMessage, showAuthLoader, userResetPassword } from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class ResetPassword extends React.Component {
  state = {
    confirmDirty: false
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //this.props.showAuthLoader();
        console.log(values);
        console.log(this.props.location.query.oobCode);
        // this.props.userResetPassword({
        //   old_password: values.old_password,
        //   new_password: values.new_password,
        //   oobc: this.props.location.query.oobCode,
        // });
      }
    });
  };
  componentDidMount(){
    this.props.hideMessage();
    // if(this.props.userInfo==null)
    //   this.props.history.push("/signin");
  }
  onConfirmPasswordChange = e=>{
    const {value} = e.target;
    this.setState({confirmDirty: this.state.confirmDirty || !!value});
  }
  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("new_password")) {
      callback("Two passwords that you enter are inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { showMessage, loader, alertMessage } = this.props;
    return (
      <div className="gx-app-auth-wrap gx-app-forgot-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-forgot-main-container">
          <div className="gx-app-forgot-content">
            <div className="gx-app-forgot-form">
              <Card className="gx-card">
                <Form
                    onSubmit={this.handleSubmit}
                    className="gx-forgot-form gx-form-row0"
                  >
                  <div className="forgot_desc">
                    <img
                      src={require("assets/images/auth/forgot_password_img.png")}
                    />
                    <h1>Reset your password now</h1>
                    <p>
                      No worries! Enter your email address and we will send you
                      the password reset instruction to your inbox.
                    </p>
                  </div>
                  {showMessage 
                    ? (
                        <span className="gx-error-msg gx-text-center gx-d-block">
                          {alertMessage.toString()}
                        </span>
                      ) 
                    : <div/>
                  }
                  {/* <FormItem>
                    <label>
                      Email address <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          type: "email",
                          message: "The input is not valid E-mail!"
                        }
                      ]
                    })(<Input placeholder="Enter email" readOnly={true}/>)}
                  </FormItem> */}
                  <FormItem>
                    <label>
                      Old password <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("old_password", {
                      rules: [
                        {
                          required: true,
                          message: "The input is not valid E-mail!"
                        }
                      ]
                    })(<Input placeholder="Enter password" type="password"/>)}
                  </FormItem>
                  <FormItem>
                    <label>
                      New password <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("new_password", {
                      rules: [
                        {
                          required: true,
                          message: "This password is required!"
                        },
                        {
                          validator: this.validateToNextPassword
                        }
                      ]
                    })(<Input placeholder="Enter password" type="password"/>)}
                  </FormItem>
                  <FormItem>
                    <label>
                      Confirm password <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("confirm", {
                      rules: [
                        {
                          required: true,
                          message: "This password is required!"
                        },
                        {
                          validator: this.compareToFirstPassword
                        }
                      ]
                    })(<Input placeholder="Enter password" type="password" onChange={this.onConfirmPasswordChange}/>)}
                  </FormItem>
                  <FormItem>
                    <Button
                      type="primary"
                      className="gx-mb-0 gx-w-100"
                      htmlType="submit"
                    >
                      <IntlMessages id="userAuth.reset_password" />
                    </Button>
                  </FormItem>
                </Form>
              </Card>
              <span>
                Go back to <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}  
          {showMessage ? message.error(alertMessage.toString()) : null}
        </div>
      </div>
    );
  }
}

const WrappedNormalResetPasswordForm = Form.create()(ResetPassword);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser, userInfo } = auth;
  return { loader, alertMessage, showMessage, authUser, userInfo };
};

export default connect(
  mapStateToProps,
  {
    userResetPassword,
    hideMessage,
    showAuthLoader
  }
)(WrappedNormalResetPasswordForm);
