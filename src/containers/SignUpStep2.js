import React from "react";
import { Card, Button, Popover, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import { hideMessage, showAuthLoader } from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";
/* global google */

const FormItem = Form.Item;

class SignUpStep2 extends React.Component {
  state = {
    screenHeight: 0,
    screenWidth: 0,
    address: ""
  };
  constructor(props) {
    super(props);
    this.autocompleteAddress = React.createRef();
    this.company_address = null;
    this.handlePlaceChanged = this.handlePlaceChanged.bind(this);
  }

  updateDimensions = () => {
    this.setState({
      screenHeight: window.innerHeight,
      screenWidth: window.innerWidth
    });
  };
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);

    this.company_address = new google.maps.places.Autocomplete(
      document.getElementById("company_address"),
      { types: ["geocode"] }
    );
    this.company_address.addListener("place_changed", this.handlePlaceChanged);
  }
  handlePlaceChanged() {
    const place = this.company_address.getPlace();
    this.props.form.setFieldsValue({
      company_address: place.formatted_address
    });
    // this.setState({ address: place.formatted_address });
  }
  handleChange = e => {
    this.setState({ address: e.target.value });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //this.props.showAuthLoader();
        //this.props.userSignUpStep2(values);
        let path = "/signup/step_3";
        this.props.history.push(path);
      }
    });
  };
  render() {
    const { showMessage, loader, alertMessage } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="gx-app-auth-wrap gx-app-signupstep-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-signupstep-main-container">
          <div className="gx-app-signupstep-content">
            <div className="gx-app-signupstep-form">
              <Card className="gx-card">
                <div className="div-step div-step-2">
                  <div className="step-bar">
                    <div className="step step-2"></div>
                  </div>
                  <label>Step 2 of 3</label>
                </div>
                <h1>Next, tell us a bit about your business</h1>
                <Form
                  onSubmit={this.handleSubmit}
                  className="gx-signupstep-form gx-form-row0"
                >
                  <FormItem>
                    <label>
                      Company name <span style={{ color: "red" }}>*</span>
                    </label>
                    <Popover
                      trigger="focus"
                      placement={
                        this.state.screenWidth <= 768 ? "top" : "right"
                      }
                      overlayClassName="signup-form-popover"
                      content={
                        <div className="form-popover-content">
                          Please write down your company name
                        </div>
                      }
                    >
                      {getFieldDecorator("company_name", {
                        rules: [
                          {
                            required: true,
                            type: "string",
                            message: "Please write full legal company name"
                          }
                        ]
                      })(<Input placeholder="Enter name" />)}
                    </Popover>
                  </FormItem>
                  <FormItem>
                    <label>
                      Company address <span style={{ color: "red" }}>*</span>
                    </label>
                    <Popover
                      trigger="focus"
                      placement={
                        this.state.screenWidth <= 768 ? "top" : "right"
                      }
                      overlayClassName="signup-form-popover"
                      content={
                        <div className="form-popover-content">
                          Please write down your company address
                        </div>
                      }
                    >
                      {getFieldDecorator("company_address", {
                        rules: [
                          {
                            required: true,
                            type: "string",
                            message: "Please write company address"
                          }
                        ],
                        onChange: this.handleChange
                      })(
                        <Input
                          ref={this.autocompleteAddress}
                          placeholder="Enter address"
                        />
                      )}
                    </Popover>
                  </FormItem>
                  <FormItem>
                    <label>
                      Zip code <span style={{ color: "red" }}>*</span>
                    </label>
                    <Popover
                      trigger="focus"
                      placement={
                        this.state.screenWidth <= 768 ? "top" : "right"
                      }
                      overlayClassName="signup-form-popover"
                      content={
                        <div className="form-popover-content">
                          Please write down your company zip code
                        </div>
                      }
                    >
                      {getFieldDecorator("company_zipcode", {
                        rules: [
                          {
                            required: true,
                            type: "string",
                            message: "Please write company zip code"
                          }
                        ]
                      })(<Input placeholder="Enter code" />)}
                    </Popover>
                  </FormItem>
                  <FormItem>
                    <Button htmlType="submit">Continue</Button>
                  </FormItem>
                </Form>
              </Card>
              <span>
                Already have an account? <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}
          {showMessage ? message.error(alertMessage.toString()) : null}
        </div>
      </div>
    );
  }
}

const WrappedNormalSignUpStep2Form = Form.create()(SignUpStep2);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser };
};

export default connect(
  mapStateToProps,
  {
    //userSignUpStep2,
    hideMessage,
    showAuthLoader
  }
)(WrappedNormalSignUpStep2Form);
