import React from "react";
import { Modal, Card, Button, Popover, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import CircularProgress from "components/CircularProgress/index";
import { hideMessage, showAuthLoader, userSignUp } from "appRedux/actions/Auth";

const FormItem = Form.Item;

class SignUpStep3 extends React.Component {
  startPosition = 0;
  endPosition = window.innerWidth * (1657 / 1995);
  refreshIntervalId = 0;
  state = {
    EmployeeList: [
      { name: "", role: "" },
      { name: "", role: "" },
      { name: "", role: "" },
      { name: "", role: "" }
    ],
    EmployeeListLength: 4,
    isLoading: false,
    loadingPosition: this.startPosition,
    screenWidth: window.innerWidth
  };

  constructor(props, context) {
    super(props, context);

    this.updatePosition = this.updatePosition.bind(this);
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("submit");
        //this.props.showAuthLoader();
        //this.props.userSignUp(values);
        //let path = "/dashboard";
        //this.props.history.push(path);

        this.setState({ isLoading: true });
        this.refreshIntervalId = setInterval(this.updatePosition, 10);
      }
    });
  };
  componentDidMount() {
    this.setState({ screenWidth: window.innerWidth });
  }
  componentWillUnmount() {
    clearInterval(this.refreshIntervalId);
  }
  updatePosition() {
    if (this.state.loadingPosition <= this.endPosition - 182)
      this.setState({ loadingPosition: this.state.loadingPosition + 3 });
    else {
      clearInterval(this.refreshIntervalId);
      let path = "/dashboard";
      this.props.history.push(path);
    }
  }
  addEmployee = () => {
    this.setState({ EmployeeListLength: this.state.EmployeeListLength + 1 });
    this.setState({
      EmployeeList: [...this.state.EmployeeList, { name: "", role: "" }]
    });
  };
  onEmployeeNameChange = (index, e) => {
    let temp = this.state.EmployeeList.slice();
    temp[index].name = e.target.value;
    this.setState({ EmployeeList: temp });
  };
  onEmployeeRoleChange = (index, e) => {
    let temp = this.state.EmployeeList.slice();
    temp[index].role = e.target.value;
    this.setState({ EmployeeList: temp });
  };
  render() {
    const { showMessage, loader, alertMessage } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="gx-app-auth-wrap gx-app-signupstep-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-signupstep-main-container">
          <div className="gx-app-signupstep-content">
            <div className="gx-app-signupstep-form">
              <Card className="gx-card">
                <div className="div-step div-step-3">
                  <div className="step-bar">
                    <div className="step step-3"></div>
                  </div>
                  <label>Step 3 of 3</label>
                </div>
                <h1>Now, start adding your team</h1>
                <Form
                  onSubmit={this.handleSubmit}
                  className="gx-signupstep-form gx-form-row0"
                >
                  {/* <div className="admin-avatar-container">
                    <div className="admin-avatar">
                      <div className="admin-avatar-circle">P</div>
                      <div className="admin-detail">
                        <span className="admin-name">Peter Jackson</span>
                        <span className="admin-role">Administrator</span>
                      </div>
                    </div>
                    <i className="material-icons admin-avatar-edit">edit</i>
                  </div> */}
                  {this.state.EmployeeList.map((element, index) => (
                    <FormItem key={index}>
                      <div className="employee-name">
                        <label>Employee name</label>
                        <Input
                          placeholder="Full name"
                          onChange={e => this.onEmployeeNameChange(index, e)}
                        />
                      </div>
                      <div className="employee-role">
                        <label>Role</label>
                        <Input
                          placeholder="Enter"
                          onChange={e => this.onEmployeeRoleChange(index, e)}
                        />
                      </div>
                    </FormItem>
                  ))}
                  <Link
                    to="#"
                    className="btn-add-employee"
                    onClick={this.addEmployee}
                  >
                    Add more employee
                  </Link>
                  <FormItem>
                    <Button htmlType="submit">Add team</Button>
                  </FormItem>
                  <p>
                    Don't worry! We won't contact anyone until you are ready.
                  </p>
                </Form>
              </Card>
              <span>
                Already have an account? <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}
          {showMessage ? message.error(alertMessage.toString()) : null}
        </div>
        <Modal
          className="modal-loading"
          closable={false}
          centered
          visible={this.state.isLoading}
          footer={[null, null]}
        >
          <div className="loading-screen">
            <img
              id="img-loading-bg"
              src={require("assets/images/auth/loading_bg_img.png")}
            />
            <img
              id="img-loading-car"
              src={require("assets/images/auth/loading_car.png")}
              style={{ left: this.state.loadingPosition + "px" }}
            />
            <img
              id="img-loading-destination"
              src={require("assets/images/auth/loading_destination.png")}
              style={{ left: this.endPosition - 18 + "px" }}
            />
          </div>
        </Modal>
      </div>
    );
  }
}

const WrappedNormalSignUpStep3Form = Form.create()(SignUpStep3);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser };
};

export default connect(
  mapStateToProps,
  {
    userSignUp,
    hideMessage,
    showAuthLoader
  }
)(WrappedNormalSignUpStep3Form);
