import React from "react";
import { Card, Button, Popover, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import { hideMessage, showAuthLoader, userSignUp } from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";
import firebase from "firebase";

const FormItem = Form.Item;

class SignUpStep1 extends React.Component {
  state = {
    screenHeight: 0,
    screenWidth: 0,
    showPassword: false,
    full_name: "",
    email: "",
    password: "",
    mobile_number: ""
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //this.props.showAuthLoader();
        this.props.userSignUp(values);
        let path = "/signup/step_2";
        this.props.history.push(path);
      }
    });
  };
  updateDimensions = () => {
    this.setState({
      screenHeight: window.innerHeight,
      screenWidth: window.innerWidth
    });
  };
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }
  onFullNameChange = e=>{
    this.setState({full_name: e.target.value});
  }
  onEmailChange = e=>{
    this.setState({email: e.target.value});
    this.props.form.setFieldsValue({email: e.target.value});
  }
  onPasswordChange = e => {
    this.setState({ password: e.target.value });
    this.props.form.setFieldsValue({password: e.target.value});
  };
  setVisiblePassword = e => {
    this.setState({ showPassword: !this.state.showPassword });
    this.passwordInput.focus();
  };
  render() {
    const { showMessage, loader, alertMessage } = this.props;
    const { getFieldDecorator } = this.props.form;
    const mobileFormatter = e =>
      e.target.value.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3");
    return (
      <div className="gx-app-auth-wrap gx-app-signupstep-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-signupstep-main-container">
          <div className="gx-app-signupstep-content">
            <div className="gx-app-signupstep-form">
              <Card className="gx-card">
                <div className="div-step div-step-1">
                  <div className="step-bar">
                    <div className="step step-1"></div>
                  </div>
                  <label>Step 1 of 3</label>
                </div>
                <h1>First, tell us about you</h1>
                <Form
                  onSubmit={this.handleSubmit}
                  className="gx-signupstep-form gx-form-row0"
                >
                  <FormItem>
                    <label>
                      Full name <span style={{ color: "red" }}>*</span>
                    </label>
                    <Popover
                      trigger="focus"
                      placement={
                        this.state.screenWidth <= 768 ? "top" : "right"
                      }
                      overlayClassName="signup-form-popover"
                      content={
                        <div className="form-popover-content">
                          Please write down your full legal name
                        </div>
                      }
                    >
                      {getFieldDecorator("full_name", {
                        rules: [
                          {
                            required: true,
                            type: "string",
                            message: "Please write your full legal name"
                          }
                        ]
                      })(<Input placeholder="Enter name" />)}
                    </Popover>
                  </FormItem>
                  <FormItem>
                    <label>
                      Email address <span style={{ color: "red" }}>*</span>
                    </label>
                    <Popover
                      trigger="focus"
                      placement={
                        this.state.screenWidth <= 768 ? "top" : "right"
                      }
                      overlayClassName="signup-form-popover"
                      content={
                        <div className="form-popover-content">
                          Please write down your email address
                        </div>
                      }
                    >
                      {getFieldDecorator("email", {
                        rules: [
                          {
                            required: true,
                            type: "email",
                            message: "Please provide a valid email address"
                          }
                        ]
                      })(<Input placeholder="Enter email" onChange={this.onEmailChange}/>)}
                    </Popover>
                  </FormItem>
                  <FormItem>
                    <label>
                      Password <span style={{ color: "red" }}>*</span>
                    </label>
                    <Popover
                      trigger="focus"
                      placement={
                        this.state.screenWidth <= 768 ? "top" : "right"
                      }
                      overlayClassName="signup-form-popover"
                      content={
                        <div className="form-popover-content">
                          Minimum 6 digits and special characters can be used
                        </div>
                      }
                    >
                      {getFieldDecorator("password", {
                        rules: [
                          {
                            required: true,
                            type: "string",
                            message: "You must use a password to continue"
                          }
                        ]
                      })(
                        <Input
                          type={
                            this.state.showPassword == true
                              ? "text"
                              : "password"
                          }
                          ref={d => (this.passwordInput = d)}
                          placeholder="Enter password"
                          onChange={this.onPasswordChange}
                          suffix={
                            <i
                              className={
                                this.state.showPassword
                                  ? "material-icons icon-show-password"
                                  : "material-icons"
                              }
                              onClick={this.setVisiblePassword}
                            >
                              {this.state.showPassword
                                ? "visibility"
                                : "visibility_off"}
                            </i>
                          }
                        />
                      )}
                    </Popover>
                  </FormItem>
                  <FormItem>
                    <label>
                      Mobile number <span style={{ color: "red" }}>*</span>
                    </label>
                    <Popover
                      trigger="focus"
                      placement={
                        this.state.screenWidth <= 768 ? "top" : "right"
                      }
                      overlayClassName="signup-form-popover"
                      content={
                        <div className="form-popover-content">
                          Please write down your mobile number
                        </div>
                      }
                    >
                      {getFieldDecorator("phone_number", {
                        rules: [
                          {
                            required: true,
                            message: "Please provide a valid phone number"
                          }
                        ],
                        getValueFromEvent: mobileFormatter
                      })(<Input placeholder="Enter number" maxLength={14} />)}
                    </Popover>
                  </FormItem>
                  <FormItem>
                    <Button htmlType="submit">Continue</Button>
                  </FormItem>
                  <p>
                    By clicking continue you are agreeing to the
                    <br />
                    <Link to="#">terms of service</Link> and{" "}
                    <Link to="#">privacy policy</Link>
                  </p>
                </Form>
              </Card>
              <span>
                Already have an account? <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}
          {showMessage ? message.error(alertMessage.toString()) : null}
        </div>
      </div>
    );
  }
}

const WrappedNormalSignUpStep1Form = Form.create()(SignUpStep1);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser };
};

export default connect(
  mapStateToProps,
  {
    userSignUp,
    hideMessage,
    showAuthLoader
  }
)(WrappedNormalSignUpStep1Form);
