import React from "react";
import { Card, Button, Carousel, Form, Input, message, Avatar } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import {
  hideMessage,
  showAuthLoader,
  userCheckWithEmail,
  userSignIn
} from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class SignIn extends React.Component {
  refreshIntervalId = 0;

  state = {
    slider_index: 0,
    sliderHeight: window.innerHeight - 60 + "px",
    sliderWidth: window.innerWidth / 2 + "px",
    user_name: "",
    email: "",
    password: ""
  };

  constructor(props, context) {
    super(props, context);

    this.toggleSlider = this.toggleSlider.bind(this);
  }
  updateDimensions = () => {
    this.setState(
      {
        sliderHeight: window.innerHeight - 60 + "px",
        sliderWidth: window.innerWidth / 2 + "px"
      },
      () => {
        this.sliderDiv1.style.height = this.state.sliderHeight;
        this.sliderDiv1.style.backgroundColor = "#257cde";
        this.sliderDiv1.style.display = "flex";
        this.sliderDiv1.style.flexDirection = "column";
        this.sliderDiv2.style.height = this.state.sliderHeight;
        this.sliderDiv2.style.backgroundColor = "#08bdc5";
        this.sliderDiv2.style.display = "flex";
        this.sliderDiv2.style.flexDirection = "column";
        this.sliderDiv3.style.height = this.state.sliderHeight;
        this.sliderDiv3.style.backgroundColor = "#4c586d";
        this.sliderDiv3.style.display = "flex";
        this.sliderDiv3.style.flexDirection = "column";

        this.slice1.style.borderLeft =
          this.state.sliderWidth + " solid transparent";
        this.slice2.style.borderRight =
          this.state.sliderWidth + " solid transparent";
        this.slice3.style.borderLeft =
          this.state.sliderWidth + " solid transparent";
      }
    );
  };
  handleEmailFormSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.userCheckWithEmail(values);
        console.log(this.props);
      }
    });
  };
  handlePasswordFormSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.userSignIn({
          email: this.state.email,
          password: this.state.password
        });
      }
    });
  };
  componentDidMount() {
    this.props.hideMessage();

    this.refreshIntervalId = setInterval(this.toggleSlider, 15000);
    this.updateDimensions();
    this.sliderDiv1.style.height = this.state.sliderHeight;
    this.sliderDiv1.style.backgroundColor = "#257cde";
    this.sliderDiv1.style.display = "flex";
    this.sliderDiv1.style.flexDirection = "column";
    this.sliderDiv2.style.height = this.state.sliderHeight;
    this.sliderDiv2.style.backgroundColor = "#08bdc5";
    this.sliderDiv2.style.display = "flex";
    this.sliderDiv2.style.flexDirection = "column";
    this.sliderDiv3.style.height = this.state.sliderHeight;
    this.sliderDiv3.style.backgroundColor = "#4c586d";
    this.sliderDiv3.style.display = "flex";
    this.sliderDiv3.style.flexDirection = "column";

    this.slice1.style.borderLeft =
      this.state.sliderWidth + " solid transparent";
    this.slice2.style.borderRight =
      this.state.sliderWidth + " solid transparent";
    this.slice3.style.borderLeft =
      this.state.sliderWidth + " solid transparent";
    window.addEventListener("resize", this.updateDimensions);
  }
  componentWillUnmount() {
    clearInterval(this.refreshIntervalId);
    window.removeEventListener("resize", this.updateDimensions);
  }
  toggleSlider() {
    this.setState({ slider_index: (this.state.slider_index + 1) % 3 });
    this.carousel.goTo(this.state.slider_index, false);
  }
  componentDidUpdate() {
    if (this.props.authUser !== null) {
      this.props.history.push("/dashboard");
    }
  }
  onEmailChange = e => {
    this.setState({ email: e.target.value });
    this.props.hideMessage();
    this.props.form.setFieldsValue({ email: e.target.value });
  };
  onPasswordChange = e => {
    this.setState({ password: e.target.value });
    this.props.form.setFieldsValue({ password: e.target.value });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { showMessage, loader, alertMessage } = this.props;
    const props = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      dotPosition: "left",
      effect: "fade"
    };
    console.log(this.props.isUserEmailExist);
    return (
      <div className="gx-app-auth-wrap gx-app-login-wrap">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-login-main-container">
          <div className="gx-app-login-carousel">
            <Carousel
              ref={node => {
                this.carousel = node;
              }}
              {...props}
              className="gx-app-login-carousel-content"
            >
              <div key={1} ref={c => (this.sliderDiv1 = c)}>
                <div className="slider-image">
                  <img src={require("assets/images/slider/slider1.jpg")} />
                  <div
                    className="triangle-slice-1"
                    ref={d => (this.slice1 = d)}
                  ></div>
                </div>
                <div className="slider-description slider-description-1">
                  <div className="desc-content-body">
                    <div className="desc-content">
                      <div>Popular feature</div>
                      <h1>Live chatting with employees</h1>
                      <p>
                        Employees can chat directly and in real-time with
                        administrator. Moved for without. Sea own creepeth can't
                        also land may after two you itself first Herb bring
                        first fruit appear lesser beast fish two whose rule face
                        lights had us. Give fowl tree saying living.
                      </p>
                    </div>
                    <div className="desc-footer">
                      <Link to="/pricing">
                        Available on Team, Business and Enterprise plan
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
              <div key={2} ref={c => (this.sliderDiv2 = c)}>
                <div className="slider-image">
                  <img src={require("assets/images/slider/slider2.jpg")} />
                  <div
                    className="triangle-slice-2"
                    ref={d => (this.slice2 = d)}
                  ></div>
                </div>
                <div className="slider-description slider-description-2">
                  <div className="desc-content-body">
                    <div className="desc-content">
                      <div>Popular feature</div>
                      <h1>Live GPS Tracking</h1>
                      <p>
                        Providing GPS tracking and management to help thousands
                        of customers save time and money by keeping track of
                        their working employees and vehicles positions, LIVE.
                        Our internet-based tracking system gives you full
                        access, full control, anywhere from any device.
                      </p>
                    </div>
                    <div className="desc-footer">
                      <Link to="/pricing">Available on Business plan</Link>
                    </div>
                  </div>
                </div>
              </div>
              <div key={3} ref={c => (this.sliderDiv3 = c)}>
                <div className="slider-image">
                  <img src={require("assets/images/slider/slider3.jpg")} />
                  <div
                    className="triangle-slice-3"
                    ref={d => (this.slice3 = d)}
                  ></div>
                </div>
                <div className="slider-description slider-description-3">
                  <div className="desc-content-body">
                    <div className="desc-content">
                      <div>Popular feature</div>
                      <h1>Employee time tracking</h1>
                      <p>
                        Track time continuosly using our top-notch time clock
                        app. Moved for without. Sea own creepeth can't also land
                        may after two you itself first Herb bring first fruit
                        appear lesser beast fish two whose rule face lights had
                        us. Give fowl tree saying living.
                      </p>
                    </div>
                    <div className="desc-footer">
                      <Link to="/pricing">
                        Available on Team, Business and Enterprise plan
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </Carousel>
          </div>
          <div className="gx-app-login-content">
            <div className="gx-app-login-form">
              <Card className="gx-card">
                {!this.props.isUserEmailExist ? (
                  <Form
                    onSubmit={this.handleEmailFormSubmit}
                    className={`gx-signin-email-form gx-form-row0`}
                  >
                    <FormItem>
                      <Avatar
                        className="gx-size-80 gx-m-auto gx-circle"
                        style={{ backgroundColor: "#e9e9e9" }}
                      >
                        <img
                          src={require("assets/images/logo/logo_icon_42x36.png")}
                        />
                      </Avatar>
                    </FormItem>
                    <div className="ant-row ant-form-item gx-flex-column gx-text-center">
                      <span
                        style={{
                          fontSize: "24px",
                          color: "#1a2f52",
                          fontWeight: "700",
                          marginBottom: "5px"
                        }}
                      >
                        <IntlMessages id="userAuth.signIn" />
                      </span>
                      {/* <ul className="gx-social-link">
                      <li>
                        <Button
                          onClick={() => {
                            this.props.showAuthLoader();
                            this.props.userFacebookSignIn();
                          }}
                        >
                          <span className="btn-login-facebook"></span>
                        </Button>
                      </li>
                      <li>
                        <Button
                          onClick={() => {
                            this.props.showAuthLoader();
                            this.props.userGoogleSignIn();
                          }}
                        >
                          <span className="btn-login-google"></span>
                        </Button>
                      </li>
                      <li>
                        <Button
                          onClick={() => {
                            this.props.showAuthLoader();
                            this.props.userTwitterSignIn();
                          }}
                        >
                          <span className="btn-login-twitter"></span>
                        </Button>
                      </li>
                    </ul> */}
                      <h4 style={{ color: "#4c586d" }}>
                        to your jobpatcher account
                      </h4>
                    </div>

                    {showMessage ? (
                      <FormItem className="gx-mb-0 gx-text-center">
                        <span className="gx-error-msg">
                          {this.props.alertMessage.toString()}
                        </span>
                      </FormItem>
                    ) : (
                      <div />
                    )}
                    <FormItem>
                      {getFieldDecorator("email", {
                        validateTrigger: "onSubmit",
                        rules: [
                          {
                            required: true,
                            message: "The email address is required!"
                          },
                          {
                            type: "email",
                            message: "Please input valid email address!"
                          }
                        ]
                      })(
                        <Input
                          placeholder="Enter email"
                          onChange={this.onEmailChange}
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      <Button
                        className="gx-mb-0 gx-w-100"
                        type="primary"
                        htmlType="submit"
                      >
                        <IntlMessages id="userAuth.signIn.next" />
                      </Button>
                    </FormItem>
                    <span>
                      Don't have an account?{" "}
                      <Link to="/welcome">Create account</Link>
                    </span>
                    <span className="gx-mt-20" style={{ color: "#9399a2" }}>
                      <span className="gx-icon-action">Privacy policy</span>
                      <i
                        className="material-icons gx-mx-10"
                        style={{ fontSize: "5px" }}
                      >
                        lens
                      </i>
                      <span className="gx-icon-action">Terms of service</span>
                    </span>
                  </Form>
                ) : this.props.userInfo != undefined ? (
                  <Form
                    onSubmit={this.handlePasswordFormSubmit}
                    className={`gx-signin-password-form gx-form-row0}`}
                  >
                    <FormItem className="gx-text-center">
                      {this.props.userInfo.avatar_img != undefined ? (
                        <Avatar
                          className="gx-size-80 gx-m-auto gx-circle"
                          src={require(this.props.userInfo.avatar_img)}
                          alt={require(this.props.userInfo.avatar_img)}
                        />
                      ) : (
                        <Avatar className="gx-size-80 gx-m-auto gx-circle gx-avatar-default">
                          <span style={{ fontSize: "50px" }}>
                            {this.props.userInfo.first_name
                              .charAt(0)
                              .toUpperCase()}
                          </span>
                        </Avatar>
                      )}
                    </FormItem>
                    <div className="ant-row ant-form-item gx-flex-column gx-text-center">
                      <span
                        style={{
                          fontSize: "24px",
                          color: "#1a2f52",
                          fontWeight: "700",
                          marginBottom: "5px"
                        }}
                      >
                        <IntlMessages id="userAuth.signIn.welcome" />
                      </span>
                      {this.props.userInfo != null ? (
                        <h4 style={{ color: "#4c586d" }}>
                          {this.props.userInfo.first_name +
                            " " +
                            this.props.userInfo.last_name}
                        </h4>
                      ) : (
                        <div />
                      )}
                    </div>
                    {showMessage ? (
                      <FormItem className="gx-mb-0 gx-text-center">
                        <span className="gx-error-msg">
                          {this.props.alertMessage.toString()}
                        </span>
                      </FormItem>
                    ) : (
                      <div />
                    )}
                    <FormItem>
                      {getFieldDecorator("password", {
                        rules: [
                          {
                            required: true,
                            message: "Please input your Password!"
                          }
                        ]
                      })(
                        <Input
                          type="password"
                          placeholder="Enter password"
                          onChange={this.onPasswordChange}
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      <Button
                        className="gx-mb-0 gx-w-100"
                        type="primary"
                        htmlType="submit"
                      >
                        <IntlMessages id="userAuth.signIn" />
                      </Button>
                    </FormItem>
                    <FormItem className="gx-text-center">
                      <Link to="/forgot_password">
                        <IntlMessages id="userAuth.forgot_password" />
                      </Link>
                    </FormItem>
                    <span style={{ color: "#9399a2" }}>
                      <span>Privacy policy</span>
                      <i
                        className="material-icons gx-mx-10"
                        style={{ fontSize: "5px" }}
                      >
                        lens
                      </i>
                      <span>Terms of service</span>
                    </span>
                  </Form>
                ) : (
                  <div />
                )}
              </Card>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(SignIn);

const mapStateToProps = ({ auth }) => {
  const {
    loader,
    alertMessage,
    showMessage,
    authUser,
    isUserEmailExist,
    userInfo
  } = auth;
  return {
    loader,
    alertMessage,
    showMessage,
    authUser,
    isUserEmailExist,
    userInfo
  };
};

export default connect(mapStateToProps, {
  userSignIn,
  userCheckWithEmail,
  hideMessage,
  showAuthLoader
})(WrappedNormalLoginForm);
