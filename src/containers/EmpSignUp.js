import React from "react";
import { Card, Button, Popover, Form, Icon, Input, message } from "antd";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import AuthTopbar from "./AuthTopbar";
import { hideMessage, showAuthLoader } from "appRedux/actions/Auth";
import Widget from "components/Widget";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class EmpSignUp extends React.Component {
  state = {
    screenHeight: 0,
    screenWidth: 0,
    old_password: "",
    new_password: "",
    confirm_password: "",
    confirmDirty: false
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //this.props.showAuthLoader();
        //this.props.userEmpSignUp(values);
        let path = "/dashboard";
        this.props.history.push(path);
      }
    });
  };
  updateDimensions = () => {
    this.setState({
      screenHeight: window.innerHeight,
      screenWidth: window.innerWidth
    });
  };
  componentDidMount() {
    this.updateDimensions();
    window.addEventListener("resize", this.updateDimensions);
  }
  onConfirmPasswordChange = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("password")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(["confirm"], { force: true });
    }
    callback();
  };
  render() {
    const { showMessage, loader, alertMessage } = this.props;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="gx-app-auth-wrap gx-app-emp-signup-wrap gx-h-100">
        <AuthTopbar></AuthTopbar>
        <div className="gx-app-emp-signup-main-container gx-h-100">
          <div className="gx-app-emp-signup-content gx-h-100">
            <div className="gx-app-emp-signup-form gx-h-100 gx-flex-column gx-align-items-center gx-justify-content-center">
              <Widget styleName="gx-card-full">
                <div className="gx-emp-signup-img">
                  <img src={require("assets/images/auth/emp_signup_img.png")} />
                </div>
                <Form
                  onSubmit={this.handleSubmit}
                  className="gx-forgot-form gx-form-row0 gx-my-30 gx-flex-column gx-justify-content-center gx-align-items-center"
                >
                  <FormItem>
                    <h1>Complete account creation</h1>
                  </FormItem>
                  <FormItem>
                    <label>
                      Invited email <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("email", {
                      initialValue: "peterjackson@gmail.com",
                      rules: [
                        {
                          required: true,
                          type: "email",
                          message: "The input is not valid E-mail!"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Enter email"
                        readOnly={true}
                        suffix={
                          <i
                            className="material-icons"
                            style={{ color: "#39bf58" }}
                          >
                            check_circle_outline
                          </i>
                        }
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    <label>
                      New password <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("password", {
                      rules: [
                        {
                          required: true,
                          message: "Password is required!"
                        },
                        {
                          validator: this.validateToNextPassword
                        }
                      ]
                    })(
                      <Input type="password" placeholder="Enter a password" />
                    )}
                  </FormItem>
                  <FormItem>
                    <label>
                      Confirm password <span style={{ color: "red" }}>*</span>
                    </label>
                    {getFieldDecorator("confirm", {
                      rules: [
                        {
                          required: true,
                          message: "Password is required!"
                        },
                        {
                          validator: this.compareToFirstPassword
                        }
                      ]
                    })(
                      <Input
                        type="password"
                        placeholder="Enter password again"
                        onChange={this.onConfirmPasswordChange}
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    <Button
                      type="primary"
                      className="gx-mb-0 gx-w-100"
                      htmlType="submit"
                    >
                      <IntlMessages id="userAuth.signUp.complete_signup" />
                    </Button>
                  </FormItem>
                </Form>
              </Widget>
              <span>
                Go to <Link to="/signin">Sign In</Link>
              </span>
            </div>
          </div>
          <div className="message-box">
            <i className="material-icons">textsms</i>
          </div>
          {loader ? (
            <div className="gx-loader-view">
              <CircularProgress />
            </div>
          ) : null}
          {showMessage ? message.error(alertMessage.toString()) : null}
        </div>
      </div>
    );
  }
}

const WrappedNormalEmpSignUpForm = Form.create()(EmpSignUp);

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser } = auth;
  return { loader, alertMessage, showMessage, authUser };
};

export default connect(mapStateToProps, {
  //userEmpSignUp,
  hideMessage,
  showAuthLoader
})(WrappedNormalEmpSignUpForm);
