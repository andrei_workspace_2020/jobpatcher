import React, { Component } from "react";
import { Select } from "antd";
import { Link } from "react-router-dom";

const { Option } = Select;

class AuthTopbar extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="gx-app-auth-topbar">
        <div className="gx-app-auth-logo">
          <img src={require("assets/images/logo/logo.png")} />
        </div>
        <div className="gx-app-auth-language">
          <Select
            className="language"
            defaultValue="en"
            suffixIcon={<i className="material-icons">expand_more</i>}
          >
            <Option value="en">English</Option>
            <Option value="ru">Russian</Option>
            <Option value="fr">French</Option>
          </Select>
          <Link to="/settings/account">
            <span className="gx-pointer gx-status-pos gx-d-block">
              <i className="material-icons">help</i>
            </span>
            Help
          </Link>
        </div>
      </div>
    );
  }
}

export default AuthTopbar;
