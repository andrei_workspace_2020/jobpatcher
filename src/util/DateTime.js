export function changeDateFormat(date) {
  var datetime = new Date(date);
  const month = datetime.toLocaleString("en-us", { month: "long" });
  const time = datetime.toLocaleString("en-us", {
    hour12: true,
    hour: "numeric",
    minute: "2-digit"
  });

  return datetime.getDate() + " " + month;
}

export function changeDateYearFormat(date) {
  let dateValue = date.split(" ")[0];
  let day = parseInt(dateValue.split("/")[0]);
  let month = parseInt(dateValue.split("/")[1]) - 1;
  let year = parseInt(dateValue.split("/")[2]);
  var datetime = new Date(year, month, day);
  const Month = datetime.toLocaleString("en-us", { month: "long" });

  return day + " " + Month + ", " + year;
}

export function changeMonthYearFormat(date) {
  var datetime = new Date(date);
  const month = datetime.toLocaleString("en-us", { month: "long" });

  return month + " " + datetime.getFullYear();
}

export function changeDateTimeFormat(date) {
  var datetime = new Date(date);
  const month = datetime.toLocaleString("en-us", { month: "long" });
  const time = datetime.toLocaleString("en-us", {
    hour12: true,
    hour: "numeric",
    minute: "2-digit"
  });

  return datetime.getDate() + " " + month + " | " + time;
}

export function changeDateTimeAtFormat(date) {
  var datetime = new Date(date);
  const month = datetime.toLocaleString("en-us", { month: "long" });
  const time = datetime.toLocaleString("en-us", {
    hour12: true,
    hour: "numeric",
    minute: "2-digit"
  });

  return datetime.getDate() + " " + month + " at " + time;
}

export function changeDateTimeDashFormat(date) {
  var datetime = new Date(date);
  const month = datetime.toLocaleString("en-us", { month: "long" });
  const time = datetime.toLocaleString("en-us", {
    hour12: true,
    hour: "numeric",
    minute: "2-digit"
  });

  return (
    datetime.getDate() +
    " " +
    month +
    ", " +
    datetime.getFullYear() +
    " - " +
    time
  );
}
export function changeTimeFormat(date) {
  var datetime = new Date(date);
  const time = datetime.toLocaleString("en-us", {
    hour12: true,
    hour: "numeric",
    minute: "2-digit"
  });

  return time;
}

export function changeSpecialDateFormat(date) {
  var today = new Date();
  var datetime = new Date(date);
  var year = datetime.getFullYear();
  var special = "";
  const month = datetime.toLocaleString("en-us", { month: "long" });

  if (year == today.getFullYear()) {
    if (
      today.getMonth() == datetime.getMonth() &&
      today.getDate() == datetime.getDate()
    ) {
      special = " - Today";
    } else {
      var specialDay = new Date();
      specialDay.setDate(today.getDate() - 1);
      if (
        specialDay.getMonth() == datetime.getMonth() &&
        specialDay.getDate() == datetime.getDate()
      ) {
        special = " - Yesterday";
      }

      specialDay = new Date();
      specialDay.setDate(today.getDate() + 1);
      if (
        specialDay.getMonth() == datetime.getMonth() &&
        specialDay.getDate() == datetime.getDate()
      ) {
        special = " - Tomorrow";
      }
    }
  } else {
    return month + " " + datetime.getDate() + ", " + year;
  }

  return month + " " + datetime.getDate() + special;
}

export function changeDateRangeFormat(dateStart, dateEnd) {
  var datetimeStart = new Date(dateStart);
  var datetimeEnd = new Date(dateEnd);
  var monthStart = datetimeStart.toLocaleString("en-us", { month: "long" });
  var monthEnd = datetimeEnd.toLocaleString("en-us", { month: "long" });
  var dateStart = datetimeStart.getDate();
  var dateEnd = datetimeEnd.getDate();
  var dateStartStr = dateStart + " " + monthStart;
  var dateEndStr = dateEnd + " " + monthEnd;
  if (monthStart == monthEnd && dateStart == dateEnd) dateEndStr = "";

  return dateStartStr + " - " + dateEndStr;
}

export function changeDateRangeWithTimeFormat(dateStart, dateEnd) {
  var datetimeStart = new Date(dateStart);
  var datetimeEnd = new Date(dateEnd);
  var monthStart = datetimeStart.toLocaleString("en-us", { month: "long" });
  var monthEnd = datetimeEnd.toLocaleString("en-us", { month: "long" });
  var dateStart = datetimeStart.getDate();
  var dateEnd = datetimeEnd.getDate();
  var dateStartStr = dateStart + " " + monthStart + " | ";
  var dateEndStr = dateEnd + " " + monthEnd + " | ";
  if (monthStart == monthEnd && dateStart == dateEnd) dateEndStr = "";

  var timeStart = datetimeStart.toLocaleString("en-us", {
    hour12: true,
    hour: "numeric",
    minute: "2-digit"
  });
  var timeEnd = datetimeEnd.toLocaleString("en-us", {
    hour12: true,
    hour: "numeric",
    minute: "2-digit"
  });

  return dateStartStr + timeStart + " - " + dateEndStr + timeEnd;
}

////////////////////////////////  ss  //////////////////////////////////
function dayString(day) {
  var dayStr = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  return dayStr[day];
}

export function changeSpecialDateFormatSS(date) {
  var today = new Date();
  var datetime = new Date(date);
  var year = datetime.getFullYear();
  var special = "";
  const month = datetime.toLocaleString("en-us", { month: "short" });

  if (year == today.getFullYear()) {
    if (
      today.getMonth() == datetime.getMonth() &&
      today.getDate() == datetime.getDate()
    ) {
      special = " - Today";
    } else {
      var specialDay = new Date();
      specialDay.setDate(today.getDate() - 1);
      if (
        specialDay.getMonth() == datetime.getMonth() &&
        specialDay.getDate() == datetime.getDate()
      ) {
        special = " - Yesterday";
      }

      specialDay = new Date();
      specialDay.setDate(today.getDate() + 1);
      if (
        specialDay.getMonth() == datetime.getMonth() &&
        specialDay.getDate() == datetime.getDate()
      ) {
        special = " - Tomorrow";
      }
    }
  } else {
    return month + " " + datetime.getDate() + ", " + year;
  }

  return month + " " + datetime.getDate() + special;
}

export function changeDayDateMonthYearFormatSS(date) {
  var datetime = new Date(date);
  var day = datetime.getDay();
  var year = datetime.getFullYear();
  var month = datetime.toLocaleString("en-us", { month: "long" });
  var date = datetime.getDate();

  return dayString(day) + ", " + month + " " + date + ", " + year;
}

export function changeDateMonthYearFormatSS(date) {
  var datetime = new Date(date);
  var day = datetime.getDay();
  var year = datetime.getFullYear();
  var month = datetime.getMonth() + 1;
  var date = datetime.getDate();

  return date + "/" + month + "/" + year;
}

export function changeDateShortMonthFormatSS(date) {
  var datetime = new Date(date);
  var month = datetime.toLocaleString("en-us", { month: "short" });
  var date = datetime.getDate();

  return date + " " + month;
}

export function changeDateMonthYearHourMinuteSecondFormatSS(date) {
  var datetime = new Date(date);
  var year = datetime.getFullYear();
  var month = datetime.getMonth() + 1;
  var date = datetime.getDate();
  var hour = datetime.getHours();
  var minute = datetime.getMinutes();
  var second = datetime.getSeconds();
  date = parseInt(date / 10) ? date : "0" + date;
  month = parseInt(month / 10) ? month : "0" + month;
  hour = parseInt(hour / 10) ? hour : "0" + hour;
  minute = parseInt(minute / 10) ? minute : "0" + minute;
  second = parseInt(second / 10) ? second : "0" + second;
  return (
    date + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second
  );
}

export function changeMonthYearFormatSS(date) {
  var datetime = new Date(date);
  var day = datetime.getDay();
  var year = datetime.getFullYear();
  var month = datetime.getMonth() + 1;

  return month + "/" + year;
}
