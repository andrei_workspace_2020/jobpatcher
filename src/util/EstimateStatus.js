import React from "react";

export function getEstimateDisplayInfo(status) {
  var retVal = {
    title: "",
    color: "white",
    icon: "",
    action: ""
  };

  if (status === "schedule") {
    retVal.color = "blue";
    retVal.icon = "event";
    retVal.title = "estimate.type.schedule";
    retVal.action = "estimate.action.schedule";
  } else if (status === "on the way") {
    retVal.color = "cyan";
    retVal.icon = "local_shipping";
    retVal.title = "estimate.type.ontheway";
    retVal.action = "estimate.action.ontheway";
  } else if (status === "send") {
    retVal.color = "yellow";
    retVal.icon = "send";
    retVal.title = "estimate.type.send";
    retVal.action = "estimate.action.send";
  } else if (status === "finish") {
    retVal.color = "yellow";
    retVal.icon = "stop";
    retVal.title = "estimate.type.finish";
    retVal.action = "estimate.action.finish";
  } else if (status === "approval") {
    retVal.color = "yellow";
    retVal.icon = "check";
    retVal.title = "estimate.type.approval";
    retVal.action = "estimate.action.approval";
  } else if (status === "convert") {
    retVal.color = "yellow";
    retVal.icon = "swap_horizontal_circle";
    retVal.title = "estimate.type.convert";
    retVal.action = "estimate.action.convert";
  }
  return retVal;
}
